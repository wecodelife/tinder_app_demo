import 'package:app_template/src/app.dart';
import 'package:app_template/src/models/home_page_hive_model.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:path_provider/path_provider.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  final appDocDir = await getApplicationDocumentsDirectory();
  Hive.init(appDocDir.path);
  Hive.registerAdapter(HomePage());
  await Hive.openBox<HomePageHiveModel>("imageBox");
  // await checkHiveBoxOpen();

  await Hive.openBox(Constants.BOX_NAME);
  //

  runApp(MyApp());
}
