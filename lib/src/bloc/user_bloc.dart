import 'dart:async';

import 'package:app_template/src/models/foreget_password_request.dart';
import 'package:app_template/src/models/forget_password_response.dart';
import 'package:app_template/src/models/get_food_desrption_response_model.dart';
import 'package:app_template/src/models/get_food_response_Model.dart';
import 'package:app_template/src/models/get_food_type_model.dart';
import 'package:app_template/src/models/get_reviews_response_model.dart';
import 'package:app_template/src/models/liked_food_response.dart';
import 'package:app_template/src/models/login_request_model.dart';
import 'package:app_template/src/models/login_response_model.dart';
import 'package:app_template/src/models/logout_response.dart';
import 'package:app_template/src/models/register_user_request.dart';
import 'package:app_template/src/models/register_user_response.dart';
import 'package:app_template/src/models/reset_password_request.dart';
import 'package:app_template/src/models/reset_password_response_model.dart';
import 'package:app_template/src/models/state.dart';
import 'package:app_template/src/models/update_food_request.dart';
import 'package:app_template/src/models/update_food_response.dart';
import 'package:app_template/src/models/update_like_request_model.dart';
import 'package:app_template/src/models/update_like_response_model.dart';
import 'package:app_template/src/models/update_mustvisit_request_model.dart';
import 'package:app_template/src/models/update_mustvisit_response_model.dart';
import 'package:app_template/src/models/update_review_request_model.dart';
import 'package:app_template/src/models/update_reviews_response_model.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/object_factory.dart';
import 'package:app_template/src/utils/validators.dart';

import 'base_bloc.dart';

/// api response of login is managed by AuthBloc
/// stream data is handled by StreamControllers

class UserBloc extends Object with Validators implements BaseBloc {
  StreamController<bool> _loading = new StreamController<bool>.broadcast();

  StreamController<LoginResponse> _login =
      new StreamController<LoginResponse>.broadcast();

  StreamController<RegisterUserResponse> _registerUser =
      new StreamController<RegisterUserResponse>.broadcast();

  StreamController<LikedFoodResponse> _likedFood =
      new StreamController<LikedFoodResponse>.broadcast();

  StreamController<GetFoodDetailsResponseModel> __getFoodDetails =
      new StreamController<GetFoodDetailsResponseModel>.broadcast();

  StreamController<GetFoodDescriptionResponseModel> __getFoodDescription =
      new StreamController<GetFoodDescriptionResponseModel>.broadcast();

  StreamController<GetReviewsResponseModel> _getReview =
      new StreamController<GetReviewsResponseModel>.broadcast();

  StreamController<UpdateReviewsResponseModel> _updateReview =
      new StreamController<UpdateReviewsResponseModel>.broadcast();

  StreamController<UpdateLikeResponseModel> _updateLike =
      new StreamController<UpdateLikeResponseModel>.broadcast();

  StreamController<UpdateMustVisitResponseModel> _updateMustVisit =
      new StreamController<UpdateMustVisitResponseModel>.broadcast();

  StreamController<String> _deleteLikeItem =
  new StreamController<String>.broadcast();

  StreamController<ForgetPasswordResponse> _forgetPassword =
      new StreamController<ForgetPasswordResponse>.broadcast();

  StreamController<ResetPasswordResponse> _resetPassword =
      new StreamController<ResetPasswordResponse>.broadcast();

  StreamController<UpdateFoodResponse> _updateFoodDetails =
      new StreamController<UpdateFoodResponse>.broadcast();

  StreamController<GetFoodType> _getFoodType =
  new StreamController<GetFoodType>.broadcast();

  StreamController<LogoutResponse> _logout =
  new StreamController<LogoutResponse>.broadcast();
  // ignore: close_sinks

  // stream controller is broadcasting the  details



  /// stream for progress bar
  Stream<bool> get loadingListener => _loading.stream;

  Stream<LoginResponse> get loginResponse => _login.stream;

  Stream<RegisterUserResponse> get registerUserResponse => _registerUser.stream;

  Stream<LikedFoodResponse> get likedFood => _likedFood.stream;

  Stream<GetFoodDetailsResponseModel> get getFoodDetailsResponse =>
      __getFoodDetails.stream;

  Stream<GetFoodDescriptionResponseModel> get getFoodDescriptionResponse =>
      __getFoodDescription.stream;

  Stream<GetReviewsResponseModel> get getReviewResponse => _getReview.stream;

  Stream<UpdateReviewsResponseModel> get updateReviewResponse =>
      _updateReview.stream;

  Stream<UpdateLikeResponseModel> get updateLikeResponse => _updateLike.stream;

  Stream<UpdateMustVisitResponseModel> get updateMustVisitResponse =>
      _updateMustVisit.stream;

  Stream<String> get deleteLikeItemResponse => _deleteLikeItem.stream;


  Stream<ForgetPasswordResponse> get forgetPasswordResponse =>
      _forgetPassword.stream;

  Stream<ResetPasswordResponse> get resetPasswordResponse =>
      _resetPassword.stream;

  Stream<UpdateFoodResponse> get updateFoodResponse =>
      _updateFoodDetails.stream;

  Stream<GetFoodType> get getFoodType =>
      _getFoodType.stream;

  Stream<LogoutResponse> get logoutResponse =>
      _logout.stream;

  StreamSink<bool> get loadingSink => _loading.sink;



///user login
  login({LoginRequest loginRequest}) async {
    loadingSink.add(true);

    State state = await ObjectFactory().repository.login(
        loginRequest: loginRequest);
    if (state is SuccessState) {
      loadingSink.add(false);
      _login.sink.add(state.value);
      // print("udrerd" + state.value.userData.toString());
    } else if (state is ErrorState) {
      loadingSink.add(false);
      print(state.msg.toString());
      _login.sink.addError(state.msg);
    }
  }

//user register
  registerUser({RegisterUserRequest registerUserRequest,String id}) async {
    print("register user user bloc ok");
    loadingSink.add(true);

    State state =
        await ObjectFactory().repository.registerUser(registerUserRequest, id:id);

    if (state is SuccessState) {
      loadingSink.add(false);
      _registerUser.sink.add(state.value);
    } else if (state is ErrorState) {
      loadingSink.add(false);
      _registerUser.sink.addError(state.msg);
    }
  }

  //user liked food

  getLikedFood({String id,String uid}) async {
    loadingSink.add(true);
    print("api bloc ok");
    State state = await ObjectFactory().repository.getLikedFood(id: id,uid: uid);

    if (state is SuccessState) {
      loadingSink.add(false);
      _likedFood.sink.add(state.value);
    } else if (state is ErrorState) {
      loadingSink.add(false);
      _likedFood.sink.addError(state.msg);
    }
  }
///get food details
  getFoodDetails(int next) async {
    loadingSink.add(true);
    print("api bloc ok");
    State state = await ObjectFactory().repository.getFoodDetails(next);

    if (state is SuccessState) {
      loadingSink.add(false);
      __getFoodDetails.sink.add(state.value);
    } else if (state is ErrorState) {
      loadingSink.add(false);
      __getFoodDetails.sink.addError(state.msg);
    }
  }

  getFoodDescription(int id) async {
    loadingSink.add(true);
    print("api bloc ok");
    State state = await ObjectFactory().repository.getFoodDescription(id);

    if (state is SuccessState) {
      loadingSink.add(false);
      __getFoodDescription.sink.add(state.value);
    } else if (state is ErrorState) {
      loadingSink.add(false);
      __getFoodDescription.sink.addError(state.msg);
    }
  }

  getReview({String id}) async {
    loadingSink.add(true);
    print("api bloc ok");
    State state = await ObjectFactory().repository.getReview(id: id);

    if (state is SuccessState) {
      loadingSink.add(false);
      _getReview.sink.add(state.value);
    } else if (state is ErrorState) {
      loadingSink.add(false);
      _getReview.sink.addError(state.msg);
    }
  }

  updateReview({UpdateReviewsRequestModel updateReviewsRequestModel}) async {
    loadingSink.add(true);
    print("api bloc ok");
    State state = await ObjectFactory()
        .repository
        .updateReview(updateReviewsRequestModel: updateReviewsRequestModel);

    if (state is SuccessState) {
      loadingSink.add(false);
      _updateReview.sink.add(state.value);
    } else if (state is ErrorState) {
      loadingSink.add(false);
      _updateReview.sink.addError(Constants.SOME_ERROR_OCCURRED);
    }
  }

  updateLike({UpdateLikeRequestModel updateLikeRequestModel}) async {
    loadingSink.add(true);
    print("api bloc ok");
    State state = await ObjectFactory()
        .repository
        .updateLikes(updateLikeRequestModel: updateLikeRequestModel);

    if (state is SuccessState) {
      loadingSink.add(false);
      _updateLike.sink.add(state.value);
    } else if (state is ErrorState) {
      loadingSink.add(false);
      _updateLike.sink.addError(Constants.SOME_ERROR_OCCURRED);
    }
  }

  updateMustVisit(
      {UpdateMustVisitRequestModel updateMustVisitRequestModel}) async {
    loadingSink.add(true);
    print("api bloc ok");
    State state = await ObjectFactory().repository.updateMustVisit(
        updateMustVisitRequestModel: updateMustVisitRequestModel);

    if (state is SuccessState) {
      loadingSink.add(false);
      _updateMustVisit.sink.add(state.value);
    } else if (state is ErrorState) {
      loadingSink.add(false);
      _updateMustVisit.sink.addError(Constants.SOME_ERROR_OCCURRED);
    }
  }


  /// like item delete
  deleteLikeItem({String id}) async {
    print("delete  user bloc");
    loadingSink.add(true);

    State state = await ObjectFactory().repository.deleteLikeItem(id);

    if (state is SuccessState) {
      loadingSink.add(false);
      _deleteLikeItem.sink.add(state.value);
    } else if (state is ErrorState) {
      loadingSink.add(false);
      _deleteLikeItem.sink.addError(Constants.SOME_ERROR_OCCURRED);
    }
  }

  forgetPassword({ForgetPasswordRequest forgetPasswordRequest}) async {
    loadingSink.add(true);
    print("api bloc ok");
    State state = await ObjectFactory()
        .repository
        .forgetPassword(forgetPasswordRequest: forgetPasswordRequest);

    if (state is SuccessState) {
      loadingSink.add(false);
      _forgetPassword.sink.add(state.value);
    } else if (state is ErrorState) {
      loadingSink.add(false);
      print("erorr");
      _forgetPassword.sink.addError(state.msg);
    }
  }

  resetPassword({ResetPasswordRequest resetPasswordRequest}) async {
    loadingSink.add(true);
    print("api bloc ok");
    State state = await ObjectFactory()
        .repository
        .resetPassword(resetPasswordRequest: resetPasswordRequest);

    if (state is SuccessState) {
      loadingSink.add(false);
      _resetPassword.sink.add(state.value);
    } else if (state is ErrorState) {
      loadingSink.add(false);
      _resetPassword.sink.addError(state.msg);
    }
  }

  updateFoodDetails({UpdateFoodRequest updateFoodRequest}) async {
    loadingSink.add(true);
    print("api bloc ok");
    print(updateFoodRequest.name);
    State state = await ObjectFactory()
        .repository
        .updateFoodDetails(updateFoodRequest: updateFoodRequest);

    if (state is SuccessState) {
      loadingSink.add(false);
      _updateFoodDetails.sink.add(state.value);
    } else if (state is ErrorState) {
      loadingSink.add(false);
      _updateFoodDetails.sink.addError(state.msg);
    }
  }
  getFoodTypeResponse() async {
    loadingSink.add(true);
    print("api bloc ok");
    State state = await ObjectFactory().repository.getFoodType();

    if (state is SuccessState) {
      loadingSink.add(false);
      _getFoodType.sink.add(state.value);
    } else if (state is ErrorState) {
      loadingSink.add(false);
      _getFoodType.sink.addError(state.msg);
    }
  }

  logout() async {
    loadingSink.add(true);
    print("api bloc ok");
    State state = await ObjectFactory().repository.logout();

    if (state is SuccessState) {
      loadingSink.add(false);
      _logout.sink.add(state.value);
    } else if (state is ErrorState) {
      loadingSink.add(false);
      _logout.sink.addError(state.msg);
    }
  }
  ///disposing the stream if it is not using
  @override
  void dispose() {
    _loading?.close();
    _login?.close();
    _registerUser?.close();
    _likedFood?.close();
    __getFoodDetails.close();
    _getReview.close();
    _updateReview.close();
    __getFoodDescription.close();
    _updateLike.close();
    _updateMustVisit.close();
    _forgetPassword.close();
    _resetPassword.close();
    _updateFoodDetails.close();
    _getFoodType.close();
    _logout.close();
    _deleteLikeItem.close();
  }
}
