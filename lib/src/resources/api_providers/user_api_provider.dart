import 'package:app_template/src/models/foreget_password_request.dart';
import 'package:app_template/src/models/forget_password_response.dart';
import 'package:app_template/src/models/get_food_desrption_response_model.dart';
import 'package:app_template/src/models/get_food_response_Model.dart';
import 'package:app_template/src/models/get_food_type_model.dart';
import 'package:app_template/src/models/get_reviews_response_model.dart';
import 'package:app_template/src/models/liked_food_response.dart';
import 'package:app_template/src/models/login_request_model.dart';
import 'package:app_template/src/models/login_response_model.dart';
import 'package:app_template/src/models/logout_response.dart';
import 'package:app_template/src/models/register_user_request.dart';
import 'package:app_template/src/models/register_user_response.dart';
import 'package:app_template/src/models/reset_password_request.dart';
import 'package:app_template/src/models/state.dart';
import 'package:app_template/src/models/update_food_request.dart';
import 'package:app_template/src/models/update_food_response.dart';
import 'package:app_template/src/models/update_like_request_model.dart';
import 'package:app_template/src/models/update_like_response_model.dart';
import 'package:app_template/src/models/update_mustvisit_request_model.dart';
import 'package:app_template/src/models/update_mustvisit_response_model.dart';
import 'package:app_template/src/models/update_review_request_model.dart';
import 'package:app_template/src/models/update_reviews_response_model.dart';
import 'package:app_template/src/utils/dio.dart';
import 'package:app_template/src/utils/network.dart';
import 'package:app_template/src/utils/object_factory.dart';
import 'package:dio/dio.dart';

class UserApiProvider {
  AppDio dio;

  Future<State> loginCall(LoginRequest loginRequest) async {
    try {
      print("Try Block");
      final response =
          await ObjectFactory().apiClient.loginRequest(loginRequest);
      print(response.toString());
      if (response.statusCode == 200 || response.statusCode == 201) {
        print("fdfdf" + response.data["userData"].toString());
        return State<LoginResponse>.success(
            LoginResponse.fromJson(response.data));
      } else
        return null;
    } on DioError catch (e) {
      print("Error" + e.toString());
      if (e != null) {
        print(e.response.toString());
        //This is the custom message coming from the backend
        // showToast(e.response.data["status"].toString());
        print("NetwrokError" + NetworkErrors.getDioException(e));
        return State<String>.error(NetworkErrors.getDioException(e));
        // throw e.response.data.toString() + "ghhghgf";
      } else {
        throw Exception("Error");
      }
    }
  }

//user request
  Future<State> registerUser(RegisterUserRequest registerUserRequest, {String id}) async {
    try {
      final response =
          await ObjectFactory().apiClient.registerUser(registerUserRequest,id: id);
      print("Response" + response.toString());
      if (response.statusCode == 200 || response.statusCode == 201) {
        return State<RegisterUserResponse>.success(
            RegisterUserResponse.fromJson(response.data));
      } else
        return null;
    } on DioError catch (e) {
      print("Error" + e.toString());
      if (e != null) {
        print(e.response.toString());
        //This is the custom message coming from the backend
        // showToast(e.response.data["status"].toString());
        print("NetwrokError" + e.response.toString());
        return State<String>.error(NetworkErrors.getDioException(e));
        // throw e.response.data.toString() + "ghhghgf";
      } else {
        throw Exception("Error");
      }
    }
  }

  //user liked food
  Future<State> getLikedFood({String id,String uid}) async {
    print("api provider ok");
    try {
      final response = await ObjectFactory().apiClient.getLikedFood(id: id,uid: uid);
      print("food updated"+response.toString());
      if (response.statusCode == 200 || response.statusCode == 201) {
        return State<LikedFoodResponse>.success(
            LikedFoodResponse.fromJson(response.data));
      } else
        return null;
    } on DioError catch (e) {
      // print("Error" + e.toString());
      if (e != null) {
        // print(e.response.toString());
        //This is the custom message coming from the backend
        // showToast(e.response.data["status"].toString());
        print("NetwrokError" + NetworkErrors.getDioException(e));
        return State<String>.error(NetworkErrors.getDioException(e));
        // throw e.response.data.toString() + "ghhghgf";
      } else {
        throw Exception("Error");
      }
    }
  }

  Future<State> getFoodDetails(int next) async {
    print("api provider ok");
    try {
      final response = await ObjectFactory().apiClient.getFoodDetails(next);
      print(response.toString());
      if (response.statusCode == 200 || response.statusCode == 201) {
        return State<GetFoodDetailsResponseModel>.success(
            GetFoodDetailsResponseModel.fromJson(response.data));
      } else
        return null;
    } on DioError catch (e) {
      // print("Error" + e.toString());
      if (e != null) {
        // print(e.response.toString());
        //This is the custom message coming from the backend
        // showToast(e.response.data["status"].toString());
        print("NetwrokError" + NetworkErrors.getDioException(e));
        return State<String>.error(NetworkErrors.getDioException(e));
        // throw e.response.data.toString() + "ghhghgf";
      } else {
        throw Exception("Error");
      }
    } catch (e) {
      throw (e);
    }
  }

  Future<State> getFoodDescription(int id) async {
    print("api provider ok");
    try {
      final response = await ObjectFactory().apiClient.getFoodDescription(id);
      print(response.toString());
      if (response.statusCode == 200 || response.statusCode == 201) {
        return State<GetFoodDescriptionResponseModel>.success(
            GetFoodDescriptionResponseModel.fromJson(response.data));
      } else
        return null;
    } on DioError catch (e) {
      // print("Error" + e.toString());
      if (e != null) {
        // print(e.response.toString());
        //This is the custom message coming from the backend
        // showToast(e.response.data["status"].toString());
        print("NetwrokError" + NetworkErrors.getDioException(e));
        return State<String>.error(NetworkErrors.getDioException(e));
        // throw e.response.data.toString() + "ghhghgf";
      } else {
        throw Exception("Error");
      }
    }
  }

  Future<State> getReview({String id}) async {
    print("api provider ok");
    try {
      final response = await ObjectFactory().apiClient.getReview(id: id);
      print(response.toString());
      if (response.statusCode == 200 || response.statusCode == 201) {
        return State<GetReviewsResponseModel>.success(
            GetReviewsResponseModel.fromJson(response.data));
      } else
        return null;
    } on DioError catch (e) {
      // print("Error" + e.toString());
      if (e != null) {
        // print(e.response.toString());
        //This is the custom message coming from the backend
        // showToast(e.response.data["status"].toString());
        print("NetwrokError" + NetworkErrors.getDioException(e));
        return State<String>.error(NetworkErrors.getDioException(e));
        // throw e.response.data.toString() + "ghhghgf";
      } else {
        throw Exception("Error");
      }
    }
  }

  Future<State> getFoodType() async {
    print("api provider ok");
    try {
      final response = await ObjectFactory().apiClient.getFoodType();
      print(response.toString());
      if (response.statusCode == 200 || response.statusCode == 201) {
        return State<GetFoodType>.success(
            GetFoodType.fromJson(response.data));
      } else
        return null;
    } on DioError catch (e) {
      // print("Error" + e.toString());
      if (e != null) {
        // print(e.response.toString());
        //This is the custom message coming from the backend
        // showToast(e.response.data["status"].toString());
        print("NetwrokError" + NetworkErrors.getDioException(e));
        return State<String>.error(NetworkErrors.getDioException(e));
        // throw e.response.data.toString() + "ghhghgf";
      } else {
        throw Exception("Error");
      }
    }
  }

  Future<State> updateReview(
      UpdateReviewsRequestModel updateReviewsRequestModel ) async {
    print("api provider ok");
    try {
      final response = await ObjectFactory()
          .apiClient
          .updateReview(updateReviewsRequestModel: updateReviewsRequestModel);
      print(response.toString());
      if (response.statusCode == 200 || response.statusCode == 201) {
        return State<UpdateReviewsResponseModel>.success(
            UpdateReviewsResponseModel.fromJson(response.data));
      } else
        return null;
    } on DioError catch (e) {
      // print("Error" + e.toString());
      if (e != null) {
        // print(e.response.toString());
        //This is the custom message coming from the backend
        print("NetwrokError" + NetworkErrors.getDioException(e));
        return State<String>.error(NetworkErrors.getDioException(e));
        // throw e.response.data.toString() + "ghhghgf";
      } else {
        throw Exception("Error");
      }
    }
  }

  Future<State> updateLikes(
      UpdateLikeRequestModel updateLikeRequestModel) async {
    print("api provider ok");
    final response = await ObjectFactory()
        .apiClient
        .updateLikes(updateLikeRequestModel: updateLikeRequestModel);
    print(response.toString());
    if (response.statusCode == 200 || response.statusCode == 201) {
      return State<UpdateLikeResponseModel>.success(
          UpdateLikeResponseModel.fromJson(response.data));
    } else
      return null;
  }


  Future<State> updateMustVisit(
      UpdateMustVisitRequestModel updateMustVisitRequestModel) async {
    print("api provider ok");
    final response = await ObjectFactory().apiClient.updateMustVisit(
        updateMustVisitRequestModel: updateMustVisitRequestModel);
    print("mustvisit"+response.toString());
    if (response.statusCode == 200 || response.statusCode == 201) {
      return State<UpdateMustVisitResponseModel>.success(
          UpdateMustVisitResponseModel.fromJson(response.data));
    } else
      return null;
  }

  /// delete cart item
  Future<State> deleteLikeItem(String id) async {
    try {
      final response = await ObjectFactory().apiClient.deleteLiked(id);
      print(" Deleted Successfully");
      if (response.statusCode == 204) {
        return State<String>.success("Deleted Successfully");
      } else
        return null;
    } on DioError catch (e) {
      // print("Error" + e.toString());
      if (e != null) {
        // print("username error      "+e.response.toString());
        //This is the custom message coming from the backend
        // showToast(e.response.data["status"].toString());
        // print("NetwrokError" + NetworkErrors.getDioException(e));
        // return State<String>.error(NetworkErrors.getDioException(e));
        // throw e.response.data.toString() + "ghhghgf";
      } else {
        throw Exception("Error");
      }
    }
  }

  Future<State> forgetPassword(
      ForgetPasswordRequest forgetPasswordRequest) async {
    print("api provider ok");
    try {
      final response = await ObjectFactory()
          .apiClient
          .forgetPassword(forgetPasswordRequest: forgetPasswordRequest);
      print(response.toString());
      if (response.statusCode == 200 || response.statusCode == 201) {
        return State<ForgetPasswordResponse>.success(
            ForgetPasswordResponse.fromJson(response.data));
      } else
        return null;
    } on DioError catch (e) {
      // print("Error" + e.toString());
      if (e != null) {
        // print(e.response.toString());
        //This is the custom message coming from the backend
        print("NetwrokError" + NetworkErrors.getDioException(e));
        return State<String>.error(NetworkErrors.getDioException(e));
        // throw e.response.data.toString() + "ghhghgf";
      } else {
        throw Exception("Error");
      }
    }
  }

  Future<State> resetPassword(ResetPasswordRequest resetPasswordRequest) async {
    print("api provider ok");
    try {
      final response = await ObjectFactory()
          .apiClient
          .resetPassword(resetPasswordRequest: resetPasswordRequest);
      print(response.toString());
      if (response.statusCode == 200 || response.statusCode == 201) {
        return State<ResetPasswordRequest>.success(
            ResetPasswordRequest.fromJson(response.data));
      } else
        return null;
    } on DioError catch (e) {
      // print("Error" + e.toString());
      if (e != null) {
        // print(e.response.toString());
        //This is the custom message coming from the backend
        print("NetwrokError" + NetworkErrors.getDioException(e));
        return State<String>.error(NetworkErrors.getDioException(e));
        // throw e.response.data.toString() + "ghhghgf";
      } else {
        throw Exception("Error");
      }
    }
  }

  Future<State> updateFoodDetails(UpdateFoodRequest updateFoodRequest) async {
    print("api provider ok");
    print(updateFoodRequest.name);
    try {
      final response = await ObjectFactory()
          .apiClient
          .updateFoodDetails(updateFoodRequest: updateFoodRequest);
      print(response.toString());
      if (response.statusCode == 200 || response.statusCode == 201) {
        return State<UpdateFoodResponse>.success(
            UpdateFoodResponse.fromJson(response.data));
      } else
        return null;
    } on DioError catch (e) {
      // print("Error" + e.toString());
      if (e != null) {
        print(e.response.toString());
        //This is the custom message coming from the backend
        // print("NetwrokError" + NetworkErrors.getDioException(e));
        return State<String>.error(NetworkErrors.getDioException(e));
        // throw e.response.data.toString() + "ghhghgf";
      } else {
        throw Exception("Error");
      }
    }
  }

  Future<State> logOut(
      ) async {
    print("api provider ok");
    try {
      final response = await ObjectFactory()
          .apiClient
          .logOut();
      print(response.toString());
      if (response.statusCode == 200 || response.statusCode == 201) {
        return State<LogoutResponse>.success(
            LogoutResponse.fromJson(response.data));
      } else
        return null;
    } on DioError catch (e) {
      // print("Error" + e.toString());
      if (e != null) {
        // print(e.response.toString());
        //This is the custom message coming from the backend
        print("NetwrokError" + NetworkErrors.getDioException(e));
        return State<String>.error(NetworkErrors.getDioException(e));
        // throw e.response.data.toString() + "ghhghgf";
      } else {
        throw Exception("Error");
      }
    }
  }
}
