import 'package:app_template/src/models/foreget_password_request.dart';
import 'package:app_template/src/models/login_request_model.dart';
import 'package:app_template/src/models/register_user_request.dart';
import 'package:app_template/src/models/reset_password_request.dart';
import 'package:app_template/src/models/state.dart';
import 'package:app_template/src/models/update_food_request.dart';
import 'package:app_template/src/models/update_like_request_model.dart';
import 'package:app_template/src/models/update_mustvisit_request_model.dart';
import 'package:app_template/src/models/update_review_request_model.dart';
import 'package:app_template/src/resources/api_providers/user_api_provider.dart';

/// Repository is an intermediary class between network and data
class Repository {
  final userApiProvider = UserApiProvider();

  Future<State> login({LoginRequest loginRequest}) =>
      UserApiProvider().loginCall(loginRequest);

  Future<State> registerUser(RegisterUserRequest registerUserRequest,{String id}) =>
      UserApiProvider().registerUser(registerUserRequest, id:id);

  Future<State> getLikedFood({String id,String uid,}) => UserApiProvider().getLikedFood(id: id,uid: uid);

  Future<State> getFoodDetails(int next) =>
      UserApiProvider().getFoodDetails(next);

  Future<State> getFoodDescription(int id) =>
      UserApiProvider().getFoodDescription(id);

  Future<State> getReview({String id}) => UserApiProvider().getReview(id:id);

  Future<State> updateReview(
          {UpdateReviewsRequestModel updateReviewsRequestModel}) =>
      UserApiProvider().updateReview(updateReviewsRequestModel);

  Future<State> updateLikes({UpdateLikeRequestModel updateLikeRequestModel}) =>
      UserApiProvider().updateLikes(updateLikeRequestModel);

  Future<State> updateMustVisit(
          {UpdateMustVisitRequestModel updateMustVisitRequestModel}) =>
      UserApiProvider().updateMustVisit(updateMustVisitRequestModel);

  Future<State> deleteLikeItem(String id) => UserApiProvider().deleteLikeItem(id);


  Future<State> forgetPassword({ForgetPasswordRequest forgetPasswordRequest}) =>
      UserApiProvider().forgetPassword(forgetPasswordRequest);

  Future<State> resetPassword({ResetPasswordRequest resetPasswordRequest}) =>
      UserApiProvider().resetPassword(resetPasswordRequest);

  Future<State> updateFoodDetails({UpdateFoodRequest updateFoodRequest}) =>
      UserApiProvider().updateFoodDetails(updateFoodRequest);

  Future<State> getFoodType() =>
      UserApiProvider().getFoodType();

  Future<State> logout() =>
      UserApiProvider().logOut();
}
