import 'package:hive/hive.dart';

import 'constants.dart';

class AppHive {
  // void main (){}

  // void openBox()async{
  //   await Hive.openBox(Constants.BOX_NAME);
  //
  // }

  static const String _USER_ID = "user_id";
  static const String _NAME = "name";
  static const String _TOKEN = "token";
  static const String _EMAIL = "email";
  static const String _PHONE_NUMBER = "phoneNumber";
  static const String _PREFERENCE = "preferenceid";
  static const String _LATITUDE = "longitude";
  static const String _XUSER = "newuserid";
  static const String _PASSWORD = "userpassword";
  static const String _IMAGE_LIST = "imageList";
  static const String _NEXT_COUNT = "nextcount";
  static const String _PROFILE_IMAGE = "profileimage";
  static const String _DEVICE_TOKEN = "device_token";


  void hivePut({String key, String value}) async {
    await Hive.box(Constants.BOX_NAME).put(key, value);
  }
  void clear(String key) async {
    await Hive.box(Constants.BOX_NAME).delete(key);
  }


  String hiveGet({String key}) {
    // openBox();
    return Hive.box(Constants.BOX_NAME).get(key);
  }

  putUserId({String userId}) {
    hivePut(key: _USER_ID, value: userId);
  }

  String getUserId() {
    return hiveGet(key: _USER_ID);
  }

  putProfileImage({String profileImage}) {
   return hivePut(key: _PROFILE_IMAGE, value: profileImage);
  }

  String getProfileImage() {
    return hiveGet(key: _PROFILE_IMAGE);
  }

  putUserPassword({String email}) {
    hivePut(key: _PASSWORD, value: email);
  }

  String getUserPassword() {
    return hiveGet(key: _PASSWORD);
  }

  putName({String name}) {
    hivePut(key: _NAME, value: name);
  }

  String getName() {
    return hiveGet(key: _NAME);
  }

  putToken({String token}) {
    hivePut(key: _TOKEN, value: token);
  }

  String getToken() {
    return hiveGet(key: _TOKEN);
  }

  String getEmail() {
    return hiveGet(key: _EMAIL);
  }

  putEmail(String value) {
    return hivePut(key: _EMAIL, value: value);
  }

  String getPhoneNumber() {
    return hiveGet(key: _PHONE_NUMBER);
  }

  putPhoneNumber(String value) {
    return hivePut(key: _PHONE_NUMBER, value: value);
  }

  putPreference(String value) {
    return hivePut(key: _PREFERENCE, value: value);
  }

  String getPreferenceId() {
    return hiveGet(key: _PREFERENCE);
  }

  putXUser(String value) {
    return hivePut(key: _XUSER, value: value);
  }

  getXUser() {
    return hiveGet(
      key: _XUSER,
    );
  }

  putNextImageValue(String value) {
    return hivePut(key: _NEXT_COUNT, value: value);
  }

  getNextImageValue() {
    return hiveGet(
      key: _NEXT_COUNT,
    );
  }

  putDeviceToken({String token}) {
    hivePut(key: _DEVICE_TOKEN, value: token);
  }

  String getDeviceToken() {
    return hiveGet(key: _DEVICE_TOKEN);
  }

  clearProfilePic() {
    return clear(_PROFILE_IMAGE);
  }
  clearToken() {
    return clear(_TOKEN);
  }
  clearID() {
    return clear(_USER_ID);
  }
  clearImageList() {
    return clear(_IMAGE_LIST);
  }
  clearPhone() {
    return clear(_PHONE_NUMBER);
  }
  clearName() {
    return clear(_NAME);
  }
  AppHive();
}
