import 'dart:async';

import 'package:app_template/src/models/foreget_password_request.dart';
import 'package:app_template/src/models/header_model.dart';
import 'package:app_template/src/models/login_request_model.dart';
import 'package:app_template/src/models/register_user_request.dart';
import 'package:app_template/src/models/reset_password_request.dart';
import 'package:app_template/src/models/update_food_request.dart';
import 'package:app_template/src/models/update_like_request_model.dart';
import 'package:app_template/src/models/update_mustvisit_request_model.dart';
import 'package:app_template/src/models/update_review_request_model.dart';
import 'package:app_template/src/utils/urls.dart';
import 'package:dio/dio.dart';

import 'object_factory.dart';

enum AccessMode { READ, WRITE }
HeaderModel authHeaderModel = new HeaderModel();

void setAuthHeaderModel({var accessMode, String xReferenceId}) {
  if (ObjectFactory().appHive != null &&
      ObjectFactory().appHive.getToken() != null &&
      ObjectFactory().appHive.getToken().trim().length > 0)
    // print("Token " + ObjectFactory().appHive.getToken());
  authHeaderModel.authorization =
  "Token ${ObjectFactory().appHive.getToken()}";
  // print(ObjectFactory().appHive.getToken());
  // authHeaderModel.xAccessMode = accessMode.toString();
  // authHeaderModel.xUdid = "";
  // authHeaderModel.xRequestTime = "";
  // authHeaderModel.xReferenceId = xReferenceId;
  // authHeaderModel.xSession = "";
  // if (ObjectFactory().appHive != null &&
  //     ObjectFactory().appHive.getXUser() != null &&
  //     ObjectFactory().appHive.getXUser().trim().length > 0)
  //   authHeaderModel.xUser = ObjectFactory().appHive.getXUser();
  // if (ObjectFactory().appHive != null &&
  //     ObjectFactory().appHive.getUserId() != null &&
  //     ObjectFactory().appHive.getUserId().trim().length > 0)
  //   authHeaderModel.xUserId = ObjectFactory().appHive.getUserId();
  // print("Token "+authHeaderModel.xTenantId);
}

class ApiClient {
  HeaderModel loginModel = new HeaderModel();

  ///  user login

  Future<Response> loginRequest(LoginRequest loginRequest) {
    // print(loginRequest.email + loginRequest.password);

    return ObjectFactory()
        .appDio
        .loginPost(url: Urls.loginUrl, data: loginRequest);
  }

  // user request
  Future<Response> registerUser(RegisterUserRequest registerUserRequest,{String id}) async{
    print("register user api client ok");
    // print("gfhf" +
    //     registerUserRequest.firstName +
    //     registerUserRequest.lastName +
    //     registerUserRequest.email +
    //     registerUserRequest.password);
    // print("url " + Urls.registerUrl);
    print("first" + registerUserRequest.firstName.toString());
    print("id" + id.toString());
    print("number" + registerUserRequest.phoneNumber.toString());
    if(registerUserRequest.profileImage != null)
    print("photo" + registerUserRequest.profileImage.path.toString());
    FormData formData = FormData.fromMap({
     if(registerUserRequest.profileImage!=null)
      "profile_image" : await MultipartFile.fromFile(
          registerUserRequest.profileImage.path,
          filename: registerUserRequest.profileImage.path.split('/').last),
      if(registerUserRequest.firstName!=null)
        "first_name" : registerUserRequest.firstName,
      if(registerUserRequest.phoneNumber!=null)
        "phone_number" : registerUserRequest.phoneNumber,
    });
    setAuthHeaderModel();
    print(authHeaderModel.authorization.toString());

    return ObjectFactory()
        .appDio
        .patch(url: Urls.registerUrl+"$id/", data: formData, header: authHeaderModel);
  }

  //user liked food
  Future<Response> getLikedFood({String id,String uid}) {
    print("api client ok");
    setAuthHeaderModel();
    print(Urls.likedFoodUrl);
    return ObjectFactory()
        .appDio
        .get(url: Urls.likedFoodUrl+"?user_id=$uid & $id ", header: authHeaderModel);
  }

  Future<Response> getFoodDetails(int next) {
    print("api client ok");
    setAuthHeaderModel();
    print("Urls.getFoodDetails" + Urls.getFoodDetails);
    print("Get Food Details" + authHeaderModel.authorization.toString());
    return ObjectFactory()
        .appDio
        .get(url: Urls.getFoodDetails, header: authHeaderModel);
  }

  Future<Response> getFoodDescription(int id) {
    print("api client ok");
    setAuthHeaderModel();
    print("Urls.getFoodDetails " + Urls.getFoodDetails + "$id/");
    return ObjectFactory()
        .appDio
        .get(url: Urls.getFoodDetails + "$id/", header: authHeaderModel);
  }

  Future<Response> getReview({String id}) {
    print("api client ok");
    setAuthHeaderModel();
    print("Urls" + id.toString());
    return ObjectFactory()
        .appDio
        .get(url: Urls.getReview + "?food_id=$id", header: authHeaderModel);
  }

  Future<Response> getFoodType() {
    print("api client ok");
    setAuthHeaderModel();
    print("Urls" + Urls.getFoodType);
    return ObjectFactory()
        .appDio
        .get(url: Urls.getFoodType, header: authHeaderModel);
  }

  Future<Response> updateReview(
      {UpdateReviewsRequestModel updateReviewsRequestModel}) {
    print("api client ok");
    setAuthHeaderModel();
    return ObjectFactory().appDio.post(
        url: Urls.putReview,
        header: authHeaderModel,
        data: updateReviewsRequestModel);
  }

  Future<Response> updateLikes(
      {UpdateLikeRequestModel updateLikeRequestModel}) {
    print("api client ok");
    setAuthHeaderModel();
    print("url" + Urls.putLikes);

    return ObjectFactory().appDio.post(
        url: Urls.putLikes,
        header: authHeaderModel,
        data: updateLikeRequestModel);
  }

  Future<Response> updateMustVisit(
      {UpdateMustVisitRequestModel updateMustVisitRequestModel}) {
    print("must visit api client ok");
    setAuthHeaderModel();

    return ObjectFactory().appDio.post(
        url: Urls.putMustVisit,
        header: authHeaderModel,
        data: updateMustVisitRequestModel);
  }

  Future<Response> forgetPassword(
      {ForgetPasswordRequest forgetPasswordRequest}) {
    print("api client ok");

    return ObjectFactory()
        .appDio
        .loginPost(url: Urls.forgetPassword, data: forgetPasswordRequest);
  }

  Future<Response> resetPassword({ResetPasswordRequest resetPasswordRequest}) {
    print("api client ok");

    return ObjectFactory()
        .appDio
        .loginPost(url: Urls.forgetPassword, data: resetPasswordRequest);
  }

  Future<Response> updateFoodDetails({UpdateFoodRequest updateFoodRequest}) {
    print("api client ok");
    print(updateFoodRequest.name);
    setAuthHeaderModel();

    FormData formDataUpdateFoodDetails = FormData.fromMap({
      "name": updateFoodRequest.name,
      "description": updateFoodRequest.description,
      "restaurant": updateFoodRequest.restaurant,
      "place": updateFoodRequest.place,
      "lat": updateFoodRequest.lat,
      "lon": updateFoodRequest.lat,
      "google_map_url": updateFoodRequest.googleMapUrl,
      "is_deleted": false,
      "ingredients": updateFoodRequest.ingredients,
      "price": updateFoodRequest.price,
      "food_type": updateFoodRequest.foodType
    });

    return ObjectFactory()
        .appDio
        .post(url: Urls.updateFoodDetails,header: authHeaderModel, data: formDataUpdateFoodDetails);
  }

  Future<Response> logOut() {
    print("api client ok");
    setAuthHeaderModel();
    // print("Urls" + Urls.getFoodType);
    return ObjectFactory()
        .appDio
        .get(url: Urls.logOut, header: authHeaderModel);
  }

  /// delete liked item
  Future<Response> deleteLiked(String id) async {
    print("delete api client");
    // print("api");
    setAuthHeaderModel();
    print(authHeaderModel.authorization.toString());
    return ObjectFactory()
        .appDio
        .delete(url: Urls.putLikes + "$id/", header: authHeaderModel);
  }
}
