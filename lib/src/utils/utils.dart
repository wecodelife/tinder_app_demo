import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/widgets/cancel_alert_box.dart';
import 'package:app_template/src/widgets/otp_message.dart';
import 'package:flutter/material.dart';
import 'package:flutter_styled_toast/flutter_styled_toast.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:top_snackbar_flutter/custom_snack_bar.dart';
import 'package:top_snackbar_flutter/top_snack_bar.dart';

///it contain common functions
class Utils {}

Size screenSize(BuildContext context) {
  return MediaQuery.of(context).size;
}

double screenHeight(BuildContext context, {double dividedBy = 1}) {
  return screenSize(context).height / dividedBy;
}

double screenWidth(BuildContext context, {double dividedBy = 1}) {
  return screenSize(context).width / dividedBy;
}

Future<dynamic> push(BuildContext context, Widget route) {
  return Navigator.push(
      context, MaterialPageRoute(builder: (context) => route));
}

void pop(BuildContext context) {
  return Navigator.pop(context);
}

Future<dynamic> pushAndRemoveUntil(
    BuildContext context, Widget route, bool goBack) {
  return Navigator.pushAndRemoveUntil(context,
      MaterialPageRoute(builder: (context) => route), (route) => goBack);
}

Future<dynamic> pushAndReplacement(BuildContext context, dynamic route) {
  return Navigator.pushReplacement(
      context, MaterialPageRoute(builder: (context) => route));
}

///common toast
void showToastCommon(String msg) {
  Fluttertoast.showToast(
    msg: msg,
    toastLength: Toast.LENGTH_SHORT,
    gravity: ToastGravity.BOTTOM,
  );
}

void cancelAlertBox(
    {context,
    msg,
    text1,
    text2,
    double insetPadding,
    bool singleButton,
    bool isLoading,
    String button1Name,
    String button2Name,
    Function onPressedYes,
    Function onPressedNo,
    double contentPadding,
    double titlePadding}) {
  showGeneralDialog(
      context: context,
      barrierDismissible: false,
      barrierLabel: MaterialLocalizations.of(context).modalBarrierDismissLabel,
      barrierColor: Colors.black.withOpacity(0.1),
      transitionDuration: const Duration(milliseconds: 200),
      pageBuilder: (BuildContext buildContext, Animation animation,
          Animation secondaryAnimation) {
        return CancelAlertBox(
          title: msg,
          text1: text1,
          text2: text2,
          singleButton: singleButton,
          isLoading: isLoading,
          onPressedYes: onPressedYes,
          button1name: button1Name,
          button2name: button2Name,
          onPressedNo: onPressedNo,
          contentPadding: contentPadding,
          titlePadding: titlePadding,
          insetPadding: insetPadding,
        );
      });
}

void showAlert(context, String msg) {
  // flutter defined function
  showDialog(
    context: context,
    builder: (BuildContext context) {
      // return object of type Dialog
      return AlertDialog(
        title: new Text(msg),
//        content: new Text("Alert Dialog body"),
        actions: <Widget>[
          // usually buttons at the bottom of the dialog
          new FlatButton(
            child: new Text("OK"),
            onPressed: (){
              Navigator.pop(context);
            },
          ),
        ],
      );
    },
  );
}

void showVerificationAlert(context, String msg, Widget navigationWidget) {
  // flutter defined function
  showDialog(
    context: context,
    builder: (BuildContext context) {
      // return object of type Dialog
      return AlertDialog(
        backgroundColor: Constants.kitGradients[31],
        title: new Text(
          msg,
          style: TextStyle(
            color: Constants.kitGradients[28],
            fontWeight: FontWeight.w400,
            fontSize: 16,
          ),
        ),
//        content: new Text("Alert Dialog body"),
        actions: <Widget>[
          // usually buttons at the bottom of the dialog
          new FlatButton(
            color: Constants.kitGradients[31],
            child: new Text(
              "OK",
              style: TextStyle(
                color: Constants.kitGradients[28],
                fontWeight: FontWeight.w600,
                fontSize: 16,
              ),
            ),
            onPressed: () {
              push(context, navigationWidget);
            },
          ),
        ],
      );
    },
  );
}

void otpAlertBox({context, title, route, stayOnPage}) {
  showGeneralDialog(
      context: context,
      barrierDismissible: false,
      barrierLabel: MaterialLocalizations.of(context).modalBarrierDismissLabel,
      barrierColor: Colors.transparent,
      transitionDuration: const Duration(milliseconds: 200),
      pageBuilder: (BuildContext buildContext, Animation animation,
          Animation secondaryAnimation) {
        return OtpMessage(
          title: title,
          route: route,
          stayOnPage: stayOnPage,
        );
      });
}

///common toast
void showToastLong(String msg) {
  Fluttertoast.showToast(
    msg: msg,
    toastLength: Toast.LENGTH_LONG,
    gravity: ToastGravity.BOTTOM,
  );
}

void showToastConnection(String msg) {
  Fluttertoast.showToast(
    msg: msg,
    toastLength: Toast.LENGTH_LONG,
    gravity: ToastGravity.BOTTOM,
  );
}

void showToastLiked(context) {
  showToast('Yum um!!',
      context: context,
      animation: StyledToastAnimation.fadeScale,
      reverseAnimation: StyledToastAnimation.fade,
      position: StyledToastPosition.center,
      animDuration: Duration(seconds: 1),
      duration: Duration(seconds: 2),
      curve: Curves.decelerate,
      textStyle: TextStyle(
        fontSize: 50,
        fontFamily: 'PangolinRegular',
        fontWeight: FontWeight.bold,
        color: Constants.kitGradients[27],
      ),
      backgroundColor: Colors.transparent,
      reverseCurve: Curves.linear);
}

void showToastInfo(String message, BuildContext context) {
  showTopSnackBar(
    context,
    CustomSnackBar.info(
      message: message,
    ),
  );
}

void showToastError(String message, BuildContext context) {
  showTopSnackBar(
    context,
    CustomSnackBar.error(
      message: message,
    ),
  );
}

void showToastSuccess(String message, BuildContext context) {
  showTopSnackBar(
    context,
    CustomSnackBar.success(
      message: message,
    ),
  );
}

class AvailableImages {
  static const logo = 'assets/images/logo.png';

  static const woman = 'assets/images/woman.jpg';
  static const burger = 'assets/images/burger.jpg';
  static const cafe = 'assets/images/cafe.jpg';
  static const gelato = 'assets/images/gelato.jpg';
  static const tacos = 'assets/images/tacos.jpg';

  static const iphone = 'assets/images/iphone.png';
  static const navigate = 'assets/images/navigate.png';
  static const website = 'assets/images/website.png';

  static const like = 'assets/images/like.png';
  static const hate = 'assets/images/hate.png';
  static const back = 'assets/images/back.png';
  static const list = 'assets/images/list.png';
}
