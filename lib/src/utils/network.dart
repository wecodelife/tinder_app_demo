import 'dart:io';

import 'package:dio/dio.dart';

class NetworkErrors {
  static String getDioException(DioError error) {
    var formatExceptionError;
    if (error is Exception) {
      formatExceptionError = error.response.data["error"];
      print(formatExceptionError.toString());
      try {
        String networkExceptions;
        if (error is DioError) {
          switch (error.type) {
            case DioErrorType.CANCEL:
              networkExceptions = "Some thing went Wrong";
              break;
            case DioErrorType.CONNECT_TIMEOUT:
              networkExceptions = "Connection request timeout";
              break;
            case DioErrorType.DEFAULT:
              if (DioErrorType.DEFAULT.index == 5) {
                print(DioErrorType.DEFAULT.index);
                networkExceptions = "No Internet Connection";
              } else {
                networkExceptions = "Some thing went wrong";
              }
              break;
            case DioErrorType.RECEIVE_TIMEOUT:
              networkExceptions = "Some thing went wrong";
              break;
            case DioErrorType.SEND_TIMEOUT:
              networkExceptions = "Some thing went wrong";
              break;
            case DioErrorType.RESPONSE:
              networkExceptions = error.response.data["error"];
              break;
          }
        } else if (error is SocketException) {
          networkExceptions = "No Internet Connection";
        } else {
          networkExceptions = "Some thing went wrong";
        }
        return networkExceptions;
      } on FormatException catch (e) {
        print(e.toString());
        return "Invalid data";
      } catch (e) {
        return "Please check the details you have entered";
      }
    } else {
      if (error.toString().contains("is not a subtype of")) {
        return "Some thing went wrong";
      } else {
        return "Some thing went wrong";
      }
    }
  }
}
