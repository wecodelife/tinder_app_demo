import 'dart:async';
import 'dart:io';

import 'package:app_template/src/models/header_model.dart';
import 'package:app_template/src/utils/urls.dart';
import 'package:cookie_jar/cookie_jar.dart';
import 'package:dio/adapter.dart';
import 'package:dio/dio.dart';
import 'package:dio_cookie_manager/dio_cookie_manager.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class AppDio {
  AppDio() {
    initClient();
  }

//for api client testing only
  AppDio.test({@required this.dio});

  Dio dio;
  BaseOptions _baseOptions;

  initClient() async {
    _baseOptions = new BaseOptions(
        baseUrl: Urls.baseUrl,
        connectTimeout: 30000,
        receiveTimeout: 1000000,
        followRedirects: false,
        headers: {
          HttpHeaders.contentTypeHeader: 'application/json',
          HttpHeaders.acceptHeader: 'application/json',
        },
        responseType: ResponseType.json,
        receiveDataWhenStatusError: true);

    dio = Dio(_baseOptions);

    (dio.httpClientAdapter as DefaultHttpClientAdapter).onHttpClientCreate =
        (client) {
      client.badCertificateCallback =
          (X509Certificate cert, String host, int port) {
        return true;
      };
    };

    dio.interceptors.add(CookieManager(new CookieJar()));
    // dio.interceptors.add(InterceptorsWrapper(
    //   onRequest: (options, handler) {
    //     // Do something before request is sent
    //     print("object" + options.data.toString());
    //     return handler.resolve(
    //         Response(requestOptions: options, data: "dhdh")); //continue
    //     // If you want to resolve the request with some custom data，
    //     // you can resolve a `Response` object eg: return `dio.resolve(response)`.
    //     // If you want to reject the request with a error message,
    //     // you can reject a `DioError` object eg: return `dio.reject(dioError)`
    //   },
    //   onResponse: (response, handler) {
    //     // Do something with response data
    //     print("hyg" + response.toString());
    //     return handler.next(response); //
    //     print(response.data.toString()); // continue
    //     // If you want to reject the request with a error message,
    //     // you can reject a `DioError` object eg: return `dio.reject(dioError)`
    //   },
    //   onError: (DioError e, handler) {
    //     // Do something with response error
    //     // print("sddh" + e.message);
    //     return handler.next(e); //continue
    //     // If you want to resolve the request with some custom data，
    //     // you can resolve a `Response` object eg: return `dio.resolve(response)`.
    //   },
    //   // onRequest:(options, handler) {
    //   //   return handler.resolve(Response(requestOptions:options,data:'fake data'));
    //   // },
    // ));
  }

  ///dio get
  Future<Response> get({String url, HeaderModel header}) {
    dio.options.headers.addAll({"Authorization": header.authorization,});
    return dio.get(url);
  }

  ///dio login post
  // Future onError(DioError err, ErrorInterceptorHandler handler) async {
  //   print("onError: ${err.response.statusCode}");
  //   return handler.next(err); // <--- THE TIP IS HERE
  // }

  Future<Response> loginPost({String url, var data}) {
    return dio.post(url, data: data);
  }

  ///dio  post
  Future<Response> post({String url, HeaderModel header, var data}) {
    dio.options.headers.addAll({
      // "X-Session": header.xSession,
      // "X-RequestTime": header.xRequestTime,
      // "X-UDID": header.xUdid,
      // "X-ReferenceId": header.xReferenceId,
      // "X-AccessMode": header.xAccessMode,
      // "X-User": header.xUserId,
      // "X-UserId": header.xUserId,
      "Authorization": header.authorization,
    });
    return dio.post(url, data: data);
  }

  Future<Response> patch({String url, HeaderModel header, var data}) {
    dio.options.headers.addAll({"Authorization": header.authorization,});
    return dio.patch(url, data: data);
  }

  /// dio delete
  Future<Response> delete({String url, HeaderModel header}) {
    dio.options.headers.addAll({"Authorization": header.authorization});
    return dio.delete(url);
  }
}
