// To parse this JSON data, do
//
//     final updateReviewsResponseModel = updateReviewsResponseModelFromMap(jsonString);
//
// import 'dart:convert';
//
// UpdateReviewsResponseModel updateReviewsResponseModelFromMap(String str) =>
//     UpdateReviewsResponseModel.fromMap(json.decode(str));
//
// String updateReviewsResponseModelToMap(UpdateReviewsResponseModel data) =>
//     json.encode(data.toMap());
//
// class UpdateReviewsResponseModel {
//   UpdateReviewsResponseModel({
//     this.id,
//     this.userName,
//     this.rating,
//     this.review,
//     this.createdAt,
//     this.food,
//     this.user,
//     this.statusCode,
//     this.message,
//   });
//
//   final int id;
//   final String userName;
//   final int rating;
//   final String review;
//   final DateTime createdAt;
//   final int food;
//   final int user;
//   final int statusCode;
//   final String message;
//
//   factory UpdateReviewsResponseModel.fromMap(Map<String, dynamic> json) =>
//       UpdateReviewsResponseModel(
//         id: json["id"] == null ? null : json["id"],
//         userName: json["user_name"] == null ? null : json["user_name"],
//         rating: json["rating"] == null ? null : json["rating"],
//         review: json["review"] == null ? null : json["review"],
//         createdAt: json["created_at"] == null
//             ? null
//             : DateTime.parse(json["created_at"]),
//         food: json["food"] == null ? null : json["food"],
//         user: json["user"] == null ? null : json["user"],
//         statusCode: json["statusCode"] == null ? null : json["statusCode"],
//         message: json["message"] == null ? null : json["message"],
//       );
//
//   Map<String, dynamic> toMap() => {
//         "id": id == null ? null : id,
//         "user_name": userName == null ? null : userName,
//         "rating": rating == null ? null : rating,
//         "review": review == null ? null : review,
//         "created_at": createdAt == null ? null : createdAt.toIso8601String(),
//         "food": food == null ? null : food,
//         "user": user == null ? null : user,
//         "statusCode": statusCode == null ? null : statusCode,
//         "message": message == null ? null : message,
//       };
// }
//

// To parse this JSON data, do
//
//     final updateReviewsResponseModel = updateReviewsResponseModelFromJson(jsonString);

import 'dart:convert';

UpdateReviewsResponseModel updateReviewsResponseModelFromJson(String str) => UpdateReviewsResponseModel.fromJson(json.decode(str));

String updateReviewsResponseModelToJson(UpdateReviewsResponseModel data) => json.encode(data.toJson());

class UpdateReviewsResponseModel {
  UpdateReviewsResponseModel({
    this.id,
    this.userName,
    this.rating,
    this.review,
    this.createdAt,
    this.food,
    this.user,
    this.statusCode,
    this.message,
  });

  final int id;
  final String userName;
  final int rating;
  final String review;
  final DateTime createdAt;
  final int food;
  final int user;
  final int statusCode;
  final String message;

  factory UpdateReviewsResponseModel.fromJson(Map<String, dynamic> json) => UpdateReviewsResponseModel(
    id: json["id"] == null ? null : json["id"],
    userName: json["user_name"] == null ? null : json["user_name"],
    rating: json["rating"] == null ? null : json["rating"],
    review: json["review"] == null ? null : json["review"],
    createdAt: json["created_at"] == null ? null : DateTime.parse(json["created_at"]),
    food: json["food"] == null ? null : json["food"],
    user: json["user"] == null ? null : json["user"],
    statusCode: json["statusCode"] == null ? null : json["statusCode"],
    message: json["message"] == null ? null : json["message"],
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "user_name": userName == null ? null : userName,
    "rating": rating == null ? null : rating,
    "review": review == null ? null : review,
    "created_at": createdAt == null ? null : createdAt.toIso8601String(),
    "food": food == null ? null : food,
    "user": user == null ? null : user,
    "statusCode": statusCode == null ? null : statusCode,
    "message": message == null ? null : message,
  };
}
