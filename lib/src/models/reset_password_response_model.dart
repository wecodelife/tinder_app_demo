// To parse this JSON data, do
//
//     final resetPasswordResponse = resetPasswordResponseFromJson(jsonString);

import 'dart:convert';

ResetPasswordResponse resetPasswordResponseFromJson(String str) =>
    ResetPasswordResponse.fromJson(json.decode(str));

String resetPasswordResponseToJson(ResetPasswordResponse data) =>
    json.encode(data.toJson());

class ResetPasswordResponse {
  ResetPasswordResponse({
    this.password,
    this.confirmPassword,
    this.token,
  });

  final String password;
  final String confirmPassword;
  final String token;

  factory ResetPasswordResponse.fromJson(Map<String, dynamic> json) =>
      ResetPasswordResponse(
        password: json["password"] == null ? null : json["password"],
        confirmPassword:
            json["confirm_password"] == null ? null : json["confirm_password"],
        token: json["token"] == null ? null : json["token"],
      );

  Map<String, dynamic> toJson() => {
        "password": password == null ? null : password,
        "confirm_password": confirmPassword == null ? null : confirmPassword,
        "token": token == null ? null : token,
      };
}
