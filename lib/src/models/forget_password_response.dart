// To parse this JSON data, do
//
//     final forgetPasswordResponse = forgetPasswordResponseFromJson(jsonString);

import 'dart:convert';

ForgetPasswordResponse forgetPasswordResponseFromJson(String str) =>
    ForgetPasswordResponse.fromJson(json.decode(str));

String forgetPasswordResponseToJson(ForgetPasswordResponse data) =>
    json.encode(data.toJson());

class ForgetPasswordResponse {
  ForgetPasswordResponse({
    this.message,
  });

  final String message;

  factory ForgetPasswordResponse.fromJson(Map<String, dynamic> json) =>
      ForgetPasswordResponse(
        message: json["message"] == null ? null : json["message"],
      );

  Map<String, dynamic> toJson() => {
        "message": message == null ? null : message,
      };
}
