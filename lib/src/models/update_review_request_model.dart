// To parse this JSON data, do
//
//     final updateReviewsRequestModel = updateReviewsRequestModelFromJson(jsonString);
//
// import 'dart:convert';
//
// UpdateReviewsRequestModel updateReviewsRequestModelFromJson(String str) =>
//     UpdateReviewsRequestModel.fromJson(json.decode(str));
//
// String updateReviewsRequestModelToJson(UpdateReviewsRequestModel data) =>
//     json.encode(data.toJson());
//
// class UpdateReviewsRequestModel {
//   UpdateReviewsRequestModel({
//     this.userName,
//     this.food,
//     this.user,
//     this.review,
//     this.rating,
//   });
//
//   final String userName;
//   final String food;
//   final String user;
//   final String review;
//   final int rating;
//
//   factory UpdateReviewsRequestModel.fromJson(Map<String, dynamic> json) =>
//       UpdateReviewsRequestModel(
//         userName: json["user_name"] == null ? null : json["user_name"],
//         food: json["food"] == null ? null : json["food"],
//         user: json["user"] == null ? null : json["user"],
//         review: json["review"] == null ? null : json["review"],
//         rating: json["rating"] == null ? null : json["rating"],
//       );
//
//   Map<String, dynamic> toJson() => {
//         "user_name": userName == null ? null : userName,
//         "food": food == null ? null : food,
//         "user": user == null ? null : user,
//         "review": review == null ? null : review,
//         "rating": rating == null ? null : rating,
//       };
// }


// To parse this JSON data, do
//
//     final updateReviewsRequestModel = updateReviewsRequestModelFromJson(jsonString);

import 'dart:convert';

UpdateReviewsRequestModel updateReviewsRequestModelFromJson(String str) => UpdateReviewsRequestModel.fromJson(json.decode(str));

String updateReviewsRequestModelToJson(UpdateReviewsRequestModel data) => json.encode(data.toJson());

class UpdateReviewsRequestModel {
  UpdateReviewsRequestModel({
    this.rating,
    this.review,
    this.food,
    this.user,
  });

  final int rating;
  final String review;
  final int food;
  final int user;

  factory UpdateReviewsRequestModel.fromJson(Map<String, dynamic> json) => UpdateReviewsRequestModel(
    rating: json["rating"] == null ? null : json["rating"],
    review: json["review"] == null ? null : json["review"],
    food: json["food"] == null ? null : json["food"],
    user: json["user"] == null ? null : json["user"],
  );

  Map<String, dynamic> toJson() => {
    "rating": rating == null ? null : rating,
    "review": review == null ? null : review,
    "food": food == null ? null : food,
    "user": user == null ? null : user,
  };
}
