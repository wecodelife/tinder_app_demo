// To parse this JSON data, do
//
//     final getFoodType = getFoodTypeFromJson(jsonString);

import 'dart:convert';

GetFoodType getFoodTypeFromJson(String str) => GetFoodType.fromJson(json.decode(str));

String getFoodTypeToJson(GetFoodType data) => json.encode(data.toJson());

class GetFoodType {
  GetFoodType({
    this.count,
    this.next,
    this.previous,
    this.results,
    this.statusCode,
    this.message,
  });

  final int count;
  final dynamic next;
  final dynamic previous;
  final List<Result> results;
  final int statusCode;
  final String message;

  factory GetFoodType.fromJson(Map<String, dynamic> json) => GetFoodType(
    count: json["count"] == null ? null : json["count"],
    next: json["next"],
    previous: json["previous"],
    results: json["results"] == null ? null : List<Result>.from(json["results"].map((x) => Result.fromJson(x))),
    statusCode: json["statusCode"] == null ? null : json["statusCode"],
    message: json["message"] == null ? null : json["message"],
  );

  Map<String, dynamic> toJson() => {
    "count": count == null ? null : count,
    "next": next,
    "previous": previous,
    "results": results == null ? null : List<dynamic>.from(results.map((x) => x.toJson())),
    "statusCode": statusCode == null ? null : statusCode,
    "message": message == null ? null : message,
  };
}

class Result {
  Result({
    this.id,
    this.name,
    this.isDeleted,
  });

  final int id;
  final String name;
  final bool isDeleted;

  factory Result.fromJson(Map<String, dynamic> json) => Result(
    id: json["id"] == null ? null : json["id"],
    name: json["name"] == null ? null : json["name"],
    isDeleted: json["is_deleted"] == null ? null : json["is_deleted"],
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "name": name == null ? null : name,
    "is_deleted": isDeleted == null ? null : isDeleted,
  };
}
