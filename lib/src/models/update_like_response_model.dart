// // To parse this JSON data, do
// //
// //     final updateLikeResponseModel = updateLikeResponseModelFromJson(jsonString);
//
// import 'dart:convert';
//
// UpdateLikeResponseModel updateLikeResponseModelFromJson(String str) =>
//     UpdateLikeResponseModel.fromJson(json.decode(str));
//
// String updateLikeResponseModelToJson(UpdateLikeResponseModel data) =>
//     json.encode(data.toJson());
//
// class UpdateLikeResponseModel {
//   UpdateLikeResponseModel({
//     this.id,
//     this.isLiked,
//     this.isFavourite,
//     this.food,
//     this.user,
//     this.statusCode,
//     this.message,
//   });
//
//   final int id;
//   final bool isLiked;
//   final bool isFavourite;
//   final int food;
//   final int user;
//   final int statusCode;
//   final String message;
//
//   factory UpdateLikeResponseModel.fromJson(Map<String, dynamic> json) =>
//       UpdateLikeResponseModel(
//         id: json["id"] == null ? null : json["id"],
//         isLiked: json["is_liked"] == null ? null : json["is_liked"],
//         isFavourite: json["is_favourite"] == null ? null : json["is_favourite"],
//         food: json["food"] == null ? null : json["food"],
//         user: json["user"] == null ? null : json["user"],
//         statusCode: json["statusCode"] == null ? null : json["statusCode"],
//         message: json["message"] == null ? null : json["message"],
//       );
//
//   Map<String, dynamic> toJson() => {
//         "id": id == null ? null : id,
//         "is_liked": isLiked == null ? null : isLiked,
//         "is_favourite": isFavourite == null ? null : isFavourite,
//         "food": food == null ? null : food,
//         "user": user == null ? null : user,
//         "statusCode": statusCode == null ? null : statusCode,
//         "message": message == null ? null : message,
//       };
// }

// To parse this JSON data, do
//
//     final updateLikeResponseModel = updateLikeResponseModelFromJson(jsonString);

import 'dart:convert';

UpdateLikeResponseModel updateLikeResponseModelFromJson(String str) => UpdateLikeResponseModel.fromJson(json.decode(str));

String updateLikeResponseModelToJson(UpdateLikeResponseModel data) => json.encode(data.toJson());

class UpdateLikeResponseModel {
  UpdateLikeResponseModel({
    this.status,
    this.message,
  });

  final int status;
  final String message;

  factory UpdateLikeResponseModel.fromJson(Map<String, dynamic> json) => UpdateLikeResponseModel(
    status: json["status"] == null ? null : json["status"],
    message: json["message"] == null ? null : json["message"],
  );

  Map<String, dynamic> toJson() => {
    "status": status == null ? null : status,
    "message": message == null ? null : message,
  };
}
