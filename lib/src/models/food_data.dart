// To parse this JSON data, do
//
//     final foodData = foodDataFromJson(jsonString);

import 'dart:convert';

FoodData foodDataFromJson(String str) => FoodData.fromJson(json.decode(str));

String foodDataToJson(FoodData data) => json.encode(data.toJson());

class FoodData {
  FoodData({
    this.id,
    this.likesCount,
    this.dislikesCount,
    this.rating,
    this.name,
    this.description,
    this.restaurant,
    this.place,
    this.image1,
    this.image2,
    this.image3,
    this.image4,
    this.image5,
    this.lat,
    this.lon,
    this.googleMapUrl,
    this.isDeleted,
    this.foodType,
  });

  final int id;
  final int likesCount;
  final int dislikesCount;
  final dynamic rating;
  final String name;
  final String description;
  final String restaurant;
  final String place;
  final dynamic image1;
  final dynamic image2;
  final dynamic image3;
  final dynamic image4;
  final dynamic image5;
  final dynamic lat;
  final dynamic lon;
  final dynamic googleMapUrl;
  final bool isDeleted;
  final dynamic foodType;

  factory FoodData.fromJson(Map<String, dynamic> json) => FoodData(
        id: json["id"] == null ? null : json["id"],
        likesCount: json["likes_count"] == null ? null : json["likes_count"],
        dislikesCount:
            json["dislikes_count"] == null ? null : json["dislikes_count"],
        rating: json["rating"],
        name: json["name"] == null ? null : json["name"],
        description: json["description"] == null ? null : json["description"],
        restaurant: json["restaurant"] == null ? null : json["restaurant"],
        place: json["place"] == null ? null : json["place"],
        image1: json["image1"],
        image2: json["image2"],
        image3: json["image3"],
        image4: json["image4"],
        image5: json["image5"],
        lat: json["lat"],
        lon: json["lon"],
        googleMapUrl: json["google_map_url"],
        isDeleted: json["is_deleted"] == null ? null : json["is_deleted"],
        foodType: json["food_type"],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "likes_count": likesCount == null ? null : likesCount,
        "dislikes_count": dislikesCount == null ? null : dislikesCount,
        "rating": rating,
        "name": name == null ? null : name,
        "description": description == null ? null : description,
        "restaurant": restaurant == null ? null : restaurant,
        "place": place == null ? null : place,
        "image1": image1,
        "image2": image2,
        "image3": image3,
        "image4": image4,
        "image5": image5,
        "lat": lat,
        "lon": lon,
        "google_map_url": googleMapUrl,
        "is_deleted": isDeleted == null ? null : isDeleted,
        "food_type": foodType,
      };
}
