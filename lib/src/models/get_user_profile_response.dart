// To parse this JSON data, do
//
//     final getUserProfilerResponse = getUserProfilerResponseFromJson(jsonString);

import 'dart:convert';

GetUserProfilerResponse getUserProfilerResponseFromJson(String str) => GetUserProfilerResponse.fromJson(json.decode(str));

String getUserProfilerResponseToJson(GetUserProfilerResponse data) => json.encode(data.toJson());

class GetUserProfilerResponse {
  GetUserProfilerResponse({
    this.count,
    this.next,
    this.previous,
    this.results,
  });

  final int count;
  final dynamic next;
  final dynamic previous;
  final List<Result> results;

  factory GetUserProfilerResponse.fromJson(Map<String, dynamic> json) => GetUserProfilerResponse(
    count: json["count"] == null ? null : json["count"],
    next: json["next"],
    previous: json["previous"],
    results: json["results"] == null ? null : List<Result>.from(json["results"].map((x) => Result.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "count": count == null ? null : count,
    "next": next,
    "previous": previous,
    "results": results == null ? null : List<dynamic>.from(results.map((x) => x.toJson())),
  };
}

class Result {
  Result({
    this.id,
    this.profileImageUrl,
    this.lastLogin,
    this.isSuperuser,
    this.firstName,
    this.lastName,
    this.isStaff,
    this.isActive,
    this.dateJoined,
    this.email,
    this.phoneNumber,
    this.profileImage,
    this.deviceId,
    this.isDeleted,
    this.isVerified,
    this.deletedOn,
    this.deletedBy,
    this.groups,
    this.userPermissions,
  });

  final int id;
  final String profileImageUrl;
  final DateTime lastLogin;
  final bool isSuperuser;
  final String firstName;
  final String lastName;
  final bool isStaff;
  final bool isActive;
  final DateTime dateJoined;
  final String email;
  final String phoneNumber;
  final String profileImage;
  final dynamic deviceId;
  final bool isDeleted;
  final bool isVerified;
  final dynamic deletedOn;
  final dynamic deletedBy;
  final List<dynamic> groups;
  final List<dynamic> userPermissions;

  factory Result.fromJson(Map<String, dynamic> json) => Result(
    id: json["id"] == null ? null : json["id"],
    profileImageUrl: json["profile_image_url"] == null ? null : json["profile_image_url"],
    lastLogin: json["last_login"] == null ? null : DateTime.parse(json["last_login"]),
    isSuperuser: json["is_superuser"] == null ? null : json["is_superuser"],
    firstName: json["first_name"] == null ? null : json["first_name"],
    lastName: json["last_name"] == null ? null : json["last_name"],
    isStaff: json["is_staff"] == null ? null : json["is_staff"],
    isActive: json["is_active"] == null ? null : json["is_active"],
    dateJoined: json["date_joined"] == null ? null : DateTime.parse(json["date_joined"]),
    email: json["email"] == null ? null : json["email"],
    phoneNumber: json["phone_number"] == null ? null : json["phone_number"],
    profileImage: json["profile_image"] == null ? null : json["profile_image"],
    deviceId: json["device_id"],
    isDeleted: json["is_deleted"] == null ? null : json["is_deleted"],
    isVerified: json["is_verified"] == null ? null : json["is_verified"],
    deletedOn: json["deleted_on"],
    deletedBy: json["deleted_by"],
    groups: json["groups"] == null ? null : List<dynamic>.from(json["groups"].map((x) => x)),
    userPermissions: json["user_permissions"] == null ? null : List<dynamic>.from(json["user_permissions"].map((x) => x)),
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "profile_image_url": profileImageUrl == null ? null : profileImageUrl,
    "last_login": lastLogin == null ? null : lastLogin.toIso8601String(),
    "is_superuser": isSuperuser == null ? null : isSuperuser,
    "first_name": firstName == null ? null : firstName,
    "last_name": lastName == null ? null : lastName,
    "is_staff": isStaff == null ? null : isStaff,
    "is_active": isActive == null ? null : isActive,
    "date_joined": dateJoined == null ? null : dateJoined.toIso8601String(),
    "email": email == null ? null : email,
    "phone_number": phoneNumber == null ? null : phoneNumber,
    "profile_image": profileImage == null ? null : profileImage,
    "device_id": deviceId,
    "is_deleted": isDeleted == null ? null : isDeleted,
    "is_verified": isVerified == null ? null : isVerified,
    "deleted_on": deletedOn,
    "deleted_by": deletedBy,
    "groups": groups == null ? null : List<dynamic>.from(groups.map((x) => x)),
    "user_permissions": userPermissions == null ? null : List<dynamic>.from(userPermissions.map((x) => x)),
  };
}
