import 'package:flutter/cupertino.dart';

class ImageModel {
  ImageModel({
    this.id,
    this.likesCount,
    this.dislikesCount,
    this.rating,
    this.images,
    this.name,
    this.description,
    this.restaurant,
    this.place,
    this.image1,
    this.image2,
    this.image3,
    this.image4,
    this.image5,
    this.lat,
    this.lon,
    this.googleMapUrl,
    this.isDeleted,
    this.ingredients,
    this.preparationMethod,
    this.price,
    this.foodType,
    this.mustBool,
    this.likeBool
  });

  int id;
  int likesCount;
  int dislikesCount;
  double rating;
  List<String> images;
  String name;
  String description;
  String restaurant;
  String place;
  Image image1;
  Image image2;
  Image image3;
  Image image4;
  Image image5;
  final String lat;
  final String lon;
  final dynamic googleMapUrl;
  final bool isDeleted;
  final bool mustBool;
  final bool likeBool;
  final List<String> ingredients;
  final String preparationMethod;
  String price;
  int foodType;
}
