// To parse this JSON data, do
//
//     final registerUserResponse = registerUserResponseFromJson(jsonString);

import 'dart:convert';

RegisterUserResponse registerUserResponseFromJson(String str) => RegisterUserResponse.fromJson(json.decode(str));

String registerUserResponseToJson(RegisterUserResponse data) => json.encode(data.toJson());

class RegisterUserResponse {
  RegisterUserResponse({
    this.id,
    this.profileImageUrl,
    this.lastLogin,
    this.isSuperuser,
    this.firstName,
    this.lastName,
    this.isStaff,
    this.isActive,
    this.dateJoined,
    this.email,
    this.phoneNumber,
    this.profileImage,
    this.deviceId,
    this.isDeleted,
    this.isVerified,
    this.deletedOn,
    this.deletedBy,
    this.groups,
    this.userPermissions,
  });

  final int id;
  final String profileImageUrl;
  final dynamic lastLogin;
  final bool isSuperuser;
  final String firstName;
  final String lastName;
  final bool isStaff;
  final bool isActive;
  final DateTime dateJoined;
  final dynamic email;
  final dynamic phoneNumber;
  final String profileImage;
  final dynamic deviceId;
  final bool isDeleted;
  final bool isVerified;
  final dynamic deletedOn;
  final dynamic deletedBy;
  final List<dynamic> groups;
  final List<dynamic> userPermissions;

  factory RegisterUserResponse.fromJson(Map<String, dynamic> json) => RegisterUserResponse(
    id: json["id"] == null ? null : json["id"],
    profileImageUrl: json["profile_image_url"] == null ? null : json["profile_image_url"],
    lastLogin: json["last_login"],
    isSuperuser: json["is_superuser"] == null ? null : json["is_superuser"],
    firstName: json["first_name"] == null ? null : json["first_name"],
    lastName: json["last_name"] == null ? null : json["last_name"],
    isStaff: json["is_staff"] == null ? null : json["is_staff"],
    isActive: json["is_active"] == null ? null : json["is_active"],
    dateJoined: json["date_joined"] == null ? null : DateTime.parse(json["date_joined"]),
    email: json["email"],
    phoneNumber: json["phone_number"],
    profileImage: json["profile_image"] == null ? null : json["profile_image"],
    deviceId: json["device_id"],
    isDeleted: json["is_deleted"] == null ? null : json["is_deleted"],
    isVerified: json["is_verified"] == null ? null : json["is_verified"],
    deletedOn: json["deleted_on"],
    deletedBy: json["deleted_by"],
    groups: json["groups"] == null ? null : List<dynamic>.from(json["groups"].map((x) => x)),
    userPermissions: json["user_permissions"] == null ? null : List<dynamic>.from(json["user_permissions"].map((x) => x)),
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "profile_image_url": profileImageUrl == null ? null : profileImageUrl,
    "last_login": lastLogin,
    "is_superuser": isSuperuser == null ? null : isSuperuser,
    "first_name": firstName == null ? null : firstName,
    "last_name": lastName == null ? null : lastName,
    "is_staff": isStaff == null ? null : isStaff,
    "is_active": isActive == null ? null : isActive,
    "date_joined": dateJoined == null ? null : dateJoined.toIso8601String(),
    "email": email,
    "phone_number": phoneNumber,
    "profile_image": profileImage == null ? null : profileImage,
    "device_id": deviceId,
    "is_deleted": isDeleted == null ? null : isDeleted,
    "is_verified": isVerified == null ? null : isVerified,
    "deleted_on": deletedOn,
    "deleted_by": deletedBy,
    "groups": groups == null ? null : List<dynamic>.from(groups.map((x) => x)),
    "user_permissions": userPermissions == null ? null : List<dynamic>.from(userPermissions.map((x) => x)),
  };
}
