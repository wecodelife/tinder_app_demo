// To parse this JSON data, do
//
//     final cardImages = cardImagesFromJson(jsonString);

import 'dart:convert';

import 'package:flutter/cupertino.dart';

CardImages cardImagesFromJson(String str) =>
    CardImages.fromJson(json.decode(str));

String cardImagesToJson(CardImages data) => json.encode(data.toJson());

class CardImages {
  CardImages({
    this.title,
    this.description,
    this.imagePath,
    this.colorList,
    this.color,
  });

  final String title;
  final String description;
  final String imagePath;
  final List<Color> colorList;
  final String color;

  factory CardImages.fromJson(Map<String, dynamic> json) => CardImages(
        title: json["title"] == null ? null : json["title"],
        description: json["description"] == null ? null : json["description"],
        imagePath: json["image_path"] == null ? null : json["image_path"],
        colorList: json["colorList"] == null
            ? null
            : List<Color>.from(json["colorList"].map((x) => x)),
        color: json["color"] == null ? null : json["color"],
      );

  Map<String, dynamic> toJson() => {
        "title": title == null ? null : title,
        "description": description == null ? null : description,
        "image_path": imagePath == null ? null : imagePath,
        "colorList": colorList == null
            ? null
            : List<Color>.from(colorList.map((x) => x)),
        "color": color == null ? null : color,
      };
}
