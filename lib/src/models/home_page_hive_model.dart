import 'package:app_template/src/models/get_food_response_Model.dart';
import 'package:hive/hive.dart';

part 'home_page_hive_model.g.dart';

@HiveType(typeId: 1, adapterName: "HomePage")
class HomePageHiveModel {
  HomePageHiveModel({
    @HiveField(0) this.results,
  });

  final List<Result> results;
}

class ResultList {
  ResultList({
    this.id,
    this.likesCount,
    this.dislikesCount,
    this.rating,
    this.images,
    this.name,
    this.description,
    this.restaurant,
    this.place,
    this.image1,
    this.image2,
    this.image3,
    this.image4,
    this.image5,
    this.lat,
    this.lon,
    this.googleMapUrl,
    this.isDeleted,
    this.foodType,
  });

  final int id;
  final int likesCount;
  final int dislikesCount;
  final int rating;
  final List<dynamic> images;
  final String name;
  final String description;
  final String restaurant;
  final String place;
  final dynamic image1;
  final dynamic image2;
  final dynamic image3;
  final dynamic image4;
  final dynamic image5;
  final dynamic lat;
  final dynamic lon;
  final dynamic googleMapUrl;
  final bool isDeleted;
  final String foodType;
}
