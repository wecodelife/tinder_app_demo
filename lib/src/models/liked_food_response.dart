// // To parse this JSON data, do
// //
// //     final likedFoodResponse = likedFoodResponseFromJson(jsonString);
//
// import 'dart:convert';
//
// LikedFoodResponse likedFoodResponseFromJson(String str) =>
//     LikedFoodResponse.fromJson(json.decode(str));
//
// String likedFoodResponseToJson(LikedFoodResponse data) =>
//     json.encode(data.toJson());
//
// class LikedFoodResponse {
//   LikedFoodResponse({
//     this.count,
//     this.next,
//     this.previous,
//     this.results,
//     this.statusCode,
//     this.message,
//   });
//
//   final int count;
//   final dynamic next;
//   final dynamic previous;
//   final List<Result> results;
//   final int statusCode;
//   final String message;
//
//   factory LikedFoodResponse.fromJson(Map<String, dynamic> json) =>
//       LikedFoodResponse(
//         count: json["count"] == null ? null : json["count"],
//         next: json["next"],
//         previous: json["previous"],
//         results: json["results"] == null
//             ? null
//             : List<Result>.from(json["results"].map((x) => Result.fromJson(x))),
//         statusCode: json["statusCode"] == null ? null : json["statusCode"],
//         message: json["message"] == null ? null : json["message"],
//       );
//
//   Map<String, dynamic> toJson() => {
//         "count": count == null ? null : count,
//         "next": next,
//         "previous": previous,
//         "results": results == null
//             ? null
//             : List<dynamic>.from(results.map((x) => x.toJson())),
//         "statusCode": statusCode == null ? null : statusCode,
//         "message": message == null ? null : message,
//       };
// }
//
// class Result {
//   Result({
//     this.food,
//     this.isLiked,
//     this.isFavourite,
//   });
//
//   final Food food;
//   final bool isLiked;
//   final bool isFavourite;
//
//   factory Result.fromJson(Map<String, dynamic> json) => Result(
//         food: json["food"] == null ? null : Food.fromJson(json["food"]),
//         isLiked: json["is_liked"] == null ? null : json["is_liked"],
//         isFavourite: json["is_favourite"] == null ? null : json["is_favourite"],
//       );
//
//   Map<String, dynamic> toJson() => {
//         "food": food == null ? null : food.toJson(),
//         "is_liked": isLiked == null ? null : isLiked,
//         "is_favourite": isFavourite == null ? null : isFavourite,
//       };
// }
//
// class Food {
//   Food({
//     this.id,
//     this.name,
//     this.description,
//     this.restaurant,
//     this.place,
//     this.image1,
//     this.image2,
//     this.image3,
//     this.image4,
//     this.image5,
//     this.lat,
//     this.lon,
//     this.googleMapUrl,
//     this.isDeleted,
//     this.ingredients,
//     this.preparationMethod,
//     this.price,
//     this.foodType,
//   });
//
//   final int id;
//   final String name;
//   final String description;
//   final String restaurant;
//   final String place;
//   final String image1;
//   final String image2;
//   final String image3;
//   final String image4;
//   final String image5;
//   final String lat;
//   final String lon;
//   final dynamic googleMapUrl;
//   final bool isDeleted;
//   final List<String> ingredients;
//   final String preparationMethod;
//   final String price;
//   final int foodType;
//
//   factory Food.fromJson(Map<String, dynamic> json) => Food(
//         id: json["id"] == null ? null : json["id"],
//         name: json["name"] == null ? null : json["name"],
//         description: json["description"] == null ? null : json["description"],
//         restaurant: json["restaurant"] == null ? null : json["restaurant"],
//         place: json["place"] == null ? null : json["place"],
//         image1: json["image1"] == null ? null : json["image1"],
//         image2: json["image2"] == null ? null : json["image2"],
//         image3: json["image3"] == null ? null : json["image3"],
//         image4: json["image4"] == null ? null : json["image4"],
//         image5: json["image5"] == null ? null : json["image5"],
//         lat: json["lat"] == null ? null : json["lat"],
//         lon: json["lon"] == null ? null : json["lon"],
//         googleMapUrl: json["google_map_url"],
//         isDeleted: json["is_deleted"] == null ? null : json["is_deleted"],
//         ingredients: json["ingredients"] == null
//             ? null
//             : List<String>.from(json["ingredients"].map((x) => x)),
//         preparationMethod: json["preparation_method"] == null
//             ? null
//             : json["preparation_method"],
//         price: json["price"] == null ? null : json["price"],
//         foodType: json["food_type"] == null ? null : json["food_type"],
//       );
//
//   Map<String, dynamic> toJson() => {
//         "id": id == null ? null : id,
//         "name": name == null ? null : name,
//         "description": description == null ? null : description,
//         "restaurant": restaurant == null ? null : restaurant,
//         "place": place == null ? null : place,
//         "image1": image1 == null ? null : image1,
//         "image2": image2 == null ? null : image2,
//         "image3": image3 == null ? null : image3,
//         "image4": image4 == null ? null : image4,
//         "image5": image5 == null ? null : image5,
//         "lat": lat == null ? null : lat,
//         "lon": lon == null ? null : lon,
//         "google_map_url": googleMapUrl,
//         "is_deleted": isDeleted == null ? null : isDeleted,
//         "ingredients": ingredients == null
//             ? null
//             : List<dynamic>.from(ingredients.map((x) => x)),
//         "preparation_method":
//             preparationMethod == null ? null : preparationMethod,
//         "price": price == null ? null : price,
//         "food_type": foodType == null ? null : foodType,
//       };
// }
//
//


// To parse this JSON data, do
//
//     final likedFoodResponse = likedFoodResponseFromJson(jsonString);
//
// To parse this JSON data, do
//
//     final likedFoodResponse = likedFoodResponseFromJson(jsonString);
// To parse this JSON data, do
//
//     final likedFoodResponse = likedFoodResponseFromJson(jsonString);

import 'dart:convert';

LikedFoodResponse likedFoodResponseFromJson(String str) => LikedFoodResponse.fromJson(json.decode(str));

String likedFoodResponseToJson(LikedFoodResponse data) => json.encode(data.toJson());

class LikedFoodResponse {
  LikedFoodResponse({
    this.count,
    this.next,
    this.previous,
    this.results,
    this.statusCode,
    this.message,
  });

  final int count;
  final dynamic next;
  final dynamic previous;
  final List<Result> results;
  final int statusCode;
  final String message;

  factory LikedFoodResponse.fromJson(Map<String, dynamic> json) => LikedFoodResponse(
    count: json["count"] == null ? null : json["count"],
    next: json["next"],
    previous: json["previous"],
    results: json["results"] == null ? null : List<Result>.from(json["results"].map((x) => Result.fromJson(x))),
    statusCode: json["statusCode"] == null ? null : json["statusCode"],
    message: json["message"] == null ? null : json["message"],
  );

  Map<String, dynamic> toJson() => {
    "count": count == null ? null : count,
    "next": next,
    "previous": previous,
    "results": results == null ? null : List<dynamic>.from(results.map((x) => x.toJson())),
    "statusCode": statusCode == null ? null : statusCode,
    "message": message == null ? null : message,
  };
}

class Result {
  Result({
    this.id,
    this.food,
    this.isLiked,
    this.isFavourite,
    this.mustVisit,
  });

  final int id;
  final Food food;
  final bool isLiked;
  final bool isFavourite;
  final bool mustVisit;

  factory Result.fromJson(Map<String, dynamic> json) => Result(
    id: json["id"] == null ? null : json["id"],
    food: json["food"] == null ? null : Food.fromJson(json["food"]),
    isLiked: json["is_liked"] == null ? null : json["is_liked"],
    isFavourite: json["is_favourite"] == null ? null : json["is_favourite"],
    mustVisit: json["must_visit"] == null ? null : json["must_visit"],
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "food": food == null ? null : food.toJson(),
    "is_liked": isLiked == null ? null : isLiked,
    "is_favourite": isFavourite == null ? null : isFavourite,
    "must_visit": mustVisit == null ? null : mustVisit,
  };
}

class Food {
  Food({
    this.id,
    this.name,
    this.description,
    this.restaurant,
    this.place,
    this.image1,
    this.image2,
    this.image3,
    this.image4,
    this.image5,
    this.lat,
    this.lon,
    this.googleMapUrl,
    this.isDeleted,
    this.ingredients,
    this.preparationMethod,
    this.price,
    this.foodType,
  });

  final int id;
  final String name;
  final String description;
  final String restaurant;
  final String place;
  final String image1;
  final String image2;
  final String image3;
  final String image4;
  final String image5;
  final String lat;
  final String lon;
  final dynamic googleMapUrl;
  final bool isDeleted;
  final List<String> ingredients;
  final String preparationMethod;
  final String price;
  final int foodType;

  factory Food.fromJson(Map<String, dynamic> json) => Food(
    id: json["id"] == null ? null : json["id"],
    name: json["name"] == null ? null : json["name"],
    description: json["description"] == null ? null : json["description"],
    restaurant: json["restaurant"] == null ? null : json["restaurant"],
    place: json["place"] == null ? null : json["place"],
    image1: json["image1"] == null ? null : json["image1"],
    image2: json["image2"] == null ? null : json["image2"],
    image3: json["image3"] == null ? null : json["image3"],
    image4: json["image4"] == null ? null : json["image4"],
    image5: json["image5"] == null ? null : json["image5"],
    lat: json["lat"] == null ? null : json["lat"],
    lon: json["lon"] == null ? null : json["lon"],
    googleMapUrl: json["google_map_url"],
    isDeleted: json["is_deleted"] == null ? null : json["is_deleted"],
    ingredients: json["ingredients"] == null ? null : List<String>.from(json["ingredients"].map((x) => x)),
    preparationMethod: json["preparation_method"] == null ? null : json["preparation_method"],
    price: json["price"] == null ? null : json["price"],
    foodType: json["food_type"] == null ? null : json["food_type"],
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "name": name == null ? null : name,
    "description": description == null ? null : description,
    "restaurant": restaurant == null ? null : restaurant,
    "place": place == null ? null : place,
    "image1": image1 == null ? null : image1,
    "image2": image2 == null ? null : image2,
    "image3": image3 == null ? null : image3,
    "image4": image4 == null ? null : image4,
    "image5": image5 == null ? null : image5,
    "lat": lat == null ? null : lat,
    "lon": lon == null ? null : lon,
    "google_map_url": googleMapUrl,
    "is_deleted": isDeleted == null ? null : isDeleted,
    "ingredients": ingredients == null ? null : List<dynamic>.from(ingredients.map((x) => x)),
    "preparation_method": preparationMethod == null ? null : preparationMethod,
    "price": price == null ? null : price,
    "food_type": foodType == null ? null : foodType,
  };
}
