// To parse this JSON data, do
//
//     final updateFoodRequest = updateFoodRequestFromJson(jsonString);

import 'dart:convert';

UpdateFoodRequest updateFoodRequestFromJson(String str) =>
    UpdateFoodRequest.fromJson(json.decode(str));

String updateFoodRequestToJson(UpdateFoodRequest data) =>
    json.encode(data.toJson());

class UpdateFoodRequest {
  UpdateFoodRequest({
    this.id,
    this.foodTypeName,
    this.name,
    this.description,
    this.restaurant,
    this.place,
    this.image1,
    this.image2,
    this.image3,
    this.image4,
    this.image5,
    this.lat,
    this.lon,
    this.googleMapUrl,
    this.isDeleted,
    this.ingredients,
    this.preparationMethod,
    this.price,
    this.foodType,
  });

  final int id;
  final String foodTypeName;
  final String name;
  final String description;
  final String restaurant;
  final String place;
  final dynamic image1;
  final dynamic image2;
  final dynamic image3;
  final dynamic image4;
  final dynamic image5;
  final String lat;
  final String lon;
  final String googleMapUrl;
  final bool isDeleted;
  final List<String> ingredients;
  final String preparationMethod;
  final String price;
  final int foodType;

  factory UpdateFoodRequest.fromJson(Map<String, dynamic> json) =>
      UpdateFoodRequest(
        id: json["id"] == null ? null : json["id"],
        foodTypeName:
            json["food_type_name"] == null ? null : json["food_type_name"],
        name: json["name"] == null ? null : json["name"],
        description: json["description"] == null ? null : json["description"],
        restaurant: json["restaurant"] == null ? null : json["restaurant"],
        place: json["place"] == null ? null : json["place"],
        image1: json["image1"],
        image2: json["image2"],
        image3: json["image3"],
        image4: json["image4"],
        image5: json["image5"],
        lat: json["lat"] == null ? null : json["lat"],
        lon: json["lon"] == null ? null : json["lon"],
        googleMapUrl:
            json["google_map_url"] == null ? null : json["google_map_url"],
        isDeleted: json["is_deleted"] == null ? null : json["is_deleted"],
        ingredients: json["ingredients"] == null
            ? null
            : List<String>.from(json["ingredients"].map((x) => x)),
        preparationMethod: json["preparation_method"] == null
            ? null
            : json["preparation_method"],
        price: json["price"] == null ? null : json["price"],
        foodType: json["food_type"] == null ? null : json["food_type"],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "food_type_name": foodTypeName == null ? null : foodTypeName,
        "name": name == null ? null : name,
        "description": description == null ? null : description,
        "restaurant": restaurant == null ? null : restaurant,
        "place": place == null ? null : place,
        "image1": image1,
        "image2": image2,
        "image3": image3,
        "image4": image4,
        "image5": image5,
        "lat": lat == null ? null : lat,
        "lon": lon == null ? null : lon,
        "google_map_url": googleMapUrl == null ? null : googleMapUrl,
        "is_deleted": isDeleted == null ? null : isDeleted,
        "ingredients": ingredients == null
            ? null
            : List<dynamic>.from(ingredients.map((x) => x)),
        "preparation_method":
            preparationMethod == null ? null : preparationMethod,
        "price": price == null ? null : price,
        "food_type": foodType == null ? null : foodType,
      };
}
