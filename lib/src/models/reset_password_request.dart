// To parse this JSON data, do
//
//     final resetPasswordRequest = resetPasswordRequestFromJson(jsonString);

import 'dart:convert';

ResetPasswordRequest resetPasswordRequestFromJson(String str) =>
    ResetPasswordRequest.fromJson(json.decode(str));

String resetPasswordRequestToJson(ResetPasswordRequest data) =>
    json.encode(data.toJson());

class ResetPasswordRequest {
  ResetPasswordRequest({
    this.password,
    this.confirmPassword,
    this.token,
  });

  final String password;
  final String confirmPassword;
  final String token;

  factory ResetPasswordRequest.fromJson(Map<String, dynamic> json) =>
      ResetPasswordRequest(
        password: json["password"] == null ? null : json["password"],
        confirmPassword:
            json["confirm_password"] == null ? null : json["confirm_password"],
        token: json["token"] == null ? null : json["token"],
      );

  Map<String, dynamic> toJson() => {
        "password": password == null ? null : password,
        "confirm_password": confirmPassword == null ? null : confirmPassword,
        "token": token == null ? null : token,
      };
}
