// // // // To parse this JSON data, do
// // // //
// // // //     final updateFavouriteResponseModel = updateFavouriteResponseModelFromJson(jsonString);
// // //
// // // import 'dart:convert';
// // //
// // // UpdateFavouriteResponseModel updateFavouriteResponseModelFromJson(String str) =>
// // //     UpdateFavouriteResponseModel.fromJson(json.decode(str));
// // //
// // // String updateFavouriteResponseModelToJson(UpdateFavouriteResponseModel data) =>
// // //     json.encode(data.toJson());
// // //
// // // class UpdateFavouriteResponseModel {
// // //   UpdateFavouriteResponseModel({
// // //     this.id,
// // //     this.isLiked,
// // //     this.isFavourite,
// // //     this.food,
// // //     this.user,
// // //     this.statusCode,
// // //     this.message,
// // //   });
// // //
// // //   final int id;
// // //   final bool isLiked;
// // //   final bool isFavourite;
// // //   final int food;
// // //   final int user;
// // //   final int statusCode;
// // //   final String message;
// // //
// // //   factory UpdateFavouriteResponseModel.fromJson(Map<String, dynamic> json) =>
// // //       UpdateFavouriteResponseModel(
// // //         id: json["id"] == null ? null : json["id"],
// // //         isLiked: json["is_liked"] == null ? null : json["is_liked"],
// // //         isFavourite: json["is_favourite"] == null ? null : json["is_favourite"],
// // //         food: json["food"] == null ? null : json["food"],
// // //         user: json["user"] == null ? null : json["user"],
// // //         statusCode: json["statusCode"] == null ? null : json["statusCode"],
// // //         message: json["message"] == null ? null : json["message"],
// // //       );
// // //
// // //   Map<String, dynamic> toJson() => {
// // //         "id": id == null ? null : id,
// // //         "is_liked": isLiked == null ? null : isLiked,
// // //         "is_favourite": isFavourite == null ? null : isFavourite,
// // //         "food": food == null ? null : food,
// // //         "user": user == null ? null : user,
// // //         "statusCode": statusCode == null ? null : statusCode,
// // //         "message": message == null ? null : message,
// // //       };
// // // }
// //
// // // To parse this JSON data, do
// // //
// // //     final updateFavouriteResponseModel = updateFavouriteResponseModelFromJson(jsonString);
// //
// // import 'dart:convert';
// //
// // UpdateFavouriteResponseModel updateFavouriteResponseModelFromJson(String str) => UpdateFavouriteResponseModel.fromJson(json.decode(str));
// //
// // String updateFavouriteResponseModelToJson(UpdateFavouriteResponseModel data) => json.encode(data.toJson());
// //
// // class UpdateFavouriteResponseModel {
// //   UpdateFavouriteResponseModel({
// //     this.statusCode,
// //     this.message,
// //     this.data,
// //   });
// //
// //   final int statusCode;
// //   final String message;
// //   final Data data;
// //
// //   factory UpdateFavouriteResponseModel.fromJson(Map<String, dynamic> json) => UpdateFavouriteResponseModel(
// //     statusCode: json["statusCode"] == null ? null : json["statusCode"],
// //     message: json["message"] == null ? null : json["message"],
// //     data: json["data"] == null ? null : Data.fromJson(json["data"]),
// //   );
// //
// //   Map<String, dynamic> toJson() => {
// //     "statusCode": statusCode == null ? null : statusCode,
// //     "message": message == null ? null : message,
// //     "data": data == null ? null : data.toJson(),
// //   };
// // }
// //
// // class Data {
// //   Data({
// //     this.id,
// //     this.isLiked,
// //     this.isFavourite,
// //     this.food,
// //     this.user,
// //   });
// //
// //   final int id;
// //   final bool isLiked;
// //   final bool isFavourite;
// //   final int food;
// //   final int user;
// //
// //   factory Data.fromJson(Map<String, dynamic> json) => Data(
// //     id: json["id"] == null ? null : json["id"],
// //     isLiked: json["is_liked"] == null ? null : json["is_liked"],
// //     isFavourite: json["is_favourite"] == null ? null : json["is_favourite"],
// //     food: json["food"] == null ? null : json["food"],
// //     user: json["user"] == null ? null : json["user"],
// //   );
// //
// //   Map<String, dynamic> toJson() => {
// //     "id": id == null ? null : id,
// //     "is_liked": isLiked == null ? null : isLiked,
// //     "is_favourite": isFavourite == null ? null : isFavourite,
// //     "food": food == null ? null : food,
// //     "user": user == null ? null : user,
// //   };
// // }
// //
//
// // To parse this JSON data, do
// //
// //     final updateFavouriteResponseModel = updateFavouriteResponseModelFromJson(jsonString);
//
// import 'dart:convert';
//
// UpdateFavouriteResponseModel updateFavouriteResponseModelFromJson(String str) => UpdateFavouriteResponseModel.fromJson(json.decode(str));
//
// String updateFavouriteResponseModelToJson(UpdateFavouriteResponseModel data) => json.encode(data.toJson());
//
// class UpdateFavouriteResponseModel {
//   UpdateFavouriteResponseModel({
//     this.statusCode,
//     this.message,
//     this.data,
//   });
//
//   final int statusCode;
//   final String message;
//   final Data data;
//
//   factory UpdateFavouriteResponseModel.fromJson(Map<String, dynamic> json) => UpdateFavouriteResponseModel(
//     statusCode: json["statusCode"] == null ? null : json["statusCode"],
//     message: json["message"] == null ? null : json["message"],
//     data: json["data"] == null ? null : Data.fromJson(json["data"]),
//   );
//
//   Map<String, dynamic> toJson() => {
//     "statusCode": statusCode == null ? null : statusCode,
//     "message": message == null ? null : message,
//     "data": data == null ? null : data.toJson(),
//   };
// }
//
// class Data {
//   Data({
//     this.id,
//     this.isLiked,
//     this.isFavourite,
//     this.mustVisit,
//     this.food,
//     this.user,
//   });
//
//   final int id;
//   final bool isLiked;
//   final bool isFavourite;
//   final bool mustVisit;
//   final int food;
//   final int user;
//
//   factory Data.fromJson(Map<String, dynamic> json) => Data(
//     id: json["id"] == null ? null : json["id"],
//     isLiked: json["is_liked"] == null ? null : json["is_liked"],
//     isFavourite: json["is_favourite"] == null ? null : json["is_favourite"],
//     mustVisit: json["must_visit"] == null ? null : json["must_visit"],
//     food: json["food"] == null ? null : json["food"],
//     user: json["user"] == null ? null : json["user"],
//   );
//
//   Map<String, dynamic> toJson() => {
//     "id": id == null ? null : id,
//     "is_liked": isLiked == null ? null : isLiked,
//     "is_favourite": isFavourite == null ? null : isFavourite,
//     "must_visit": mustVisit == null ? null : mustVisit,
//     "food": food == null ? null : food,
//     "user": user == null ? null : user,
//   };
// }


// To parse this JSON data, do
//
//     final updateMustVisitResponseModel = updateMustVisitResponseModelFromJson(jsonString);

import 'dart:convert';

UpdateMustVisitResponseModel updateMustVisitResponseModelFromJson(String str) => UpdateMustVisitResponseModel.fromJson(json.decode(str));

String updateMustVisitResponseModelToJson(UpdateMustVisitResponseModel data) => json.encode(data.toJson());

class UpdateMustVisitResponseModel {
  UpdateMustVisitResponseModel({
    this.status,
    this.message,
  });

  final int status;
  final String message;

  factory UpdateMustVisitResponseModel.fromJson(Map<String, dynamic> json) => UpdateMustVisitResponseModel(
    status: json["status"] == null ? null : json["status"],
    message: json["message"] == null ? null : json["message"],
  );

  Map<String, dynamic> toJson() => {
    "status": status == null ? null : status,
    "message": message == null ? null : message,
  };
}
