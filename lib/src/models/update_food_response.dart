// To parse this JSON data, do
//
//     final updateFoodResponse = updateFoodResponseFromJson(jsonString);

import 'dart:convert';

UpdateFoodResponse updateFoodResponseFromJson(String str) =>
    UpdateFoodResponse.fromJson(json.decode(str));

String updateFoodResponseToJson(UpdateFoodResponse data) =>
    json.encode(data.toJson());

class UpdateFoodResponse {
  UpdateFoodResponse({
    this.id,
    this.likesCount,
    this.dislikesCount,
    this.rating,
    this.images,
    this.foodTypeName,
    this.name,
    this.description,
    this.restaurant,
    this.place,
    this.image1,
    this.image2,
    this.image3,
    this.image4,
    this.image5,
    this.lat,
    this.lon,
    this.googleMapUrl,
    this.isDeleted,
    this.ingredients,
    this.preparationMethod,
    this.price,
    this.foodType,
  });

  final int id;
  final int likesCount;
  final int dislikesCount;
  final dynamic rating;
  final List<dynamic> images;
  final String foodTypeName;
  final String name;
  final String description;
  final String restaurant;
  final String place;
  final dynamic image1;
  final dynamic image2;
  final dynamic image3;
  final dynamic image4;
  final dynamic image5;
  final String lat;
  final String lon;
  final String googleMapUrl;
  final bool isDeleted;
  final List<String> ingredients;
  final String preparationMethod;
  final String price;
  final int foodType;

  factory UpdateFoodResponse.fromJson(Map<String, dynamic> json) =>
      UpdateFoodResponse(
        id: json["id"] == null ? null : json["id"],
        likesCount: json["likes_count"] == null ? null : json["likes_count"],
        dislikesCount:
            json["dislikes_count"] == null ? null : json["dislikes_count"],
        rating: json["rating"],
        images: json["images"] == null
            ? null
            : List<dynamic>.from(json["images"].map((x) => x)),
        foodTypeName:
            json["food_type_name"] == null ? null : json["food_type_name"],
        name: json["name"] == null ? null : json["name"],
        description: json["description"] == null ? null : json["description"],
        restaurant: json["restaurant"] == null ? null : json["restaurant"],
        place: json["place"] == null ? null : json["place"],
        image1: json["image1"],
        image2: json["image2"],
        image3: json["image3"],
        image4: json["image4"],
        image5: json["image5"],
        lat: json["lat"] == null ? null : json["lat"],
        lon: json["lon"] == null ? null : json["lon"],
        googleMapUrl:
            json["google_map_url"] == null ? null : json["google_map_url"],
        isDeleted: json["is_deleted"] == null ? null : json["is_deleted"],
        ingredients: json["ingredients"] == null
            ? null
            : List<String>.from(json["ingredients"].map((x) => x)),
        preparationMethod: json["preparation_method"] == null
            ? null
            : json["preparation_method"],
        price: json["price"] == null ? null : json["price"],
        foodType: json["food_type"] == null ? null : json["food_type"],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "likes_count": likesCount == null ? null : likesCount,
        "dislikes_count": dislikesCount == null ? null : dislikesCount,
        "rating": rating,
        "images":
            images == null ? null : List<dynamic>.from(images.map((x) => x)),
        "food_type_name": foodTypeName == null ? null : foodTypeName,
        "name": name == null ? null : name,
        "description": description == null ? null : description,
        "restaurant": restaurant == null ? null : restaurant,
        "place": place == null ? null : place,
        "image1": image1,
        "image2": image2,
        "image3": image3,
        "image4": image4,
        "image5": image5,
        "lat": lat == null ? null : lat,
        "lon": lon == null ? null : lon,
        "google_map_url": googleMapUrl == null ? null : googleMapUrl,
        "is_deleted": isDeleted == null ? null : isDeleted,
        "ingredients": ingredients == null
            ? null
            : List<dynamic>.from(ingredients.map((x) => x)),
        "preparation_method":
            preparationMethod == null ? null : preparationMethod,
        "price": price == null ? null : price,
        "food_type": foodType == null ? null : foodType,
      };
}
