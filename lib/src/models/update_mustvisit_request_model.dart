// // // // To parse this JSON data, do
// // // //
// // // //     final updateFavouriteRequestModel = updateFavouriteRequestModelFromJson(jsonString);
// // //
// // // import 'dart:convert';
// // //
// // // UpdateFavouriteRequestModel updateFavouriteRequestModelFromJson(String str) =>
// // //     UpdateFavouriteRequestModel.fromJson(json.decode(str));
// // //
// // // String updateFavouriteRequestModelToJson(UpdateFavouriteRequestModel data) =>
// // //     json.encode(data.toJson());
// // //
// // // class UpdateFavouriteRequestModel {
// // //   UpdateFavouriteRequestModel({
// // //     this.isFavourite,
// // //     this.food,
// // //     this.user,
// // //   });
// // //
// // //   final bool isFavourite;
// // //   final int food;
// // //   final int user;
// // //
// // //   factory UpdateFavouriteRequestModel.fromJson(Map<String, dynamic> json) =>
// // //       UpdateFavouriteRequestModel(
// // //         isFavourite: json["is_favourite"] == null ? null : json["is_favourite"],
// // //         food: json["food"] == null ? null : json["food"],
// // //         user: json["user"] == null ? null : json["user"],
// // //       );
// // //
// // //   Map<String, dynamic> toJson() => {
// // //         "is_favourite": isFavourite == null ? null : isFavourite,
// // //         "food": food == null ? null : food,
// // //         "user": user == null ? null : user,
// // //       };
// // // }
// // // To parse this JSON data, do
// // //
// // //     final updateFavouriteRequestModel = updateFavouriteRequestModelFromJson(jsonString);
// //
// // import 'dart:convert';
// //
// // UpdateFavouriteRequestModel updateFavouriteRequestModelFromJson(String str) => UpdateFavouriteRequestModel.fromJson(json.decode(str));
// //
// // String updateFavouriteRequestModelToJson(UpdateFavouriteRequestModel data) => json.encode(data.toJson());
// //
// // class UpdateFavouriteRequestModel {
// //   UpdateFavouriteRequestModel({
// //     this.food,
// //     this.user,
// //     this.isFavourite,
// //   });
// //
// //   final String food;
// //   final String user;
// //   final bool isFavourite;
// //
// //   factory UpdateFavouriteRequestModel.fromJson(Map<String, dynamic> json) => UpdateFavouriteRequestModel(
// //     food: json["food"] == null ? null : json["food"],
// //     user: json["user"] == null ? null : json["user"],
// //     isFavourite: json["is_favourite"] == null ? null : json["is_favourite"],
// //   );
// //
// //   Map<String, dynamic> toJson() => {
// //     "food": food == null ? null : food,
// //     "user": user == null ? null : user,
// //     "is_favourite": isFavourite == null ? null : isFavourite,
// //   };
// // }
//
//
// // To parse this JSON data, do
// //
// //     final updateFavouriteRequestModel = updateFavouriteRequestModelFromJson(jsonString);
//
// import 'dart:convert';
//
// UpdateFavouriteRequestModel updateFavouriteRequestModelFromJson(String str) => UpdateFavouriteRequestModel.fromJson(json.decode(str));
//
// String updateFavouriteRequestModelToJson(UpdateFavouriteRequestModel data) => json.encode(data.toJson());
//
// class UpdateFavouriteRequestModel {
//   UpdateFavouriteRequestModel({
//     this.isLiked,
//     this.isFavourite,
//     this.mustVisit,
//     this.food,
//     this.user,
//   });
//
//   final bool isLiked;
//   final bool isFavourite;
//   final bool mustVisit;
//   final int food;
//   final int user;
//
//   factory UpdateFavouriteRequestModel.fromJson(Map<String, dynamic> json) => UpdateFavouriteRequestModel(
//     isLiked: json["is_liked"] == null ? null : json["is_liked"],
//     isFavourite: json["is_favourite"] == null ? null : json["is_favourite"],
//     mustVisit: json["must_visit"] == null ? null : json["must_visit"],
//     food: json["food"] == null ? null : json["food"],
//     user: json["user"] == null ? null : json["user"],
//   );
//
//   Map<String, dynamic> toJson() => {
//     "is_liked": isLiked == null ? null : isLiked,
//     "is_favourite": isFavourite == null ? null : isFavourite,
//     "must_visit": mustVisit == null ? null : mustVisit,
//     "food": food == null ? null : food,
//     "user": user == null ? null : user,
//   };
// }


// To parse this JSON data, do
//
//     final updateMustVisitRequestModel = updateMustVisitRequestModelFromJson(jsonString);

import 'dart:convert';

UpdateMustVisitRequestModel updateMustVisitRequestModelFromJson(String str) => UpdateMustVisitRequestModel.fromJson(json.decode(str));

String updateMustVisitRequestModelToJson(UpdateMustVisitRequestModel data) => json.encode(data.toJson());

class UpdateMustVisitRequestModel {
  UpdateMustVisitRequestModel({
    this.mustVisit,
    this.food,
  });

  final bool mustVisit;
  final int food;

  factory UpdateMustVisitRequestModel.fromJson(Map<String, dynamic> json) => UpdateMustVisitRequestModel(
    mustVisit: json["must_visit"] == null ? null : json["must_visit"],
    food: json["food"] == null ? null : json["food"],
  );

  Map<String, dynamic> toJson() => {
    "must_visit": mustVisit == null ? null : mustVisit,
    "food": food == null ? null : food,
  };
}
