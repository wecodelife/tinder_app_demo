// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'home_page_hive_model.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class HomePage extends TypeAdapter<HomePageHiveModel> {
  @override
  final int typeId = 1;

  @override
  HomePageHiveModel read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return HomePageHiveModel();
  }

  @override
  void write(BinaryWriter writer, HomePageHiveModel obj) {
    writer..writeByte(0);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is HomePage &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
