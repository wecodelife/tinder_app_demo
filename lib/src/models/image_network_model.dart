import 'package:flutter/cupertino.dart';

class ImageNetworkModel {
  ImageNetworkModel({
    this.images,
  });

  List<Image> images;
}
