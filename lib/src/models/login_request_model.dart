// // To parse this JSON data, do
// //
// //     final loginRequest = loginRequestFromJson(jsonString);
//
// import 'dart:convert';
//
// LoginRequest loginRequestFromJson(String str) => LoginRequest.fromJson(json.decode(str));
//
// String loginRequestToJson(LoginRequest data) => json.encode(data.toJson());
//
// class LoginRequest {
//     LoginRequest({
//         this.email,
//         this.password,
//     });
//
//     String email;
//     String password;
//
//     factory LoginRequest.fromJson(Map<String, dynamic> json) => LoginRequest(
//         email: json["email"],
//         password: json["password"],
//     );
//
//     Map<String, dynamic> toJson() => {
//         "email": email,
//         "password": password,
//     };
// }



// To parse this JSON data, do
//
//     final loginRequest = loginRequestFromJson(jsonString);

import 'dart:convert';

LoginRequest loginRequestFromJson(String str) => LoginRequest.fromJson(json.decode(str));

String loginRequestToJson(LoginRequest data) => json.encode(data.toJson());

class LoginRequest {
    LoginRequest({
        this.uid,
    });

    final String uid;

    factory LoginRequest.fromJson(Map<String, dynamic> json) => LoginRequest(
        uid: json["uid"] == null ? null : json["uid"],
    );

    Map<String, dynamic> toJson() => {
        "uid": uid == null ? null : uid,
    };
}
