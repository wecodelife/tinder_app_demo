// To parse this JSON data, do
//
//     final registerUserRequest = registerUserRequestFromJson(jsonString);

import 'dart:convert';

import 'dart:io';

RegisterUserRequest registerUserRequestFromJson(String str) => RegisterUserRequest.fromJson(json.decode(str));

String registerUserRequestToJson(RegisterUserRequest data) => json.encode(data.toJson());

class RegisterUserRequest {
    RegisterUserRequest({
        this.firstName,
        this.profileImage,
        this.phoneNumber,
        this.isDeleted,
    });

    final String firstName;
    final File profileImage;
    final String phoneNumber;
    final bool isDeleted;

    factory RegisterUserRequest.fromJson(Map<String, dynamic> json) => RegisterUserRequest(
        firstName: json["first_name"] == null ? null : json["first_name"],
        profileImage: json["profile_image"] == null ? null : json["profile_image"],
        phoneNumber: json["phone_number"] == null ? null : json["phone_number"],
        isDeleted: json["is_deleted"] == null ? null : json["is_deleted"],
    );

    Map<String, dynamic> toJson() => {
        "first_name": firstName == null ? null : firstName,
        "profile_image": profileImage == null ? null : profileImage,
        "phone_number": phoneNumber == null ? null : phoneNumber,
        "is_deleted": isDeleted == null ? null : isDeleted,
    };
}
