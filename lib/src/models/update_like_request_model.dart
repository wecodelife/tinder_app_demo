// To parse this JSON data, do
//
//     final updateLikeRequestModel = updateLikeRequestModelFromJson(jsonString);
//
// import 'dart:convert';
//
// UpdateLikeRequestModel updateLikeRequestModelFromJson(String str) =>
//     UpdateLikeRequestModel.fromJson(json.decode(str));
//
// String updateLikeRequestModelToJson(UpdateLikeRequestModel data) =>
//     json.encode(data.toJson());
//
// class UpdateLikeRequestModel {
//   UpdateLikeRequestModel({
//     this.isLiked,
//     this.food,
//     this.user,
//   });
//
//   final bool isLiked;
//   final int food;
//   final int user;
//
//   factory UpdateLikeRequestModel.fromJson(Map<String, dynamic> json) =>
//       UpdateLikeRequestModel(
//         isLiked: json["is_liked"] == null ? null : json["is_liked"],
//         food: json["food"] == null ? null : json["food"],
//         user: json["user"] == null ? null : json["user"],
//       );
//
//   Map<String, dynamic> toJson() => {
//         "is_liked": isLiked == null ? null : isLiked,
//         "food": food == null ? null : food,
//         "user": user == null ? null : user,
//       };
// }


// To parse this JSON data, do
//
//     final updateLikeRequestModel = updateLikeRequestModelFromJson(jsonString);

import 'dart:convert';

UpdateLikeRequestModel updateLikeRequestModelFromJson(String str) => UpdateLikeRequestModel.fromJson(json.decode(str));

String updateLikeRequestModelToJson(UpdateLikeRequestModel data) => json.encode(data.toJson());

class UpdateLikeRequestModel {
  UpdateLikeRequestModel({
    this.isLiked,
    this.food,
  });

  final bool isLiked;
  final int food;

  factory UpdateLikeRequestModel.fromJson(Map<String, dynamic> json) => UpdateLikeRequestModel(
    isLiked: json["is_liked"] == null ? null : json["is_liked"],
    food: json["food"] == null ? null : json["food"],
  );

  Map<String, dynamic> toJson() => {
    "is_liked": isLiked == null ? null : isLiked,
    "food": food == null ? null : food,
  };
}
