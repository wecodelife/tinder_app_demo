// To parse this JSON data, do
//
//     final getReviewsResponseModel = getReviewsResponseModelFromMap(jsonString);
//
// import 'dart:convert';
//
//   getReviewsResponseModelFromMap(String str) =>
//     GetReviewsResponseModel.fromMap(json.decode(str));
//
// String getReviewsResponseModelToMap(GetReviewsResponseModel data) =>
//     json.encode(data.toMap());
//
// class GetReviewsResponseModel {
//   GetReviewsResponseModel({
//     this.count,
//     this.next,
//     this.previous,
//     this.results,
//     this.statusCode,
//     this.message,
//   });
//
//   final int count;
//   final dynamic next;
//   final dynamic previous;
//   final List<Result> results;
//   final int statusCode;
//   final String message;
//
//   factory GetReviewsResponseModel.fromMap(Map<String, dynamic> json) =>
//       GetReviewsResponseModel(
//         count: json["count"] == null ? null : json["count"],
//         next: json["next"],
//         previous: json["previous"],
//         results: json["results"] == null
//             ? null
//             : List<Result>.from(json["results"].map((x) => Result.fromMap(x))),
//         statusCode: json["status_code"] == null ? null : json["status_code"],
//         message: json["message"] == null ? null : json["message"],
//       );
//
//   Map<String, dynamic> toMap() => {
//         "count": count == null ? null : count,
//         "next": next,
//         "previous": previous,
//         "results": results == null
//             ? null
//             : List<dynamic>.from(results.map((x) => x.toMap())),
//         "status_code": statusCode == null ? null : statusCode,
//         "message": message == null ? null : message,
//       };
// }
//
// class Result {
//   Result({
//     this.id,
//     this.userName,
//     this.rating,
//     this.review,
//     this.createdAt,
//     this.food,
//     this.user,
//   });
//
//   final int id;
//   final String userName;
//   final int rating;
//   final String review;
//   final DateTime createdAt;
//   final int food;
//   final int user;
//
//   factory Result.fromMap(Map<String, dynamic> json) => Result(
//         id: json["id"] == null ? null : json["id"],
//         userName: json["user_name"] == null ? null : json["user_name"],
//         rating: json["rating"] == null ? null : json["rating"],
//         review: json["review"] == null ? null : json["review"],
//         createdAt: json["created_at"] == null
//             ? null
//             : DateTime.parse(json["created_at"]),
//         food: json["food"] == null ? null : json["food"],
//         user: json["user"] == null ? null : json["user"],
//       );
//
//   Map<String, dynamic> toMap() => {
//         "id": id == null ? null : id,
//         "user_name": userName == null ? null : userName,
//         "rating": rating == null ? null : rating,
//         "review": review == null ? null : review,
//         "created_at": createdAt == null ? null : createdAt.toIso8601String(),
//         "food": food == null ? null : food,
//         "user": user == null ? null : user,
//       };
// }


// To parse this JSON data, do
//
//     final likedFoodResponse = likedFoodResponseFromJson(jsonString);

// To parse this JSON data, do
//
//     final getReviewsResponseModel = getReviewsResponseModelFromJson(jsonString);
// To parse this JSON data, do
//
//     final getReviewsResponseModel = getReviewsResponseModelFromJson(jsonString);

import 'dart:convert';

GetReviewsResponseModel getReviewsResponseModelFromJson(String str) => GetReviewsResponseModel.fromJson(json.decode(str));

String getReviewsResponseModelToJson(GetReviewsResponseModel data) => json.encode(data.toJson());

class GetReviewsResponseModel {
  GetReviewsResponseModel({
    this.count,
    this.next,
    this.previous,
    this.results,
    this.statusCode,
    this.message,
  });

  final int count;
  final dynamic next;
  final dynamic previous;
  final List<Result> results;
  final int statusCode;
  final String message;

  factory GetReviewsResponseModel.fromJson(Map<String, dynamic> json) => GetReviewsResponseModel(
    count: json["count"] == null ? null : json["count"],
    next: json["next"],
    previous: json["previous"],
    results: json["results"] == null ? null : List<Result>.from(json["results"].map((x) => Result.fromJson(x))),
    statusCode: json["statusCode"] == null ? null : json["statusCode"],
    message: json["message"] == null ? null : json["message"],
  );

  Map<String, dynamic> toJson() => {
    "count": count == null ? null : count,
    "next": next,
    "previous": previous,
    "results": results == null ? null : List<dynamic>.from(results.map((x) => x.toJson())),
    "statusCode": statusCode == null ? null : statusCode,
    "message": message == null ? null : message,
  };
}

class Result {
  Result({
    this.id,
    this.userName,
    this.rating,
    this.review,
    this.createdAt,
    this.food,
    this.user,
  });

  final int id;
  final String userName;
  final int rating;
  final String review;
  final DateTime createdAt;
  final int food;
  final int user;

  factory Result.fromJson(Map<String, dynamic> json) => Result(
    id: json["id"] == null ? null : json["id"],
    userName: json["user_name"] == null ? null : json["user_name"],
    rating: json["rating"] == null ? null : json["rating"],
    review: json["review"] == null ? null : json["review"],
    createdAt: json["created_at"] == null ? null : DateTime.parse(json["created_at"]),
    food: json["food"] == null ? null : json["food"],
    user: json["user"] == null ? null : json["user"],
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "user_name": userName == null ? null : userName,
    "rating": rating == null ? null : rating,
    "review": review == null ? null : review,
    "created_at": createdAt == null ? null : createdAt.toIso8601String(),
    "food": food == null ? null : food,
    "user": user == null ? null : user,
  };
}
