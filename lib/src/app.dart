import 'package:app_template/src/models/home_page_hive_model.dart';
import 'package:app_template/src/screens/home_page.dart';
import 'package:app_template/src/screens/home_page_screen.dart';
import 'package:app_template/src/screens/login_page.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/object_factory.dart';
import 'package:feature_discovery/feature_discovery.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:hive/hive.dart';

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  Widget homeWidget;
  Box<HomePageHiveModel> imageBox;
  bool isLoading = false;

  // checkHiveBoxOpen() {
  //   if (imageBox == null) {
  //     isLoading = false;
  //     setState(() {});
  //     // homeWidget = Center(child: CircularProgressIndicator());
  //
  //     // } else {
  //     //   ObjectFactory().appHive.getToken() == null
  //     //       ? homeWidget = LoginPage()
  //     //       : homeWidget = HomeScreen();
  //     //   setState(() {});
  //     // }
  //   } else {
  //     isLoading = true;
  //     setState(() {});
  //   }
  // }

  @override
  void initState() {
    // openBox();
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(
      SystemUiOverlayStyle(
        statusBarColor: Colors.transparent,
        statusBarBrightness: Brightness.dark,
        statusBarIconBrightness: Brightness.dark,
      ),
    );
    return FeatureDiscovery(
      recordStepsInSharedPreferences: true,
      child: MaterialApp(
          title: 'Flutter Demo',
          debugShowCheckedModeBanner: false,
          home: ObjectFactory().appHive.getToken() == null
              ? LoginPage()
              : HomePageScreen()),
    );
  }
}
