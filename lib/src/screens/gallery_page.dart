import 'package:app_template/src/screens/update_food_details.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/widgets/build_button.dart';
import 'package:app_template/src/widgets/gallery.dart';
import 'package:flutter/material.dart';

class GalleryPage extends StatefulWidget {
  final String restaurantName;
  final int foodType;
  final String description;
  final String title;
  final List<String> recipe;
  final String place;
  final String googleMapUrl;
  final String itemType;
  final String price;
  final String lat;
  final String lon;
  GalleryPage(
      {this.foodType,
      this.googleMapUrl,
      this.place,
      this.description,
      this.title,
      this.itemType,
      this.recipe,
      this.restaurantName,
      this.price,
      this.lat,
      this.lon});
  @override
  _GalleryPageState createState() => _GalleryPageState();
}

class _GalleryPageState extends State<GalleryPage> {
  List<String> imageList = [];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Constants.kitGradients[27],
      appBar: AppBar(
        backgroundColor: Constants.kitGradients[27],
        elevation: 0,
        leading: Container(),
        actions: [
          GestureDetector(
            onTap: () {
              push(
                  context,
                  UpdateFoodDetails(
                    restaurantName: widget.restaurantName,
                    place: widget.place,
                    price: widget.price,
                    googleMapUrl: widget.googleMapUrl,
                    foodType: widget.foodType,
                    description: widget.description,
                    title: widget.title,
                    foodImages: imageList,
                    itemType: widget.itemType,
                    recipe: widget.recipe,
                  ));
            },
            child: Icon(
              Icons.check,
              size: 20,
              color: Constants.kitGradients[28],
            ),
          )
          // AppBarRevolution(title: "Gallery"),
        ],
      ),
      body: Gallery(
        pageValue: "Post",
        title: SizedBox(
          height: screenHeight(context, dividedBy: 80),
        ),
        imageSelected: (val) {
          print("gfgvfghv" + val.toString());
          imageList = val;
          setState(() {});
        },
        footer: Center(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              SizedBox(
                height: screenHeight(context, dividedBy: 80),
              ),
              BuildButton(
                title: "Done",
                onPressed: () {
                  pop(context);
                  // print("sgs" + widget.id.toString());
                  // push(
                  //     context,
                  //     AddNewsFeedPage(
                  //       eventId: widget.eventId,
                  //       postId: widget.postId,
                  //       isEdit: widget.isEdit,
                  //       imageFileList: imageList,
                  //       userName: widget.username,
                  //       profileImage: widget.userPic,
                  //     ));
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}
