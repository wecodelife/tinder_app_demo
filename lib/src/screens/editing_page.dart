import 'dart:io';
import 'dart:typed_data';

// import 'package:app_template/src/screens/add_newsfeed_page.dart';
// import 'package:app_template/src/screens/add_profile_pic_page.dart';
import 'package:app_template/src/screens/edit_profile_page.dart';
import 'package:app_template/src/widgets/crop_service.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/widgets/app_bar.dart';
import 'package:extended_image/extended_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:path_provider/path_provider.dart';

class EditorPage extends StatefulWidget {
File image;
final String userName;
final int id;
final String profileImage;
final String phoneNumber;
final String pageValue;
final bool isFirstTime;

  EditorPage(
      {this.profileImage,this.phoneNumber,this.isFirstTime,this.userName,this.id,this.image,this.pageValue});

  @override
  _EditorPageState createState() => _EditorPageState();
}

class _EditorPageState extends State<EditorPage> {
  final GlobalKey<ExtendedImageEditorState> editorKey =
      GlobalKey<ExtendedImageEditorState>();
  File cropImage;
  List<String> images = [];
  bool editLoad = false;
  Future<File> writeToFile(Uint8List data) async {
    // setState(() {
    //   editLoad = true;
    // });
    String dir = (await getApplicationDocumentsDirectory()).path;
    // print("path" + path.toString());
    // final buffer = data.buffer;
    return File('$dir/${DateTime.now().toIso8601String()}_image.png')
        .writeAsBytes(data);
  }

  Future<void> editLoading() {
    setState(() {
      editLoad = true;
    });
    return null;
  }

  Future<void> cropImageFun() async {
    await editLoading();
    getCropImage();
    return null;
  }

  Future<void> getCropImage() async {
    // // setState(() {
    // editLoad == true;
    // // });

    return null;
  }

  void handleNavigation(String value) {
    switch (value) {
      case 'Profile':
        push(
            context,
            EditProfilePage(
              id: widget.id,
              userName: widget.userName,
              profileImage: widget.profileImage,
              isFirstTime: widget.isFirstTime,
              phoneNumber: widget.phoneNumber,
              image: cropImage,

            ));
        break;
    }
  }

  Future<bool> _willPopCallback() async {
    setState(() {
      cropImage = null;
    });
    handleNavigation(widget.pageValue);
    // await showDialog or Show add banners or whatever
    // then
    return true; // return true if the route to be popped
  }

  @override
  void initState() {
    // if (widget.imageList != null && widget.imageList.isNotEmpty)
    //   images = widget.imageList;
    // cropImage = widget.image;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(
      SystemUiOverlayStyle(
        statusBarColor:Constants.kitGradients[31],
        statusBarBrightness: Brightness.light,
        statusBarIconBrightness: Brightness.dark,
      ),
    );
    return WillPopScope(
      onWillPop: _willPopCallback,
      child: Scaffold(
          appBar: AppBar(
            backgroundColor: Constants.kitGradients[31],
            actions: [
              Stack(
                children: [
                  AppBarFoodApp(
                    title: "Crop",
                    leftIcon: Icon(
                      Icons.arrow_back_ios,
                      color: Constants.kitGradients[28],
                    ),
                    onPressedLeftIcon: () {
                      handleNavigation(widget.pageValue);
                    },
                    rightIcon: Icon(Icons.check,
                            size: 40, color: Constants.kitGradients[28]),
                    onPressedRightIcon: () async{
                      cropImage = await writeToFile(
                          await CropService().cropImageDataWithDartLibrary(
                          state: editorKey.currentState,
                          editLoading: (val) {
                            setState(() {
                              editLoad = val;
                            });
                          }));
                      setState(() {
                      images.add(cropImage.path);
                      editLoad = false;
                      });
                      // print(imageList.toString());
                      // if (cropImage != null) {
                      //   // setState(() {
                      //   images.add(cropImage.path);
                      //   // });
                      handleNavigation(widget.pageValue);
                      // } else {
                      //   showInfoAnimatedToast(
                      //       context: context, msg: "Please crop the image");
                      // }
                    },
                  ),

                  // AppBarRevolution(
                  //   onTapLeftIcon: () {
                  //     // imageList.clear();
                  //     setState(() {
                  //       cropImage = null;
                  //     });
                  //     handleNavigation(widget.pageValue);
                  //   },
                  //   title: "Crop",
                  //   rightIcon: Icon(Icons.check,
                  //       size: 40, color: Constants.kitGradients[30]),
                  //   onTapRightIcon: () async {
                  //     cropImage = await writeToFile(
                  //         await CropService().cropImageDataWithDartLibrary(
                  //             state: editorKey.currentState,
                  //             editLoading: (val) {
                  //               setState(() {
                  //                 editLoad = val;
                  //               });
                  //             }));
                  //     setState(() {
                  //       images.add(cropImage.path);
                  //       editLoad = false;
                  //     });
                  //     // print(imageList.toString());
                  //     // if (cropImage != null) {
                  //     //   // setState(() {
                  //     //   images.add(cropImage.path);
                  //     //   // });
                  //     handleNavigation(widget.pageValue);
                  //     // } else {
                  //     //   showInfoAnimatedToast(
                  //     //       context: context, msg: "Please crop the image");
                  //     // }
                  //   },
                  // ),
                  if (editLoad == true)
                    Container(
                      color: Constants.kitGradients[8].withOpacity(0.8),
                      // height: screenHeight(context),
                      width: screenWidth(context),
                      // child: Center(
                      //   child: SpinKitThreeBounce(
                      //     color: Constants.kitGradients[30],
                      //     // size: 50.0,
                      //     // controller: AnimationController(
                      //     //     duration: const Duration(milliseconds: 1200))
                      //   ),
                      // ),
                    )
                ],
              )
            ],
          ),
          // floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
          // floatingActionButton: editLoad != true
          //     ? FloatingActionButton(
          //         // heroTag: null,
          //         child: cropImage != null ? Icon(Icons.clear) : Icon(Icons.crop),
          //         onPressed: () async {
          //           // setState(() {
          //           //   editLoad = true;
          //           // });
          //           if (cropImage == null) {
          //             cropImage = await writeToFile(
          //                 await CropService().cropImageDataWithDartLibrary(
          //                     state: editorKey.currentState,
          //                     editLoading: (val) {
          //                       setState(() {
          //                         editLoad = val;
          //                       });
          //                     }));
          //
          //             setState(() {
          //               editLoad = false;
          //             });
          //           } else {
          //             setState(() {
          //               cropImage = null;
          //             });
          //           }
          //         },
          //       )
          //     : Container(),
          body: Stack(
            children: [
              Container(
                  color: Constants.kitGradients[29],
                  // height: screenHeight(context, dividedBy: 1),
                  // width: screenWidth(context, dividedBy: 1),
                  child: ExtendedImage.file(
                    cropImage != null ? cropImage : widget.image,
                    // height: screenHeight(context, dividedBy: 1),
                    // width: screenWidth(context, dividedBy: 1),
                    fit: BoxFit.contain,
                    mode: ExtendedImageMode.editor,
                    // enableLoadState: true,
                    extendedImageEditorKey: editorKey,

                    initEditorConfigHandler: (ExtendedImageState state) {
                      return EditorConfig(
                        maxScale: 3,
                        hitTestBehavior: HitTestBehavior.translucent,
                        // maxScale: 8.0,
                        cropRectPadding: const EdgeInsets.all(20.0),
                        hitTestSize: 20.0,
                        // cropLayerPainter: _cropLayerPainter,
                        initCropRectType: InitCropRectType.imageRect,
                        cropAspectRatio: 1,
                      );
                    },
                    // cacheRawData: true,
                  )),
              if (editLoad == true)
                Container(
                  color: Constants.kitGradients[8].withOpacity(0.8),
                  height: screenHeight(context, dividedBy: 1.1),
                  width: screenWidth(context),
                  child: Center(
                    child: SpinKitThreeBounce(
                      color: Constants.kitGradients[28],
                      // size: 50.0,
                      // controller: AnimationController(
                      //     duration: const Duration(milliseconds: 1200))
                    ),
                  ),
                ),
            ],
          )),
    );
  }
}
