import 'package:app_template/src/screens/profile_feed_page.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/widgets/bottom_bar.dart';
import 'package:app_template/src/widgets/user_feed_card.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class SingleImagePage extends StatefulWidget {
  final List<String> userImage;
  final List<String> userCaption;
  final String userName;
  final String userProfile;
  final List<bool> hasCaption;
  final double currentIndex;
  //final List<String> userImage;
  SingleImagePage(
      {this.userImage,
      this.userCaption,
      this.userName,
      this.userProfile,
      this.hasCaption,
      this.currentIndex});
  @override
  _SingleImagePageState createState() => _SingleImagePageState();
}

class _SingleImagePageState extends State<SingleImagePage> {
//  ScrollController _scrollController = new ScrollController(
//     initialScrollOffset: widget.currentIndex,
//     keepScrollOffset: true,
//   );

  @override
  Widget build(BuildContext context) {
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: SystemUiOverlayStyle(
        statusBarColor: Constants.kitGradients[28],
      ),
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: AppBar(
          backgroundColor: Constants.kitGradients[28],
          elevation: 0,
          leading: Icon(
            Icons.lock_outline_rounded,
            color: Constants.kitGradients[27],
          ),
          title: Row(
            children: [
              Text(
                widget.userName,
                style: TextStyle(
                  fontSize: 17,
                  fontWeight: FontWeight.w600,
                  color: Constants.kitGradients[27],
                ),
              ),
              Spacer(),
              Icon(
                Icons.settings_sharp,
                color: Constants.kitGradients[27],
              ),
            ],
          ),
        ),
        body: Container(
          width: screenWidth(context, dividedBy: 1),
          //height: screenWidth(context, dividedBy: 1),
          //color: Colors.cyan,
          child: Stack(
            children: [
              SingleChildScrollView(
                padding: EdgeInsets.only(
                    bottom: MediaQuery.of(context).viewInsets.bottom),
                controller: ScrollController(
                  initialScrollOffset: screenHeight(context, dividedBy: 1.27) *
                      widget.currentIndex,
                  keepScrollOffset: true,
                ),
                child: Container(
                  padding: EdgeInsets.symmetric(
                    horizontal: screenWidth(context, dividedBy: 60),
                  ),
                  color: Constants.kitGradients[31],
                  child: Column(
                    children: [
                      SizedBox(
                        height: screenHeight(context, dividedBy: 40),
                      ),
                      Container(
                        child: ListView.builder(
                          shrinkWrap: true,
                          physics: NeverScrollableScrollPhysics(),
                          itemCount: widget.userImage.length,
                          itemBuilder: (BuildContext context, int index) {
                            return Column(children: [
                              Container(
                                  child: UserFeedCard(
                                      profilePic: widget.userProfile,
                                      name: widget.userName,
                                      feedImage: widget.userImage[index],
                                      caption: widget.userCaption[index],
                                      location: "Delhi, India",
                                      likeCount: "21,000",
                                      postDate: "19 June 2021",
                                      //comment: null,
                                      comment:
                                      "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore.",
                                      commentBy: "ABBd")),
                              // SizedBox(
                              //   height: screenHeight(context, dividedBy: 40),
                              // ),
                            ]);
                          },
                        ),
                      ),
                      SizedBox(
                        height: screenHeight(context, dividedBy: 10),
                      ),
                    ],
                  ),
                ),
              ),
              Positioned(
                bottom: 0,
                child: CustomBottomBar(
                  currentIndex: 0,
                  //onTapSearch: () {},
                  onTapProfile: () {
                    push(
                        context,
                        ProfileFeedPage(
                          name: "Bi Kines",
                          profilePic:
                          'https://images.unsplash.com/photo-1520342868574-5fa3804e551c?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=6ff92caffcdd63681a35134a6770ed3b&auto=format&fit=crop&w=1951&q=80',
                        ));
                  },
                  onTapHome: () {},
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
