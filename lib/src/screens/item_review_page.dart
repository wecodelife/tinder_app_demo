import 'dart:ui';

import 'package:app_template/src/bloc/user_bloc.dart';
import 'package:app_template/src/models/update_review_request_model.dart';
import 'package:app_template/src/screens/food_information_page.dart';
import 'package:app_template/src/screens/home_page.dart';
import 'package:app_template/src/screens/home_page_screen.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/object_factory.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/widgets/build_button.dart';
import 'package:app_template/src/widgets/slide_transition_widget.dart';
import 'package:app_template/src/widgets/star_rating_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_styled_toast/flutter_styled_toast.dart';

class ItemReviewPage extends StatefulWidget {
  int foodId;
  int userId;
  ItemReviewPage({this.foodId, this.userId});
  //const ItemReviewPage({Key? key}) : super(key: key);

  @override
  _ItemReviewPageState createState() => _ItemReviewPageState();
}

class _ItemReviewPageState extends State<ItemReviewPage> {
  TextEditingController reviewTextEditingController =
      new TextEditingController();
  UserBloc userBloc = UserBloc();
  double rating = 0;
  bool isLoading = false;
  void submitReview() {
    cancelAlertBox(
        context: context,
        button1Name: "Yes",
        button2Name: "No",
        msg: "Do you wish to Submit the Review?",
        titlePadding: 3,
        text2: "",
        text1: "",
        contentPadding: 20,
        insetPadding: 2.7,
        onPressedNo: () {
          pop(context);
          setState(() {});
        },
        onPressedYes: () {
          isLoading = true;
          setState(() {});
          pop(context);

          userBloc.updateReview(
              updateReviewsRequestModel: UpdateReviewsRequestModel(
                user: int.parse(ObjectFactory().appHive.getUserId()),
            food: widget.foodId,
            rating: rating.floor(),
            review: reviewTextEditingController.text,
           // userName: ObjectFactory().appHive.getName(),
          ));
        });
  }

  @override
  void initState() {
    userBloc.updateReviewResponse.listen((event) {
      isLoading = false;

      if (event.statusCode == 201) {
        showToastCommon("Review Submitted Successfully");
        Navigator.push(
            context,
            SlideRightRoute(
                page: FoodInformationPage(
              navigationWidget: HomePageScreen(),
              foodId: widget.foodId,
            )));
      } else {
        showToastCommon(
          "Review Submission Failed",
        );
      }
    }).onError((event) {
      setState(() {
        isLoading = false;
        showToastError(event, context);
      });
    });
    print(rating);
    print(reviewTextEditingController.text);
    reviewTextEditingController.clear();
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Constants.kitGradients[31],
        appBar: AppBar(
          backgroundColor: Constants.kitGradients[31],
          leading: GestureDetector(
            onTap: () {
              pop(context);
            },
            child: Icon(Icons.arrow_back, color: Constants.kitGradients[28]),
          ),
          title: Text(
            "Review Product",
            style: TextStyle(
              color: Constants.kitGradients[28],
              fontWeight: FontWeight.w500,
              fontSize: 20,
            ),
          ),
        ),
        body: SingleChildScrollView(
            child: Container(
                width: screenWidth(context, dividedBy: 1),
                padding: EdgeInsets.symmetric(
                  horizontal: screenWidth(context, dividedBy: 25),
                  vertical: screenHeight(context, dividedBy: 60),
                ),
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(
                        height: screenHeight(context, dividedBy: 30),
                      ),
                      Center(
                        child: Text(
                          "Rate your Review",
                          style: TextStyle(
                            color: Constants.kitGradients[28],
                            fontWeight: FontWeight.w500,
                            fontSize: 20,
                          ),
                        ),
                      ),
                      SizedBox(
                        height: screenHeight(context, dividedBy: 30),
                      ),
                      Center(
                        child: StarRatingBar(
                          rating: rating,
                          onRatingChanged: (rating) =>
                              setState(() => this.rating = rating),
                        ),
                      ),
                      SizedBox(
                        height: screenHeight(context, dividedBy: 20),
                      ),
                      Text(
                        "Write your Review",
                        style: TextStyle(
                          color: Constants.kitGradients[28],
                          fontWeight: FontWeight.w500,
                          fontSize: 17,
                        ),
                      ),
                      SizedBox(
                        height: screenHeight(context, dividedBy: 30),
                      ),
                      Container(
                        child: TextField(
                          controller: reviewTextEditingController,
                          cursorColor: Constants.kitGradients[19],
                          keyboardType: TextInputType.multiline,
                          maxLines: 6,
                          obscureText: false,
                          style: TextStyle(
                            fontSize: 15,
                            fontWeight: FontWeight.w400,
                            color: Constants.kitGradients[4],
                            fontFamily: "Prompt-Light",
                          ),
                          //readOnly: widget.readOnly,
                          //onChanged:(){},
                          decoration: InputDecoration(
                            contentPadding: EdgeInsets.symmetric(
                                horizontal: screenWidth(context, dividedBy: 40),
                                vertical: screenHeight(context, dividedBy: 80)),
                            hintText: "Type something",
                            hintStyle: TextStyle(
                              fontSize: 15,
                              fontWeight: FontWeight.w400,
                              color:
                                  Constants.kitGradients[19].withOpacity(0.5),
                              fontFamily: "Prompt-Light",
                            ),
                            fillColor: Colors.white,
                            focusedBorder: OutlineInputBorder(
                              borderSide: BorderSide(
                                  color: Constants.kitGradients[28]
                                      .withOpacity(0.5),
                                  width: 0.0),
                              borderRadius: BorderRadius.circular(8.0),
                            ),
                            enabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(
                                  color: Constants.kitGradients[28]
                                      .withOpacity(0.5),
                                  width: 0.0),
                              borderRadius: BorderRadius.circular(8.0),
                            ),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: screenHeight(context, dividedBy: 30),
                      ),
                      Container(
                        width: screenWidth(context, dividedBy: 1),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            BuildButton(
                              title: "Submit",
                              isLoading: isLoading,
                              onPressed: () {
                                if (rating != null &&
                                    reviewTextEditingController.text != null)
                                  submitReview();
                                else {
                                  showToast("Please fill all the details");
                                }
                              },
                            ),
                          ],
                        ),
                      ),
                    ]))));
  }
}
