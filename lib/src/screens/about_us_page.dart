
import 'package:app_template/src/screens/profile_page.dart';
import 'package:app_template/src/screens/terms_and_conditions_page.dart';
import 'package:app_template/src/widgets/app_bar.dart';
import 'package:app_template/src/widgets/creator_name.dart';
import 'package:app_template/src/widgets/list_tile.dart';
import 'package:flutter/material.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:flutter/services.dart';

class AboutUsPage extends StatefulWidget {
  //const AboutUsPage({Key? key}) : super(key: key);

  @override
  _AboutUsPageState createState() => _AboutUsPageState();
}

class _AboutUsPageState extends State<AboutUsPage> {

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(
      SystemUiOverlayStyle(
        statusBarColor:  Constants.kitGradients[31],
        statusBarBrightness: Brightness.dark,
        statusBarIconBrightness: Brightness.dark,
      ),
    );
    return WillPopScope(
      onWillPop: () {
        Navigator.of(context).pushAndRemoveUntil(
            MaterialPageRoute(builder: (context) => ProfilePage()),
                (Route<dynamic> route) => false);
      },
      child: SafeArea(
        child: Scaffold(
          backgroundColor: Constants.kitGradients[31],
          appBar: AppBar(
            backgroundColor: Constants.kitGradients[31],
            elevation: 0,
            leading: Container(),
            actions: [
              AppBarFoodApp(
                title: "About Us",
                leftIcon: Icon(
                  Icons.arrow_back_ios,
                  color: Constants.kitGradients[28],
                ),
                onPressedLeftIcon: () {pop(context);},
              ),
            ],
          ),
          body: Container(
              width: screenWidth(context, dividedBy: 1),
              height: screenHeight(context, dividedBy: 1),
              decoration: BoxDecoration(
                color: Constants.kitGradients[31],
              ),
              padding: EdgeInsets.symmetric(
                horizontal: screenWidth(context, dividedBy: 20),
              ),
              child: Column(
                children: [
                  SizedBox(
                    height: screenHeight(context, dividedBy: 70),
                  ),
                  Text(
                    "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore",
                    textAlign: TextAlign.justify,
                    style: TextStyle(
                      fontSize: 15,
                      fontFamily: "Prompt-Light",
                      color: Constants.kitGradients[19],
                    ),
                  ),
                  SizedBox(
                    height: screenHeight(context, dividedBy: 30),
                  ),
                  // AppListTile(
                  //   text: "Data Policy",
                  //   onTap: (){
                  //     push(context, TermsAndConditions());
                  //   },
                  // ),
                  AppListTile(
                    text: "Terms and Conditions",
                    onTap: (){
                      push(context, TermsAndConditions());
                    },
                  ),
                  // AppListTile(
                  //   text: "Help Center",
                  //   onTap: (){
                  //     push(context, TermsAndConditions());
                  //   },
                  // ),
                  Spacer(),
                  CreatorName(),
                  SizedBox(
                    height: screenHeight(context, dividedBy: 60),
                  ),
                ],
              )),
        ),
      ),
    );
  }
}
