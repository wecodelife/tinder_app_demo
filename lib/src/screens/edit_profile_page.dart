import 'dart:io';
import 'dart:ui';

import 'package:app_template/src/bloc/user_bloc.dart';
import 'package:app_template/src/models/register_user_request.dart';
import 'package:app_template/src/screens/editing_page.dart';
import 'package:app_template/src/screens/home_page.dart';
import 'package:app_template/src/screens/home_page_screen.dart';
import 'package:app_template/src/screens/otp_page.dart';
import 'package:app_template/src/screens/profile_page.dart';
import 'package:app_template/src/utils/object_factory.dart';
import 'package:app_template/src/widgets/app_bar.dart';
import 'package:app_template/src/widgets/build_button.dart';
import 'package:app_template/src/widgets/user_form_field.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/widgets/bottom_sheet_item.dart';
import 'package:flutter/services.dart';
import 'package:image_picker/image_picker.dart';

class EditProfilePage extends StatefulWidget {
  // const EditProfilePage({Key? key}) : super(key: key);
  final String userName;
  final int id;
  final String profileImage;
  final String phoneNumber;
  final bool isFirstTime;
  final File image;
  EditProfilePage({this.userName, this.id, this.profileImage, this.phoneNumber,this.isFirstTime,this.image});

  @override
  _EditProfilePageState createState() => _EditProfilePageState();
}

class _EditProfilePageState extends State<EditProfilePage> {
  TextEditingController userNameTextEditingController = TextEditingController();
  TextEditingController phoneNumberTextEditingController =
      TextEditingController();
  bool loading = false;
  File _image;
  final picker = ImagePicker();
  bool imageSelected = false;
  String id;
  UserBloc userBloc = UserBloc();
  imgFromCamera() async {
    final pickedFile = await ImagePicker. pickImage(
        source: ImageSource.camera, imageQuality: 50);
    setState(() {
      if (pickedFile != null) {
        setState(() {
          _image = File(pickedFile.path);
        });
        push(context, EditorPage(image: _image,phoneNumber: phoneNumberTextEditingController.text,isFirstTime: widget.isFirstTime,id: widget.id,userName: userNameTextEditingController.text,pageValue: "Profile",));
      } else {
        print('No image selected.');
      }
    });
  }

  imgFromGallery() async {
    final pickedFile = await ImagePicker.pickImage(
        source: ImageSource.gallery, imageQuality: 50);

    setState(() {
      if (pickedFile != null) {
        _image = File(pickedFile.path);
        push(context, EditorPage(image: _image,phoneNumber: phoneNumberTextEditingController.text,isFirstTime: widget.isFirstTime,id: widget.id,userName: userNameTextEditingController.text,pageValue: "Profile",));

      } else {
        print('No image selected.');
      }
    });
  }

  int selected;
  showPicker() {
    showModalBottomSheet(
        context: context,
        backgroundColor: Constants.kitGradients[31],
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(8), topRight: Radius.circular(8))),
        builder: (BuildContext context) {
          return Container(
            // height:screenHeight(context, dividedBy:4),
            padding: EdgeInsets.symmetric(
              horizontal: screenWidth(context, dividedBy: 30),
              vertical: screenHeight(context, dividedBy: 30),
            ),
            decoration: BoxDecoration(
                color: Constants.kitGradients[31],
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(8), topRight: Radius.circular(8))),
            child: Wrap(
              //crossAxisAlignment:CrossAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.symmetric(
                    horizontal: screenWidth(context, dividedBy: 20),
                    //vertical: screenHeight(context, dividedBy: 20),
                  ),
                  child: Text(
                    "Select Picture from",
                    style: TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.bold,
                      fontFamily: "JosefinSans-Light",
                      color: Constants.kitGradients[28],
                    ),
                  ),
                ),
                SizedBox(
                  height: screenHeight(context, dividedBy: 15),
                ),
                BottomSheetItem(
                  title: "Photo Library",
                  leading: new Icon(
                    Icons.photo_library,
                    color: Constants.kitGradients[28],
                  ),
                  trailing: null,
                  onTap: () {
                    imgFromGallery();
                    //showToast("msg");
                    Navigator.of(context).pop();
                  },
                ),
                Divider(
                  color: Constants.kitGradients[28].withOpacity(0.2),
                  indent: 10,
                  endIndent: 10,
                ),
                BottomSheetItem(
                  title: "Camera",
                  leading: new Icon(
                    Icons.photo_camera,
                    color: Constants.kitGradients[28],
                  ),
                  trailing: null,
                  onTap: () {
                    imgFromCamera();
                    Navigator.of(context).pop();
                  },
                ),
                Divider(
                  color: Constants.kitGradients[28].withOpacity(0.2),
                  indent: 10,
                  endIndent: 10,
                ),
              ],
            ),
          );
        });
  }

  @override
  void initState() {
    // TODO: implement initStat
    print("first time"+widget.isFirstTime.toString());
    // if(widget.isFirstTime!=true) {
      userNameTextEditingController.text = widget.userName;
      phoneNumberTextEditingController.text = widget.phoneNumber;
      _image = widget.image;
    // }
    userBloc.registerUserResponse.listen((event) {
      setState(() {
        loading = false;
      });
      ObjectFactory().appHive.putName(name: event.firstName);
      ObjectFactory().appHive.putProfileImage(profileImage: event.profileImage);
      ObjectFactory().appHive.putPhoneNumber(event.phoneNumber);
      if(widget.isFirstTime!=true)
        push(context, ProfilePage());
      else
        push(context,HomePageScreen());
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // SystemChrome.setSystemUIOverlayStyle(
    //   SystemUiOverlayStyle(
    //     // statusBarColor: Constants.kitGradients[31],
    //     // statusBarBrightness: Brightness.light,
    //     // statusBarIconBrightness: Brightness.dark,
    //     // systemNavigationBarIconBrightness: Brightness.dark,
    //     // systemNavigationBarColor: Constants.kitGradients[31],
    //   ),
    // );
    return WillPopScope(
      onWillPop: () {
        // Navigator.of(context).pushAndRemoveUntil(
        //     MaterialPageRoute(builder: (context) => ProfilePage()),
        //         (Route<dynamic> route) => false);
        pop(context);
      },
      child:AnnotatedRegion<SystemUiOverlayStyle>(
        value: SystemUiOverlayStyle(
          statusBarColor: Constants.kitGradients[31],
          statusBarBrightness: Brightness.dark,
          statusBarIconBrightness: Brightness.dark,
        ),
        child: Scaffold(
          backgroundColor: Constants.kitGradients[31],
          appBar: AppBar(
            backgroundColor: Constants.kitGradients[31],
            elevation: 0,
            leading: Container(),
            actions: [
              AppBarFoodApp(
                title: "Update Profile",
                leftIcon: Icon(
                  Icons.arrow_back_ios,
                  color: Constants.kitGradients[28],
                ),
                onPressedLeftIcon: () {
                  pop(context);
                },
                // rightIcon:Icon(Icons.check,
                //   color: Constants.kitGradients[28],
                // ),
                // onPressedRightIcon:(){
                //   // push(context, ProfilePage(
                //   //   userName: usernameTextEditingController.text,
                //   //   profileImage:
                //   // ));
                // }
              ),
            ],
          ),
          body: SingleChildScrollView(
            child: Container(
              width: screenWidth(context, dividedBy: 1),
              padding: EdgeInsets.symmetric(
                horizontal: screenWidth(context, dividedBy: 30),
              ),
              child:
                  Column(crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        SizedBox(height: screenHeight(context, dividedBy: 30)),
                        GestureDetector(
                  onTap: showPicker,
                  child: Container(
                    width: screenWidth(context, dividedBy: 2.8),
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      //border: Border.all(color: Constants.kitGradients[29], width: 10),
                      color: Constants.kitGradients[28],
                      // image:DecorationImage(
                      //     fit: BoxFit.cover,
                      //     image: _image == null
                      //         ? AssetImage("assets/images/white.png")
                      //     // NetworkImage("https://as2.ftcdn.net/v2/jpg/02/45/13/75/1000_F_245137550_Q9fbN1XemwMrrTXf9njmgUhjS4G7aVgq.jpg")
                      //         : FileImage(_image)
                      // )
                    ),
                    child: widget.isFirstTime == true && _image == null ?
                    CircleAvatar(radius: screenWidth(context, dividedBy: 4),
                      backgroundColor: Constants.kitGradients[28],
                      child: Container(
                        width: screenWidth(context, dividedBy: 4),
                        height: screenWidth(context, dividedBy: 4),
                        decoration: BoxDecoration(
                          color: Constants.kitGradients[28],
                          shape: BoxShape.circle,
                          // image: DecorationImage(
                          //     image:_image == null
                          //         ? AssetImage("assets/images/dummy.png"),
                          //     fit: BoxFit.cover),
                        ),
                        child: Center(child:
                        Icon(Icons.add_a_photo_outlined,
                          size: screenWidth(context,dividedBy: 8),
                          color: Constants.kitGradients[31],),),
                      ),) :CircleAvatar(
                      radius: screenWidth(context, dividedBy: 4),
                      backgroundColor: Constants.kitGradients[28],
                      child: CachedNetworkImage(
                        imageUrl: widget.profileImage == null
                            ? "https://www.cornwallbusinessawards.co.uk/wp-content/uploads/2017/11/dummy450x450.jpg"
                            : widget.profileImage,
                        imageBuilder: (context, imageProvider) => Container(
                          width: screenWidth(context, dividedBy: 3),
                          height: screenWidth(context, dividedBy: 3),
                          decoration: BoxDecoration(
                            color: Constants.kitGradients[29],
                            shape: BoxShape.circle,
                            image: DecorationImage(
                                image:
                                _image == null ?
                                imageProvider
                                    : FileImage(_image),
                                fit: BoxFit.cover),
                          ),
                        ),
                        placeholder: (context, url) => Center(
                          heightFactor: 1,
                          widthFactor: 1,
                          // child:Center(child:
                          // Icon(Icons.add_a_photo_outlined,
                          //   size: screenWidth(context,dividedBy: 8),
                          //   color: Constants.kitGradients[31],),),
                        ),
                        //fit: BoxFit.cover,
                      ),
                    ),
                  ),
                ),
                SizedBox(height: screenHeight(context, dividedBy: 30)),
                FormFeildUserDetails(
                  labelText: "UserName",
                  textEditingController: userNameTextEditingController,
                  //otp: false,
                ),
                SizedBox(
                  height: screenHeight(context, dividedBy: 20),
                ),
                FormFeildUserDetails(
                    labelText: "Phone Number",
                    textEditingController: phoneNumberTextEditingController,
                    isNumber: true
                    //otp: false,
                    ),
                SizedBox(
                  height: screenHeight(context, dividedBy: 10),
                ),
                Center(
                  child: BuildButton(
                    title: "Update",
                    isLoading: loading,
                    onPressed: () {
                      // push(context,
                      //     OtpPage(verificationId: "1", phoneNumber: "34567890"));
                      if(widget.isFirstTime!=true || (userNameTextEditingController.text!=''&&phoneNumberTextEditingController.text!=''&&_image!=null)) {
                        setState(() {
                          loading = true;
                        });
                        userBloc.registerUser(
                            id: widget.id.toString(), registerUserRequest: RegisterUserRequest(
                            firstName: userNameTextEditingController.text != ''
                                ? userNameTextEditingController.text
                                : null,
                            phoneNumber: phoneNumberTextEditingController.text != ''
                                ? phoneNumberTextEditingController.text
                                : null,
                            profileImage: _image != null ? _image : null
                        ));
                        showToastSuccess("Profile Updated", context);

                      } else {
                        showToastError("Fill In All Fields!", context);
                      }
                      // push(context, ProfilePage(
                      //     userName: userNameTextEditingController.text,
                      //     profileImage:
                      //   ));
                    },
                  ),
                ),
                SizedBox(
                  height: screenHeight(context, dividedBy: 30),
                ),
              ]),
            ),
          ),
        ),
      ),
    );
  }
}
