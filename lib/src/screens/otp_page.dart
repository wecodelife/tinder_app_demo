import 'package:app_template/src/bloc/user_bloc.dart';
import 'package:app_template/src/models/login_request_model.dart';
import 'package:app_template/src/screens/edit_profile_page.dart';
import 'package:app_template/src/screens/home_page.dart';
import 'package:app_template/src/screens/home_page_screen.dart';
// import 'package:app_template/src/models/update_profile_request.dart';
import 'package:app_template/src/screens/profile_page.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/object_factory.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/widgets/build_button.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:app_template/src/widgets/creator_name.dart';
import 'package:app_template/src/widgets/otp_field.dart';

class OtpPage extends StatefulWidget {
  String verificationId;
  String phoneNumber;
  OtpPage({this.verificationId, this.phoneNumber});
  @override
  _OtpPageState createState() => _OtpPageState();
}

class _OtpPageState extends State<OtpPage> {
  TextEditingController _phoneNumberTextEditingController =
      TextEditingController();
  TextEditingController _otpTextEditingController = new TextEditingController();
  bool isLoading = false;
  UserBloc userBloc = UserBloc();
  @override
  void initState() {
    // TODO: implement initState
    userBloc.loginResponse.listen((event) {
      if (event.status == 200 || event.status == 201) {
        print("User Login Successful");
        print("Token  " + event.data.token);
        print("user  " + event.data.userData.firstName);
        setState(() {
          isLoading = false;
        });
        setState(() {
          isLoading = false;
        });
        ObjectFactory().appHive.putUserId(userId: event.data.userData.id.toString());
        ObjectFactory().appHive.putToken(token: event.data.token);
        ObjectFactory().appHive.putName(name: event.data.userData.firstName);
        ObjectFactory().appHive.putProfileImage(profileImage: event.data.userData.profileImage);
        ObjectFactory().appHive.putPhoneNumber(event.data.userData.phoneNumber);
        // ObjectFactory().appHive.putProfileImage(profileImage: event.data.userData.)
        // print("Done" + ObjectFactory().appHive.getProfileImage());
        if(event.data.userData.profileImage!=null)
          push(context, HomePageScreen());
        else
          push(context, EditProfilePage(phoneNumber: event.data.userData.phoneNumber,id: event.data.userData.id,isFirstTime: true,profileImage: event.data.userData.profileImage,userName: event.data.userData.firstName,));
      }
    });
    // userBloc.updateProfileResponse.listen((event) {
    //   setState(() {
    //     isLoading = false;
    //   });
    //   if (event.status == 200 || event.status == 201) {
    //     ObjectFactory().appHive.putIsNumberVerified(event.isPhoneVerified);
    //     showSuccessAnimatedToast(
    //         context: context, msg: "Phone Number Added Successfully");
    //     pushAndRemoveUntil(context, ProfilePage(), false);
    //   } else {
    //     showErrorAnimatedToast(context: context, msg: "Something went wrong");
    //   }
    // }).onError((event) {
    //   setState(() {
    //     isLoading = false;
    //   });
    //   showErrorAnimatedToast(context: context, msg: event);
    // });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: isLoading == true
          ? Container(
              height: screenHeight(context),
              width: screenWidth(context),
              child: Center(
                child: SpinKitThreeBounce(
                  color: Constants.kitGradients[28],
                  // size: 50.0,
                  // controller: AnimationController(
                  //     duration: const Duration(milliseconds: 1200))
                ),
              ),
            )
          :
      SingleChildScrollView(
              child: Padding(
                padding: EdgeInsets.symmetric(
                    horizontal: screenWidth(context, dividedBy: 16),
                    vertical: screenHeight(context, dividedBy: 4)),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Align(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        "Verify your Phone Number",
                        style: TextStyle(
                          fontSize: 24,
                          fontWeight: FontWeight.bold,
                          color: Constants.kitGradients[30],
                          fontFamily: "Prompt-Light",
                        ),
                      ),
                    ),
                    SizedBox(
                      height: screenHeight(context, dividedBy: 70),
                    ),
                    Align(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        "Please enter the OTP sent on your registered phone number ",
                        //textAlign: TextAlign.center,
                        maxLines: 2,
                        style: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.normal,
                          color: Constants.kitGradients[7],
                          fontFamily: "Prompt-Light",
                        ),
                      ),
                    ),
                    SizedBox(
                      height: screenHeight(context, dividedBy: 20),
                    ),
                    // SizedBox(
                    //   height: screenHeight(context, dividedBy: 30),
                    // ),
                    // FormFieldUserDetails(
                    //     labelText: "OTP",
                    //     hintText: "",
                    //     textEditingController: _otpTextEditingController),
                    OtpFormFeild(
                      otpTap: () {},
                      otpTextEditingController: _otpTextEditingController,
                    ),
                    SizedBox(
                      height: screenHeight(context, dividedBy: 30),
                    ),
                    Text(
                      "( If you don’t see it in your Inbox, it may have been sorted into your Spam folder )",
                      style: TextStyle(
                          color: Constants.kitGradients[19],
                          fontFamily: "Prompt-Light",
                          fontSize: screenWidth(context, dividedBy: 25),
                          fontStyle: FontStyle.italic),
                    ),
                    SizedBox(
                      height: screenHeight(context, dividedBy: 10),
                    ),
                    BuildButton(
                      onPressed: () {
                        if (widget.verificationId != null)
                          verifyOtp(_otpTextEditingController.text,
                              widget.verificationId, widget.phoneNumber);
                      },
                      title: "Verify",
                      isLoading: false,
                      buttonWidth: 3.6,
                    ),
                    SizedBox(
                      height: screenHeight(context, dividedBy: 5),
                    ),
                    Center(child: CreatorName()),
                    SizedBox(
                      height: screenHeight(context, dividedBy: 40),
                    ),
                  ],
                ),
              ),
            ),
    );
  }

  void verifyOtp(String otp, String verificationId, String phoneNumber) {
    setState(() {
      isLoading = true;
    });
    // emit(AuthLoading());
    AuthCredential authCredential = PhoneAuthProvider.credential(
      verificationId: verificationId,
      smsCode: otp,
    );
    FirebaseAuth.instance
        .signInWithCredential(authCredential)
        .then((value) async {
      // userModel.copyWith(phone: phoneNumber);
      // final req = GWhaUsersByPhoneReq((r) => r.vars.phone = phoneNumber);
      // final response = client.request(req);
      // response.listen((event) {
      //   if (event.hasErrors) {
      //     emit(AuthError());
      //   }
      //   if (event.data != null) {
      //     if (event.data.whaUsers != null &&
      //         event.data.whaUsers.isNotEmpty &&
      //         event.data.whaUsers.first.id != null) {
      //       emit(ExistingUser(event.data.whaUsers.first.id));
      //     } else {
      //       emit(NewUser(value));
      //     }
      //   }
      userBloc.login(loginRequest: LoginRequest(uid: value.user.uid));
      // push(context, HomeScreen());
      // });
      // setState(() {
      //   isLoading = false;
      // });
      // userBloc.updateProfile(
      //     id: ObjectFactory().appHive.getId().toString(),
      //     updateProfileRequest: UpdateProfileRequest(isPhoneVerified: true));
    }).catchError((onError) {
      setState(() {
        isLoading = false;
      });
      // showErrorAnimatedToast(context: context, msg: onError.toString());

      print("Error" + onError.toString());
      // showSnackbar(onError.toString());

      // emit(AuthError());
    });
  }

  // void showSnackbar(String message) {
  //   ScaffoldMessenger.of(context).showSnackBar(SnackBar(
  //     backgroundColor: Colors.grey.withOpacity(0.6),
  //     content: Text(
  //       message,
  //       style: TextStyle(color: Colors.black),
  //     ),
  //     duration: const Duration(seconds: 5),
  //   ));
  // }
}
