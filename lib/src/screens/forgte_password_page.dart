import 'dart:async';

import 'package:app_template/src/bloc/user_bloc.dart';
import 'package:app_template/src/models/foreget_password_request.dart';
import 'package:app_template/src/screens/login_page.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/widgets/build_button.dart';
import 'package:app_template/src/widgets/email_form_field.dart';
import 'package:email_validator/email_validator.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ForgetPasswordPage extends StatefulWidget {
  @override
  _ForgetPasswordPageState createState() => _ForgetPasswordPageState();
}

class _ForgetPasswordPageState extends State<ForgetPasswordPage> {
  TextEditingController emailTextEditingController =
      new TextEditingController();
  TextEditingController passwordTextEditingController =
      new TextEditingController();
  TextEditingController confirmPasswordTextEditingController =
      new TextEditingController();
  UserBloc userBloc = UserBloc();
  bool loading = false;
  final auth = FirebaseAuth.instance;

  FutureOr<Set<void>> timeOut() {
    showToastCommon("Connection TimeOut");
  }

 Future passwordCheck(String email) async{
    if (email.isNotEmpty) {
      final bool isValid =
          EmailValidator.validate(emailTextEditingController.text);
      if (isValid == true) {
        setState(() {
          loading = true;
        });
        User user = FirebaseAuth.instance.currentUser;
        if(user==null) {
          showToastError("User not found, Please Sign Up", context);
          setState(() {
            loading=false;
          });
        }
        else
        try {
          await auth
              .sendPasswordResetEmail(
            email: email,
          )
              .then((value) => {
                showToastSuccess("A password reset link has been sent to your mail", context)
          })
              .timeout(Duration(minutes: 1), onTimeout: timeOut);

          setState(() {
            loading = false;
          });
          pushAndReplacement(context, LoginPage());
        } on FirebaseAuthException catch (e) {
          setState(() {
            loading = false;
          });
          print(e.toString());
          showToastError(e.message.split(']').last, context);
        }

        // userBloc.forgetPassword(
        //     forgetPasswordRequest:
        //         ForgetPasswordRequest(email: emailTextEditingController.text));
      } else {
        showToastCommon("Enter Valid Email");
      }
    } else {
      showToastCommon("Please fill all the details");
    }
    return null;
  }

  @override
  void initState() {
    userBloc.forgetPasswordResponse.listen((event) {
      print("listening");
    }).onError((event) {
      setState(() {
        loading = false;
      });
      showToastError(event, context);
    });
    // userBloc.registerUserResponse.listen((event) {
    //   // push(context, HomePage());
    //   showVerificationAlert(context,
    //       "Please Check Verification Mail send to your Mail ID ", LoginPage());
    //   print("done");
    //   setState(() {
    //     loading = false;
    //   });
    // });
    // super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Constants.kitGradients[0],
      body: SingleChildScrollView(
        child: Container(
          width: screenWidth(context, dividedBy: 1),
          height: screenHeight(context, dividedBy: 1),
          decoration: BoxDecoration(
            color: Constants.kitGradients[27],
            //color: Colors.grey[900],
          ),
          alignment: Alignment.center,
          child: Padding(
            padding: EdgeInsets.symmetric(
                horizontal: screenWidth(context, dividedBy: 17)),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SizedBox(
                  height: screenHeight(context, dividedBy: 17),
                ),
                Icon(
                  Icons.lock,
                  color: Constants.kitGradients[28],
                  size: 60,
                ),
                SizedBox(
                  height: screenWidth(context, dividedBy: 4),
                ),
                Row(
                  children: [
                    Text(
                      "Reset Your Password !",
                      textAlign: TextAlign.left,
                      style: TextStyle(
                        color: Constants.kitGradients[28],
                        fontWeight: FontWeight.w500,
                        fontSize: 22,
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: screenHeight(context, dividedBy: 60),
                ),
                Text(
                  "Please enter your email.A password reset link will be sent to your email.",
                  textAlign: TextAlign.left,
                  style: TextStyle(
                    color: Constants.kitGradients[19],
                    fontWeight: FontWeight.w500,
                    fontSize: 17,
                  ),
                ),
                SizedBox(
                  height: screenHeight(context, dividedBy: 20),
                ),
                Row(
                  children: [
                    Align(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        "Enter Your Email !",
                        textAlign: TextAlign.left,
                        style: TextStyle(
                          color: Constants.kitGradients[28],
                          fontWeight: FontWeight.w500,
                          fontSize: 18,
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: screenHeight(context, dividedBy: 30),
                ),
                FormFieldUserEmail(
                  labelText: "Email",
                  textEditingController: emailTextEditingController,
                  //otp: false,
                ),
                SizedBox(
                  height: screenHeight(context, dividedBy: 20),
                ),
                BuildButton(
                  title: "Submit",
                  isLoading: loading,
                  onPressed: () {
                    passwordCheck(emailTextEditingController.text);
                    // push(context, OTPInputPage());
                  },
                ),
                SizedBox(
                  height: screenHeight(context, dividedBy: 30),
                ),
                SizedBox(
                  height: screenHeight(context, dividedBy: 6),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
