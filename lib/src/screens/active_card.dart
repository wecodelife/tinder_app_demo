import 'package:app_template/src/screens/food_information_page.dart';
import 'package:app_template/src/screens/home_page.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/widgets/favourites_box.dart';
import 'package:app_template/src/widgets/image_slider_indicator.dart';
import 'package:app_template/src/widgets/information_icon.dart';
import 'package:app_template/src/widgets/like_button.dart';
import 'package:flutter/material.dart';

class CardDemo extends StatefulWidget {
  final String foodName;
  final String hotel;
  final String price;
  final String type;
  final double rating;
  final int foodId;
  final String imgUrl;
  final AnimationController buttonController;
  final double bottom;
  final double right;
  final double left;
  final double cardWidth;
  final double rotation;
  final double skew;
  final BuildContext context;
  final Function dismissImg;
  final int flag;
  final Function addImg;
  final Function swipeRight;
  final Function swipeLeft;
  final List<Image> foodImageSection;
  final Function onPressedFavourite;
  final Function onDoubleTap;
  final bool rotationStatus;
  final int keyValue;
  final double lat;
  final double lon;
  final Function onVerticalDrag;
  final bool reverse;
  CardDemo(
      {this.type,
      this.foodId,
      this.hotel,
      this.addImg,
      this.price,
      this.bottom,
      this.cardWidth,
      this.dismissImg,
      this.flag,
      this.imgUrl,
      this.left,
      this.foodImageSection,
      this.foodName,
      this.rating,
      this.right,
        this.lat,
        this.lon,
      this.rotation,
      this.skew,
      this.swipeLeft,
      this.onDoubleTap,
      this.onPressedFavourite,
      this.context,
      this.swipeRight,
      this.rotationStatus,
      this.keyValue,
      this.reverse,
      this.buttonController,
      this.onVerticalDrag});
  @override
  _CardDemoState createState() => _CardDemoState();
}

class _CardDemoState extends State<CardDemo> {
  int index = 0;
  bool reverse = false;
  bool swipe = false;
  List<Image> images = [];
  List<Image> dummyImages = [];
  bool visible = false;
  bool checkIndex = false;
  bool bgColor = false;


  int key;
  bool imageChange = false;

  void imageSwitch() {
    print("image change");
    imageChange = true;
    if (index < (widget.foodImageSection.length - 1) && reverse == false) {
      index = index + 1;
      print("index" + index.toString());
      setState(() {});
    } else {
      if (index == 0) {
        reverse = false;
        index = index + 1;
        setState(() {});
      } else {
        reverse = true;
        setState(() {
          index = index - 1;
        });
      }
    }
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  @override
  void initState() {
    index = 0;
    reverse = false;
    print("ImageListLength" + widget.foodImageSection.length.toString());
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    if (imageChange == false) index = 0;
    if (index >= widget.foodImageSection.length) index = 0;

    // // print(widget.bottom + "bottom");
    // for (int i = 0; i < widget.foodImageSection.length; i++) {
    //   print(widget.foodImageSection[i].toString());
    // }
    print(index);
    return new Positioned(
        right: widget.flag == 0
            ? widget.right != 0.0
                ? widget.right
                : null
            : null,
        left: widget.flag == 1
            ? widget.right != 0.0
                ? widget.right
                : null
            : null,
        top: 0,
        bottom: 0,
        child: GestureDetector(
          onTap: () {
            if (widget.foodImageSection.length > 1) imageSwitch();
          },
          onDoubleTap: () {
            widget.onDoubleTap();
          },
          onVerticalDragEnd: (DragEndDetails details) {
            widget.onVerticalDrag();
          },

          onHorizontalDragEnd: (DragEndDetails details) async {
            if (details.primaryVelocity > 0) {
              // User swiped Left
              // toggleImageCache(context);
              index = 0;
              imageChange = false;
              widget.swipeRight();

              //widget.dismissImg(widget.img);
            } else if (details.primaryVelocity < 0) {
              // User swiped Right
              index = 0;
              imageChange = false;

              widget.swipeLeft();

              //widget.addImg(widget.img);
            }
          },
          child: new RotationTransition(
            alignment:
                widget.flag == 0 ? Alignment.bottomLeft : Alignment.bottomRight,
            turns: new AlwaysStoppedAnimation(widget.flag == 0
                ? widget.rotation / 360
                : -widget.rotation / 360),
            // turns: widget.buttonController,
            child:
                // new Card(
                //   color: Colors.transparent,
                //   elevation: 4.0,
                //   child:
                widget.foodImageSection.isNotEmpty
                    ? Stack(
                        children: [
                          Image(
                              width: screenWidth(context, dividedBy: 1),
                              height: screenHeight(context, dividedBy: 1),
                              image: widget.foodImageSection[index].image,
                              fit: BoxFit.cover,
                              loadingBuilder: (BuildContext context,
                                  Widget child,
                                  ImageChunkEvent loadingProgress) {
                                if (loadingProgress == null)
                                  return Container(
                                    width: screenWidth(context, dividedBy: 1),
                                    height: screenHeight(context, dividedBy: 1),
                                    decoration: BoxDecoration(
                                        color: Constants.kitGradients[28],
                                        // borderRadius: BorderRadius.circular(20),
                                        border: Border.all(
                                            color: Constants.kitGradients[28],
                                            width: 2.0)),
                                    child: ClipRRect(
                                        borderRadius: BorderRadius.circular(20),
                                        child: child),
                                  );
                                return Container(
                                  width: screenWidth(context, dividedBy: 1),
                                  height: screenHeight(context, dividedBy: 1),
                                  color: Colors.white,
                                  child: Center(
                                      child: CircularProgressIndicator(
                                          valueColor:
                                              AlwaysStoppedAnimation<Color>(
                                    Constants.kitGradients[28],
                                  ))),
                                );
                              }),
                          Column(
                            children: [
                              ImageSliderIndicator(
                                imageCount: widget.foodImageSection.length,
                                currentIndex: index,
                              ),
                              Spacer(),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: [
                                  Padding(
                                    padding: EdgeInsets.symmetric(
                                        horizontal: screenWidth(context,
                                            dividedBy: 20)),
                                    child: Container(
                                      width:
                                          screenWidth(context, dividedBy: 1.2),
                                      child: Stack(
                                        children: [
                                          FavouritesBox(
                                            foodName: widget.foodName,
                                            price: widget.price,
                                            foodType: widget.type,
                                            hotelName: widget.hotel,
                                            rating: widget.rating,
                                            id: widget.foodId,
                                            onPressedFavourite: () {},
                                          ),
                                          InformationIcon(
                                            icon: Icons.star,
                                            rightPadding: 4,
                                            onPressed: () {
                                              widget.onPressedFavourite();
                                            },
                                          ),
                                          InformationIcon(
                                            icon: Icons.info,
                                            rightPadding: 17,
                                            onPressed: () {
                                              Navigator.pushReplacement(
                                                context,
                                                MaterialPageRoute(
                                                    builder: (context) =>
                                                        FoodInformationPage(
                                                          foodId: widget.foodId,
                                                          navigationWidget:
                                                              HomeScreen(
                                                            count: false,
                                                          ),
                                                        )),
                                              );
                                            },
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                  SizedBox(
                                    height:
                                        screenHeight(context, dividedBy: 40),
                                  ),
                                  Padding(
                                    padding: EdgeInsets.symmetric(
                                        horizontal: screenWidth(context,
                                            dividedBy: 20)),
                                    child: Container(
                                      width:
                                          screenWidth(context, dividedBy: 1.2),
                                      child: Row(
                                        children: [
                                          LikeButton(
                                            title: "Nope",
                                            onPressed: () {
                                              widget.swipeRight();
                                            },
                                          ),
                                          Spacer(
                                            flex: 1,
                                          ),
                                          LikeButton(
                                            title: "Like",
                                            onPressed: () {
                                              widget.swipeLeft();
                                            },
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                  SizedBox(
                                    height:
                                        screenHeight(context, dividedBy: 15),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ],
                      )
                    : Container(
                  height: screenHeight(context, dividedBy: 1),
                      width:screenWidth(context, dividedBy:1),
                      child: Center(
                          child: Text(
                            "No Image Left",
                            style: new TextStyle(
                                color: Colors.white, fontSize: 24.0),
                          ),
                        ),
                    ),
          ),
          //),
        ));
  }
}
