import 'package:app_template/src/screens/home_page.dart';
import 'package:app_template/src/screens/home_page_screen.dart';
import 'package:app_template/src/screens/liked_item_screen.dart';
import 'package:app_template/src/screens/new_post_page.dart';
import 'package:app_template/src/screens/profile_feed_page.dart';
import 'package:app_template/src/screens/profile_page.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/widgets/app_bar_dog_app.dart';
import 'package:app_template/src/widgets/bottom_bar.dart';
import 'package:app_template/src/widgets/home_page_cards.dart';
import 'package:app_template/src/widgets/home_tab_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class FeedPage extends StatefulWidget {
  @override
  _FeedPageState createState() => _FeedPageState();
}

class _FeedPageState extends State<FeedPage> {
  final List<String> imgList = [
    'https://images.unsplash.com/photo-1520342868574-5fa3804e551c?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=6ff92caffcdd63681a35134a6770ed3b&auto=format&fit=crop&w=1951&q=80',
    'https://images.unsplash.com/photo-1522205408450-add114ad53fe?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=368f45b0888aeb0b7b08e3a1084d3ede&auto=format&fit=crop&w=1950&q=80',
    'https://images.unsplash.com/photo-1519125323398-675f0ddb6308?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=94a1e718d89ca60a6337a6008341ca50&auto=format&fit=crop&w=1950&q=80',
    'https://images.unsplash.com/photo-1523205771623-e0faa4d2813d?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=89719a0d55dd05e2deae4120227e6efc&auto=format&fit=crop&w=1953&q=80',
    'https://images.unsplash.com/photo-1508704019882-f9cf40e475b4?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=8c6e5e3aba713b17aa1fe71ab4f0ae5b&auto=format&fit=crop&w=1352&q=80',
    'https://images.unsplash.com/photo-1519985176271-adb1088fa94c?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=a0c8d632e977f94e5d312d9893258f59&auto=format&fit=crop&w=1355&q=80'
  ];
  List<String> name = [
    "Rajat Palankar",
    "BB ki Vines",
    "ashishchanchlani",
  ];
  List<String> profileImage = [
    'https://images.unsplash.com/photo-1619092502089-76f475610e1d?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxleHBsb3JlLWZlZWR8MzN8fHxlbnwwfHx8fA%3D%3D&auto=format&fit=crop&w=600&q=60',
    'https://images.unsplash.com/photo-1619100222436-65bb02d45195?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxleHBsb3JlLWZlZWR8MzR8fHxlbnwwfHx8fA%3D%3D&auto=format&fit=crop&w=600&q=60',
    'https://images.unsplash.com/photo-1614984979819-41750aef55d6?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxleHBsb3JlLWZlZWR8MzV8fHxlbnwwfHx8fA%3D%3D&auto=format&fit=crop&w=600&q=60',
  ];

  List<String> imageFeed = [
    "https://img.17qq.com/images/fjjhjggbebz.jpeg",
    "https://img.17qq.com/images/fjjhjggbebz.jpeg",
    "https://img.17qq.com/images/fjjhjggbebz.jpeg"
  ];

  List<bool> captionStatus = [true, true, true];

  List<String> caption = [
    "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi",
    "",
    "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi"
  ];
  bool visible = true;

  @override
  Widget build(BuildContext context) {
    return
        // SafeArea(
        //   child:
        AnnotatedRegion<SystemUiOverlayStyle>(
      value: SystemUiOverlayStyle(
        statusBarColor: Constants.kitGradients[28],
        statusBarIconBrightness: Brightness.light,
      ),
      child: Scaffold(
        backgroundColor: Colors.transparent,
        resizeToAvoidBottomInset: false,
        // appBar: AppBar(
        //   // backgroundColor: Constants.kitGradients[28].withOpacity(0.12),
        //   leading: Container(),
        //   elevation: 0,
        //   actions: [
        //     AppBarDogApp(
        //       leftIcon: Icon(
        //         Icons.camera_alt,
        //         color: Constants.kitGradients[27],
        //       ),
        //       onTapLeftIcon: () {
        //         //imgFromGallery(context);
        //         push(
        //             context,
        //             NewPostPage(
        //               profileIcon:
        //                   'https://images.unsplash.com/photo-1619092502089-76f475610e1d?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxleHBsb3JlLWZlZWR8MzN8fHxlbnwwfHx8fA%3D%3D&auto=format&fit=crop&w=600&q=60',
        //               userName: "Bi kines",
        //               //filteredImage: uint8list,
        //             ));
        //       },
        //       rightIcon: Icon(
        //         Icons.notifications_outlined,
        //         color: Constants.kitGradients[27],
        //       ),
        //       onTapRightIcon: () {
        //         //push(context, NotificationPage());
        //         //showReportModal(context);
        //         // push(context, ReportPostPage());
        //       },
        //       title: Text(
        //         "News Feed",
        //         style: TextStyle(
        //           fontSize: 17,
        //           fontWeight: FontWeight.w600,
        //           color: Constants.kitGradients[27],
        //         ),
        //       ),
        //     )
        //   ],
        //   backgroundColor: Constants.kitGradients[28],
        // ),
        body: Container(
          width: screenWidth(context, dividedBy: 1),
          //height: screenWidth(context, dividedBy: 1),
          // color: Colors.cyan,
          color: Constants.kitGradients[31],
          child: Stack(
            children: [
              SingleChildScrollView(
                controller: new ScrollController(
                  initialScrollOffset: 0.0,
                  keepScrollOffset: true,
                ),
                padding: EdgeInsets.only(
                    bottom: MediaQuery.of(context).viewInsets.bottom),
                child: Container(
                  padding: EdgeInsets.symmetric(
                    horizontal: screenWidth(context, dividedBy: 60),
                  ),
                  // color: Constants.kitGradients[19].withOpacity(.2),
                  // color:Colors.grey[300],
                  color: Constants.kitGradients[31],
                  child: Column(
                    children: [
                      SizedBox(
                        height: screenHeight(context, dividedBy: 40),
                      ),
                      Container(
                        child: ListView.builder(
                          shrinkWrap: true,
                          physics: NeverScrollableScrollPhysics(),
                          itemCount: name.length,
                          itemBuilder: (BuildContext context, int index) {
                            return Column(children: [
                              Container(
                                  child: GestureDetector(
                                    child: HomePageCard(
                                        profilePic: profileImage[index],
                                        name: name[index],
                                        feedImage: imgList[index],
                                        caption: caption[index],
                                        location: "Delhi, India",
                                        likeCount: "21,000",
                                        postDate: "19 June 2021",
                                        //comment: null,
                                        comment:
                                            "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore.",
                                        commentBy: "ABBd"),
                                  )),
                              // SizedBox(
                              //   height: screenHeight(context, dividedBy: 40),
                              // ),
                            ]);
                          },
                        ),
                      ),
                      SizedBox(
                        height: screenHeight(context, dividedBy: 13),
                      ),
                    ],
                  ),
                ),
              ),
              Visibility(
                visible: visible ? true : false,
                child: HomeTabBar(
                  onTapLiked: () {
                    push(context, LikedItemsScreen());
                  },
                  onTapHome: () {
                    push(context, HomePageScreen(count: true));
                  },
                  onTapSaved: () {
                    push(context, FeedPage());
                  },
                  onTapProfile: () {
                    pushAndReplacement(
                        context,
                        ProfilePage(
                            userPic:
                                "https://images.unsplash.com/photo-1467003909585-2f8a72700288?ixid=MnwxMjA3fDB8MHxzZWFyY2h8OXx8Zm9vZHxlbnwwfHwwfHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=600&q=60",
                            userName: "SanJE"));
                  },
                ),
              ),
              // Align(
              //   alignment: Alignment.bottomCenter,
              //   child: CustomBottomBar(
              //       currentIndex: 0,
              //       onTapSearch: () {
              //         push(context, HomePageCard());
              //       },
              //       onTapProfile: () {
              //         push(
              //             context,
              //             ProfileFeedPage(
              //               name: "Bi Kines",
              //               profilePic:
              //                   'https://images.unsplash.com/photo-1520342868574-5fa3804e551c?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=6ff92caffcdd63681a35134a6770ed3b&auto=format&fit=crop&w=1951&q=80',
              //             ));
              //       },
              //       onTapHome: () {}),
              // )
            ],
          ),
        ),
      ),
    );
  }
}
