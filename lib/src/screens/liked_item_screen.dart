import 'package:app_template/src/bloc/user_bloc.dart';
import 'package:app_template/src/models/liked_food_response.dart';
import 'package:app_template/src/screens/profile_page.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/object_factory.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/widgets/app_bar.dart';
import 'package:app_template/src/widgets/creator_name.dart';
import 'package:app_template/src/widgets/liked_food_list_view.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class LikedItemsScreen extends StatefulWidget {
  final bool mustLike;
  LikedItemsScreen({this.mustLike});  @override
  _LikedItemsScreenState createState() => _LikedItemsScreenState();
}

class _LikedItemsScreenState extends State<LikedItemsScreen> {
  // Future<bool> _willPopCallback() async {
  //   pushAndReplacement(
  //       context,
  //       HomePageScreen(
  //         count: false,
  //       ));
  // }

  UserBloc userBloc = UserBloc();
  List<Result> likeFood = [];
  List<bool> likeBool = [];
  bool showError = false;
  bool isLoading = false;
  String errorMsg = "";

  @override
  void initState() {
    // TODO: implement initState
    userBloc.getLikedFood(id: "?is_liked=true",
        uid: ObjectFactory().appHive.getUserId()
    );
    /*setState(() {
      isLoading = true;
    });*/

    userBloc.likedFood.listen((event) {
      if(event.statusCode == 200 || event.statusCode == 201) {
        print("Favorite list response successfull");
        for (int i = 0; i < event.results.length; i++) {
          if (event.results[i].isLiked == true) {
            setState(() {
              likeFood.add(event.results[i]);
            });
          }
        }
        print("Favourite Success");
        print(event.results);
        print(likeFood);
      }
      // for(int i=0;i<event.results.length;i++){
      //     likeBool.add(event.results[i].isLiked);
      //   }
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(
      SystemUiOverlayStyle(
        statusBarColor: Constants.kitGradients[31],
        statusBarBrightness: Brightness.dark,
        statusBarIconBrightness: Brightness.dark,
      ),
    );
    return WillPopScope(
      onWillPop: () {
        Navigator.of(context).pushAndRemoveUntil(
            MaterialPageRoute(builder: (context) => ProfilePage()),
                (Route<dynamic> route) => false);
      },
      child:AnnotatedRegion<SystemUiOverlayStyle>(
        value: SystemUiOverlayStyle(
          statusBarColor: Constants.kitGradients[31],
          statusBarBrightness: Brightness.dark,
          statusBarIconBrightness: Brightness.dark,
        ),
        child: Scaffold(
          backgroundColor: Constants.kitGradients[31],
          appBar: AppBar(
            leading: Container(),
            elevation: 0,
            actions: [
              AppBarFoodApp(
                title: "Liked",
                leftIcon: Icon(
                  Icons.arrow_back_ios,
                  color: Constants.kitGradients[28],
                ),
                onPressedLeftIcon: () {
                  pushAndRemoveUntil(context, ProfilePage(), false);
                },
                rightIcon: IconButton(
                  alignment: Alignment.center,
                  icon: Icon(Icons.favorite,
                    color: Constants.kitGradients[28],
                    size: 25,
                  ),
                ),
              ),
            ],
            backgroundColor: Constants.kitGradients[31],
          ),
          body: SingleChildScrollView(
            padding: EdgeInsets.zero,
            child: Container(
              color: Constants.kitGradients[31],
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(
                    height: screenHeight(context, dividedBy: 50),
                  ),
                  Container(
                      padding: EdgeInsets.symmetric(
                        horizontal: screenWidth(context, dividedBy: 30),
                      ),
                      child: isLoading == false
                              ? likeFood.length != 0
                                  ?  ListView.builder(
                                      itemCount: likeFood.length,
                                      shrinkWrap: true,
                        reverse: true,
                        physics: NeverScrollableScrollPhysics(),
                                      itemBuilder:
                                          (BuildContext context, int index) {
                                        return LikedFoodListView(
                                          shopName:
                                              likeFood[index].food.restaurant,
                                          foodName: likeFood[index].food.name,
                                          index: index,
                                          rating: 4.0,
                                          price: likeFood[index].food.price,
                                          foodType: likeFood[index].food.foodType,
                                          likedFoodImageUrl:
                                              likeFood[index].food.image1,
                                          navigationPage: LikedItemsScreen(),
                                          isLikedList: true,
                                          foodId: likeFood[index].food.id,
                                          isMustVisit: true,
                                        );
                                      },
                                    )
                                  : Container(
                                      height: screenHeight(context, dividedBy: 1.2),
                                      child: Center(
                                        child: Container(
                                          width: screenWidth(context, dividedBy: 2.5),
                                          height: screenHeight(context, dividedBy: 2.8),
                                          child: Column(
                                            children: [
                                              Image.asset('assets/images/paziq_logo.png'),
                                              SizedBox(
                                                height: screenHeight(context, dividedBy: 25),
                                              ),
                                          Text("There is nothing in your list",
                                            style: TextStyle(
                                              color: Constants.kitGradients[28],
                                              fontSize: 15,
                                              fontFamily: "OswaldRegular",
                                              fontWeight: FontWeight.bold
                                            ),)
                                            ],
                                          ),
                                          alignment: Alignment.center,)
                                      ),
                                    )
                              : Container(
                                  height: screenHeight(context, dividedBy: 1.4),
                                  child: Center(
                                    child: CircularProgressIndicator(
                                      valueColor: AlwaysStoppedAnimation(
                                          Constants.kitGradients[28]),
                                      strokeWidth: 2,
                                    ),
                                  ),
                                )

        ),
                  likeFood.length == 1
                      ? SizedBox(
                          height: screenHeight(context, dividedBy: 2),
                        ):
                  likeFood.length == 2
                      ? SizedBox(
                          height: screenHeight(context, dividedBy: 7),
                        ):
                  SizedBox(
                    height: screenHeight(context, dividedBy: 90),
                  ),
                  Center(child: CreatorName()),
                  SizedBox(
                    height: screenHeight(context, dividedBy: 100),
                  ),
                  // Row(
                  //   crossAxisAlignment: CrossAxisAlignment.start,
                  //   mainAxisAlignment:MainAxisAlignment.start,
                  //
                  //   children: [
                  //     SizedBox(
                  //       width: 10,
                  //     ),
                  //     Expanded(
                  //       child: likedFood.length != 0
                  //           ? ListView.builder(
                  //               padding: EdgeInsets.zero,
                  //               itemCount: likedFood.length,
                  //               shrinkWrap: true,
                  //               physics: NeverScrollableScrollPhysics(),
                  //               itemBuilder: (BuildContext context, int index) {
                  //                 print("Liked Food" + index.toString());
                  //                 return index % 2 == 0
                  //                     ? Column(
                  //                         children: [
                  //                           ClipRRect(
                  //                             borderRadius:
                  //                                 BorderRadius.circular(20),
                  //                             child: GestureDetector(
                  //                               onTap: () {
                  //                                 push(
                  //                                     context,
                  //                                     FoodInformationPage(
                  //                                       navigationWidget:
                  //                                           LikedItemsList(),
                  //                                     ));
                  //                               },
                  //                               child: CachedNetworkImage(
                  //                                 height: screenHeight(context,
                  //                                     dividedBy: 2.4),
                  //                                 width: screenWidth(context,
                  //                                     dividedBy: 2),
                  //                                 fit: BoxFit.cover,
                  //                                 imageUrl: likedFood[index]
                  //                                             .food
                  //                                             .image1 ==
                  //                                         null
                  //                                     ? "https://images.unsplash.com/photo-1476224203421-9ac39bcb3327?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MTJ8fGZvb2R8ZW58MHx8MHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=600&q=60"
                  //                                     : "https://images.unsplash.com/photo-1476224203421-9ac39bcb3327?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MTJ8fGZvb2R8ZW58MHx8MHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=600&q=60",
                  //                                 imageBuilder:
                  //                                     (context, imageProvider) =>
                  //                                         Container(
                  //                                   height: screenHeight(context,
                  //                                       dividedBy: 2.4),
                  //                                   width: screenWidth(context,
                  //                                       dividedBy: 2),
                  //                                   decoration: BoxDecoration(
                  //                                     image: DecorationImage(
                  //                                       image: imageProvider,
                  //                                       fit: BoxFit.cover,
                  //                                     ),
                  //                                     borderRadius:
                  //                                         BorderRadius.circular(
                  //                                             20),
                  //                                   ),
                  //                                   child: Stack(
                  //                                     children: [
                  //                                       InformationIcon(
                  //                                         deleteSaved: true,
                  //                                         icon: Icons.favorite,
                  //                                         icon2: Icons
                  //                                             .favorite_outline,
                  //                                         onPressed: () {},
                  //                                         rightPadding: 20,
                  //                                       ),
                  //                                       Column(
                  //                                         crossAxisAlignment:
                  //                                             CrossAxisAlignment
                  //                                                 .center,
                  //                                         mainAxisAlignment:
                  //                                             MainAxisAlignment
                  //                                                 .end,
                  //                                         children: [
                  //                                           FavouritesBox(
                  //                                             id: 1,
                  //                                             foodName: "Chicken",
                  //                                             price: "19",
                  //                                             foodType: likedFood[
                  //                                                             index]
                  //                                                         .food
                  //                                                         .foodType ==
                  //                                                     null
                  //                                                 ? "Not Available"
                  //                                                 : "Non-Veg",
                  //                                             hotelName:
                  //                                                 likedFood[index]
                  //                                                     .food
                  //                                                     .restaurant,
                  //                                             rating: 19,
                  //                                           ),
                  //                                           SizedBox(
                  //                                             height:
                  //                                                 screenHeight(
                  //                                                     context,
                  //                                                     dividedBy:
                  //                                                         60),
                  //                                           ),
                  //                                         ],
                  //                                       ),
                  //                                     ],
                  //                                   ),
                  //                                 ),
                  //                                 placeholder: (context, url) =>
                  //                                     Center(
                  //                                   heightFactor: 1,
                  //                                   widthFactor: 1,
                  //                                   child: SizedBox(
                  //                                     height: 16,
                  //                                     width: 16,
                  //                                     child:
                  //                                         CircularProgressIndicator(
                  //                                       valueColor:
                  //                                           AlwaysStoppedAnimation(
                  //                                               Constants
                  //                                                       .kitGradients[
                  //                                                   28]),
                  //                                       strokeWidth: 2,
                  //                                     ),
                  //                                   ),
                  //                                 ),
                  //                               ),
                  //                             ),
                  //                           ),
                  //                           SizedBox(
                  //                             height: screenHeight(context,
                  //                                 dividedBy: 30),
                  //                           )
                  //                         ],
                  //                       )
                  //                     : Container();
                  //               },
                  //             )
                  //           : Container(),
                  //     ),
                  //     SizedBox(
                  //       width: 10,
                  //     ),
                  //     likedFood.length != 1
                  //         ? Expanded(
                  //             child: ListView.builder(
                  //               padding: EdgeInsets.zero,
                  //               itemCount: likedFood.length,
                  //               shrinkWrap: true,
                  //               physics: NeverScrollableScrollPhysics(),
                  //               itemBuilder: (BuildContext context, int index) {
                  //                 return index % 2 == 1
                  //                     ? Column(
                  //                         children: [
                  //                           index == 1
                  //                               ? Container(
                  //                                   height: screenHeight(context,
                  //                                       dividedBy: 16),
                  //                                 )
                  //                               : Container(),
                  //                           ClipRRect(
                  //                             borderRadius:
                  //                                 BorderRadius.circular(20),
                  //                             child: GestureDetector(
                  //                               onTap: () {
                  //                                 push(
                  //                                     context,
                  //                                     FoodInformationPage(
                  //                                       navigationWidget:
                  //                                           LikedItemsList(),
                  //                                     ));
                  //                               },
                  //                               child: CachedNetworkImage(
                  //                                 height: screenHeight(context,
                  //                                     dividedBy: 2.4),
                  //                                 width: screenWidth(context,
                  //                                     dividedBy: 2),
                  //                                 fit: BoxFit.cover,
                  //                                 imageUrl: likedFood[index]
                  //                                             .food
                  //                                             .image1 ==
                  //                                         null
                  //                                     ? "https://images.unsplash.com/photo-1476224203421-9ac39bcb3327?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MTJ8fGZvb2R8ZW58MHx8MHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=600&q=60"
                  //                                     : likedFood[index]
                  //                                         .food
                  //                                         .image1,
                  //                                 imageBuilder:
                  //                                     (context, imageProvider) =>
                  //                                         Container(
                  //                                   height: screenHeight(context,
                  //                                       dividedBy: 2.4),
                  //                                   width: screenWidth(context,
                  //                                       dividedBy: 2),
                  //                                   decoration: BoxDecoration(
                  //                                     image: DecorationImage(
                  //                                       image: imageProvider,
                  //                                       fit: BoxFit.cover,
                  //                                     ),
                  //                                     borderRadius:
                  //                                         BorderRadius.circular(
                  //                                             20),
                  //                                   ),
                  //                                   child: Stack(
                  //                                     children: [
                  //                                       InformationIcon(
                  //                                         deleteSaved: true,
                  //                                         icon: Icons.favorite,
                  //                                         icon2: Icons
                  //                                             .favorite_outline,
                  //                                         onPressed: () {},
                  //                                         rightPadding: 20,
                  //                                       ),
                  //                                       Column(
                  //                                         crossAxisAlignment:
                  //                                             CrossAxisAlignment
                  //                                                 .center,
                  //                                         mainAxisAlignment:
                  //                                             MainAxisAlignment
                  //                                                 .end,
                  //                                         children: [
                  //                                           FavouritesBox(
                  //                                             id: 1,
                  //                                             foodName:
                  //                                                 likedFood[index]
                  //                                                     .food
                  //                                                     .name,
                  //                                             price: "19",
                  //                                             foodType: likedFood[
                  //                                                             index]
                  //                                                         .food
                  //                                                         .foodType ==
                  //                                                     null
                  //                                                 ? "Not Available"
                  //                                                 : "Non-Veg",
                  //                                             hotelName:
                  //                                                 likedFood[index]
                  //                                                     .food
                  //                                                     .restaurant,
                  //                                             rating: 19,
                  //                                           ),
                  //                                           SizedBox(
                  //                                             height:
                  //                                                 screenHeight(
                  //                                                     context,
                  //                                                     dividedBy:
                  //                                                         60),
                  //                                           ),
                  //                                         ],
                  //                                       ),
                  //                                     ],
                  //                                   ),
                  //                                 ),
                  //                                 placeholder: (context, url) =>
                  //                                     Center(
                  //                                   heightFactor: 1,
                  //                                   widthFactor: 1,
                  //                                   child: SizedBox(
                  //                                     height: 16,
                  //                                     width: 16,
                  //                                     child:
                  //                                         CircularProgressIndicator(
                  //                                       valueColor:
                  //                                           AlwaysStoppedAnimation(
                  //                                               Constants
                  //                                                       .kitGradients[
                  //                                                   28]),
                  //                                       strokeWidth: 2,
                  //                                     ),
                  //                                   ),
                  //                                 ),
                  //                               ),
                  //                             ),
                  //                           ),
                  //                           SizedBox(
                  //                             height: screenHeight(context,
                  //                                 dividedBy: 30),
                  //                           )
                  //                         ],
                  //                       )
                  //                     : Container(
                  //                         height: 1,
                  //                       );
                  //               },
                  //             ),
                  //           )
                  //         : Container(
                  //             height: 1,
                  //           ),
                  //     SizedBox(
                  //       width: 10,
                  //     ),
                  //   ],
                  // ),
                ],
              ),
            ),
          ),
          // DragabbleTabBar(
          //   currentIndex: 1,
          //   onTapLiked: () {
          //     //push(context, LikedItemsList());
          //   },
          //   onTapSaved: () {
          //     push(context, SavedItemsList());
          //   },
          //   onTapHome: () {
          //     push(context, HomeScreen());
          //   },
          //   // onTapChat: () {
          //   //   push(context, ChatListPage());
          //   // },
          // ),
        ),
      ),
    );
  }
}
