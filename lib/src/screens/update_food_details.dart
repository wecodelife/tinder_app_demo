import 'dart:io';
import 'package:app_template/src/bloc/user_bloc.dart';
import 'package:app_template/src/models/get_food_type_model.dart';
import 'package:app_template/src/models/update_food_request.dart';
import 'package:app_template/src/screens/data_page.dart';
import 'package:app_template/src/screens/gallery_page.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/widgets/app_bar.dart';
import 'package:app_template/src/widgets/build_button.dart';
import 'package:app_template/src/widgets/build_dropcurrency.dart';
import 'package:app_template/src/widgets/food_description_textfield.dart';
import 'package:app_template/src/widgets/recipe_textfield.dart';
import 'package:flutter/material.dart';
import 'package:google_map_location_picker/google_map_location_picker.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:shimmer/shimmer.dart';

class UpdateFoodDetails extends StatefulWidget {
  final String restaurantName;
  final int foodType;
  final String description;
  final String title;
  final List<String> recipe;
  final String place;
  final String googleMapUrl;
  final String itemType;
  final String price;
  final List<String> foodImages;
  final String lat;
  final String lon;
  UpdateFoodDetails(
      {this.foodImages,
      this.restaurantName,
      this.recipe,
      this.itemType,
      this.title,
      this.description,
      this.place,
      this.price,
      this.googleMapUrl,
      this.foodType,
      this.lon,
      this.lat});
  // const UpdateFoodDetails({Key? key}) : super(key: key);

  @override
  _UpdateFoodDetailsState createState() => _UpdateFoodDetailsState();
}

class _UpdateFoodDetailsState extends State<UpdateFoodDetails> {
  TextEditingController descTextEditingController = new TextEditingController();
  TextEditingController titleTextEditingController =
      new TextEditingController();
  TextEditingController restaurantNameTextEditingController =
      new TextEditingController();
  TextEditingController placeNameTextEditingController =
      new TextEditingController();
  TextEditingController googleMapUrlTextEditingController =
      new TextEditingController();
  TextEditingController priceTextEditingController =
      new TextEditingController();
  bool tapped = false;
  int foodType;
  int index;
  int i = 0;
  List<String> foodRecipe = [];
  List<String> foodImages = [];
  String googleMapUrl = "";
  UserBloc userBloc = new UserBloc();
  String lat = "76.00";
  String lon = "86.00";
  LocationResult _pickedLocation;
  bool loading=true;
  GetFoodType getFoodType;
  // extractPositions() {
  //   lat = double.parse(
  //       googleMapUrlTextEditingController.text.split("<")[1].split(">").first);
  //   lat = double.parse(
  //       googleMapUrlTextEditingController.text.split("<")[2].split(">").first);
  //   print(lat.toString());
  //   print(lon.toString());
  // }

  @override
  void initState() {
    index = widget.foodImages != null ? widget.foodImages.length - 1 : 0;
    titleTextEditingController.text = widget.title;
    descTextEditingController.text = widget.description;
    foodImages = widget.foodImages;
    foodType = widget.foodType;
    googleMapUrlTextEditingController.text = widget.googleMapUrl;
    placeNameTextEditingController.text = widget.place;
    priceTextEditingController.text = widget.price;
    restaurantNameTextEditingController.text = widget.restaurantName;
    lat = widget.lat;
    lon = widget.lon;
    setState(() {
      loading=true;
    });
    userBloc.getFoodTypeResponse();
    userBloc.getFoodType.listen((event) {
      if (event.statusCode == 201 || event.statusCode == 200) {
        setState(() {
          loading=false;
          getFoodType=event;
        });
        print("abcd" + loading.toString());
      } else {
        setState(() {
          loading=false;

        });
        showToastCommon(event.message);
      }
    });

    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Constants.kitGradients[31],
        appBar: AppBar(
          backgroundColor: Constants.kitGradients[31],
          elevation: 0,
          leading: Container(),
          actions: [
            AppBarFoodApp(
              title: "Add New Food",
              leftIcon: Icon(
                Icons.arrow_back_ios,
                color: Constants.kitGradients[28],
              ),
              onPressedLeftIcon: () {
                pop(context);
              },
            ),
          ],
        ),
        body: SingleChildScrollView(
          child: Container(
              width: screenWidth(context, dividedBy: 1),
              //height: screenHeight(context, dividedBy: 1),
              decoration: BoxDecoration(
                color: Constants.kitGradients[31],
              ),
              padding: EdgeInsets.symmetric(
                horizontal: screenWidth(context, dividedBy: 20),
              ),
              child: Column(
                //crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  // SizedBox(
                  //   height: screenHeight(context, dividedBy: 70),
                  // ),
                  SizedBox(
                    height: screenHeight(context, dividedBy: 40),
                  ),
                  FoodDetailsTextField(
                      labelText: "Restaurant Name",
                      textEditingController:
                          restaurantNameTextEditingController,
                      isContent: false,
                      hintText: "Restaurant Name"),

                  SizedBox(
                    height: screenHeight(context, dividedBy: 40),
                  ),
                  FoodDetailsTextField(
                      labelText: "Title",
                      textEditingController: titleTextEditingController,
                      isContent: false,
                      hintText: "title"),
                  SizedBox(
                    height: screenHeight(context, dividedBy: 40),
                  ),

                  FoodDetailsTextField(
                      labelText: "Description",
                      textEditingController: descTextEditingController,
                      isContent: true,
                      hintText: "type something"),
                  SizedBox(
                    height: screenHeight(context, dividedBy: 40),
                  ),
                  GridView.builder(
                      shrinkWrap: true,
                      physics: NeverScrollableScrollPhysics(),
                      itemCount: widget.foodImages != null
                          ? widget.foodImages.length
                          : 1,
                      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: 4,
                        mainAxisSpacing: 5,
                        crossAxisSpacing: 5,
                        childAspectRatio: screenWidth(context, dividedBy: 3.5) /
                            (MediaQuery.of(context).size.height / 6),
                      ),
                      itemBuilder: (BuildContext context, int index) {
                        print(foodUpdateImages.toString());
                        return GestureDetector(
                          onTap: () {
                            push(
                                context,
                                GalleryPage(
                                  itemType: foodType.toString(),
                                  title: titleTextEditingController.text,
                                  description: descTextEditingController.text,
                                  foodType: foodType,
                                  googleMapUrl: googleMapUrl,
                                  place: placeNameTextEditingController.text,
                                  restaurantName:
                                      restaurantNameTextEditingController.text,
                                  price: priceTextEditingController.text,
                                  recipe: foodRecipe,
                                  lat: lat.toString(),
                                  lon: lon.toString(),
                                ));
                          },
                          child: Container(
                            height: screenWidth(context, dividedBy: 3.5),
                            width: screenWidth(context, dividedBy: 3.5),
                            decoration: BoxDecoration(
                              color:
                                  Constants.kitGradients[28].withOpacity(0.12),
                              borderRadius: BorderRadius.circular(8),
                            ),
                            child: ClipRRect(
                              borderRadius: BorderRadius.circular(8),
                              child: widget.foodImages == null
                                  ? Icon(
                                      Icons.add_a_photo_rounded,
                                      color: Constants.kitGradients[28]
                                          .withOpacity(0.6),
                                      size: 30,
                                    )
                                  : Container(
                                      decoration: BoxDecoration(
                                          image: DecorationImage(
                                              fit: BoxFit.fill,
                                              image: FileImage(
                                                File(widget.foodImages[index]),
                                              ))),
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.end,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          GestureDetector(
                                            onTap: () {},
                                            child: Icon(
                                              Icons.close,
                                              color: Constants.kitGradients[31],
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                            ),
                          ),
                        );
                      }),
                  SizedBox(
                    height: screenHeight(context, dividedBy: 30),
                  ),

                  RecipeTextField(
                    onRecipeAdded: (value) {
                      foodRecipe = value;
                      print("recipe"+foodRecipe.toString());
                      },
                  ),
                  SizedBox(
                    height: screenHeight(context, dividedBy: 60),
                  ),

                  FoodDetailsTextField(
                      labelText: "Place",
                      textEditingController: placeNameTextEditingController,
                      isContent: false,
                      hintText: "place"),
                  SizedBox(
                    height: screenHeight(context, dividedBy: 20),
                  ),

                  GestureDetector(
                    onTap: () async {
                      LocationResult result = await showLocationPicker(
                        context, "AIzaSyD8YBSYyFKVebxM7tZZESn02aUQnTbHJ44",
                        initialCenter: LatLng(9.9312, 76.2673),
                        automaticallyAnimateToCurrentLocation: true,
                        hintText: "Search Location",
                        mapStylePath: 'assets/mapStyle.json',
                        myLocationButtonEnabled: true,
                        requiredGPS: true,
                        layersButtonEnabled: true,
                        countries: [
                          'IN',
                        ],
                        language: "en",

//                      resultCardAlignment: Alignment.bottomCenter,
                        desiredAccuracy: LocationAccuracy.best,
                      );
                      print("result = $result");
                      setState(() {
                        _pickedLocation = result;
                        googleMapUrlTextEditingController.text =
                            _pickedLocation.address.toString();
                        googleMapUrl = googleMapUrlTextEditingController.text;
                        lat = _pickedLocation.latLng.latitude.toString();
                        lon = _pickedLocation.latLng.longitude.toString();
                      });

                      print("latLon :" + lat.toString() + "," + lon.toString());
                    },
                    child: Container(
                      width: screenWidth(context, dividedBy: 1),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          border: Border.all(
                              color: Constants.kitGradients[28], width: 0.0)),
                      child: Padding(
                        padding: EdgeInsets.symmetric(
                            vertical: screenHeight(context, dividedBy: 40),
                            horizontal: screenWidth(context, dividedBy: 70)),
                        child: Text(
                          _pickedLocation == null
                              ? "Click to add location"
                              : _pickedLocation.address,
                          style: TextStyle(color: Colors.grey),
                        ),
                      ),
                    ),
                  ),

                  SizedBox(
                    height: screenHeight(context, dividedBy: 30),
                  ),

                  FoodDetailsTextField(
                      labelText: "Enter Your Price",
                      textEditingController: priceTextEditingController,
                      isContent: false,
                      hintText: "Price"),
                  SizedBox(
                    height: screenHeight(context, dividedBy: 20),
                  ),
                  Row(
                    children: [
                      loading?Shimmer.fromColors(
                        highlightColor: Constants.kitGradients[28].withOpacity(0.6),
                        baseColor: Colors.grey.withOpacity(0.8),
                        child: Container(
                          width: screenWidth(context, dividedBy: 2),
                          height: screenHeight(context,dividedBy: 15),
                          decoration: BoxDecoration(
                            color: Constants.kitGradients[28].withOpacity(0.14),
                            borderRadius: BorderRadius.circular(20),
                          ),
                        ),
                      ):DropDownFood(
                        getFoodType: getFoodType,
                        onDropDownSelected: (value) {
                          setState(() {
                            foodType = value;
                          });
                        },
                      ),
                    ],
                  ),
                  SizedBox(
                    height: screenHeight(context, dividedBy: 30),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      BuildButton(
                          title: "Update",
                          isLoading: false,
                          onPressed: () {
                            print("FoodImage Length" +
                                foodUpdateImages.length.toString());
                            if (googleMapUrl != "") {
                              // extractPositions();
                              if (descTextEditingController.text != "" &&
                                  titleTextEditingController.text != "" &&
                                  restaurantNameTextEditingController.text !=
                                      "" &&
                                  priceTextEditingController.text != "" &&
                                  placeNameTextEditingController.text != "" &&
                                  foodType != null &&
                                  foodRecipe != null) {
                                userBloc.updateFoodDetails(
                                    updateFoodRequest: UpdateFoodRequest(
                                  name: widget.title,
                                  description: widget.description,
                                  restaurant: widget.restaurantName,
                                  foodTypeName: widget.foodType.toString(),
                                  place: widget.place,
                                  image1: widget.foodImages.length > 0
                                      ? File(widget.foodImages[0])
                                      : null,
                                  image2: widget.foodImages.length > 1
                                      ? File(widget.foodImages[1])
                                      : null,
                                  image3: widget.foodImages.length > 2
                                      ? File(widget.foodImages[2])
                                      : null,
                                  image4: foodUpdateImages.length > 3
                                      ? File(widget.foodImages[3])
                                      : null,
                                  image5: widget.foodImages.length > 4
                                      ? File(widget.foodImages[4])
                                      : null,
                                  foodType: foodType,
                                  lat: lat,
                                  lon: lon,
                                  price: "600",
                                  googleMapUrl: googleMapUrl,
                                  ingredients: foodRecipe,
                                ));
                              } else {
                                showToastInfo(
                                    "please upddate all the feilds", context);
                              }
                              // push(context, OTPInputPage());
                            } else {
                              showToastInfo(
                                  "Please fill all thr details", context);
                            }
                          }),
                    ],
                  ),
                  SizedBox(
                    height: screenHeight(context, dividedBy: 60),
                  ),
                ],
              )),
        ),
      ),
    );
  }
}
