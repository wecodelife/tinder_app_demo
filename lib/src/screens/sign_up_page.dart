import 'dart:ui';

import 'package:app_template/src/bloc/user_bloc.dart';
import 'package:app_template/src/models/register_user_request.dart';
import 'package:app_template/src/screens/login_page.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/widgets/build_button.dart';
import 'package:app_template/src/widgets/email_form_field.dart';
import 'package:app_template/src/widgets/password_form_field.dart';
import 'package:email_validator/email_validator.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

class SignUpPage extends StatefulWidget {
  final String firstName;
  final String lastName;
  SignUpPage({this.firstName, this.lastName});
  @override
  _SignUpPageState createState() => _SignUpPageState();
}

class _SignUpPageState extends State<SignUpPage> {
  TextEditingController emailTextEditingController =
      new TextEditingController();
  TextEditingController passwordTextEditingController =
      new TextEditingController();
  TextEditingController confirmPasswordTextEditingController =
      new TextEditingController();
  UserBloc userBloc = UserBloc();
  bool loading = false;

  Future passwordCheck(
      String email, String password, String confirmPassword) async {
    if (email.isNotEmpty && password.isNotEmpty && confirmPassword.isNotEmpty) {
      final bool isValid =
          EmailValidator.validate(emailTextEditingController.text);
      print(isValid);
      if (isValid == true) {
        if (passwordTextEditingController.text.length >= 6 &&
            confirmPasswordTextEditingController.text.length >= 6) {
          if (passwordTextEditingController.text ==
              confirmPasswordTextEditingController.text) {
            setState(() {
              loading = true;
            });
            User user = FirebaseAuth.instance.currentUser;
            print(user.toString() + "avcd");
            if (user != null) {
              showToastError("User Exist, Please Login", context);
              setState(() {
                loading = false;
              });
            } else
              try {
                UserCredential userCredential = await FirebaseAuth.instance
                    .createUserWithEmailAndPassword(
                        email: emailTextEditingController.text,
                        password: passwordTextEditingController.text);
                User user = FirebaseAuth.instance.currentUser;
                print("Firebase UID  " + user.uid);

                if (!user.emailVerified) {
                  await user.sendEmailVerification();
                }
                setState(() {
                  showToastSuccess(
                      "Registered Successfully, Please verify mail", context);
                  loading = false;
                  pushAndReplacement(context, LoginPage());
                });
              } on FirebaseAuthException catch (e) {
                setState(() {
                  loading = false;
                  showToastError("Some error occurred", context);
                });
              }
          } else {
            showToastCommon("Passwords does not Match");
          }
        } else {
          showToastCommon("Minimum 6 character required");
        }
      } else {
        showToastCommon("Enter Valid Email");
      }
    } else {
      showToastCommon("Please fill all the details");
    }
    return null;
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      //backgroundColor: Constants.kitGradients[30],
      body: SingleChildScrollView(
        child: Container(
          width: screenWidth(context, dividedBy: 1),
          height: screenHeight(context, dividedBy: 1),
          decoration: BoxDecoration(
            color: Constants.kitGradients[0],
            //color: Colors.grey[900],
          ),
          alignment: Alignment.center,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                width: screenWidth(context, dividedBy: 1),
                height: screenHeight(context, dividedBy: 1),
                decoration: BoxDecoration(
                  //border: Border(top:BorderSide(color:Constants.kitGradients[28],width: 2.0),),
                  color: Constants.kitGradients[0],
                  borderRadius: BorderRadius.circular(8),
                  // image: DecorationImage(
                  //   image: AssetImage(
                  //     "assets/images/foodapp_bg.jpg",
                  //   ),
                  //   fit: BoxFit.cover,
                  // ),
                ),
                padding: EdgeInsets.symmetric(
                    horizontal: screenWidth(context, dividedBy: 10)),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    SizedBox(
                      height: screenHeight(context, dividedBy: 8),
                    ),
                    Image.asset(
                      "assets/images/logo.png",
                      color: Constants.kitGradients[28],
                    ),
                    SizedBox(
                      height: screenHeight(context, dividedBy: 10),
                    ),
                    Align(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        "Hey Welcome !",
                        textAlign: TextAlign.left,
                        style: TextStyle(
                          fontFamily: "Montserrat",
                          color: Constants.kitGradients[28],
                          fontWeight: FontWeight.bold,
                          fontSize: 24,
                        ),
                      ),
                    ),
                    SizedBox(
                      height: screenHeight(context, dividedBy: 30),
                    ),
                    FormFieldUserEmail(
                      labelText: "Email ",
                      textEditingController: emailTextEditingController,
                      //otp: false,
                    ),
                    SizedBox(
                      height: screenHeight(context, dividedBy: 20),
                    ),
                    FormFieldUserPassword(
                      labelText: "Password",
                      textEditingController: passwordTextEditingController,
                      //otp: false,
                    ),
                    SizedBox(
                      height: screenHeight(context, dividedBy: 20),
                    ),
                    FormFieldUserPassword(
                      labelText: "Confirm Password",
                      textEditingController:
                          confirmPasswordTextEditingController,
                      //otp: false,
                    ),
                    SizedBox(
                      height: screenHeight(context, dividedBy: 20),
                    ),
                    BuildButton(
                      title: "Sign Up",
                      isLoading: loading,
                      onPressed: () {
                        passwordCheck(
                            emailTextEditingController.text,
                            passwordTextEditingController.text,
                            confirmPasswordTextEditingController.text);
                        // push(context, OTPInputPage());
                      },
                    ),
                    SizedBox(
                      height: screenHeight(context, dividedBy: 30),
                    ),
                    SizedBox(
                      height: screenHeight(context, dividedBy: 20),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
