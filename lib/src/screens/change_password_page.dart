import 'package:app_template/src/bloc/user_bloc.dart';
import 'package:app_template/src/models/reset_password_request.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/widgets/build_button.dart';
import 'package:app_template/src/widgets/user_form_field.dart';
import 'package:flutter/material.dart';
import 'package:flutter_styled_toast/flutter_styled_toast.dart';

class ChangePasswordPage extends StatefulWidget {
  @override
  _ChangePasswordPageState createState() => _ChangePasswordPageState();
}

class _ChangePasswordPageState extends State<ChangePasswordPage> {
  TextEditingController passwordTextEditingController = TextEditingController();
  TextEditingController reenterPasswordTextEditingController =
      new TextEditingController();
  TextEditingController oldPasswordTextEditingController =
      new TextEditingController();
  String verificationId;
  bool isLoading = false;
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  UserBloc userBloc = new UserBloc();

  @override
  void initState() {
    userBloc.resetPasswordResponse.listen((event) {}).onError((event) {
      showToastError(event, context);
    });
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          width: screenWidth(context, dividedBy: 1),
          padding: EdgeInsets.symmetric(
              horizontal: screenWidth(context, dividedBy: 16),
              vertical: screenHeight(context, dividedBy: 5.5)),
          child: Padding(
            padding: EdgeInsets.only(
                bottom: MediaQuery.of(context).viewInsets.bottom),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SizedBox(
                  height: screenHeight(context, dividedBy: 20),
                ),
                Align(
                  alignment: Alignment.centerLeft,
                  child: Text(
                    "Reset Password ?",
                    textAlign: TextAlign.left,
                    style: TextStyle(
                      fontSize: 24,
                      fontWeight: FontWeight.bold,
                      color: Constants.kitGradients[30],
                      fontFamily: "Prompt-Light",
                    ),
                  ),
                ),
                Align(
                  alignment: Alignment.centerLeft,
                  child: Text(
                    "Enter your new password below, we're just being extra safe.",
                    style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.normal,
                      color: Constants.kitGradients[19],
                      fontFamily: "Prompt-Light",
                    ),
                  ),
                ),
                SizedBox(
                  height: screenHeight(context, dividedBy: 30),
                ),
                FormFeildUserDetails(
                    labelText: "Current Password",
                    textEditingController: oldPasswordTextEditingController),
                SizedBox(
                  height: screenHeight(context, dividedBy: 30),
                ),
                FormFeildUserDetails(
                    labelText: "New Password",
                    textEditingController: passwordTextEditingController),
                SizedBox(
                  height: screenHeight(context, dividedBy: 30),
                ),
                FormFeildUserDetails(
                    labelText: "Re-Enter New Password",
                    textEditingController:
                        reenterPasswordTextEditingController),
                SizedBox(
                  height: screenHeight(context, dividedBy: 30),
                ),
                BuildButton(
                  onPressed: () {
                    setState(() {
                      isLoading = true;
                    });
                    if (passwordTextEditingController != null &&
                        reenterPasswordTextEditingController != null) {
                      if (passwordTextEditingController.text ==
                          passwordTextEditingController.text) {
                        userBloc.resetPassword(
                            resetPasswordRequest: ResetPasswordRequest(
                                password: "", confirmPassword: "", token: ""));
                      } else {
                        setState(() {
                          isLoading = false;
                        });
                        showToast("Password does not match");
                      }
                    } else {
                      setState(() {
                        isLoading = false;
                      });
                      // showInfoAnimatedToast(
                      //     msg: "Please fill all the fields.", context: context);
                    }

                    setState(() {
                      isLoading = true;
                    });
                  },
                  // label: "Submit",
                  // buttonHeight: 16,
                  title: "Verify",
                  buttonWidth: 3,
                  isLoading: isLoading,
                ),
                SizedBox(
                  height: screenHeight(context, dividedBy: 30),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
