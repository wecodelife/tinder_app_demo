import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

Positioned cardDemoDummy(String img, double bottom, double right, double left,
    double cardWidth, double rotation, double skew, BuildContext context) {
  Size screenSize = MediaQuery.of(context).size;
  // Size screenSize=(500.0,200.0);
  // print("dummyCard");
  int value = 1;
  return new Positioned(
    bottom: screenHeight(context, dividedBy: 4.8) + bottom,
    // right: flag == 0 ? right != 0.0 ? right : null : null,
    //left: flag == 1 ? right != 0.0 ? right : null : null,
    child: new Card(
      elevation: 4.0,
      child: new Container(
        alignment: Alignment.center,
        width: screenSize.width / 1.2,
        height: screenSize.height / 1.7,
        margin: EdgeInsets.symmetric(
          horizontal: screenWidth(context, dividedBy: 40),
        ),
        // width: screenSize.width / 1.2,
        // height: screenSize.height / 1.7,
        decoration: new BoxDecoration(
          //color: new Color.fromRGBO(121, 114, 173, 1.0),
          borderRadius: new BorderRadius.circular(8.0),
        ),
        child: CachedNetworkImage(
          width: screenWidth(context, dividedBy: 1.2),
          height: screenHeight(context, dividedBy: 1.3),
          imageUrl: img,
          placeholder: (context, url) => Shimmer.fromColors(
            baseColor:  Constants.kitGradients[0],
            highlightColor: Constants.kitGradients[27].withOpacity(0.3),
            enabled: true,
            period: Duration(milliseconds: 500),
            child: Center(
              child: Container(
                height: screenHeight(context, dividedBy: 1.2),
                width: screenWidth(context, dividedBy: 1.1),
                margin: EdgeInsets.symmetric(
                    horizontal: screenWidth(context, dividedBy: 30),
                    vertical: screenHeight(context, dividedBy: 80)),
                decoration: BoxDecoration(
                  //border: Border.all(color: Constants.kitGradients[28]),
                  borderRadius: BorderRadius.circular(7),
                  color:Constants.kitGradients[23],
                ),
                alignment: Alignment.center,
              ),
            ),
          ),
          // placeholder: (context, url) => Center(
          //   heightFactor: 1,
          //   widthFactor: 1,
          //   child: SizedBox(
          //     height: 16,
          //     width: 16,
          //     child: CircularProgressIndicator(
          //       valueColor: AlwaysStoppedAnimation(Constants.kitGradients[28]),
          //       strokeWidth: 2,
          //     ),
          //   ),
          // ),
          imageBuilder: (context, imageProvider) {
            return Container(
              width: screenWidth(context, dividedBy: 1.2),
              //height: screenHeight(context, dividedBy: 1.5),
              padding: EdgeInsets.symmetric(
                horizontal: screenWidth(context, dividedBy: 40),
              ),
              //alignment: Alignment.bottomLeft,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(7),
                image: DecorationImage(
                    image: imageProvider, fit: BoxFit.cover),
              ),
            );
          },
        ),
      ),
    ),
  );
}
