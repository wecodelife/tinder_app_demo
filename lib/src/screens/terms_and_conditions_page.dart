import 'package:app_template/src/widgets/app_bar.dart';
import 'package:app_template/src/widgets/creator_name.dart';
import 'package:app_template/src/widgets/terms_list_items.dart';
import 'package:flutter/material.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/utils/constants.dart';

class TermsAndConditions extends StatefulWidget {
  //const TermsAndConditions({Key? key}) : super(key: key);

  @override
  _TermsAndConditionsState createState() => _TermsAndConditionsState();
}

class _TermsAndConditionsState extends State<TermsAndConditions> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Constants.kitGradients[31],
        appBar: AppBar(
          backgroundColor: Constants.kitGradients[31],
          elevation: 0,
          leading: Container(),
          actions: [
            AppBarFoodApp(
              title: "Terms and Conditions",
              leftIcon: Icon(
                Icons.arrow_back_ios,
                color: Constants.kitGradients[28],
              ),
              onPressedLeftIcon: () {
                pop(context);
              },
            ),
          ],
        ),
        body: SingleChildScrollView(
          child: Container(
              width: screenWidth(context, dividedBy: 1),
              //height: screenHeight(context, dividedBy: 1),
              decoration: BoxDecoration(
                color: Constants.kitGradients[31],
              ),
              padding: EdgeInsets.symmetric(
                horizontal: screenWidth(context, dividedBy: 20),
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(
                    height: screenHeight(context, dividedBy: 70),
                  ),
                  Text(
                    "Welcome to Foodie!",
                    textAlign: TextAlign.justify,
                    style: TextStyle(
                      fontSize: 15,
                      fontFamily: "Prompt-Light",
                      color: Constants.kitGradients[19],
                    ),
                  ),
                  SizedBox(
                    height: screenHeight(context, dividedBy: 70),
                  ),
                  Text(
                    "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore\n\n\nLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore",
                    textAlign: TextAlign.justify,
                    style: TextStyle(
                      fontSize: 15,
                      fontFamily: "Prompt-Light",
                      color: Constants.kitGradients[19],
                    ),
                  ),
                  SizedBox(
                    height: screenHeight(context, dividedBy: 30),
                  ),
                  // Text(
                  //   "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore\n\n\nLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore",
                  //   textAlign: TextAlign.justify,
                  //   style: TextStyle(
                  //     fontSize: 15,
                  //     fontFamily: "Prompt-Light",
                  //     color: Constants.kitGradients[19],
                  //   ),
                  // ),
                  // SizedBox(
                  //   height: screenHeight(context, dividedBy: 30),
                  // ),
                  Text(
                    "The Foodie Services",
                    textAlign: TextAlign.left,
                    style: TextStyle(
                      fontSize: 15,
                      fontWeight: FontWeight.w600,
                      fontFamily: "Prompt-Light",
                      color: Constants.kitGradients[19],
                    ),
                  ),
                  SizedBox(
                    height: screenHeight(context, dividedBy: 30),
                  ),
                  Container(
                    child:ListView.builder(
                      itemCount:10,
                      shrinkWrap: true,
                      physics:NeverScrollableScrollPhysics(),
                      itemBuilder: (BuildContext context, int index){
                        return Column(
                          children: [
                            Container(
                              child:TermsListItems(
                                title:
                                "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt",
                                data:
                                "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore",
                              ),
                            ),
                            SizedBox(
                              height: screenHeight(context, dividedBy: 50),
                            ),
                          ],
                        );
                      },
                    )
                  ),

                  // Spacer(),
                  Center(child: CreatorName()),
                  SizedBox(
                    height: screenHeight(context, dividedBy: 60),
                  ),
                ],
              )),
        ),
      ),
    );
  }
}
