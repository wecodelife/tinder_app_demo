import 'package:app_template/src/bloc/user_bloc.dart';
import 'package:app_template/src/models/liked_food_response.dart';
import 'package:app_template/src/screens/home_page_screen.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/object_factory.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/widgets/app_bar.dart';
import 'package:app_template/src/widgets/creator_name.dart';
import 'package:app_template/src/widgets/liked_food_list_view.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:shimmer/shimmer.dart';

class MustVisitItemScreen extends StatefulWidget {
  final bool mustLike;
  final String userId;
  final String mustVisit;
  MustVisitItemScreen({this.mustLike,this.userId,this.mustVisit});
  @override
  _MustVisitItemScreenState createState() => _MustVisitItemScreenState();
}

class _MustVisitItemScreenState extends State<MustVisitItemScreen> {

  UserBloc userBloc = UserBloc();
  List<Result> likeFood = [];
  bool showError = false;
  bool isLoading = false;
  String errorMsg = "";

  @override
  void initState() {
    // TODO: implement initState
    userBloc.getLikedFood(id: "?must_visit=true",
      uid: ObjectFactory().appHive.getUserId()
    );
    /*setState(() {
      isLoading = true;
    });*/

    userBloc.likedFood.listen((event) {
      if(event.statusCode == 200 || event.statusCode == 201) {
        print("Favorite list response successfull");
        for (int i = 0; i < event.results.length; i++) {
          if (event.results[i].mustVisit == true) {
            setState(() {
              likeFood.add(event.results[i]);
            });
          }
        }

        print("Favourite Success");
        print(event.results);
        print(likeFood);
      }
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(
      SystemUiOverlayStyle(
        statusBarColor:Colors.transparent,
        statusBarBrightness: Brightness.dark,
        statusBarIconBrightness: Brightness.dark,

      ),
    );
    return WillPopScope(
      onWillPop: () {
        Navigator.of(context).pushAndRemoveUntil(
            MaterialPageRoute(builder: (context) => HomePageScreen()),
                (Route<dynamic> route) => false);
      },
      child:AnnotatedRegion<SystemUiOverlayStyle>(
        value: SystemUiOverlayStyle(
          statusBarColor: Colors.transparent,
          statusBarBrightness: Brightness.dark,
          statusBarIconBrightness: Brightness.dark,
        ),
        child: Scaffold(
          backgroundColor: Constants.kitGradients[31],
          appBar: AppBar(
            leading: Container(),
            elevation: 0,
            backgroundColor: Constants.kitGradients[31],
            actions: [
              AppBarFoodApp(
                title: "Must Visit",
                leftIcon: Container(
                  child: Icon(
                    Icons.arrow_back_ios,
                    color: Constants.kitGradients[28],
                  ),
                ),
                rightIcon: IconButton(
                  alignment: Alignment.center,
                  icon: Icon(Icons.star,
                    color: Constants.kitGradients[28],
                    size: 25,
                  ),
                ),
                onPressedLeftIcon: () {
                  pushAndRemoveUntil(context, HomePageScreen(), false);
                },
              ),
            ],
          ),
          body: SingleChildScrollView(
            padding: EdgeInsets.zero,
            child: Container(
              color: Constants.kitGradients[31],
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(
                    height: screenHeight(context, dividedBy: 50),
                  ),
                  Container(
                      padding: EdgeInsets.symmetric(
                        horizontal: screenWidth(context, dividedBy: 30),
                      ),
                      child: isLoading == false
                          ? likeFood.length != 0
                          ?  ListView.builder(
                        itemCount: likeFood.length,
                        shrinkWrap: true,
                        reverse: true,
                        physics: NeverScrollableScrollPhysics(),
                        itemBuilder:
                            (BuildContext context, int index) {
                          return LikedFoodListView(
                            shopName:
                            likeFood[index].food.restaurant,
                            foodName: likeFood[index].food.name,
                            index: index,
                            rating: 4.0,
                            price: likeFood[index].food.price,
                            foodType: likeFood[index].food.foodType,
                            likedFoodImageUrl:
                            likeFood[index].food.image1,
                            navigationPage: MustVisitItemScreen(),
                            isLikedList: false,
                            foodId: likeFood[index].food.id,
                            isMustVisit: false,
                          );
                        },
                      )
                          : Container(
                        height: screenHeight(context, dividedBy: 1.2),
                        child: Center(
                            child: Container(
                              width: screenWidth(context, dividedBy: 2.5),
                              height: screenHeight(context, dividedBy: 2.8),
                              child: Column(
                                children: [
                                  Image.asset('assets/images/paziq_logo.png'),
                                  SizedBox(
                                    height: screenHeight(context, dividedBy: 25),
                                  ),
                                  Text("There is nothing in your list",
                                    style: TextStyle(
                                        color: Constants.kitGradients[28],
                                        fontSize: 15,
                                        fontFamily: "OswaldRegular",
                                        fontWeight: FontWeight.bold
                                    ),)
                                ],
                              ),
                              alignment: Alignment.center,)
                        ),
                      )
                          : Shimmer.fromColors(
                        baseColor: Constants.kitGradients[31],
                        highlightColor: Colors.grey[100],
                        enabled: true,
                        child: Container(
                          height: screenHeight(context, dividedBy: 3),
                          width: screenWidth(context, dividedBy:1),
                          padding: EdgeInsets.symmetric(
                              horizontal: screenWidth(context, dividedBy: 30),
                              vertical: screenHeight(context, dividedBy: 80)),
                          decoration: BoxDecoration(
                            color: Constants.kitGradients[31],
                            border: Border.all(color: Constants.kitGradients[31]),
                            borderRadius: BorderRadius.all(Radius.circular(15)),
                          ),
                          alignment: Alignment.center,
                        ),
                      )

                  ),
                  likeFood.length == 1
                      ? SizedBox(
                    height: screenHeight(context, dividedBy: 2),
                  ):
                  likeFood.length == 2
                      ? SizedBox(
                    height: screenHeight(context, dividedBy: 7),
                  ):
                  SizedBox(
                    height: screenHeight(context, dividedBy: 90),
                  ),
                  Center(child: CreatorName()),
                  SizedBox(
                    height: screenHeight(context, dividedBy: 100),
                  ),
                  // Row(
                  //   crossAxisAlignment: CrossAxisAlignment.start,
                  //   mainAxisAlignment:MainAxisAlignment.start,
                  //
                  //   children: [
                  //     SizedBox(
                  //       width: 10,
                  //     ),
                  //     Expanded(
                  //       child: likedFood.length != 0
                  //           ? ListView.builder(
                  //               padding: EdgeInsets.zero,
                  //               itemCount: likedFood.length,
                  //               shrinkWrap: true,
                  //               physics: NeverScrollableScrollPhysics(),
                  //               itemBuilder: (BuildContext context, int index) {
                  //                 print("Liked Food" + index.toString());
                  //                 return index % 2 == 0
                  //                     ? Column(
                  //                         children: [
                  //                           ClipRRect(
                  //                             borderRadius:
                  //                                 BorderRadius.circular(20),
                  //                             child: GestureDetector(
                  //                               onTap: () {
                  //                                 push(
                  //                                     context,
                  //                                     FoodInformationPage(
                  //                                       navigationWidget:
                  //                                           LikedItemsList(),
                  //                                     ));
                  //                               },
                  //                               child: CachedNetworkImage(
                  //                                 height: screenHeight(context,
                  //                                     dividedBy: 2.4),
                  //                                 width: screenWidth(context,
                  //                                     dividedBy: 2),
                  //                                 fit: BoxFit.cover,
                  //                                 imageUrl: likedFood[index]
                  //                                             .food
                  //                                             .image1 ==
                  //                                         null
                  //                                     ? "https://images.unsplash.com/photo-1476224203421-9ac39bcb3327?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MTJ8fGZvb2R8ZW58MHx8MHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=600&q=60"
                  //                                     : "https://images.unsplash.com/photo-1476224203421-9ac39bcb3327?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MTJ8fGZvb2R8ZW58MHx8MHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=600&q=60",
                  //                                 imageBuilder:
                  //                                     (context, imageProvider) =>
                  //                                         Container(
                  //                                   height: screenHeight(context,
                  //                                       dividedBy: 2.4),
                  //                                   width: screenWidth(context,
                  //                                       dividedBy: 2),
                  //                                   decoration: BoxDecoration(
                  //                                     image: DecorationImage(
                  //                                       image: imageProvider,
                  //                                       fit: BoxFit.cover,
                  //                                     ),
                  //                                     borderRadius:
                  //                                         BorderRadius.circular(
                  //                                             20),
                  //                                   ),
                  //                                   child: Stack(
                  //                                     children: [
                  //                                       InformationIcon(
                  //                                         deleteSaved: true,
                  //                                         icon: Icons.favorite,
                  //                                         icon2: Icons
                  //                                             .favorite_outline,
                  //                                         onPressed: () {},
                  //                                         rightPadding: 20,
                  //                                       ),
                  //                                       Column(
                  //                                         crossAxisAlignment:
                  //                                             CrossAxisAlignment
                  //                                                 .center,
                  //                                         mainAxisAlignment:
                  //                                             MainAxisAlignment
                  //                                                 .end,
                  //                                         children: [
                  //                                           FavouritesBox(
                  //                                             id: 1,
                  //                                             foodName: "Chicken",
                  //                                             price: "19",
                  //                                             foodType: likedFood[
                  //                                                             index]
                  //                                                         .food
                  //                                                         .foodType ==
                  //                                                     null
                  //                                                 ? "Not Available"
                  //                                                 : "Non-Veg",
                  //                                             hotelName:
                  //                                                 likedFood[index]
                  //                                                     .food
                  //                                                     .restaurant,
                  //                                             rating: 19,
                  //                                           ),
                  //                                           SizedBox(
                  //                                             height:
                  //                                                 screenHeight(
                  //                                                     context,
                  //                                                     dividedBy:
                  //                                                         60),
                  //                                           ),
                  //                                         ],
                  //                                       ),
                  //                                     ],
                  //                                   ),
                  //                                 ),
                  //                                 placeholder: (context, url) =>
                  //                                     Center(
                  //                                   heightFactor: 1,
                  //                                   widthFactor: 1,
                  //                                   child: SizedBox(
                  //                                     height: 16,
                  //                                     width: 16,
                  //                                     child:
                  //                                         CircularProgressIndicator(
                  //                                       valueColor:
                  //                                           AlwaysStoppedAnimation(
                  //                                               Constants
                  //                                                       .kitGradients[
                  //                                                   28]),
                  //                                       strokeWidth: 2,
                  //                                     ),
                  //                                   ),
                  //                                 ),
                  //                               ),
                  //                             ),
                  //                           ),
                  //                           SizedBox(
                  //                             height: screenHeight(context,
                  //                                 dividedBy: 30),
                  //                           )
                  //                         ],
                  //                       )
                  //                     : Container();
                  //               },
                  //             )
                  //           : Container(),
                  //     ),
                  //     SizedBox(
                  //       width: 10,
                  //     ),
                  //     likedFood.length != 1
                  //         ? Expanded(
                  //             child: ListView.builder(
                  //               padding: EdgeInsets.zero,
                  //               itemCount: likedFood.length,
                  //               shrinkWrap: true,
                  //               physics: NeverScrollableScrollPhysics(),
                  //               itemBuilder: (BuildContext context, int index) {
                  //                 return index % 2 == 1
                  //                     ? Column(
                  //                         children: [
                  //                           index == 1
                  //                               ? Container(
                  //                                   height: screenHeight(context,
                  //                                       dividedBy: 16),
                  //                                 )
                  //                               : Container(),
                  //                           ClipRRect(
                  //                             borderRadius:
                  //                                 BorderRadius.circular(20),
                  //                             child: GestureDetector(
                  //                               onTap: () {
                  //                                 push(
                  //                                     context,
                  //                                     FoodInformationPage(
                  //                                       navigationWidget:
                  //                                           LikedItemsList(),
                  //                                     ));
                  //                               },
                  //                               child: CachedNetworkImage(
                  //                                 height: screenHeight(context,
                  //                                     dividedBy: 2.4),
                  //                                 width: screenWidth(context,
                  //                                     dividedBy: 2),
                  //                                 fit: BoxFit.cover,
                  //                                 imageUrl: likedFood[index]
                  //                                             .food
                  //                                             .image1 ==
                  //                                         null
                  //                                     ? "https://images.unsplash.com/photo-1476224203421-9ac39bcb3327?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MTJ8fGZvb2R8ZW58MHx8MHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=600&q=60"
                  //                                     : likedFood[index]
                  //                                         .food
                  //                                         .image1,
                  //                                 imageBuilder:
                  //                                     (context, imageProvider) =>
                  //                                         Container(
                  //                                   height: screenHeight(context,
                  //                                       dividedBy: 2.4),
                  //                                   width: screenWidth(context,
                  //                                       dividedBy: 2),
                  //                                   decoration: BoxDecoration(
                  //                                     image: DecorationImage(
                  //                                       image: imageProvider,
                  //                                       fit: BoxFit.cover,
                  //                                     ),
                  //                                     borderRadius:
                  //                                         BorderRadius.circular(
                  //                                             20),
                  //                                   ),
                  //                                   child: Stack(
                  //                                     children: [
                  //                                       InformationIcon(
                  //                                         deleteSaved: true,
                  //                                         icon: Icons.favorite,
                  //                                         icon2: Icons
                  //                                             .favorite_outline,
                  //                                         onPressed: () {},
                  //                                         rightPadding: 20,
                  //                                       ),
                  //                                       Column(
                  //                                         crossAxisAlignment:
                  //                                             CrossAxisAlignment
                  //                                                 .center,
                  //                                         mainAxisAlignment:
                  //                                             MainAxisAlignment
                  //                                                 .end,
                  //                                         children: [
                  //                                           FavouritesBox(
                  //                                             id: 1,
                  //                                             foodName:
                  //                                                 likedFood[index]
                  //                                                     .food
                  //                                                     .name,
                  //                                             price: "19",
                  //                                             foodType: likedFood[
                  //                                                             index]
                  //                                                         .food
                  //                                                         .foodType ==
                  //                                                     null
                  //                                                 ? "Not Available"
                  //                                                 : "Non-Veg",
                  //                                             hotelName:
                  //                                                 likedFood[index]
                  //                                                     .food
                  //                                                     .restaurant,
                  //                                             rating: 19,
                  //                                           ),
                  //                                           SizedBox(
                  //                                             height:
                  //                                                 screenHeight(
                  //                                                     context,
                  //                                                     dividedBy:
                  //                                                         60),
                  //                                           ),
                  //                                         ],
                  //                                       ),
                  //                                     ],
                  //                                   ),
                  //                                 ),
                  //                                 placeholder: (context, url) =>
                  //                                     Center(
                  //                                   heightFactor: 1,
                  //                                   widthFactor: 1,
                  //                                   child: SizedBox(
                  //                                     height: 16,
                  //                                     width: 16,
                  //                                     child:
                  //                                         CircularProgressIndicator(
                  //                                       valueColor:
                  //                                           AlwaysStoppedAnimation(
                  //                                               Constants
                  //                                                       .kitGradients[
                  //                                                   28]),
                  //                                       strokeWidth: 2,
                  //                                     ),
                  //                                   ),
                  //                                 ),
                  //                               ),
                  //                             ),
                  //                           ),
                  //                           SizedBox(
                  //                             height: screenHeight(context,
                  //                                 dividedBy: 30),
                  //                           )
                  //                         ],
                  //                       )
                  //                     : Container(
                  //                         height: 1,
                  //                       );
                  //               },
                  //             ),
                  //           )
                  //         : Container(
                  //             height: 1,
                  //           ),
                  //     SizedBox(
                  //       width: 10,
                  //     ),
                  //   ],
                  // ),
                ],
              ),
            ),
          ),
          // DragabbleTabBar(
          //   currentIndex: 1,
          //   onTapLiked: () {
          //     //push(context, LikedItemsList());
          //   },
          //   onTapSaved: () {
          //     push(context, SavedItemsList());
          //   },
          //   onTapHome: () {
          //     push(context, HomeScreen());
          //   },
          //   // onTapChat: () {
          //   //   push(context, ChatListPage());
          //   // },
          // ),
        ),
      ),
    );
  }
}
