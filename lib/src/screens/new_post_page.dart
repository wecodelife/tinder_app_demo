 import 'dart:io';
import 'dart:typed_data';

import 'package:app_template/src/screens/filter_image_page.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/widgets/app_bar_dog_app.dart';
import 'package:app_template/src/widgets/bottom_sheet_item.dart';
import 'package:app_template/src/widgets/filter_utilities.dart';
import 'package:app_template/src/widgets/onboarding_textfield.dart';
import 'package:app_template/src/widgets/profile_icon.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:image/image.dart' as img;
import 'package:image_picker/image_picker.dart';

class NewPostPage extends StatefulWidget {
  // const NewPostPage({Key? key}) : super(key: key);
  final String profileIcon;
  final String userName;
  final Uint8List filteredImage;
  NewPostPage({this.profileIcon, this.userName, this.filteredImage});
  @override
  _NewPostPageState createState() => _NewPostPageState();
}

class _NewPostPageState extends State<NewPostPage> {
  TextEditingController captionTextEditingController =
      new TextEditingController();
  TextEditingController locationTextEditingController =
      new TextEditingController();

  final GlobalKey<NavigatorState> navigatorKey =
      new GlobalKey<NavigatorState>();

  //image picker
  img.Image image;
  File _image;
  bool isLoading;

  Future imgFromGallery(context) async {
    final image = await ImagePicker()
        .getImage(source: ImageSource.gallery, imageQuality: 50);
    final imageBytes = File(image.path).readAsBytesSync();
    final newImage = img.decodeImage(imageBytes);
    newImage == null
        ? setState(() {
            isLoading = true;
          })
        : setState(() {
            isLoading = false;
          });
    FilterUtils.clearCache();
    pop(context);
    setState(() {
      if (image != null) {
        _image = File(image.path);
        push(
            context,
            FilterImagePage(
              selectedImage: _image,
              image: newImage,
              loading: true,
            ));
      } else {
        print('No image selected.');
      }
    });
    // Future.delayed(const Duration(milliseconds: 500), () {
    //
    // // Here you can write your code
    //   push(
    //       context,
    //       FilterImagePage(
    //         selectedImage: _image,
    //         image: newImage,
    //         loading: isLoading,
    //       ));
    //
    //   setState(() {
    //     // Here you can write your code for open new view
    //     isLoading = true ;
    //   });

    //});
  }

  // getFilter(image) async {
  //   final imageBytes = File(image.path).readAsBytesSync();
  //   final newImage = img.decodeImage(imageBytes);
  //   FilterUtils.clearCache();
  //   pop(context);
  //   setState(() {
  //     if (image != null) {
  //       _image = File(image.path);
  //       push(
  //           context,
  //           FilterImagePage(
  //             selectedImage: _image,
  //             image: newImage,
  //           ));
  //     } else {
  //       print('No image selected.');
  //     }
  //   });
  // }

  int selected;
  void showPicker(context) {
    Future<void> future = showModalBottomSheet(
        context: context,
        backgroundColor: Constants.kitGradients[31],
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(8), topRight: Radius.circular(8))),
        builder: (BuildContext context) {
          return Container(
            // height:screenHeight(context, dividedBy:4),
            padding: EdgeInsets.symmetric(
              horizontal: screenWidth(context, dividedBy: 30),
              vertical: screenHeight(context, dividedBy: 30),
            ),
            decoration: BoxDecoration(
                color: Constants.kitGradients[31],
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(8), topRight: Radius.circular(8))),
            child: Wrap(
              //crossAxisAlignment:CrossAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.symmetric(
                    horizontal: screenWidth(context, dividedBy: 20),
                    //vertical: screenHeight(context, dividedBy: 20),
                  ),
                  child: Text(
                    "Select Picture from",
                    style: TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.bold,
                      fontFamily: "Montserrat",
                      color: Constants.kitGradients[28],
                    ),
                  ),
                ),
                SizedBox(
                  height: screenHeight(context, dividedBy: 15),
                ),
                BottomSheetItem(
                    title: "Photo Library",
                    leading: new Icon(
                      Icons.photo_library,
                      color: Constants.kitGradients[28],
                    ),
                    trailing: null,
                    onTap: () async {
                      imgFromGallery(context);
                      // pop(context);
                    }

                    // },
                    ),
                Divider(
                  color: Constants.kitGradients[28].withOpacity(0.12),
                  indent: 10,
                  endIndent: 10,
                ),
                BottomSheetItem(
                  title: "Camera",
                  leading: new Icon(
                    Icons.photo_camera,
                    color: Constants.kitGradients[28],
                  ),
                  trailing: null,
                  onTap: () async {
                    final image = await ImagePicker()
                        .getImage(source: ImageSource.camera);
                    final imageBytes = File(image.path).readAsBytesSync();
                    final newImage = img.decodeImage(imageBytes);
                    imageBytes == null
                        ? setState(() {
                            isLoading = true;
                          })
                        : setState(() {
                            isLoading = false;
                          });
                    FilterUtils.clearCache();
                    print("outside loop");
                    if (image != null) {
                      _image = File(image.path);
                      push(
                          context,
                          FilterImagePage(
                            selectedImage: _image,
                            image: newImage,
                            loading: true,
                          ));
                    } else {
                      print('No image selected.');
                    }
                  },
                ),
                Divider(
                  color: Constants.kitGradients[28].withOpacity(0.2),
                  indent: 10,
                  endIndent: 10,
                ),
              ],
            ),
          );
        });
    future.then((void value) => print("closed modal"));
  }

  @override
  Widget build(BuildContext context) {
    return isLoading == true
        ? Container(
            width: screenWidth(context, dividedBy: 2),
            height: screenHeight(context, dividedBy: 2),
            color: Constants.kitGradients[31],
            child: Center(child: CircularProgressIndicator()))
        : Scaffold(
            backgroundColor: Constants.kitGradients[31],
            appBar: AppBar(
              elevation: 1,
              backgroundColor: Constants.kitGradients[28],
              actions: [
                AppBarDogApp(
                  leftIcon: Icon(
                    Icons.arrow_back,
                    color: Constants.kitGradients[27],
                  ),
                  onTapLeftIcon: () {
                    pop(context);
                  },
                  rightIcon: Text(
                    "Done",
                    style: TextStyle(
                      fontSize: 17,
                      fontFamily: 'Montserrat-bold',
                      fontWeight: FontWeight.w600,
                      color: Constants.kitGradients[27],
                    ),
                  ),
                  onTapRightIcon: () {},
                  title: Text(
                    "New Post",
                    style: TextStyle(
                      fontSize: 17,
                      fontFamily: 'Montserrat',
                      fontWeight: FontWeight.w600,
                      color: Constants.kitGradients[27],
                    ),
                  ),
                )
              ],
              leading: Container(),
            ),
            body: SingleChildScrollView(
              child: Container(
                padding: EdgeInsets.symmetric(
                  horizontal: screenWidth(context, dividedBy: 30),
                  //vertical: screenHeight(context, dividedBy: 20),
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(
                      height: screenHeight(context, dividedBy: 50),
                    ),
                    Row(
                      children: [
                        ProfileIcon(
                          profileImage: widget.profileIcon,
                          circleRadius: 7,
                        ),
                        SizedBox(width: screenWidth(context, dividedBy: 40)),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              widget.userName,
                              style: TextStyle(
                                fontSize: 17,
                                fontFamily: 'Montserrat',
                                fontWeight: FontWeight.w600,
                                color: Constants.kitGradients[28],
                              ),
                            ),
                            SizedBox(
                              height: screenHeight(context, dividedBy: 200),
                            ),
                            GestureDetector(
                                child: Row(
                              // crossAxisAlignment: CrossAxisAlignment.start,
                              // mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                Icon(Icons.privacy_tip_outlined,
                                    color: Constants.kitGradients[28],
                                    size: 12),
                                SizedBox(
                                    width:
                                        screenWidth(context, dividedBy: 100)),
                                Text(
                                  "Public",
                                  style: TextStyle(
                                    fontSize: 12,
                                    fontFamily: 'Montserrat',
                                    fontWeight: FontWeight.w600,
                                    color: Constants.kitGradients[28],
                                  ),
                                )
                              ],
                            )),
                          ],
                        )
                      ],
                    ),
                    SizedBox(height: screenHeight(context, dividedBy: 30)),
                    Container(
                      width: screenWidth(context, dividedBy: 1),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          GestureDetector(
                            onTap: () {
                              showPicker(context);
                            },
                            child: Container(
                              height: screenWidth(context, dividedBy: 3.3),
                              width: screenWidth(context, dividedBy: 3.5),
                              decoration: BoxDecoration(
                                color: Constants.kitGradients[28]
                                    .withOpacity(0.12),
                                borderRadius: BorderRadius.circular(8),
                              ),
                              // constraints: BoxConstraints(
                              //   maxHeight: screenWidth(context, dividedBy: 3.5),
                              //   maxWidth: screenWidth(context, dividedBy: 3.5),
                              // ),
                              child: ClipRRect(
                                borderRadius: BorderRadius.circular(8),
                                child: widget.filteredImage == null
                                    ? Icon(
                                        Icons.add_a_photo_rounded,
                                        color: Constants.kitGradients[28],
                                        size: 30,
                                      )
                                    : Image.memory(
                                        widget.filteredImage,
                                        fit: BoxFit.fitHeight,
                                        height: screenWidth(context,
                                            dividedBy: 3.2),
                                        width: screenWidth(context,
                                            dividedBy: 3.5),
                                      ),
                              ),
                            ),
                          ),
                          SizedBox(width: screenWidth(context, dividedBy: 40)),
                          Container(
                            width: screenWidth(context, dividedBy: 1.65),
                            child: OnBoardingTextField(
                              labelText: "Type something..",
                              textEditingController:
                                  captionTextEditingController,
                              hasSuffix: false,
                              suffixIcon: Container(),
                              onTapIcon: () {},
                              textArea: true,
                              onTap: () {},
                              readOnly: false,
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(height: screenHeight(context, dividedBy: 30)),
                    Text(
                      "Add Location",
                      style: TextStyle(
                        fontSize: 14,
                        fontFamily: 'Montserrat',
                        fontWeight: FontWeight.w600,
                        color: Constants.kitGradients[28],
                      ),
                    ),
                    SizedBox(height: screenHeight(context, dividedBy: 50)),
                    Container(
                      child: OnBoardingTextField(
                        labelText: "Location",
                        textEditingController: locationTextEditingController,
                        hasSuffix: false,
                        suffixIcon: Container(),
                        onTapIcon: () {},
                        textArea: false,
                        onTap: () {},
                        readOnly: false,
                      ),
                    ),
                    SizedBox(height: screenHeight(context, dividedBy: 30)),
                    // Divider(color: Constants.kitGradients[27].withOpacity(0.2),),
                    // Text("Tags",
                    //   style: TextStyle(
                    //     fontSize: 14,
                    //     fontFamily: 'Montserrat',
                    //     fontWeight: FontWeight.w600,
                    //     color: Constants.kitGradients[27],
                    //   ),),
                    // SizedBox(height: screenHeight(context, dividedBy: 50)),
                    // Container(
                    //   child: OnBoardingTextField(
                    //     labelText: "",
                    //     textEditingController: captionTextEditingController,
                    //     hasSuffix: false,
                    //     suffixIcon: Container(),
                    //     onTapIcon: () {},
                    //     textArea: false,
                    //     onTap: () {},
                    //     readOnly: false,
                    //   ),
                    // ),
                  ],
                ),
              ),
            ),
          );
  }
}
