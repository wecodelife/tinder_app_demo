import 'package:app_template/src/screens/feed_page.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/widgets/app_bar_dog_app.dart';
import 'package:app_template/src/widgets/bottom_bar.dart';
import 'package:app_template/src/widgets/bottom_sheet_item.dart';
import 'package:app_template/src/widgets/profile_icon.dart';
import 'package:app_template/src/widgets/profile_tab.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

class ProfileFeedPage extends StatefulWidget {
  final String profilePic;
  final String name;
  ProfileFeedPage({this.name, this.profilePic});
  @override
  _ProfileFeedPageState createState() => _ProfileFeedPageState();
}

//ProfileTabBar
class _ProfileFeedPageState extends State<ProfileFeedPage> {
  Widget example() {
    return new DropdownButton(
        isExpanded: true,
        items: [
          new DropdownMenuItem(child: new Text("Abc")),
          new DropdownMenuItem(child: new Text("Xyz")),
        ],
        hint: new Text("Select City"),
        onChanged: null);
  }

  List<String> image = [
    'https://images.unsplash.com/photo-1520342868574-5fa3804e551c?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=6ff92caffcdd63681a35134a6770ed3b&auto=format&fit=crop&w=1951&q=80',
    'https://images.unsplash.com/photo-1522205408450-add114ad53fe?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=368f45b0888aeb0b7b08e3a1084d3ede&auto=format&fit=crop&w=1950&q=80',
    'https://images.unsplash.com/photo-1519125323398-675f0ddb6308?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=94a1e718d89ca60a6337a6008341ca50&auto=format&fit=crop&w=1950&q=80',
    'https://images.unsplash.com/photo-1523205771623-e0faa4d2813d?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=89719a0d55dd05e2deae4120227e6efc&auto=format&fit=crop&w=1953&q=80',
    'https://images.unsplash.com/photo-1508704019882-f9cf40e475b4?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=8c6e5e3aba713b17aa1fe71ab4f0ae5b&auto=format&fit=crop&w=1352&q=80',
    'https://images.unsplash.com/photo-1519985176271-adb1088fa94c?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=a0c8d632e977f94e5d312d9893258f59&auto=format&fit=crop&w=1355&q=80',
    'https://images.unsplash.com/photo-1619009642046-c1961c2a73e7?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxleHBsb3JlLWZlZWR8Mzd8fHxlbnwwfHx8fA%3D%3D&auto=format&fit=crop&w=600&q=60',
    // 'https://images.unsplash.com/photo-1619092738159-9801f5f07cbe?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxleHBsb3JlLWZlZWR8Mzh8fHxlbnwwfHx8fA%3D%3D&auto=format&fit=crop&w=600&q=60',
    // 'https://images.unsplash.com/photo-1619105587572-e2c62edf1db2?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxleHBsb3JlLWZlZWR8Mzl8fHxlbnwwfHx8fA%3D%3D&auto=format&fit=crop&w=600&q=60',
    // 'https://images.unsplash.com/photo-1619100244920-b417eff59772?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxleHBsb3JlLWZlZWR8NDB8fHxlbnwwfHx8fA%3D%3D&auto=format&fit=crop&w=600&q=60',
    // 'https://images.unsplash.com/photo-1619098501887-a70a4b628f9f?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxleHBsb3JlLWZlZWR8NDF8fHxlbnwwfHx8fA%3D%3D&auto=format&fit=crop&w=600&q=60'
  ];

  List<String> tagged = [
    'https://images.unsplash.com/photo-1520342868574-5fa3804e551c?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=6ff92caffcdd63681a35134a6770ed3b&auto=format&fit=crop&w=1951&q=80',
    'https://images.unsplash.com/photo-1522205408450-add114ad53fe?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=368f45b0888aeb0b7b08e3a1084d3ede&auto=format&fit=crop&w=1950&q=80',
    'https://images.unsplash.com/photo-1519125323398-675f0ddb6308?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=94a1e718d89ca60a6337a6008341ca50&auto=format&fit=crop&w=1950&q=80',
    'https://images.unsplash.com/photo-1523205771623-e0faa4d2813d?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=89719a0d55dd05e2deae4120227e6efc&auto=format&fit=crop&w=1953&q=80',
    'https://images.unsplash.com/photo-1508704019882-f9cf40e475b4?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=8c6e5e3aba713b17aa1fe71ab4f0ae5b&auto=format&fit=crop&w=1352&q=80',
    'https://images.unsplash.com/photo-1519985176271-adb1088fa94c?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=a0c8d632e977f94e5d312d9893258f59&auto=format&fit=crop&w=1355&q=80'
  ];

  List<bool> captionStatus = [true, true, true, true, true, true, true];

  List<String> caption = [
    "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.",
    "",
    "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
    "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.",
    "",
    "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
    "",
  ];
  showPicker() {
    showModalBottomSheet(
        context: context,
        backgroundColor: Constants.kitGradients[31],
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(8), topRight: Radius.circular(8))),
        builder: (BuildContext context) {
          return Container(
            // height:screenHeight(context, dividedBy:4),
            padding: EdgeInsets.symmetric(
              horizontal: screenWidth(context, dividedBy: 30),
              vertical: screenHeight(context, dividedBy: 30),
            ),
            decoration: BoxDecoration(
                color: Constants.kitGradients[31],
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(8), topRight: Radius.circular(8))),
            child: Wrap(
              //crossAxisAlignment:CrossAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.symmetric(
                    horizontal: screenWidth(context, dividedBy: 30),
                    //vertical: screenHeight(context, dividedBy: 20),
                  ),
                  child: Text(
                    "Choose account",
                    style: TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.bold,
                      fontFamily: "Montserrat",
                      color: Constants.kitGradients[28],
                    ),
                  ),
                ),
                SizedBox(
                  height: screenHeight(context, dividedBy: 20),
                ),
                BottomSheetItem(
                  title: widget.name,
                  leading: ProfileIcon(
                      profileImage: widget.profilePic, circleRadius: 10),
                  trailing: Icon(Icons.pets_rounded,
                      color: Constants.kitGradients[28], size: 20),
                  onTap: () {
                    //imgFromGallery();
                    //showToast("msg");
                    Navigator.of(context).pop();
                  },
                ),
                Divider(
                  color: Constants.kitGradients[28].withOpacity(0.2),
                  indent: 10,
                  endIndent: 10,
                ),
                BottomSheetItem(
                  title: "Kines Bi",
                  leading: ProfileIcon(
                      profileImage: widget.profilePic, circleRadius: 8),
                  trailing: null,
                  onTap: () {
                    //imgFromCamera();
                    Navigator.of(context).pop();
                  },
                ),
                Divider(
                  color: Constants.kitGradients[27].withOpacity(0.2),
                  indent: 10,
                  endIndent: 10,
                ),
                BottomSheetItem(
                  title: "Add a paw",
                  leading: Icon(Icons.add, color: Constants.kitGradients[28]),
                  trailing: null,
                  onTap: () {
                    //imgFromCamera();
                    Navigator.of(context).pop();
                  },
                ),
              ],
            ),
          );
        });
  }

  String accountName;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Constants.kitGradients[31],
      appBar: AppBar(
          backgroundColor: Constants.kitGradients[31],
          elevation: 0,
          leading: Container(),
          actions: [
            AppBarDogApp(
                leftIcon: Icon(
                  Icons.lock_outline_rounded,
                  color: Constants.kitGradients[28],
                ),
                onTapLeftIcon: () {},
                rightIcon: Icon(
                  Icons.settings_sharp,
                  color: Constants.kitGradients[28],
                ),
                onTapRightIcon: () {},
                title: GestureDetector(
                    onTap: () {
                      showPicker();
                    },
                    child: Row(
                      children: [
                        Text(
                          widget.name,
                          style: TextStyle(
                            fontSize: 16,
                            fontWeight: FontWeight.w700,
                            fontFamily: 'Montserrat',
                            color: Constants.kitGradients[28],
                          ),
                        ),
                        Icon(
                          Icons.arrow_drop_down,
                          color: Constants.kitGradients[28],
                        )
                      ],
                    ))
                // SwitchAccountDropdown(
                //   title: widget.name,
                //   dropDownList: [
                //     widget.name,
                //     "Bi Kii",
                //     "Viness",
                //   ],
                //   dropDownValue: accountName,
                //   onClicked: (value) {
                //     setState(() {
                //       accountName = value;
                //     });
                //   },
                // ),
                ),
          ]),
      body: Stack(
        children: [
          Container(
            height: screenHeight(context, dividedBy: 1),
            width: screenWidth(context, dividedBy: 1),
            padding: EdgeInsets.symmetric(
              horizontal: screenWidth(context, dividedBy: 30),
            ),
            child: SingleChildScrollView(
              child: Column(
                //crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(height: screenHeight(context, dividedBy: 40)),
                  Center(
                    child: Container(
                        height: screenWidth(context, dividedBy: 3.7),
                        width: screenWidth(context, dividedBy: 3.7),
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          color: Constants.kitGradients[28].withOpacity(0.2),
                        ),
                        child: CachedNetworkImage(
                          // height: screenWidth(context, dividedBy: 8),
                          // width: screenWidth(context, dividedBy: 8),
                          imageUrl: widget.profilePic,
                          fit: BoxFit.fill,
                          imageBuilder: (context, imageProvider) {
                            return Container(
                              height: screenWidth(context, dividedBy: 3.7),
                              width: screenWidth(context, dividedBy: 3.7),
                              decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  image: DecorationImage(
                                      image: imageProvider, fit: BoxFit.cover)),
                            );
                          },
                          placeholder: (context, url) => Center(
                            heightFactor: 1,
                            widthFactor: 1,
                            child: SizedBox(
                              height: 16,
                              width: 16,
                              child: CircularProgressIndicator(
                                valueColor: AlwaysStoppedAnimation(
                                    Constants.kitGradients[28]),
                                strokeWidth: 2,
                              ),
                            ),
                          ),
                        )),
                  ),
                  SizedBox(height: screenHeight(context, dividedBy: 70)),
                  Text(
                    "Bi Kines",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.w700,
                      fontFamily: 'Montserrat',
                      color: Constants.kitGradients[28],
                    ),
                  ),
                  Text(
                    "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
                    textAlign: TextAlign.justify,
                    style: TextStyle(
                      fontSize: 15,
                      fontWeight: FontWeight.w200,
                      fontFamily: 'Prompt-Light',
                      color: Constants.kitGradients[28],
                    ),
                  ),
                  SizedBox(height: screenHeight(context, dividedBy: 50)),
                  ProfileTabBar(
                    imgCollection: image,
                    taggedImages: tagged,
                    hasCaption: captionStatus,
                    captions: caption,
                    userName: widget.name,
                    userProfile: widget.profilePic,
                  ),
                ],
              ),
            ),
          ),
          Positioned(
            bottom: 0,
            child: CustomBottomBar(
              currentIndex: 2,
              onTapSearch: () {
                // push(context, MarketPlacePage());
              },
              onTapProfile: () {},
              onTapHome: () {
                push(
                    context,
                    FeedPage(
                        // name: "Bi",
                        // profilePic:
                        //     "https://img.17qq.com/images/fjjhjggbebz.jpeg",
                        ));
              },
            ),
          )
        ],
      ),
    );
  }
}
// SwitchAccountDropdown(
// title: widget.name,
// dropDownList: ["Bi Kii", "Viness"],
// dropDownValue: accountName,
// onClicked: (value) {
// setState(() {
// accountName = value;
// });
// },
// )
