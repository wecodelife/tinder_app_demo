
//import 'dart:html'
import 'package:app_template/src/models/get_food_desrption_response_model.dart';
import 'package:app_template/src/models/get_reviews_response_model.dart';
import 'package:app_template/src/models/update_review_request_model.dart';
import 'package:app_template/src/screens/home_page_screen.dart';
import 'package:app_template/src/screens/liked_item_screen.dart';
import 'package:app_template/src/screens/must_vist_screen.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/object_factory.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/widgets/build_button.dart';
import 'package:app_template/src/widgets/food_detail_widget.dart';
import 'package:app_template/src/widgets/heading_widget.dart';
import 'package:app_template/src/widgets/rating_widget.dart';
import 'package:app_template/src/widgets/star_rating_bar.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_pro/carousel_pro.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:flutter_styled_toast/flutter_styled_toast.dart';
import 'package:flutter/scheduler.dart' show timeDilation;
import '../bloc/user_bloc.dart';

class FoodInformationPage extends StatefulWidget {
  final Widget navigationWidget;
  final int foodId;
  final int pageIndex;
  final bool count;
  final bool mustLike;
  final List<String> pageViewImage;

  FoodInformationPage({this.navigationWidget, this.foodId,this.pageIndex,this.count,this.mustLike,this.pageViewImage});
  @override
  _FoodInformationPageState createState() => _FoodInformationPageState();
}

class _FoodInformationPageState extends State<FoodInformationPage>
    with SingleTickerProviderStateMixin {
  //animations
  AnimationController _controller;
  Animation<double> _heightFactorAnimation;
  ScrollController scrollController;
  int counter = 1;
  ScrollController scrollControllerReview = new ScrollController();
  double collapsedHeightFactor = 200;
  double expandedHeightFactor = 0.67;
  bool isAnimationCompleted = false;
  BoxFit boxFit = BoxFit.cover;
  List<String> imgList = [
    "assets/images/cafe.jpg",
    "assets/images/gelato.jpg",
    "assets/images/tacos.jpg",
  ];
  DragStartDetails startVerticalDragDetails;
  DragUpdateDetails updateVerticalDragDetails;
  bool expanded = false;
  int index = 0;
  bool reverse = false;
  String message = "";
  String messageReview = "";
  bool errorReview = false;
  bool moreReview = false;
  List<String> imageUrl = [];
  List<Image> images = [];
  bool drag= true;
  bool bottomClose=true;
  bool imageChange;
  bool isMoreReviewAvailable = false;
  UserBloc userBloc = new UserBloc();
  TextEditingController reviewTextEditingController =
  new TextEditingController();
  double rating = 0;
  bool isLoading = false;
  bool hasReview;
  double _width = 100;
  double _height = 50;

  Future<bool> _willPopCallback() async {
    print("--------------------"+widget.count.toString());

    if(widget.count == false){
    Navigator.pushReplacement(
      context,
      MaterialPageRoute(
          builder: (context) =>
              HomePageScreen(
                foodId: widget.foodId,
                pageIndex: widget.pageIndex,
                navigationWidget: FoodInformationPage(foodId: widget.foodId,),

              ))

    );
    }
    else if(widget.mustLike== false){
     // pop(context);
      Navigator.pushReplacement(
          context,
          MaterialPageRoute(
              builder: (context) =>
                  MustVisitItemScreen(
                    mustLike: true,
                  ))
          );
    }
        else{
      Navigator.pushReplacement(
          context,
          MaterialPageRoute(
              builder: (context) =>
                  LikedItemsScreen(
                    mustLike: true,
                  ))
      );
    }



    }

  _scrollListener() {
    //timeDilation=0.5;
    if (scrollController.offset >= scrollController.position.minScrollExtent &&
        !scrollController.position.outOfRange) {
      setState(() {
        bottomClose=false;
        drag=false;

        print(bottomClose.toString()+"aa");
      });
    }
    // if(scrollController.offset==scrollController.position.maxScrollExtent)

    if (scrollController.offset <= scrollController.position.minScrollExtent &&
        !scrollController.position.outOfRange) {
      setState(() {
        bottomClose=true;
        drag=true;
        print(bottomClose.toString()+"aa");
      });
    }
  }

  cacheImage(BuildContext context) {
    for (int i = 0; i < (images.length); i++) {
      precacheImage(images[i].image, context);
    }

    // print("isLoading" + isLoading.toString());
  }

  insertImage(List<String> imagesList) {
    images.clear();
    // print("ImagesLength :" + imagesList.length.toString());
    for (int i = 0; i < imagesList.length; i++) {
      images.add(Image.network(imagesList[i]));
      // print((imagesList[i]));
    }
  }

  toggleImageCache(BuildContext context, List<String> imageList) async {
    await insertImage(imageList);
    cacheImage(context);
    setState(() {
      isLoading = false;
    });
  }





  @override
  void initState() {
    print("food Id " + widget.foodId.toString());
    print("food index................................... " + widget.pageIndex.toString());
    scrollControllerReview.addListener(() {
      print(scrollControllerReview.position.pixels.toString());
      print(scrollControllerReview.position.maxScrollExtent.toString());
      if(scrollControllerReview.position.pixels == scrollControllerReview.position.maxScrollExtent-200) {
        print("listening");
      }
    });
    print(ObjectFactory().appHive.getName().toString()+"asd");

    userBloc.updateReviewResponse.listen((event) async{
     // isLoading = false;
      if (event.statusCode == 201) {
        showToastCommon("ThankYou for your Review");
        userBloc.getReview(id: widget.foodId.toString());
        pop(context);
      } else {
        showToastCommon(
          "Review Submission Failed",
        );
      }
    }).onError((event) {
      setState(() {
       // isLoading = false;
        reviewTextEditingController.clear();
        showToastError(event, context);
      });
    });
    print(rating.toString()+"abc");
    print(reviewTextEditingController.text+"abc");

    userBloc.getFoodDescription(widget.foodId);
    print(widget.foodId.toString()+"number");

    scrollController = ScrollController();
    scrollController.addListener(_scrollListener);

    userBloc.getReview(id: widget.foodId.toString());

    userBloc.getFoodDescriptionResponse.listen((event) {
      setState(() {});
      if (event.statusCode == 200) {
        userBloc.getReview(id: widget.foodId.toString());
      } else {
        setState(() {});
      }
    }).onError((event) {
      showToastInfo(event, context);
    });

   //  userBloc.getReview(id: widget.foodId.toString());
     print("foodid"+ widget.foodId.toString());

    userBloc.getReviewResponse.listen((event)async {
      // print("Review");
      setState(() {
       counter++;
      });
      print("count"+counter.toString());
      if (event.statusCode == 200) {
        // print("Status 200");

      } else {
        errorReview = true;
        messageReview = event.message;
        setState(() {});
      }
    }).onError((event) {
      showToastError(event, context);
    });

    // _controller = AnimationController(
    //   vsync: this,
    //   duration: Duration(
    //     milliseconds: 2000,
    //   ),
    // );

    //animations
    _controller =
        AnimationController(vsync: this, duration: Duration(milliseconds: 500));
    _heightFactorAnimation =
        Tween<double>(begin: 0.45, end: 0.90).animate(_controller);
    super.initState();
  }




  onBottomPartTap() {
    setState(() {
      if (isAnimationCompleted) {
        _controller.forward();
      } else {
        _controller.reverse();
      }
    });
    isAnimationCompleted = !isAnimationCompleted;
  }

  _handleVerticalUpdate(DragUpdateDetails updateDetails) {
    double fractionDragged = (updateDetails.primaryDelta);
    _controller.value = _controller.value - 5 * fractionDragged;
    print(_controller.value);
    if (_controller.value >= 0.5) {
      _controller.forward();
    } else {
      _controller.reverse();
    }
  }

  _handlePanStartTop(DragStartDetails updateDetails) {
    double fractionDragged = screenHeight(context, dividedBy: 1);
    _controller.value = _controller.value - 5 * fractionDragged;
    print(_controller.value);
    int sensitivity = 8;
    setState(() {
      boxFit = BoxFit.cover;
      drag=true;
      bottomClose=true;
      print("top"+drag.toString());
    });
    _controller.forward();
    // if (_controller.value <= 0.5) {
    //   setState(() {
    //     boxFit = BoxFit.fitHeight;
    //   });
    //   _controller.forward();
    // } else {
    //   _controller.reverse();
    //   setState(() {
    //     boxFit = BoxFit.cover;
    //   });
    // }
  }

  _handleVerticalEndTop(DragEndDetails endDetails) {
    _controller.reverse();
    setState(() {
      boxFit = BoxFit.cover;
      drag=true;
      print("top"+drag.toString());
    });
    // if (_controller.value <= 0.5) {
    //   _controller.forward();
    //   setState(() {
    //     boxFit = BoxFit.fitHeight;
    //   });
    // } else {
    //   _controller.reverse();
    //   setState(() {
    //     boxFit = BoxFit.cover;
    //   });
    // }
  }

  void imageSwitch() {
    imageChange = true;
    if (index < (images.length - 1) && reverse == false) {
      index = index + 1;
      // print("ImageListLength" + images.length.toString());
      // print("index" + index.toString());
      setState(() {});
    } else {
      if (index == 0) {
        reverse = false;
        index = index + 1;
        setState(() {});
      } else {
        reverse = true;
        setState(() {
          index = index - 1;
        });
      }
    }
  }

  void addReview(){

    showModalBottomSheet(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(topLeft: Radius.circular(20),topRight: Radius.circular(20)),      ),
        context: context,
        isScrollControlled: true,
        builder: (context) {
          return Padding(
            padding:  EdgeInsets.only(left: 15,right: 15,
                bottom: MediaQuery.of(context).viewInsets.bottom),
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: [
                  SizedBox(
                    height: screenHeight(context, dividedBy: 30),
                  ),
                  Center(
                    child: Text(
                      "Rate this Recipe",
                      style: TextStyle(
                        color: Constants.kitGradients[28],
                        fontWeight: FontWeight.w500,
                        fontSize: 20,
                      ),
                    ),
                  ),
                  SizedBox(
                    height: screenHeight(context, dividedBy: 50),
                  ),
                  Center(

                    child: StatefulBuilder(
                  builder: (BuildContext context, setState) {
                    return
                    StarRatingBar(
                      rating: rating,
                      onRatingChanged: (rating) =>
                          setState(() => this.rating = rating),
                    );
                  }
                    ),
                  ),
                  SizedBox(
                    height: screenHeight(context, dividedBy: 20),
                  ),
                  Container(
                    child: TextField(
                      controller: reviewTextEditingController,
                      cursorColor: Constants.kitGradients[19],
                      keyboardType: TextInputType.multiline,
                      maxLines: 6,
                      obscureText: false,
                      style: TextStyle(
                        fontSize: 15,
                        fontWeight: FontWeight.w400,
                        color: Constants.kitGradients[4],
                        fontFamily: "Prompt-Light",
                      ),
                      //readOnly: widget.readOnly,
                      //onChanged:(){},
                      decoration: InputDecoration(
                        contentPadding: EdgeInsets.symmetric(
                            horizontal: screenWidth(context, dividedBy: 40),
                            vertical: screenHeight(context, dividedBy: 80)),
                        hintText: "Write your Review",
                        hintStyle: TextStyle(
                          fontSize: 15,
                          fontWeight: FontWeight.w400,
                          color:
                          Constants.kitGradients[19].withOpacity(0.5),
                          fontFamily: "Prompt-Light",
                        ),
                        fillColor: Colors.white,
                        focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                              color: Constants.kitGradients[28]

                                  .withOpacity(0.5),
                              width: 0.0),
                          borderRadius: BorderRadius.circular(8.0),
                        ),
                        enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                              color: Constants.kitGradients[28]
                                  .withOpacity(0.5),
                              width: 0.0),
                          borderRadius: BorderRadius.circular(8.0),
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: screenHeight(context, dividedBy: 30),
                  ),
                  Container(
                    width: screenWidth(context, dividedBy: 1),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        Container(
                          width: screenWidth(context, dividedBy: 2.4),
                          child: BuildButton(
                            title: "Submit",
                            isLoading: isLoading,
                            onPressed: () {
                              if (rating != 0.0 &&
                                  reviewTextEditingController.text != null)
                                submitReview();
                              else {
                                showToast("Please fill all the details",context: context);
                              }
                             // reviewTextEditingController.clear();
                            },
                          ),

                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: screenHeight(context, dividedBy: 30),
                  ),
                ]),
          );
        });
  }


  void submitReview() {
    cancelAlertBox(
        context: context,
        button1Name: "Yes",
        button2Name: "No",
        msg: "Do you wish to Submit the Review?",
        titlePadding: 3,
        text2: "",
        text1: "",
        contentPadding: 20,
        insetPadding: 2.7,
        onPressedNo: () {
          pop(context);
          setState(() {});
        },
        onPressedYes: () {
          isLoading = true;
          setState(() {
          });
          pop(context);

          userBloc.updateReview(
              updateReviewsRequestModel: UpdateReviewsRequestModel(

                user: int.parse(ObjectFactory().appHive.getUserId()),
                food: widget.foodId,
                rating: rating.floor(),
                review: reviewTextEditingController.text,


                // userName:
                //  ObjectFactory().appHive.getName(),

              ));
        });
  }
  // _handlePanStart(DragStartDetails ){
  //
  // }

  @override
  void dispose() async {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(
      SystemUiOverlayStyle(
        statusBarColor: Colors.transparent,
        statusBarBrightness: Brightness.dark,
        statusBarIconBrightness: Brightness.dark,
      ),
    );
    return WillPopScope(
      onWillPop: _willPopCallback,
      child: AnnotatedRegion<SystemUiOverlayStyle>(
        value: SystemUiOverlayStyle(
          statusBarColor: Colors.transparent,
          statusBarBrightness: Brightness.dark,
          statusBarIconBrightness: Brightness.dark,
        ),
        child: Scaffold(
          backgroundColor: Constants.kitGradients[31],
          body: SingleChildScrollView(
            child: StreamBuilder<GetFoodDescriptionResponseModel>(
                stream: userBloc.getFoodDescriptionResponse,
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    if (snapshot.data!= null) {
                      if (snapshot.data.images.isNotEmpty) {
                        toggleImageCache(context, snapshot.data.images);
                      }
                    }
                  }

                  return snapshot.hasError
                      ? Container(
                          height: screenHeight(context, dividedBy: 1),
                          child: Center(
                            child: Text(
                              snapshot.error.toString(),
                              style: TextStyle(
                                  color: Constants.kitGradients[28],
                                  fontSize: 16,
                                  fontFamily: "OswaldRegular"),
                            ),
                          ),
                        )
                      : snapshot.hasData
                          ? snapshot.data != null
                              ? SingleChildScrollView(
                                  child: AnimatedBuilder(
                                      animation: _controller,
                                      builder: (context, child) {
                                        return Container(
                                            width: screenWidth(context,
                                                dividedBy: 1),
                                            height: screenHeight(context,
                                                dividedBy: 1),
                                            decoration: BoxDecoration(
                                                color:
                                                    Constants.kitGradients[31],
                                                borderRadius: BorderRadius.only(
                                                    topLeft:
                                                        Radius.circular(20),
                                                    topRight:
                                                        Radius.circular(20))),
                                            child: Stack(
                                              fit: StackFit.expand,
                                              children: [
                                                isLoading== false?
                                                GestureDetector(
                                                  // onVerticalDragUpdate:
                                                  //     _handleVerticalUpdate,
                                                  onVerticalDragEnd:
                                                      _handleVerticalEndTop,
                                                  // onPanStart: _handlePanStart,
                                                  onVerticalDragStart:
                                                      _handlePanStartTop,
                                                  // onPanUpdate:
                                                  //     _handleVerticalUpdate,
                                                  // onPanEnd: _handleVerticalEnd,
                                                  child: isLoading == false?
                                                  FractionallySizedBox(
                                                    alignment:
                                                        Alignment.topCenter,
                                                    heightFactor:drag?
                                                        _heightFactorAnimation
                                                            .value:1.05-_heightFactorAnimation.value,
                                                    child: GestureDetector(
                                                      onTap: () {
                                                        if (snapshot.data.images.length > 1) {
                                                          imageSwitch();
                                                          print(
                                                              "image change");
                                                        }
                                                      },
                                                      child:isLoading == false?
                                                      Container(
                                                        // height: screenHeight(
                                                        //     context,
                                                        //     dividedBy: 2),
                                                        // height: _controller.value,
                                                        // decoration: BoxDecoration(
                                                        //     color: Constants
                                                        //         .kitGradients[
                                                        //             28]
                                                        //         .withOpacity(
                                                        //             0.4)),
                                                        child:Carousel(
                                                            images:widget.pageViewImage
                                                                .map(
                                                                  (item) =>
                                                              isLoading == false ?
                                                              CachedNetworkImage(
                                                                fit: BoxFit.fill,
                                                                imageUrl:item,
                                                                placeholder: (context, url) => Center(
                                                                    heightFactor: 1,
                                                                    widthFactor: 1,
                                                                    child: SpinKitCircle(color: Constants.kitGradients[28],)
                                                                ),
                                                                // "https://media.istockphoto.com/photos/group-of-south-indian-food-like-masala-dosa-uttapam-idliidly-wadavada-picture-id1024558626?k=20&m=1024558626&s=612x612&w=0&h=n4Qct-82qgwP5X8pqcdFX6GCkgs_v56_6hlpLuwO6pI=",
                                                                imageBuilder: (context, imageProvider) => Container(
                                                                  // height: screenHeight(context, dividedBy: 1.5),
                                                                  // width: screenWidth(context, dividedBy: 1),
                                                                  decoration: BoxDecoration(
                                                                    //  borderRadius: BorderRadius.circular(40),
                                                                    image: DecorationImage(
                                                                      image: imageProvider,
                                                                      fit: BoxFit.fill,
                                                                    ),
                                                                  ),
                                                                ),
                                                              ):Container(),
                                                    ).toList(),
                                                            autoplay: true,
                                                            dotSize: 5.0,
                                                            dotPosition: DotPosition.topCenter,
                                                            dotIncreaseSize: 2,
                                                            dotSpacing: 15.0,
                                                            dotColor: Constants.kitGradients[28],
                                                            // dotColor: Colors.grey[850],
                                                            dotBgColor: Colors.transparent,
                                                            dotIncreasedColor:Constants.kitGradients[28].withOpacity(.5)
                                                          // borderRadius: true,
                                                        ),
                                                      ):
                                                      Container(
                                                        width: screenWidth(context, dividedBy: 1),
                                                        height: screenHeight(context, dividedBy: 2),
                                                        color: Constants.kitGradients[17].withOpacity(0.4),
                                                        child: Center(
                                                          child: SpinKitFadingCircle(
                                                            color: Constants.kitGradients[28],
                                                          ),
                                                        ),
                                                      ),
                                                    ),
                                                  ):
                                                  Container(
                                                width: screenWidth(context, dividedBy: 1),
                                          height: screenHeight(context, dividedBy: 2),
                                          color: Constants.kitGradients[17].withOpacity(0.4),
                                          child: Center(
                                            child: SpinKitFadingCircle(
                                              color: Constants.kitGradients[28],
                                            ),
                                          ),
                                        ),
                                                ):
                                                Container(
                                                  width: screenWidth(context, dividedBy: 1),
                                                  height: screenHeight(context, dividedBy: 2),
                                                  color: Constants.kitGradients[17].withOpacity(0.4),
                                                  child: Center(
                                                    child: SpinKitFadingCircle(
                                                      color: Constants.kitGradients[28],
                                                    ),
                                                  ),
                                                ),

                                                GestureDetector(
                                                  onVerticalDragDown: (DragDownDetails updateDetails){
                                                    setState(() {
                                                      _width=screenWidth(context,dividedBy: 1);
                                                      _height=screenHeight(context,dividedBy: 2.2);
                                                      // final random = Random();
                                                      // _width = random.nextInt(300).toDouble();
                                                      // _height = random.nextInt(300).toDouble();

                                                    });
                                                  },
                                                  onVerticalDragStart:(DragStartDetails updateDetails){
                                                    setState(() {
                                                      _width=screenWidth(context,dividedBy: 1);
                                                      _height=screenHeight(context,dividedBy: 1.3);
                                                      // final random = Random();
                                                      // _width = random.nextInt(300).toDouble();
                                                      // _height = random.nextInt(300).toDouble();

                                                    });
                                                  },
                                                  child: Container(
                                                    child: FractionallySizedBox(
                                                        alignment:
                                                            Alignment.bottomCenter,
                                                        heightFactor: drag?1.05-_heightFactorAnimation.value:bottomClose?1.05-_heightFactorAnimation.value:.9,
                                                        child: Container(
                                                            width: screenWidth(
                                                                context,
                                                                dividedBy: 1),
                                                            // height: screenHeight(
                                                            //     context,
                                                            //     dividedBy: 2.7),
                                                            decoration: BoxDecoration(
                                                                color: Constants
                                                                        .kitGradients[
                                                                    31],
                                                                borderRadius: BorderRadius.only(
                                                                    topLeft: Radius
                                                                        .circular(
                                                                            20),
                                                                    topRight: Radius
                                                                        .circular(
                                                                            20))),
                                                            child: Padding(
                                                              padding: EdgeInsets.symmetric(
                                                                  horizontal:
                                                                      screenWidth(
                                                                          context,
                                                                          dividedBy:
                                                                              20)),
                                                              child: ListView(
                                                                controller: scrollController,
                                                                children: [
                                                                  Row(
                                                                    children: [
                                                                      Container(
                                                                        width: screenWidth(
                                                                            context,
                                                                            dividedBy:
                                                                                1.5),
                                                                        child:
                                                                            Text(
                                                                          snapshot
                                                                              .data
                                                                              .name,
                                                                          style: TextStyle(
                                                                              color: Constants.kitGradients[
                                                                                  28],
                                                                              fontWeight: FontWeight
                                                                                  .w900,
                                                                              fontSize:
                                                                                  20,
                                                                              fontFamily:
                                                                                  'OpenSansLight'),
                                                                        ),
                                                                      ),
                                                                      FoodLocationWidget(
                                                                        latitude: snapshot.data.lat !=null ? double.parse(snapshot.data.lat) : 0.0,
                                                                        longitude: snapshot.data.lon !=null ?double.parse(snapshot.data.lon) : 0.0,
                                                                      ),
                                                                    ],
                                                                    crossAxisAlignment:
                                                                        CrossAxisAlignment
                                                                            .start,
                                                                  ),
                                                                  SizedBox(
                                                                    height:
                                                                        screenHeight(
                                                                            context,
                                                                            dividedBy:
                                                                                40),
                                                                  ),
                                                                  Row(
                                                                    crossAxisAlignment:
                                                                        CrossAxisAlignment
                                                                            .start,
                                                                    children: [
                                                                      Text(
                                                                        "Description :",
                                                                        style:
                                                                            TextStyle(
                                                                          color: Constants
                                                                              .kitGradients[28],
                                                                          fontWeight:
                                                                              FontWeight
                                                                                  .bold,
                                                                          fontSize:
                                                                              17,
                                                                          fontFamily:
                                                                              "Prompt-Light",
                                                                        ),
                                                                      ),
                                                                    ],
                                                                  ),
                                                                  Padding(
                                                                    padding: EdgeInsets.symmetric(
                                                                        vertical: screenHeight(
                                                                            context,
                                                                            dividedBy:
                                                                                90)),
                                                                    child: Row(
                                                                      crossAxisAlignment:
                                                                          CrossAxisAlignment
                                                                              .start,
                                                                      children: [
                                                                        Text(
                                                                          snapshot
                                                                              .data
                                                                              .description,
                                                                          style:
                                                                              TextStyle(
                                                                            fontSize: screenWidth(
                                                                                context,
                                                                                dividedBy:
                                                                                    25),
                                                                            fontFamily:
                                                                                "Prompt-Light",
                                                                            color: Constants
                                                                                .kitGradients[19],
                                                                          ),
                                                                        ),
                                                                      ],
                                                                    ),
                                                                  ),
                                                                  SizedBox(
                                                                    height:
                                                                        screenHeight(
                                                                            context,
                                                                            dividedBy:
                                                                                60),
                                                                  ),
                                                                  Divider(
                                                                    //thickness: 1,
                                                                    color: Constants
                                                                        .kitGradients[
                                                                            19]
                                                                        .withOpacity(
                                                                            0.4),
                                                                  ),
                                                                  Row(
                                                                    children: [
                                                                      HeadingWidget(
                                                                        title:
                                                                            "Recipe :",
                                                                      ),
                                                                      // Text(
                                                                      //   "RECIPE :",
                                                                      //   style: TextStyle(
                                                                      //       color: Constants
                                                                      //           .kitGradients[28],
                                                                      //       fontWeight:
                                                                      //           FontWeight.bold,
                                                                      //       fontSize: 17,
                                                                      //     fontFamily: "Prompt-Light",),
                                                                      // ),
                                                                    ],
                                                                  ),
                                                                  SizedBox(
                                                                    height:
                                                                        screenHeight(
                                                                            context,
                                                                            dividedBy:
                                                                                100),
                                                                  ),
                                                                  snapshot.data.preparationMethod!=
                                                                          null
                                                                      ? Container(
                                                                    width: screenHeight(context,dividedBy: 1),
                                                                    // height: screenHeight(context, dividedBy: 20),
                                                                    child: Text(
                                                                      snapshot.data.preparationMethod,
                                                                      maxLines: null,
                                                                      overflow: TextOverflow.visible,
                                                                      style: TextStyle(
                                                                        fontSize: screenWidth(context, dividedBy: 25),
                                                                        fontFamily: "Prompt-Light",
                                                                        color: Constants.kitGradients[30],
                                                                      ),
                                                                    ),
                                                                  )
                                                                  // ListView
                                                                  //         .builder(
                                                                  //             itemCount: snapshot
                                                                  //                 .data
                                                                  //                 .data
                                                                  //                 .preparationMethod
                                                                  //                 .length,
                                                                  //             shrinkWrap:
                                                                  //                 true,
                                                                  //             physics:
                                                                  //                 NeverScrollableScrollPhysics(),
                                                                  //             padding: EdgeInsets
                                                                  //                 .zero,
                                                                  //             itemBuilder:
                                                                  //                 (BuildContext context, int index) {
                                                                  //               return Container(
                                                                  //                 height: screenHeight(context, dividedBy: 20),
                                                                  //                 child: Row(
                                                                  //                   crossAxisAlignment: CrossAxisAlignment.center,
                                                                  //                   children: [
                                                                  //                     Container(
                                                                  //                       width: screenWidth(context, dividedBy: 40),
                                                                  //                       decoration: BoxDecoration(color: Constants.kitGradients[28], shape: BoxShape.circle),
                                                                  //                     ),
                                                                  //                     SizedBox(
                                                                  //                       width: screenWidth(context, dividedBy: 60),
                                                                  //                     ),
                                                                  //                     Row(
                                                                  //                       mainAxisAlignment: MainAxisAlignment.start,
                                                                  //                       children: [
                                                                  //                         Text(
                                                                  //                           snapshot.data.data.ingredients[index],
                                                                  //                           style: TextStyle(
                                                                  //                             fontSize: screenWidth(context, dividedBy: 25),
                                                                  //                             fontFamily: "Prompt-Light",
                                                                  //                             color: Constants.kitGradients[30],
                                                                  //                           ),
                                                                  //                         ),
                                                                  //                       ],
                                                                  //                     ),
                                                                  //                   ],
                                                                  //                 ),
                                                                  //               );
                                                                  //             })
                                                                      : Container(
                                                                          height: screenHeight(
                                                                              context,
                                                                              dividedBy:
                                                                                  20),
                                                                          width: screenWidth(
                                                                              context,
                                                                              dividedBy:
                                                                                  1),
                                                                          child:
                                                                              Center(
                                                                            child:
                                                                                Text(
                                                                              "No Recipe Available",
                                                                              style:
                                                                                  TextStyle(
                                                                                fontSize:
                                                                                    15,
                                                                                fontFamily:
                                                                                    "Prompt-Light",
                                                                                color:
                                                                                    Constants.kitGradients[30],
                                                                              ),
                                                                            ),
                                                                          ),
                                                                        ),
                                                                  SizedBox(
                                                                    height:
                                                                        screenHeight(
                                                                            context,
                                                                            dividedBy:
                                                                                70),
                                                                  ),
                                                                  Divider(
                                                                    //thickness: 1,
                                                                    color: Constants
                                                                        .kitGradients[
                                                                            19]
                                                                        .withOpacity(
                                                                            0.4),
                                                                  ),
                                                                  Row(
                                                                    mainAxisAlignment:
                                                                    MainAxisAlignment
                                                                        .spaceBetween,
                                                                    children: [
                                                                      HeadingWidget(
                                                                        title:
                                                                            "Reviews :",
                                                                      ),
                                                                      //Spacer(),
                                                                      GestureDetector(
                                                                        onTap: () {
                                                                          addReview();
                                                                          // push(
                                                                          //     context,
                                                                          //     ItemReviewPage(
                                                                          //       foodId:
                                                                          //           widget.foodId,
                                                                          //       userId: int.parse(ObjectFactory().appHive.getUserId()),
                                                                          //     ));
                                                                        },
                                                                        child: Row(
                                                                          mainAxisAlignment: MainAxisAlignment.end,
                                                                          children: [
                                                                            Text("Add",
                                                                              style:
                                                                              TextStyle(
                                                                                color: Constants.kitGradients[28],
                                                                                fontWeight: FontWeight.w400,
                                                                                fontSize: 17,
                                                                                fontFamily: "Prompt-Light",
                                                                              ),
                                                                            ),
                                                                            Icon(
                                                                                Icons.add,
                                                                                color: Constants.kitGradients[28],
                                                                                size: 17),],),),
                                                                    ],
                                                                  ),
                                                                  SizedBox(
                                                                    height:
                                                                    screenHeight(
                                                                        context,
                                                                        dividedBy:
                                                                        70),
                                                                  ),
                                                                  StreamBuilder<GetReviewsResponseModel>(
                                                                      stream: userBloc.getReviewResponse,
                                                                      builder: (context, snapshot) {
                                                                        if(snapshot.hasData !=true)
                                                                          // print("data"+snapshot.data.results[0].review.toString());
                                                                          userBloc.getReview(id: widget.foodId.toString());

                                                                        return Container(
                                                                        child: snapshot.hasData
                                                                                  // snapshot.data.results.length == 0
                                                                                      ? Container(
                                                                                 width: screenWidth(context, dividedBy: 1),
                                                                                 child: ListView.builder(
                                                                                            shrinkWrap: true,
                                                                                            physics: NeverScrollableScrollPhysics(),
                                                                                            //padding: EdgeInsets.zero,
                                                                                            itemCount:
                                                                                             moreReview == true
                                                                                                ? snapshot.data.results.length
                                                                                                : snapshot.data.results.length > 4
                                                                                                    ? 3 :
                                                                                             snapshot.data.results.length,
                                                                                            itemBuilder: (BuildContext context, int index) {
                                                                                              if (snapshot.data.results.length > 4) {
                                                                                                isMoreReviewAvailable = true;
                                                                                              }
                                                                                             // print(snapshot.data.results[index].userName+"......................................asdf");
                                                                                             //  print(snapshot.data.results[index].review.toString());
                                                                                              return Container(
                                                                                                child: ReviewWidget(
                                                                                                  foodId: snapshot.data.results[index].food.toString(),
                                                                                                  body: snapshot.data.results[index].review.toString(),
                                                                                                  reviewTitle: snapshot.data.results[index].userName.toString(),
                                                                                                  rating: snapshot.data.results[index].rating.toString(),
                                                                                                ),
                                                                                              );
                                                                                            }),
                                                                                      )
                                                                                      // : Container(
                                                                                      //     width: screenWidth(context, dividedBy: 1),
                                                                                      //   //  height: screenHeight(context, dividedBy: 5),
                                                                                      //     child: Center(
                                                                                      //       child: Text(
                                                                                      //         "No Reviews",
                                                                                      //         style: TextStyle(color: Constants.kitGradients[28], fontSize: 16, fontFamily: "OswaldRegular"),
                                                                                      //       ),
                                                                                      //     ),
                                                                                      //   )
                                                                                  : Container(
                                                                                      width: screenWidth(context, dividedBy: 1),
                                                                                     // height: screenHeight(context, dividedBy: 1),
                                                                                      child: Center(
                                                                                        child: Text(
                                                                                          "No Reviews",
                                                                                          style: TextStyle(
                                                                                              color: Constants.kitGradients[28], fontSize: 16, fontFamily: "OswaldRegular"),
                                                                                        ),
                                                                                      ),
                                                                                    ),
                                                                        );
                                                                      }),
                                                                  SizedBox(
                                                                    height:
                                                                        screenHeight(
                                                                            context,
                                                                            dividedBy:
                                                                                40),
                                                                  ),
                                                                  isMoreReviewAvailable ==
                                                                          true
                                                                      ? GestureDetector(
                                                                          onTap:
                                                                              () {
                                                                            moreReview =
                                                                                !moreReview;
                                                                            if (moreReview ==
                                                                                true)
                                                                              userBloc.getReview(
                                                                                  id: widget.foodId.toString());
                                                                          },
                                                                          child:
                                                                              Row(
                                                                            crossAxisAlignment:
                                                                                CrossAxisAlignment.start,
                                                                            children: [
                                                                              Text(
                                                                                moreReview?"Less Review":"More Review",
                                                                                style:
                                                                                    TextStyle(fontSize: 16, color: Constants.kitGradients[28]),
                                                                              ),
                                                                            ],
                                                                          ),
                                                                        )
                                                                      : Container(),
                                                                  SizedBox(
                                                                    height:
                                                                        screenHeight(
                                                                            context,
                                                                            dividedBy:
                                                                                50),
                                                                  ),
                                                                 // Spacer(),
                                                                 //  SizedBox(
                                                                 //    height:
                                                                 //    screenHeight(
                                                                 //        context,
                                                                 //        dividedBy:
                                                                 //        100),
                                                                 //  ),

                                                                  // GestureDetector(
                                                                  //   onTap: () {
                                                                  //     push(context,
                                                                  //         UpdateFoodDetails());
                                                                  //   },
                                                                  //   child: Text(
                                                                  //     "Edit",
                                                                  //     style: TextStyle(
                                                                  //         fontSize:
                                                                  //             20,
                                                                  //         color: Constants
                                                                  //                 .kitGradients[
                                                                  //             28]),
                                                                  //   ),
                                                                  // ),

                                                                ],
                                                              ),
                                                            ))
                                                    ),
                                                 /*   child: AnimatedContainer(
                                                      height: screenHeight(context,dividedBy: 2.2),
                                                      width: screenWidth(context,dividedBy: 1),

                                                      duration: const Duration(seconds: 1),
                                                      decoration: BoxDecoration(
                                                          color: Constants
                                                              .kitGradients[
                                                          31],
                                                          borderRadius: BorderRadius.only(
                                                              topLeft: Radius
                                                                  .circular(
                                                                  20),
                                                              topRight: Radius
                                                                  .circular(
                                                                  20))),
                                                      child:  Padding(
                                                        padding: EdgeInsets.symmetric(
                                                            horizontal:
                                                                screenWidth(
                                                                    context,
                                                                    dividedBy:
                                                                        20)),
                                                        child: ListView(
                                                          controller: scrollController,
                                                          children: [
                                                            Row(
                                                              children: [
                                                                Container(
                                                                  width: screenWidth(
                                                                      context,
                                                                      dividedBy:
                                                                          1.5),
                                                                  child:
                                                                      Text(
                                                                    snapshot
                                                                        .data
                                                                        .data
                                                                        .name,
                                                                    style: TextStyle(
                                                                        color: Constants.kitGradients[
                                                                            28],
                                                                        fontWeight: FontWeight
                                                                            .w900,
                                                                        fontSize:
                                                                            20,
                                                                        fontFamily:
                                                                            'OpenSansLight'),
                                                                  ),
                                                                ),
                                                                FoodLocationWidget(
                                                                  latitude: snapshot.data.data.lat !=null ? double.parse(snapshot.data.data.lat) : 0.0,
                                                                  longitude: snapshot.data.data.lon !=null ?double.parse(snapshot.data.data.lon) : 0.0,
                                                                ),
                                                              ],
                                                              crossAxisAlignment:
                                                                  CrossAxisAlignment
                                                                      .start,
                                                            ),
                                                            SizedBox(
                                                              height:
                                                                  screenHeight(
                                                                      context,
                                                                      dividedBy:
                                                                          40),
                                                            ),
                                                            Row(
                                                              crossAxisAlignment:
                                                                  CrossAxisAlignment
                                                                      .start,
                                                              children: [
                                                                Text(
                                                                  "Description :",
                                                                  style:
                                                                      TextStyle(
                                                                    color: Constants
                                                                        .kitGradients[28],
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .bold,
                                                                    fontSize:
                                                                        17,
                                                                    fontFamily:
                                                                        "Prompt-Light",
                                                                  ),
                                                                ),
                                                              ],
                                                            ),
                                                            Padding(
                                                              padding: EdgeInsets.symmetric(
                                                                  vertical: screenHeight(
                                                                      context,
                                                                      dividedBy:
                                                                          90)),
                                                              child: Row(
                                                                crossAxisAlignment:
                                                                    CrossAxisAlignment
                                                                        .start,
                                                                children: [
                                                                  Text(
                                                                    snapshot
                                                                        .data
                                                                        .data
                                                                        .description,
                                                                    style:
                                                                        TextStyle(
                                                                      fontSize: screenWidth(
                                                                          context,
                                                                          dividedBy:
                                                                              25),
                                                                      fontFamily:
                                                                          "Prompt-Light",
                                                                      color: Constants
                                                                          .kitGradients[19],
                                                                    ),
                                                                  ),
                                                                ],
                                                              ),
                                                            ),
                                                            SizedBox(
                                                              height:
                                                                  screenHeight(
                                                                      context,
                                                                      dividedBy:
                                                                          60),
                                                            ),
                                                            Divider(
                                                              //thickness: 1,
                                                              color: Constants
                                                                  .kitGradients[
                                                                      19]
                                                                  .withOpacity(
                                                                      0.4),
                                                            ),
                                                            Row(
                                                              children: [
                                                                HeadingWidget(
                                                                  title:
                                                                      "Recipe :",
                                                                ),
                                                                // Text(
                                                                //   "RECIPE :",
                                                                //   style: TextStyle(
                                                                //       color: Constants
                                                                //           .kitGradients[28],
                                                                //       fontWeight:
                                                                //           FontWeight.bold,
                                                                //       fontSize: 17,
                                                                //     fontFamily: "Prompt-Light",),
                                                                // ),
                                                              ],
                                                            ),
                                                            SizedBox(
                                                              height:
                                                                  screenHeight(
                                                                      context,
                                                                      dividedBy:
                                                                          100),
                                                            ),
                                                            snapshot.data.data.preparationMethod!=
                                                                    null
                                                                ? Container(
                                                              width: screenHeight(context,dividedBy: 1),
                                                              // height: screenHeight(context, dividedBy: 20),
                                                              child: Text(
                                                                snapshot.data.data.preparationMethod,
                                                                maxLines: null,
                                                                overflow: TextOverflow.visible,
                                                                style: TextStyle(
                                                                  fontSize: screenWidth(context, dividedBy: 25),
                                                                  fontFamily: "Prompt-Light",
                                                                  color: Constants.kitGradients[30],
                                                                ),
                                                              ),
                                                            )
                                                            // ListView
                                                            //         .builder(
                                                            //             itemCount: snapshot
                                                            //                 .data
                                                            //                 .data
                                                            //                 .preparationMethod
                                                            //                 .length,
                                                            //             shrinkWrap:
                                                            //                 true,
                                                            //             physics:
                                                            //                 NeverScrollableScrollPhysics(),
                                                            //             padding: EdgeInsets
                                                            //                 .zero,
                                                            //             itemBuilder:
                                                            //                 (BuildContext context, int index) {
                                                            //               return Container(
                                                            //                 height: screenHeight(context, dividedBy: 20),
                                                            //                 child: Row(
                                                            //                   crossAxisAlignment: CrossAxisAlignment.center,
                                                            //                   children: [
                                                            //                     Container(
                                                            //                       width: screenWidth(context, dividedBy: 40),
                                                            //                       decoration: BoxDecoration(color: Constants.kitGradients[28], shape: BoxShape.circle),
                                                            //                     ),
                                                            //                     SizedBox(
                                                            //                       width: screenWidth(context, dividedBy: 60),
                                                            //                     ),
                                                            //                     Row(
                                                            //                       mainAxisAlignment: MainAxisAlignment.start,
                                                            //                       children: [
                                                            //                         Text(
                                                            //                           snapshot.data.data.ingredients[index],
                                                            //                           style: TextStyle(
                                                            //                             fontSize: screenWidth(context, dividedBy: 25),
                                                            //                             fontFamily: "Prompt-Light",
                                                            //                             color: Constants.kitGradients[30],
                                                            //                           ),
                                                            //                         ),
                                                            //                       ],
                                                            //                     ),
                                                            //                   ],
                                                            //                 ),
                                                            //               );
                                                            //             })
                                                                : Container(
                                                                    height: screenHeight(
                                                                        context,
                                                                        dividedBy:
                                                                            20),
                                                                    width: screenWidth(
                                                                        context,
                                                                        dividedBy:
                                                                            1),
                                                                    child:
                                                                        Center(
                                                                      child:
                                                                          Text(
                                                                        "No Recipe Available",
                                                                        style:
                                                                            TextStyle(
                                                                          fontSize:
                                                                              15,
                                                                          fontFamily:
                                                                              "Prompt-Light",
                                                                          color:
                                                                              Constants.kitGradients[30],
                                                                        ),
                                                                      ),
                                                                    ),
                                                                  ),
                                                            SizedBox(
                                                              height:
                                                                  screenHeight(
                                                                      context,
                                                                      dividedBy:
                                                                          70),
                                                            ),
                                                            Divider(
                                                              //thickness: 1,
                                                              color: Constants
                                                                  .kitGradients[
                                                                      19]
                                                                  .withOpacity(
                                                                      0.4),
                                                            ),
                                                            Container(
                                                              width:
                                                                  screenWidth(
                                                                      context,
                                                                      dividedBy:
                                                                          1),
                                                              child: Row(
                                                                children: [
                                                                  HeadingWidget(
                                                                    title:
                                                                        "Reviews :",
                                                                  ),
                                                                  Spacer(),
                                                                  GestureDetector(
                                                                    onTap: () {
                                                                      addReview();
                                                                      // push(
                                                                      //     context,
                                                                      //     ItemReviewPage(
                                                                      //       foodId:
                                                                      //           widget.foodId,
                                                                      //       userId: int.parse(ObjectFactory().appHive.getUserId()),
                                                                      //     ));
                                                                    },
                                                                    child: Row(
                                                                      mainAxisAlignment:
                                                                      MainAxisAlignment
                                                                          .end,
                                                                      children: [
                                                                        Text(
                                                                          "Add",
                                                                          style:
                                                                          TextStyle(
                                                                            color: Constants
                                                                                .kitGradients[28],
                                                                            fontWeight:
                                                                            FontWeight.w400,
                                                                            fontSize:
                                                                            17,
                                                                            fontFamily:
                                                                            "Prompt-Light",
                                                                          ),
                                                                        ),
                                                                        Icon(
                                                                            Icons
                                                                                .add,
                                                                            color: Constants.kitGradients[
                                                                            28],
                                                                            size:
                                                                            17),
                                                                        // HeadingWidget(
                                                                        //   title: "Add Reviews",
                                                                        // ),
                                                                      ],
                                                                    ),
                                                                  ),
                                                                ],
                                                              ),
                                                            ),
                                                            StreamBuilder<
                                                                    GetReviewsResponseModel>(
                                                                stream: userBloc
                                                                    .getReviewResponse,
                                                                builder: (context,
                                                                    snapshot) {
                                                                  return snapshot
                                                                          .hasError
                                                                      ? Container(
                                                                          height:
                                                                              screenHeight(context, dividedBy: 5),
                                                                          child:
                                                                              Center(
                                                                            child: Text(
                                                                              snapshot.error.toString(),
                                                                              style: TextStyle(color: Constants.kitGradients[28], fontSize: 16, fontFamily: "OswaldRegular"),
                                                                            ),
                                                                          ),
                                                                        )
                                                                      : snapshot.hasData
                                                                          ? snapshot.data.results.isNotEmpty
                                                                              ? Container(
                                                                                child: Column(
                                                                                  children: [
                                                                                    ListView.builder(
                                                                                        shrinkWrap: true,
                                                                                        physics: NeverScrollableScrollPhysics(),
                                                                                        //padding: EdgeInsets.zero,
                                                                                        itemCount:*//* moreReview == true
                                                                                            ? snapshot.data.results.length
                                                                                            : snapshot.data.results.length > 4
                                                                                                ? 3
                                                                                                :*//* snapshot.data.results.length,
                                                                                        itemBuilder: (BuildContext context, int index) {
                                                                                         *//* if (snapshot.data.results.length > 4) {
                                                                                            isMoreReviewAvailable = true;
                                                                                          }*//*
                                                                                          print(snapshot.data.results[index].userName+"asdf");
                                                                                          return ReviewWidget(
                                                                                            body: snapshot.data.results[index].review,
                                                                                            reviewTitle: snapshot.data.results[index].userName.toString(),
                                                                                            rating: snapshot.data.results[index].rating.toString(),
                                                                                          );
                                                                                        }),
                                                                                  ],
                                                                                ),
                                                                              )
                                                                              : Container(
                                                                                  width: screenWidth(context, dividedBy: 1),
                                                                                  height: screenHeight(context, dividedBy: 5),
                                                                                  child: Center(
                                                                                    child: Text(
                                                                                      "No Reviews",
                                                                                      style: TextStyle(color: Constants.kitGradients[28], fontSize: 16, fontFamily: "OswaldRegular"),
                                                                                    ),
                                                                                  ),
                                                                                )
                                                                          : Container(
                                                                              // width: screenWidth(context, dividedBy: 1),
                                                                              // height: screenHeight(context, dividedBy: 1),
                                                                              // child: Text(
                                                                              //   messageReview,
                                                                              //   style: TextStyle(fontSize: 20),
                                                                              // ),
                                                                            );
                                                                }),
                                                            SizedBox(
                                                              height:
                                                                  screenHeight(
                                                                      context,
                                                                      dividedBy:
                                                                          30),
                                                            ),
                                                            isMoreReviewAvailable ==
                                                                    true
                                                                ? GestureDetector(
                                                                    onTap:
                                                                        () {
                                                                      moreReview =
                                                                          !moreReview;
                                                                      if (moreReview ==
                                                                          true)
                                                                        userBloc.getReview(
                                                                            id: widget.foodId.toString());
                                                                    },
                                                                    child:
                                                                        Row(
                                                                      crossAxisAlignment:
                                                                          CrossAxisAlignment.start,
                                                                      children: [
                                                                        Text(
                                                                          moreReview?"Less Review":"More Review",
                                                                          style:
                                                                              TextStyle(fontSize: 16, color: Constants.kitGradients[28]),
                                                                        ),
                                                                      ],
                                                                    ),
                                                                  )
                                                                : Container(),
                                                            SizedBox(
                                                              height:
                                                                  screenHeight(
                                                                      context,
                                                                      dividedBy:
                                                                          10),
                                                            ),
                                                            Center(child: CreatorName()),

                                                            // GestureDetector(
                                                            //   onTap: () {
                                                            //     push(context,
                                                            //         UpdateFoodDetails());
                                                            //   },
                                                            //   child: Text(
                                                            //     "Edit",
                                                            //     style: TextStyle(
                                                            //         fontSize:
                                                            //             20,
                                                            //         color: Constants
                                                            //                 .kitGradients[
                                                            //             28]),
                                                            //   ),
                                                            // ),

                                                          ],
                                                        ),
                                                      ),
                                                    ),*/
                                                  ),
                                                ),
                                              ],
                                            ));
                                      }),
                                )
                              : Container(
                                  width: screenWidth(context, dividedBy: 1),
                                  height: screenHeight(context, dividedBy: 1),
                                  child: Center(
                                    child: Text(
                                      "No Data Found",
                                      style: TextStyle(fontSize: 20),
                                    ),
                                  ),
                                )
                          : CircularProgressIndicator(
                              valueColor: AlwaysStoppedAnimation(
                                  Constants.kitGradients[27]),
                              strokeWidth: 2,
                            );
                }),
          ),
        ),
      ),
    );
  }
}

// import 'package:app_template/src/models/get_food_desrption_response_model.dart';
// import 'package:app_template/src/screens/item_review_page.dart';
// import 'package:app_template/src/utils/constants.dart';
// import 'package:app_template/src/utils/utils.dart';
// import 'package:app_template/src/widgets/creator_name.dart';
// import 'package:app_template/src/widgets/food_detail_widget.dart';
// import 'package:app_template/src/widgets/heading_widget.dart';
// import 'package:flutter/material.dart';
// import 'package:flutter/services.dart';
// import 'package:pinch_zoom/pinch_zoom.dart';
// import 'package:shimmer/shimmer.dart';
// import 'package:swipedetector/swipedetector.dart';
//
// import '../bloc/user_bloc.dart';
// import '../models/get_reviews_response_model.dart';
// import '../utils/urls.dart';
// import '../utils/utils.dart';
// import '../widgets/rating_widget.dart';
// import 'package:app_template/src/screens/home_page.dart';
//
// class FoodInformationPage extends StatefulWidget {
//   final Widget navigationWidget;
//   final int foodId;
//   FoodInformationPage({this.navigationWidget, this.foodId});
//   @override
//   _FoodInformationPageState createState() => _FoodInformationPageState();
// }
//
// class _FoodInformationPageState extends State<FoodInformationPage>
//     with SingleTickerProviderStateMixin {
//   //animations
//   AnimationController _controller;
//   Animation<double> _heightFactorAnimation;
//   double collapsedHeightFactor = 0.80;
//   double expandedHeightFactor = 0.67;
//   bool isAnimationCompleted = false;
//
//   List<String> imgList = [
//     "assets/images/cafe.jpg",
//     "assets/images/gelato.jpg",
//     "assets/images/tacos.jpg",
//   ];
//   DragStartDetails startVerticalDragDetails;
//   DragUpdateDetails updateVerticalDragDetails;
//   bool expanded = false;
//   int index = 0;
//   bool reverse = false;
//   bool error = false;
//   String message = "";
//   String messageReview = "";
//   bool errorReview = false;
//   bool moreReview = false;
//   List<String> imageUrl = [];
//   List<Image> images = [];
//   UserBloc userBloc = new UserBloc();
//   Future<bool> _willPopCallback() async {
//     pushAndReplacement(context, HomeScreen(count: true));
//   }
//
//   cacheImage(BuildContext context) {
//     for (int i = 0; i < (images.length); i++) {
//       precacheImage(images[i].image, context);
//     }
//
//     // print("isLoading" + isLoading.toString());
//   }
//
//   insertImage(List<String> imagesList) {
//     images.clear();
//     print("ImagesLength :" + imagesList.length.toString());
//     for (int i = 0; i < (imagesList.length); i++) {
//       images.add(Image.network(Urls.baseUrlImage + imagesList[i]));
//       print((Urls.baseUrlImage + imagesList[i]));
//     }
//   }
//
//   toggleImageCache(BuildContext context, List<String> imageList) async {
//     await insertImage(imageList);
//     cacheImage(context);
//   }
//
//   @override
//   void initState() {
//     print("food Id " + widget.foodId.toString());
//     userBloc.getFoodDescription(widget.foodId);
//
//     // TODO: implement initState
//     userBloc.getFoodDescriptionResponse.listen((event) {
//       setState(() {});
//       if (event.status == 200) {
//         userBloc.getReview(id: widget.foodId.toString());
//       } else {
//         error = true;
//         message = event.message;
//         setState(() {});
//       }
//     });
//     userBloc.getReviewResponse.listen((event) {
//       setState(() {});
//       if (event.status == 200) {
//         print("Status 200");
//         setState(() {});
//       } else {
//         errorReview = true;
//         messageReview = event.message;
//         setState(() {});
//       }
//     });
//
//     // _controller = AnimationController(
//     //   vsync: this,
//     //   duration: Duration(
//     //     milliseconds: 2000,
//     //   ),
//     // );
//
//     //animations
//     _controller =
//         AnimationController(vsync: this, duration: Duration(milliseconds: 500));
//     _heightFactorAnimation =
//         Tween<double>(begin: 0.80, end: 0.67).animate(_controller);
//     // _controller.addStatusListener((currentStatus){
//     //   if(currentStatus == AnimationStatus.completed) {
//     //     isAnimationCompleted = true;
//     //   }
//     // });
//
//     super.initState();
//   }
//
//   onBottomPartTap() {
//     setState(() {
//       if (isAnimationCompleted) {
//         _controller.reverse();
//       } else {
//         _controller.forward();
//       }
//     });
//     isAnimationCompleted = !isAnimationCompleted;
//   }
//
//   _handleVerticalUpdate(DragUpdateDetails updateDetails) {
//     double fractionDragged =
//         updateDetails.primaryDelta / screenHeight(context, dividedBy: 1);
//     _controller.value = _controller.value - 5 * fractionDragged;
//     print(_controller.value);
//   }
//
//   _handleVerticalEnd(DragEndDetails endDetails) {
//     if (_controller.value >= 0.5) {
//       _controller.forward();
//     } else {
//       _controller.reverse();
//     }
//   }
//
//   @override
//   void dispose() {
//     super.dispose();
//     _controller.dispose();
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     return AnnotatedRegion<SystemUiOverlayStyle>(
//       value: SystemUiOverlayStyle(
//         statusBarColor: Colors.transparent,
//         statusBarBrightness: Brightness.dark,
//         statusBarIconBrightness: Brightness.dark,
//       ),
//       child: WillPopScope(
//         onWillPop: _willPopCallback,
//         child: Scaffold(
//           backgroundColor: Constants.kitGradients[31],
//           body: SingleChildScrollView(
//             child: StreamBuilder<GetFoodDescriptionResponseModel>(
//                 stream: userBloc.getFoodDescriptionResponse,
//                 builder: (context, snapshot) {
//                   if (snapshot.hasData) {
//                     if (snapshot.data.data != null) {
//                       if (snapshot.data.data.images.isNotEmpty) {
//                         toggleImageCache(context, snapshot.data.data.images);
//                       }
//                     }
//                   }
//
//                   return error == false
//                       ? snapshot.hasData
//                       ? snapshot.data.data != null
//                       ? AnimatedBuilder(
//                       animation: _controller,
//                       builder: (context, child) {
//                         return Container(
//                             width:
//                             screenWidth(context, dividedBy: 1),
//                             height:
//                             screenHeight(context, dividedBy: 1),
//                             child: Stack(
//                               children: [
//                                 GestureDetector(
//                                   onTap: () {
//                                     if (snapshot.data.data.images
//                                         .length >
//                                         0) if (index <
//                                         snapshot.data.data
//                                             .images.length -
//                                             1 &&
//                                         reverse == false) {
//                                       setState(() {
//                                         index = index + 1;
//                                       });
//                                     } else {
//                                       if (index == 1) {
//                                         reverse = false;
//                                       } else {
//                                         reverse = true;
//                                       }
//                                       setState(() {
//                                         index = index - 1;
//                                       });
//                                     }
//                                   },
//                                   onVerticalDragUpdate: _handleVerticalUpdate,
//                                   onVerticalDragEnd: _handleVerticalEnd,
//                                   child: Container(
//                                     // height: screenHeight(context,
//                                     //     dividedBy: 1),
//                                     height: _heightFactorAnimation
//                                         .value,
//                                     decoration: BoxDecoration(),
//                                     child: PinchZoom(
//                                       image: Image(
//                                         image: images[index].image,
//                                         fit: BoxFit.cover,
//                                       ),
//                                       zoomedBackgroundColor: Colors
//                                           .black
//                                           .withOpacity(0.5),
//                                       resetDuration: const Duration(
//                                           milliseconds: 100),
//                                       maxScale: 2.5,
//                                       onZoomStart: () {},
//                                       onZoomEnd: () {},
//                                     ),
//                                   ),
//                                 ),
//                                 Positioned(
//                                   top: screenHeight(context,
//                                       dividedBy: 17),
//                                   left: screenWidth(context,
//                                       dividedBy: 30),
//                                   child: Container(
//                                     // height: screenHeight(context, dividedBy: 21),
//                                     width: screenWidth(context,
//                                         dividedBy: 10),
//                                     alignment: Alignment.center,
//                                     // decoration: BoxDecoration(
//                                     //   shape: BoxShape.circle,
//                                     //   color: Colors.grey.withOpacity(0.5),
//                                     // ),
//                                     child: GestureDetector(
//                                       onTap: () {
//                                         pop(context);
//                                       },
//                                       child: Icon(
//                                         Icons.arrow_back_sharp,
//                                         size: 28,
//                                         color: Constants
//                                             .kitGradients[27]
//                                             .withOpacity(0.5),
//                                       ),
//                                     ),
//                                   ),
//                                 ),
//                                 Positioned(
//                                   bottom: 0,
//                                   //left: screenWidth(context, dividedBy: 30),
//                                   child: SwipeDetector(
//                                     onSwipeUp: () {
//                                       setState(() {
//                                         expanded = true;
//                                         // _controller.reverse();
//                                       });
//                                     },
//                                     onSwipeDown: () {
//                                       expanded == false
//                                           ? setState(() {
//                                         expanded = true;
//                                         //_controller.reverse();
//                                       })
//                                           : setState(() {
//                                         expanded = false;
//                                         //_controller.forward();
//                                       });
//                                     },
//                                     swipeConfiguration: SwipeConfiguration(
//                                         verticalSwipeMinVelocity:
//                                         50.0,
//                                         verticalSwipeMinDisplacement:
//                                         50.0,
//                                         verticalSwipeMaxWidthThreshold:
//                                         50.0,
//                                         horizontalSwipeMaxHeightThreshold:
//                                         50.0,
//                                         horizontalSwipeMinDisplacement:
//                                         50.0,
//                                         horizontalSwipeMinVelocity:
//                                         200.0),
//
//                                     // onVerticalDragUpdate: (details) {
//                                     //   int sensitivity = 8;
//                                     //   if (details.delta.dy >
//                                     //       sensitivity) {
//                                     //     // Down Swipe
//                                     //     expanded == false
//                                     //         ? setState(() {
//                                     //             expanded = true;
//                                     //             _controller.reverse();
//                                     //           })
//                                     //         : setState(() {
//                                     //             expanded = false;
//                                     //             _controller.forward();
//                                     //           });
//                                     //   } else if (details.delta.dy <
//                                     //       -sensitivity) {
//                                     //     // Up Swipe
//                                     //     setState(() {
//                                     //       expanded = true;
//                                     //       _controller.reverse();
//                                     //     });
//                                     //   }
//                                     // },
//                                     child:
//                                     expanded == true
//                                         ?
//                                     Column(
//                                       children: [
//                                         SizedBox(
//                                           height:
//                                           screenHeight(
//                                               context,
//                                               dividedBy:
//                                               2.7),
//                                         ),
//                                         Container(
//                                             width:
//                                             screenWidth(
//                                                 context,
//                                                 dividedBy:
//                                                 1),
//                                             height: 1 -
//                                                 _heightFactorAnimation
//                                                     .value,
//                                             decoration: BoxDecoration(
//                                                 color: Constants
//                                                     .kitGradients[
//                                                 31],
//                                                 borderRadius: BorderRadius.only(
//                                                     topLeft: Radius
//                                                         .circular(
//                                                         20),
//                                                     topRight:
//                                                     Radius.circular(
//                                                         20))),
//                                             child: Padding(
//                                               padding: EdgeInsets.symmetric(
//                                                   horizontal: screenWidth(
//                                                       context,
//                                                       dividedBy:
//                                                       20)),
//                                               child: Column(
//                                                 children: [
//                                                   SizedBox(
//                                                     height: screenHeight(
//                                                         context,
//                                                         dividedBy:
//                                                         30),
//                                                   ),
//                                                   Row(
//                                                     children: [
//                                                       Container(
//                                                         width: screenWidth(
//                                                             context,
//                                                             dividedBy: 1.5),
//                                                         child:
//                                                         Text(
//                                                           snapshot.data.data.name,
//                                                           style: TextStyle(
//                                                               color: Constants.kitGradients[28],
//                                                               fontWeight: FontWeight.w900,
//                                                               fontSize: 22,
//                                                               fontFamily: 'OpenSansLight'),
//                                                         ),
//                                                       ),
//                                                       FoodLocationWidget(
//                                                         latitude:
//                                                         int.parse("76").toDouble(),
//                                                         longitude:
//                                                         int.parse("88").toDouble(),
//                                                       ),
//                                                     ],
//                                                     crossAxisAlignment:
//                                                     CrossAxisAlignment
//                                                         .start,
//                                                   ),
//                                                   SizedBox(
//                                                     height: screenHeight(
//                                                         context,
//                                                         dividedBy:
//                                                         40),
//                                                   ),
//                                                   Row(
//                                                     crossAxisAlignment:
//                                                     CrossAxisAlignment
//                                                         .start,
//                                                     children: [
//                                                       Text(
//                                                         "DESCRIPTION :",
//                                                         style:
//                                                         TextStyle(
//                                                           color:
//                                                           Constants.kitGradients[28],
//                                                           fontWeight:
//                                                           FontWeight.bold,
//                                                           fontSize:
//                                                           17,
//                                                           fontFamily:
//                                                           "Prompt-Light",
//                                                         ),
//                                                       ),
//                                                     ],
//                                                   ),
//                                                   Padding(
//                                                     padding: EdgeInsets.symmetric(
//                                                         vertical: screenHeight(
//                                                             context,
//                                                             dividedBy: 90)),
//                                                     child:
//                                                     Row(
//                                                       crossAxisAlignment:
//                                                       CrossAxisAlignment.start,
//                                                       children: [
//                                                         Text(
//                                                           snapshot.data.data.description,
//                                                           style:
//                                                           TextStyle(
//                                                             fontSize: screenWidth(context, dividedBy: 25),
//                                                             fontFamily: "Prompt-Light",
//                                                             color: Constants.kitGradients[19],
//                                                           ),
//                                                         ),
//                                                       ],
//                                                     ),
//                                                   ),
//                                                   SizedBox(
//                                                     height: screenHeight(
//                                                         context,
//                                                         dividedBy:
//                                                         60),
//                                                   ),
//                                                   Divider(
//                                                     //thickness: 1,
//                                                     color: Constants
//                                                         .kitGradients[
//                                                     19]
//                                                         .withOpacity(
//                                                         0.4),
//                                                   ),
//                                                   Row(
//                                                     children: [
//                                                       HeadingWidget(
//                                                         title:
//                                                         "RECIPE :",
//                                                       ),
//                                                       // Text(
//                                                       //   "RECIPE :",
//                                                       //   style: TextStyle(
//                                                       //       color: Constants
//                                                       //           .kitGradients[28],
//                                                       //       fontWeight:
//                                                       //           FontWeight.bold,
//                                                       //       fontSize: 17,
//                                                       //     fontFamily: "Prompt-Light",),
//                                                       // ),
//                                                     ],
//                                                   ),
//                                                   SizedBox(
//                                                     height: screenHeight(
//                                                         context,
//                                                         dividedBy:
//                                                         100),
//                                                   ),
//                                                   snapshot.data.data.ingredients !=
//                                                       null
//                                                       ? ListView.builder(
//                                                       itemCount: snapshot.data.data.ingredients.length,
//                                                       shrinkWrap: true,
//                                                       physics: NeverScrollableScrollPhysics(),
//                                                       padding: EdgeInsets.zero,
//                                                       itemBuilder: (BuildContext context, int index) {
//                                                         return Container(
//                                                           height: screenHeight(context, dividedBy: 20),
//                                                           child: Row(
//                                                             crossAxisAlignment: CrossAxisAlignment.center,
//                                                             children: [
//                                                               Container(
//                                                                 width: screenWidth(context, dividedBy: 40),
//                                                                 decoration: BoxDecoration(color: Constants.kitGradients[28], shape: BoxShape.circle),
//                                                               ),
//                                                               SizedBox(
//                                                                 width: screenWidth(context, dividedBy: 60),
//                                                               ),
//                                                               Row(
//                                                                 mainAxisAlignment: MainAxisAlignment.start,
//                                                                 children: [
//                                                                   Text(
//                                                                     snapshot.data.data.ingredients[index],
//                                                                     style: TextStyle(
//                                                                       fontSize: screenWidth(context, dividedBy: 25),
//                                                                       fontFamily: "Prompt-Light",
//                                                                       color: Constants.kitGradients[30],
//                                                                     ),
//                                                                   ),
//                                                                 ],
//                                                               ),
//                                                             ],
//                                                           ),
//                                                         );
//                                                       })
//                                                       : Container(
//                                                     height:
//                                                     screenHeight(context, dividedBy: 20),
//                                                     width:
//                                                     screenWidth(context, dividedBy: 1),
//                                                     child:
//                                                     Center(
//                                                       child: Text(
//                                                         "No Recipe Available",
//                                                         style: TextStyle(
//                                                           fontSize: 15,
//                                                           fontFamily: "Prompt-Light",
//                                                           color: Constants.kitGradients[30],
//                                                         ),
//                                                       ),
//                                                     ),
//                                                   ),
//                                                   SizedBox(
//                                                     height: screenHeight(
//                                                         context,
//                                                         dividedBy:
//                                                         70),
//                                                   ),
//                                                   Divider(
//                                                     //thickness: 1,
//                                                     color: Constants
//                                                         .kitGradients[
//                                                     19]
//                                                         .withOpacity(
//                                                         0.4),
//                                                   ),
//                                                   Container(
//                                                     width: screenWidth(
//                                                         context,
//                                                         dividedBy:
//                                                         1),
//                                                     child:
//                                                     Row(
//                                                       children: [
//                                                         HeadingWidget(
//                                                           title:
//                                                           "REVIEWS :",
//                                                         ),
//                                                       ],
//                                                     ),
//                                                   ),
//                                                   SizedBox(
//                                                     height: screenHeight(
//                                                         context,
//                                                         dividedBy:
//                                                         70),
//                                                   ),
//                                                   GestureDetector(
//                                                     onTap:
//                                                         () {
//                                                       push(
//                                                           context,
//                                                           ItemReviewPage(
//                                                             foodId: widget.foodId,
//                                                           ));
//                                                     },
//                                                     child:
//                                                     Row(
//                                                       mainAxisAlignment:
//                                                       MainAxisAlignment.end,
//                                                       children: [
//                                                         Text(
//                                                           "Add",
//                                                           style:
//                                                           TextStyle(
//                                                             color: Constants.kitGradients[28],
//                                                             fontWeight: FontWeight.w400,
//                                                             fontSize: 17,
//                                                             fontFamily: "Prompt-Light",
//                                                           ),
//                                                         ),
//                                                         Icon(
//                                                             Icons.add,
//                                                             color: Constants.kitGradients[28],
//                                                             size: 17),
//                                                         // HeadingWidget(
//                                                         //   title: "Add Reviews",
//                                                         // ),
//                                                       ],
//                                                     ),
//                                                   ),
//                                                   // StreamBuilder<
//                                                   //         GetReviewsResponseModel>(
//                                                   //     stream: userBloc
//                                                   //         .getReviewResponse,
//                                                   //     builder: (context,
//                                                   //         snapshot) {
//                                                   //       return errorReview ==
//                                                   //               false
//                                                   //           ? snapshot.hasData
//                                                   //               ? snapshot.data.results != null
//                                                   //                   ? ListView.builder(
//                                                   //                       shrinkWrap: true,
//                                                   //                       physics: NeverScrollableScrollPhysics(),
//                                                   //                       padding: EdgeInsets.zero,
//                                                   //                       itemCount: moreReview == true ? snapshot.data.results.length : 3,
//                                                   //                       itemBuilder: (BuildContext context, int index) {
//                                                   //                         return ReviewWidget(
//                                                   //                           body: snapshot.data.results[index].review,
//                                                   //                           reviewTitle: snapshot.data.results[index].userName,
//                                                   //                           rating: snapshot.data.results[index].rating.toString(),
//                                                   //                         );
//                                                   //                       })
//                                                   //                   : Container(
//                                                   //                       width: screenWidth(context, dividedBy: 1),
//                                                   //                       height: screenHeight(context, dividedBy: 1),
//                                                   //                       child: Text(
//                                                   //                         "No Data Available",
//                                                   //                         style: TextStyle(fontSize: 20),
//                                                   //                       ),
//                                                   //                     )
//                                                   //               : Center(
//                                                   //                   child: CircularProgressIndicator(
//                                                   //                     valueColor: AlwaysStoppedAnimation(Constants.kitGradients[28]),
//                                                   //                     strokeWidth: 2,
//                                                   //                   ),
//                                                   //                 )
//                                                   //           : Container(
//                                                   //               width:
//                                                   //                   screenWidth(context, dividedBy: 1),
//                                                   //               height:
//                                                   //                   screenHeight(context, dividedBy: 1),
//                                                   //               child:
//                                                   //                   Text(
//                                                   //                 messageReview,
//                                                   //                 style: TextStyle(fontSize: 20),
//                                                   //               ),
//                                                   //             );
//                                                   //     }),
//                                                   SizedBox(
//                                                     height: screenHeight(
//                                                         context,
//                                                         dividedBy:
//                                                         30),
//                                                   ),
//                                                   GestureDetector(
//                                                     onTap:
//                                                         () {
//                                                       moreReview =
//                                                       !moreReview;
//                                                       if (moreReview ==
//                                                           true)
//                                                         userBloc.getReview(
//                                                             id: widget.foodId.toString());
//                                                     },
//                                                     child:
//                                                     Row(
//                                                       crossAxisAlignment:
//                                                       CrossAxisAlignment.start,
//                                                       children: [
//                                                         Text(
//                                                           "More Review",
//                                                           style:
//                                                           TextStyle(fontSize: 16, color: Constants.kitGradients[28]),
//                                                         ),
//                                                       ],
//                                                     ),
//                                                   ),
//                                                   SizedBox(
//                                                     height: screenHeight(
//                                                         context,
//                                                         dividedBy:
//                                                         10),
//                                                   ),
//                                                   CreatorName(),
//                                                   SizedBox(
//                                                     height: screenHeight(
//                                                         context,
//                                                         dividedBy:
//                                                         90),
//                                                   ),
//                                                 ],
//                                               ),
//                                             )),
//                                       ],
//                                     )
//                                         : Container(
//                                       width: screenWidth(
//                                           context,
//                                           dividedBy: 1),
//                                       padding: EdgeInsets.only(
//                                           bottom:
//                                           screenHeight(
//                                               context,
//                                               dividedBy:
//                                               70)),
//                                       child:
//                                       Shimmer.fromColors(
//                                         direction:
//                                         ShimmerDirection
//                                             .ltr,
//                                         //period: Duration(milliseconds:100),
//                                         baseColor: Constants
//                                             .kitGradients[0],
//                                         highlightColor:
//                                         Constants
//                                             .kitGradients[
//                                         27]
//                                             .withOpacity(
//                                             0.3),
//                                         enabled: true,
//                                         child: Image(
//                                             image: AssetImage(
//                                                 "assets/images/arrow_up.png"),
//                                             height: 30,
//                                             width: 30),
//                                       ),
//                                     ),
//                                   ),
//                                 ),
//                               ],
//                             ));
//                       })
//                       : Container(
//                     width: screenWidth(context, dividedBy: 1),
//                     height: screenHeight(context, dividedBy: 1),
//                     child: Center(
//                       child: Text(
//                         "No Data Available",
//                         style: TextStyle(fontSize: 20),
//                       ),
//                     ),
//                   )
//                       : CircularProgressIndicator(
//                     valueColor: AlwaysStoppedAnimation(
//                         Constants.kitGradients[27]),
//                     strokeWidth: 2,
//                   )
//                       : Container(
//                     width: screenWidth(context, dividedBy: 1),
//                     height: screenHeight(context, dividedBy: 1),
//                     child: Text(
//                       message,
//                       style: TextStyle(fontSize: 20),
//                     ),
//                   );
//                 }),
//           ),
//         ),
//       ),
//     );
//   }
// }
