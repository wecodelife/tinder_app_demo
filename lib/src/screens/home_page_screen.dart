import 'package:app_template/src/models/get_food_response_Model.dart';
import 'package:app_template/src/models/imageModel.dart';
import 'package:app_template/src/models/update_like_request_model.dart';
import 'package:app_template/src/models/update_mustvisit_request_model.dart';
import 'package:app_template/src/screens/data_page.dart';
import 'package:app_template/src/screens/food_information_page.dart';
import 'package:app_template/src/screens/must_vist_screen.dart';
import 'package:app_template/src/utils/object_factory.dart';
import 'package:app_template/src/widgets/filter_utilities.dart';
import 'package:feature_discovery/feature_discovery.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:app_template/src/bloc/user_bloc.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:maps_launcher/maps_launcher.dart';
import 'package:shimmer/shimmer.dart';
import 'package:app_template/src/widgets/image_card.dart';
import 'package:app_template/src/widgets/back_button.dart';
import 'package:app_template/src/widgets/feature_discovery_swipe.dart';
import 'package:app_template/src/widgets/home_tab_bar.dart';
import 'package:app_template/src/screens/profile_page.dart';
import 'package:app_template/src/screens/home_page.dart';
import 'package:app_template/src/screens/liked_item_screen.dart';
class HomePageScreen extends StatefulWidget {
  final bool count;
  final Widget navigationWidget;
  final int foodId;
  final int pageIndex;
  final String mustVisit;
  HomePageScreen({
    this.count = false,  this.navigationWidget, this.foodId,this.pageIndex,this.mustVisit
  });

  @override
  _HomePageScreenState createState() => _HomePageScreenState();
}

class _HomePageScreenState extends State<HomePageScreen> {
  int currentIndex = 0;
  bool visible = false;
  int _index = 0;
  List<Image> dummyImages = [];
  List<List<Image>> images = [];
  List<Image> foodImagelist = [];
  bool doubleTapTrue = false;
  bool showError = false;
  bool isLoading = false;
  int index = 0;
 // Image images;
 // PageController _controller;
  PageController _controller = PageController(
    //  viewportFraction: 1,
    //  initialPage: 0,
      //keepPage: true
  );

  insertImage(List<String> imageUrl, int index) async {
    dummyImages.clear();
    // print("ImagesLength :" + imageUrl.length.toString());
    for (int i = 0; i < imageUrl.length; i++) {
      // print("insertImage" + index.toString());
      dummyImages.add((Image.network(imageUrl[i])));
    }
    images.add(dummyImages.toList());
    // print("ImageNetwork" + images.first.toString());
    // print("Image" + dummyImages.toString());
    // if (images.length > 1) print("ImageNetwork" + images[1].toString());
    cacheImage(context, index);
  }

  cacheImage(BuildContext context, int index) {
    // print("cacheImages");
    for (int i = 0; i < (images[index].length); i++) {
      // print("cahche" + i.toString());
      precacheImage(images[index][i].image, context);
      foodImagelist.add(images[index][i]);
    }
    print("Food Image List************************* " + foodImagelist.toString());
  }

  toggleImageCache(
      BuildContext context, List<String> imageUrl, int index) async {
    await insertImage(imageUrl, index);
  }
  /* insertImage(List<String> imageUrl, int index) async {
    dummyImages.clear();
    // print("ImagesLength :" + imageUrl.length.toString());
    for (int i = 0; i < imageUrl.length; i++) {
      // print("insertImage" + index.toString());
      dummyImages.add((Image.network(imageUrl[i])));
    }
    images.add(dummyImages.toList());

    // print("ImageNetwork" + images.first.toString());
    // print("Image" + dummyImages.toString());
    // if (images.length > 1) print("ImageNetwork" + images[1].toString());
    cacheImage(context, index);
  }

  cacheImage(BuildContext context, int index) {
    // print("cacheImages");
    for (int i = 0; i < (images[index].length); i++) {
      // print("cahche" + i.toString());
      precacheImage(images[index][i].image, context);
      foodImagelist.add(images[index][i]);
    }
    print("Food Image List " + foodImagelist.toString());
  }*/
  UserBloc userBloc = new UserBloc();
@override
  void initState() {
    // TODO: implement initState
  // if(_index!=null)
     if(widget.pageIndex!=null)
       setState(() {
         _index=widget.pageIndex;
       });
  userBloc.getFoodDetails(null);
  userBloc.getFoodDetailsResponse.listen((event)async {
    if (event.statusCode == 200) {
      ObjectFactory().appHive.putNextImageValue(event.next);

      for (int i = 0; i < event.results.length; i++) {
        // print("cahcing");

        await toggleImageCache(context, event.results[i].images, i);
      }
      for (int i = 0; i < event.results.length; i++) {
        // print("data adding");
        print("index" + i.toString());
        print("index" + event.results[i].images.toString());
        print("index" + event.results[i].name.toString());

        data.insert(
            i,
            ImageModel(
              id: event.results[i].id,
              name: event.results[i].name,
              images: event.results[i].images,
              price: event.results[i].price,
              description: event.results[i].description,
              foodType: event.results[i].foodType,
              rating: event.results[i].rating,
              place: event.results[i].place,
              restaurant: event.results[i].restaurant,
              mustBool: event.results[i].mustVisit,
              likeBool: event.results[i].isLiked


            ));
      }

      print(data.length);
      index = data.length - 1;
      print("DataLength@@@@@@@@@@@@@@@@@@@@  " + index.toString());
      print("Images##########" + images.toString());

      // print("dataList 0 " + data[0].images[0].toString());
      // print("dataList 1 " + data[1].images[1].toString());
      // print("dataList 2 " + data[2].images[2].toString());
      // print("dataList 3 " + data[3].images[3].toString());

      // print("count value Api: " + count.toString());
    }
    setState(() {
      isLoading = false;
    });

  }).onError((event) {
    setState(() {
      isLoading = false;
      showError = true;
    });
    //errorMessage = event;
  });


  userBloc.updateLikeResponse.listen((event) {
    if (event.status == 201 || event.status == 200) {
      print("Liked item added successfully");

    } else {
      showToastCommon(event.message);
    }
  });

  userBloc.updateMustVisitResponse.listen((event) {
    if (event.status == 201 || event.status == 200) {
      print("Must Visit  added successfully");
     // print("Favorite Data " + event.data.toString());
      // showToastCommon("Food Added to Favourite List");
    } else {
      showToastCommon(event.message);
    }
  });

  // _controller = PageController(
  //     viewportFraction: 1,
  //     initialPage: 0,
  //     keepPage: true
  // );

  super.initState();
  }

  @override
  void didChangeDependencies() {
  // print("sgsgg"+widget.pageIndex.toS tring());
  // if(widget.pageIndex!=null)
  //   for (int i = 0; i < (images[index].length); i++) {
  //     // print("cahche" + i.toString());
  //     precacheImage(images[index][i].image, context);
  //   }
      super.didChangeDependencies();

      //   foodImagelist.add(images[index][i]);
    // }
}
  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(
      SystemUiOverlayStyle(
        statusBarColor: Colors.transparent,
        statusBarBrightness: Brightness.dark,
        statusBarIconBrightness: Brightness.dark,
      ),
    );
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: SystemUiOverlayStyle(
        statusBarColor:  Colors.transparent,
        statusBarBrightness: Brightness.dark,
        statusBarIconBrightness: Brightness.dark,
       ),
      child: Scaffold(
        backgroundColor: Constants.kitGradients[31],
          body: Stack(
              children: [
            Column(
              children: [
                Expanded(
                  child: StreamBuilder<GetFoodDetailsResponseModel>(
                    stream: userBloc.getFoodDetailsResponse,
                    builder: (context, snapshot) {
                      return snapshot.hasData ?
                      PageView.builder(
                        itemCount: snapshot.data.results.length,
                        controller: PageController(
                            initialPage: widget.pageIndex!=null ? widget.pageIndex:0),
                        scrollDirection: Axis.vertical,
                        onPageChanged: (int index) =>
                            setState(() => _index = index),
                        itemBuilder: (_, i) {
                          // if(widget.pageIndex!=null)
                          //   _controller.jumpToPage(1);
                          return Transform.scale(
                              scale: i == _index ? 1 : 1,
                              child:  index >= 0 ?
                              ImageCard(
                               // index: i,
                                rating:data[_index].rating == null?0:data[_index].rating,
                                pageViewImage: data[_index].images,
                                price: data[_index].price,
                                foodId: data[_index].id,
                                foodName:data[_index].name,
                                hotel: data[_index].restaurant,
                                type: data[_index].foodType == 1? "Veg":"Non-veg",
                                  mustBool: data[_index].mustBool,
                                  likeBool: data[_index].likeBool,
                                  index: _index,
                                  onPressedDetails:(){
                                  // print("...................index............."+_index.toString());
                                   // _controller.jumpToPage(widget.pageIndex);
                                    Navigator.pushReplacement(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) =>
                                              FoodInformationPage(
                                                foodId: snapshot.data.results[_index].id,
                                                pageIndex: _index,
                                                count: false,
                                                pageViewImage: snapshot.data.results[_index].images,
                                                navigationWidget:
                                                HomePageScreen(
                                                  count: false,
                                                ),
                                              )),
                                    );
                                  },

                                onPressedFavourite: (){

                                  showToastCommon("Added to Liked Items");
                                  print("Saved");
                                  print("Saved By " + ObjectFactory().appHive.getUserId() ) ;
                                  print("fOOD id " + snapshot.data.results[_index].id.toString() ) ;
                                  userBloc.updateLike(
                                      updateLikeRequestModel:
                                      UpdateLikeRequestModel(

                                        food: snapshot.data.results[_index].id,
                                        // user:
                                        // int.parse(ObjectFactory().appHive.getUserId()),
                                        isLiked: true,

                                      ));
                                },
                               // onPressedLocation: (){
                                  latitude: snapshot.data.results[_index].lat !=null ? double.parse(snapshot.data.results[_index].lat) : 0.0,
                                  longitude: snapshot.data.results[_index].lon !=null ?double.parse(snapshot.data.results[_index].lon) : 0.0,

                               // },
                                onPressedStar: (){
                                  showToastCommon("Added to Must Visit");
                                  userBloc.updateMustVisit(
                                      updateMustVisitRequestModel:
                                      UpdateMustVisitRequestModel(
                                        food: snapshot.data.results[_index].id,
                                        // user:
                                        // int.parse(ObjectFactory().appHive.getUserId()),

                                       mustVisit: true,
                                       // mustVisit: true,
                                      ));

                                },
                                onTap: () {
                                  visible = !visible;
                                  print("visible" + visible.toString());
                                  setState(() {});
                                },
                                onDoubleTap: (){
                                  doubleTapTrue = true;
                                  showToastLiked(context);
                                },
                              ):Container()
                          );
                        },
                      ):GestureDetector(
                        onTap: (){
                          visible = !visible;
                          setState(() {});
                        },
                        child: Container(
                          width: screenWidth(context, dividedBy: 1),
                          height: screenHeight(context, dividedBy: 2),
                          color: Constants.kitGradients[17].withOpacity(0.4),
                          child: Center(
                          child:  SpinKitThreeBounce(
                            color: Constants.kitGradients[28],
                           // size: 8.0,

                          )
                            ),
                        ),
                      );
                    }
                  ),
                ),

               /* AnimatedSmoothIndicator(
                  activeIndex: _index,
                  count: widget.article.Magazine_cards.magazine_cards.length,
                  effect: ExpandingDotsEffect(
                      activeDotColor: Utils.kitGradients[0],
                      dotColor: Utils.kitGradients[0],
                      dotHeight: screenWidth(context, dividedBy: 40),
                      dotWidth: screenWidth(context, dividedBy: 40),
                      strokeWidth: 10,
                      spacing: 15),
                )*/

              ],
            ),
            Visibility(
              visible: visible ? true : false,
              child: HomeTabBar(
                onTapLiked: () {
                  push(context, MustVisitItemScreen());
                },
                onTapHome: () {
                  push(context, HomePageScreen(count: true));
                },
                // onTapSaved: () {
                //   push(context, FeedPage());
                // },
                onTapProfile: () {
                  pushAndReplacement(
                      context,
                      ProfilePage(
                          // userPic:
                          // "https://images.unsplash.com/photo-1467003909585-2f8a72700288?ixid=MnwxMjA3fDB8MHxzZWFyY2h8OXx8Zm9vZHxlbnwwfHwwfHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=600&q=60",
                          // userName: "SanJE"
                      ));
                },
              ),
            ),
                Positioned(
                  left: screenWidth(context, dividedBy:9),
                  top: screenHeight(context, dividedBy: 16),
                  child:      DescribedFeatureOverlay(
                      featureId: 'feature1',
                      targetColor: Constants.kitGradients[31],
                      
                      // textColor: Constants.kitGradients[19],
                      pulseDuration: Duration(seconds: 1),
                      enablePulsingAnimation: true,
                      barrierDismissible: false,
                      backgroundColor: Constants.kitGradients[28],
                      backgroundOpacity: 0.6,
                      contentLocation: ContentLocation.below,
                      // onComplete: action,

                      title: Text(
                        'Swipe Down',
                        style: TextStyle(
                          fontSize: 20,
                          fontWeight: FontWeight.bold,
                          color: Constants.kitGradients[0],
                          fontFamily: "Prompt-Light",
                        ),
                      ),
                      overflowMode: OverflowMode.extendBackground,
                      openDuration: Duration(seconds: 1),
                      description: Text(
                        'Swipe down to explore other food items ',
                        style: TextStyle(
                          fontSize: 17,
                          fontWeight: FontWeight.w500,
                          color: Constants.kitGradients[0].withOpacity(0.6),
                          fontFamily: "Prompt-Light",
                        ),
                      ),
                      tapTarget: Container(
                        child:Shimmer.fromColors(
                            baseColor: Constants.kitGradients[28],
                            highlightColor: Colors.white54,
                            enabled: true,
                            child:Icon(Icons.keyboard_arrow_down_outlined,size: 25,)
                          //   Image(
                          //   image: AssetImage("assets/images/arrow_down.png"),
                          //   //fit: BoxFit.fitHeight,
                          //   height: screenWidth(context, dividedBy: 20),
                          //   width: screenWidth(context, dividedBy: 20),
                          // ),
                        ),
                      ),
                      child:Container()
                  ),
                ),
                Positioned(
                  left: 30,
                  top: screenHeight(context, dividedBy: 2),
                  child:      DescribedFeatureOverlay(
                      featureId: 'feature2',
                      targetColor: Constants.kitGradients[31],
                      // textColor: Constants.kitGradients[19],
                      pulseDuration: Duration(seconds: 1),
                      enablePulsingAnimation: false,
                      barrierDismissible: false,
                      backgroundColor: Constants.kitGradients[28],
                      backgroundOpacity: 0.6,

                      contentLocation: ContentLocation.below,
                      // onComplete: action,

                      title: Text(
                        'Swipe Left',
                        style: TextStyle(
                          fontSize: 20,
                          fontWeight: FontWeight.bold,
                          color: Constants.kitGradients[0],
                          fontFamily: "Prompt-Light",
                        ),
                      ),
                      overflowMode: OverflowMode.extendBackground,
                      openDuration: Duration(seconds: 1),
                      description: Text(
                        'Swipe Left to swipe past the current food item',
                        style: TextStyle(
                          fontSize: 17,
                          fontWeight: FontWeight.w500,
                          color: Constants.kitGradients[0].withOpacity(0.6),
                          fontFamily: "Prompt-Light",
                        ),
                      ),
                      tapTarget: Container(
                        child:Shimmer.fromColors(
                            baseColor: Constants.kitGradients[28].withOpacity(0.6),
                            highlightColor: Colors.white,
                            enabled: true,
                            child:Icon(Icons.chevron_left,size: 25,)
                          //   Image(
                          //   image: AssetImage("assets/images/arrow_down.png"),
                          //   //fit: BoxFit.fitHeight,
                          //   height: screenWidth(context, dividedBy: 20),
                          //   width: screenWidth(context, dividedBy: 20),
                          // ),
                        ),
                      ),
                      child:Container()
                  ),
                ),
                DescribedFeatureOverlay(
                    featureId: 'feature3',
                    targetColor: Constants.kitGradients[31],
                    // textColor: Constants.kitGradients[19],
                    pulseDuration: Duration(seconds: 1),
                    enablePulsingAnimation: true,
                    barrierDismissible: false,
                    backgroundColor: Constants.kitGradients[28],
                    backgroundOpacity: 0.6,

                    contentLocation: ContentLocation.above,
                    // onComplete: action,

                    title: Text(
                      'OnTap',
                      style: TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                        color: Constants.kitGradients[0],
                        fontFamily: "Prompt-Light",
                      ),
                    ),
                    overflowMode: OverflowMode.extendBackground,
                    openDuration: Duration(seconds: 1),
                    description: Text(
                      'OnTap to  get the Tab bar menu of other fields',
                      style: TextStyle(
                        fontSize: 17,
                        fontWeight: FontWeight.w500,
                        color: Constants.kitGradients[0].withOpacity(0.6),
                        fontFamily: "Prompt-Light",
                      ),
                    ),
                    tapTarget: Container(
                      child:Shimmer.fromColors(
                          baseColor: Constants.kitGradients[28].withOpacity(0.6),
                          highlightColor: Colors.white,
                          enabled: true,
                          child:Icon(Icons.pause,size: 25,)
                        //   Image(
                        //   image: AssetImage("assets/images/arrow_down.png"),
                        //   //fit: BoxFit.fitHeight,
                        //   height: screenWidth(context, dividedBy: 20),
                        //   width: screenWidth(context, dividedBy: 20),
                        // ),
                      ),
                    ),
                    child:Container()
                ),
                index >= 0
                    ? Positioned(
                    left: 0,
                    top: screenHeight(context, dividedBy: 3),
                    child: FeatureSwipe(
                      count: true,
                    ))
                    : Center(
                  child: CircularProgressIndicator(
                    valueColor: AlwaysStoppedAnimation(
                        Constants.kitGradients[27]),
                    strokeWidth: 2,
                  ),
                ),



              ]
          )
      ),
    );



  }
}
