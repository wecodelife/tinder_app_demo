import 'dart:ui';

import 'package:app_template/src/bloc/user_bloc.dart';
import 'package:app_template/src/models/login_request_model.dart';
import 'package:app_template/src/models/register_user_request.dart';
import 'package:app_template/src/screens/edit_profile_page.dart';
import 'package:app_template/src/screens/forgte_password_page.dart';
import 'package:app_template/src/screens/home_page_screen.dart';
import 'package:app_template/src/screens/sign_up_personal_info.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/object_factory.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/widgets/build_button.dart';
import 'package:app_template/src/widgets/creator_name.dart';
import 'package:app_template/src/widgets/email_form_field.dart';
import 'package:app_template/src/widgets/otp_field.dart';
import 'package:app_template/src/widgets/password_form_field.dart';
import 'package:email_validator/email_validator.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_styled_toast/flutter_styled_toast.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:sms_autofill/sms_autofill.dart';

import '../utils/object_factory.dart';
import 'otp_page.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {

  TextEditingController phoneNumberTextEditingController =
      new TextEditingController();
  TextEditingController _otpTextEditingController = new TextEditingController();
  TextEditingController passwordTextEditingController =
      new TextEditingController();
  final FirebaseAuth _auth = FirebaseAuth.instance;
  final GoogleSignIn googleSignIn = new GoogleSignIn();
  String name = 'Log In';
  String status;
  UserBloc userBloc = UserBloc();
  bool loading = false;
  bool gLoading = false;
  bool otpSend = false;

  Future<void> userCheck(account) {
    if (account == null) {
      setState(() {
        gLoading = false;
      });
      return null;
    }
  }

  void exitApp(){
    cancelAlertBox(
        context: context,
        button1Name: "Yes",
        button2Name: "No",
        msg: "Are you sure you want to exit?",
        titlePadding: 4,
        text2: "",
        text1: "",
        contentPadding: 20,
        insetPadding: 2.7,
        onPressedNo: () {
          pop(context);
          // setState(() {});
        },
        onPressedYes: () {
          SystemNavigator.pop();
        });
  }

  Future<User> _signIn() async {
    setState(() {
      gLoading = true;
      phoneNumberTextEditingController.text = null;
    });
    // GoogleSignInAccount googleSignInAccount = await googleSignIn.signIn();
    // GoogleSignInAuthentication gSA  = await googleSignInAccount.authentication;

    try {
      final GoogleSignInAccount account = await googleSignIn.signIn();
      await userCheck(account);
      final GoogleSignInAuthentication authentication =
          await account.authentication;

      final GoogleAuthCredential credential = GoogleAuthProvider.credential(
          idToken: authentication.idToken,
          accessToken: authentication.accessToken);

      final UserCredential authResult =
          await _auth.signInWithCredential(credential);
      final User user = authResult.user;
      if (user != null) {
        print(user.email.toString() + "fgfg");
        print("Login ");
        ObjectFactory().appHive.putEmail(user.email);
        // ObjectFactory().appHive.putUserId(userId: user.uid);
        // ObjectFactory().appHive.putProfileImage(profileImage:user.photoURL);
        // ObjectFactory().appHive.putName(name: user.displayName);
        // ObjectFactory().appHive.putEmail(user.email);
        // print("Profile image from Hive  "+ ObjectFactory().appHive.getProfileImage());
        // print("Profile name from Hive  "+ ObjectFactory().appHive.getName());
        // print("Email from Hive  "+ ObjectFactory().appHive.getEmail());
        userBloc.login(loginRequest: LoginRequest(uid: user.uid));
      }
      print("UserName :${user.displayName}");
      setState(() {
         name = user.displayName.toString();
        //gLoading = false;
      });
      return user;
    } on FirebaseAuthException catch (e) {
      print("error");
      setState(() {
        gLoading = false;
         status = e.message.trim();
       // showToastError(e.message.trim(), context);
      });
      showToastError( status,  context);
    }
  }

  // Future validCheck(String phone) async {
  //   if (phone.isNotEmpty ) {
  //     // print("valid email and password");
  //     final bool isValid = EmailValidator.validate(phone);
  //     if (isValid == true) {
  //       // print("valid email");
  //       if (phone.isNotEmpty) {
  //         // print("password not empty");
  //         setState(() {
  //           loading = true;
  //         });
  //         User user = FirebaseAuth.instance.currentUser;
  //         if (user == null) {
  //           showToastError("User not found, Please Sign Up", context);
  //           setState(() {
  //             loading = false;
  //           });
  //         } else
  //           try {
  //             UserCredential userCredential = await FirebaseAuth.instance
  //                 .signInWithEmailAndPassword(
  //                     email: phoneNumberTextEditingController.text,
  //                     password: passwordTextEditingController.text);
  //             User user = FirebaseAuth.instance.currentUser;
  //             print(user.toString() + "abc");
  //             if (!user.emailVerified) {
  //               showToastInfo("Please verify email", context);
  //               setState(() {
  //                 loading = false;
  //               });
  //             } else {
  //               // ObjectFactory().appHive.putUserId(userId: user.uid);
  //               userBloc.login(loginRequest: LoginRequest(uid: user.uid));
  //             }
  //             // print("uid" + ObjectFactory().appHive.getUserId());
  //             // print("photo" + user.photoURL.toString());
  //           } on FirebaseAuthException catch (e) {
  //             setState(() {
  //               loading = false;
  //               showToastError(e.message.trim(), context);
  //               // status = e.message.trim();
  //             });
  //           }
  //       } else {
  //         // print("password is empty");
  //         showToastCommon("Enter password");
  //       }
  //     } else {
  //       // print("Invalid Email");
  //       showToastCommon("Invalid Email");
  //     }
  //   } else {
  //     showToastCommon("Please fill all the details");
  //     // print("invalid email and password");
  //   }
  //   return null;
  // }
  bool isLoading = false;
  String message = "";
  String verificationId = "";
  final FirebaseAuth auth = FirebaseAuth.instance;

  void _verifyPhoneNumber() async {
    setState(() {
      message = '';
      isLoading = true;
    });

    // userBlocSingle.login(loginRequestModel: LoginRequestModel(uid:Hive.box<String>(HiveBoxes.user).get(UserLoginHive().uid)));
    PhoneVerificationCompleted verificationCompleted =
        (PhoneAuthCredential phoneAuthCredential) async {
      await _auth.signInWithCredential(phoneAuthCredential).then((authResult) {
        print("Success!!! UUID is: " + authResult.user.uid);
         // Hive.box('user').put('uid', authResult.user.uid);
        userBloc.login(loginRequest: LoginRequest(uid: authResult.user.uid));
      }
      );
      setState(() {
        isLoading = false;
      });
      // print('UUID' + '$phoneAuthCredential');
    };

    PhoneVerificationFailed verificationFailed =
        (FirebaseAuthException authException) {
      setState(() {
        message =
        'Phone number verification failed. Code: ${authException.code}';
      });
      if (authException.code == 'too-many-requests')
        showToast(
            "You have reached the threshold limit for sending requests.Try again later");
    };

    PhoneCodeSent codeSent =
        (String verificationId, [int forceResendingToken]) async {
         setState(() {
           isLoading = false;
           otpSend = true;
           verificationId = verificationId;
         });
    };

    PhoneCodeAutoRetrievalTimeout codeAutoRetrievalTimeout =
        (String verificationId) {
      verificationId = verificationId;
      setState(() {
        isLoading = false;
      });
      push(context, OtpPage(
        phoneNumber: phoneNumberTextEditingController.text,
        verificationId: verificationId,));
    };
    try {
      await auth.verifyPhoneNumber(
          phoneNumber: '+91' + phoneNumberTextEditingController.text,
          timeout: const Duration(seconds: 120),
          verificationCompleted: verificationCompleted,
          verificationFailed: verificationFailed,
          codeSent: codeSent,
          codeAutoRetrievalTimeout: codeAutoRetrievalTimeout);
    } on FirebaseAuthException catch (e) {
      print("error"+ e.toString());
      setState(() {
        isLoading = false;
      });
    }
  }
// Future openBox() async {
  //   final appDocDir = await path_provider.getApplicationDocumentsDirectory();
  //   Hive.init(appDocDir.path);
  //   Hive.registerAdapter(HomePage());
  //   await Hive.openBox<HomePageHiveModel>("imageBox");
  //   // await checkHiveBoxOpen();
  //
  //   await Hive.openBox(Constants.BOX_NAME);
  //   // imageBox.isOpen ? print("Box IS oPEN") : print("Box is closed");
  // }

  @override
  void initState() {
    // TODO: implement initState
    userBloc.loginResponse.listen((event) {
      if (event.status == 200 || event.status == 201) {
        print("User Login Successful");
        print("Token  " + event.data.token);
        print("user  " + event.data.userData.firstName);
        setState(() {
          loading = false;
        });
        ObjectFactory().appHive.putUserId(userId: event.data.userData.id.toString());
        ObjectFactory().appHive.putToken(token: event.data.token);
        ObjectFactory().appHive.putName(name: event.data.userData.firstName);
        ObjectFactory().appHive.putProfileImage(profileImage: event.data.userData.profileImage);
        ObjectFactory().appHive.putPhoneNumber(event.data.userData.phoneNumber);
        // ObjectFactory().appHive.putProfileImage(profileImage: event.data.userData.)
        print("Done");
        if(event.data.userData.profileImage!=null)
        push(context, HomePageScreen());
        else
          push(context, EditProfilePage(phoneNumber: event.data.userData.phoneNumber,id: event.data.userData.id,isFirstTime: true,profileImage: event.data.userData.profileImage,userName: event.data.userData.firstName,));
        // userBloc.registerUser(
        //   id:event.data.userData.id.toString(),
        //     registerUserRequest: RegisterUserRequest(
        //         email: event.data.userData.email,
        //         lastName: event.data.userData.lastName,
        //         firstName: event.data.userData.firstName,
        //         password: passwordTextEditingController.text,
        //         isStaff:true,
        //         isDeleted:false,
        //         isVerified: true));
        // userBloc.registerUserResponse.listen((event) {
        //   print("Register response successful");
        //   print("Email after registration: "+ event.email);
        //   print("done");
        // }).onError((event) {
        //   // setState(() {
        //   //   loading = false;
        //   // });
        //   showToastCommon(event);
        // });
        setState(() {
          loading = false;
        });
      } else {
        setState(() {
          loading = false;
        });
        showToastCommon(event.message);
      }
    }).onError((event) {
      setState(() {
        loading = false;
      });
      showToastError(event, context);
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: (){
        exitApp();
      },
      child: Scaffold(
          backgroundColor: Constants.kitGradients[30],
          body:otpSend == false?
          Container(
            height: screenHeight(context, dividedBy: 1),
            width: screenWidth(context, dividedBy: 1),
            child: SingleChildScrollView(
                child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Container(
                  width: screenWidth(context, dividedBy: 1),
                  height: screenHeight(context, dividedBy: 1),
                  decoration: BoxDecoration(
                    //border: Border(top:BorderSide(color:Constants.kitGradients[28],width: 2.0),),
                    color: Constants.kitGradients[31],
                    borderRadius: BorderRadius.circular(8),
                    // image: DecorationImage(
                    //   image: AssetImage(
                    //     "assets/images/foodapp_bg.jpg",
                    //   ),
                    //   fit: BoxFit.cover,
                    // ),
                  ),
                  padding: EdgeInsets.symmetric(
                      horizontal: screenWidth(context, dividedBy: 10)),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      SizedBox(
                        height: screenHeight(context, dividedBy: 8  ),
                      ),
                      Image.asset(
                        "assets/images/login_logo.png",
                        // color: Constants.kitGradients[28],
                      ),
                      SizedBox(
                        height: screenHeight(context, dividedBy: 15),
                      ),
                      // Align(
                      //   alignment: Alignment.centerLeft,
                      //   child: Text(
                      //     "Hello Again !",
                      //     textAlign: TextAlign.left,
                      //     style: TextStyle(
                      //       fontFamily: "Montserrat-ExtraBold",
                      //       color: Constants.kitGradients[28],
                      //       fontWeight: FontWeight.bold,
                      //       fontSize: 24,
                      //     ),
                      //   ),
                      // ),
                      // SizedBox(
                      //   height: screenHeight(context, dividedBy: 150),
                      // ),
                      Align(
                        alignment: Alignment.centerLeft,
                        child: Text(
                          "Welcome back",
                          textAlign: TextAlign.left,
                          style: TextStyle(
                            fontFamily: "Montserrat",
                            color: Constants.kitGradients[28],
                            fontWeight: FontWeight.bold,
                            fontSize: 24,
                          ),
                        ),
                      ),
                      SizedBox(
                        height: screenHeight(context, dividedBy: 30),
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          FormFieldUserEmail(
                            labelText: "Phone Number",
                            textEditingController: phoneNumberTextEditingController,
                            //otp: false,
                          ),
                          SizedBox(
                            height: screenHeight(context, dividedBy: 25),
                          ),
                          // FormFieldUserPassword(
                          //   labelText: "Password",
                          //   textEditingController: passwordTextEditingController,
                          //   //otp: false,
                          // ),
                        ],
                      ),
                      SizedBox(
                        height: screenHeight(context, dividedBy: 50),
                      ),
                      // GestureDetector(
                      //   onTap: () {
                      //     push(context, ForgetPasswordPage());
                      //   },
                      //   child: Row(
                      //     children: [
                      //       SizedBox(width: screenWidth(context, dividedBy: 40)),
                      //       Text(
                      //         "Forgot Password ? Need Help.",
                      //         style: TextStyle(
                      //           color: Constants.kitGradients[28],
                      //           fontWeight: FontWeight.w500,
                      //           fontFamily: "Prompt-Light",
                      //           fontSize: 14,
                      //         ),
                      //       ),
                      //     ],
                      //   ),
                      // ),
                      // SizedBox(
                      //   height: screenHeight(context, dividedBy: 20),
                      // ),
                      BuildButton(
                        title: "Login",
                        isLoading: isLoading,
                        onPressed: () {
                          _verifyPhoneNumber();
                          // validCheck(phoneNumberTextEditingController.text
                          //     // passwordTextEditingController.text
                          // );
                        },
                      ),
                      SizedBox(
                        height: screenHeight(context, dividedBy: 50),
                      ),
                      BuildButton(
                        title: "Google",
                        isLoading: gLoading,
                        isImage: true,
                        onPressed: () {
                          _signIn();
                        },
                      ),
                      // SizedBox(
                      //   height: screenHeight(context, dividedBy: 50),
                      // ),
                      // GestureDetector(
                      //   onTap: () {
                      //     push(context, SignUpPersonalInfo());
                      //   },
                      //   child: Text(
                      //     "Are you New Here ? Sign Up",
                      //     style: TextStyle(
                      //       color: Constants.kitGradients[28],
                      //       fontWeight: FontWeight.w500,
                      //       fontFamily: "Prompt-Light",
                      //       fontSize: 16,
                      //     ),
                      //   ),
                      // ),
                      Spacer(),
                      CreatorName(),
                      SizedBox(
                        height: screenHeight(context, dividedBy: 90),
                      ),
                    ],
                  ),
                ),
              ],
            )),
          )
              :Container(
              height: screenHeight(context, dividedBy: 1),
              width: screenWidth(context, dividedBy: 1),
            color:  Constants.kitGradients[31],
            child:SingleChildScrollView(
              child: Padding(
                padding: EdgeInsets.symmetric(
                    horizontal: screenWidth(context, dividedBy: 16),
                    vertical: screenHeight(context, dividedBy: 8)),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Container(
                      height: screenHeight(context, dividedBy: 5.5),
                      child: Image.asset(
                        "assets/images/login_logo.png",
                        // color: Constants.kitGradients[28],
                      ),
                    ),
                    SizedBox(
                      height: screenHeight(context, dividedBy: 15 ),
                    ),
                    Align(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        "Verify your Phone Number",
                        style: TextStyle(
                          fontSize: 27,
                          fontWeight: FontWeight.bold,
                          color: Constants.kitGradients[28],
                          fontFamily: "Montserrat-ExtraBold",
                        ),
                      ),
                    ),
                    SizedBox(
                      height: screenHeight(context, dividedBy: 55),
                    ),
                    Align(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        "Please enter the OTP sent on your registered phone number ",
                        //textAlign: TextAlign.center,
                        maxLines: 2,
                        style: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.normal,
                          color: Constants.kitGradients[28].withOpacity(0.5),
                          fontFamily: "Prompt-Light",
                        ),
                      ),
                    ),
                    SizedBox(
                      height: screenHeight(context, dividedBy: 20),
                    ),
                    // SizedBox(
                    //   height: screenHeight(context, dividedBy: 30),
                    // ),
                    // FormFieldUserDetails(
                    //     labelText: "OTP",
                    //     hintText: "",
                    //     textEditingController: _otpTextEditingController),
                    Container(
                      padding: EdgeInsets.symmetric(horizontal: 10),
                      width: screenWidth(context, dividedBy: 1),
                      child: PinFieldAutoFill(
                        controller: _otpTextEditingController,
                        decoration: UnderlineDecoration(
                          textStyle: TextStyle(
                            color: Constants.kitGradients[28],
                            fontFamily: "Prompt-Light",
                            fontSize: 16,
                          ),
                          colorBuilder: FixedColorBuilder(
                              Constants.kitGradients[28]),
                        ),
                        currentCode: _otpTextEditingController.text,
                        onCodeSubmitted: (code) {
                          // setState(() {
                          //   otpTextEditingController.text = code;
                          // });
                          print("tfgf" + code);
                        },
                        onCodeChanged: (code) {
                          print("dedd" + code);
                          print("dedde" +
                              _otpTextEditingController.text);
                          // if (code.length == 6) {
                          //   // FocusScope.of(context).requestFocus(FocusNode());
                          // }
                        },
                      ),
                    ),
                    // SizedBox(
                    //   height: screenHeight(context, dividedBy: 30),
                    // ),
                    // Text(
                    //   "( If you don’t see it in your Inbox, it may have been sorted into your Spam folder )",
                    //   style: TextStyle(
                    //       color: Constants.kitGradients[28].withOpacity(0.5),
                    //       fontFamily: "Prompt-Light",
                    //       fontSize: screenWidth(context, dividedBy: 25),
                    //       fontStyle: FontStyle.italic),
                    // ),
                    SizedBox(
                      height: screenHeight(context, dividedBy: 10),
                    ),
                    BuildButton(
                      onPressed: () async{
                        PhoneAuthCredential
                        phoneAuthCredential =
                        PhoneAuthProvider.credential(
                            verificationId: verificationId,
                            smsCode:
                            _otpTextEditingController
                                .text);

                        verifyOtp(
                            phoneAuthCredential);
                      },
                      title: "Verify",
                      isLoading: false,
                      buttonWidth: 3.6,
                    ),
                    SizedBox(
                      height: screenHeight(context, dividedBy: 3.5),
                    ),
                    Center(child: CreatorName()),
                    SizedBox(
                      height: screenHeight(context, dividedBy: 40),
                    ),
                  ],
                ),
              ),
            ),)
      ),
    );
  }
  void verifyOtp( PhoneAuthCredential phoneAuthCredential)async {
    setState(() {
      isLoading = true;
    });
    // emit(AuthLoading());
    try {
      final authCredential = await _auth.signInWithCredential(
          phoneAuthCredential);
      setState(() {
        isLoading = false;
      });
      if (authCredential?.user != null){
        await userBloc.login(
            loginRequest: LoginRequest(uid: _auth.currentUser.uid));
    }
  } on FirebaseAuthException catch (e){
      setState(() {
        isLoading = false;
      });
      // showErrorAnimatedToast(context: context, msg: onError.toString());

    }
  }

}
