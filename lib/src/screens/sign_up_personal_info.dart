import 'dart:ui';

import 'package:app_template/src/screens/sign_up_page.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/widgets/build_button.dart';
import 'package:app_template/src/widgets/user_form_field.dart';
import 'package:flutter/material.dart';

class SignUpPersonalInfo extends StatefulWidget {
  @override
  _SignUpPersonalInfoState createState() => _SignUpPersonalInfoState();
}

class _SignUpPersonalInfoState extends State<SignUpPersonalInfo> {
  TextEditingController firstNameTextEditingController =
      new TextEditingController();
  TextEditingController lastNameTextEditingController =
      new TextEditingController();
  bool loading = false;
  detailsCheck(String firstname, String lastname) {
    if (firstname.isNotEmpty && lastname.isNotEmpty) {
      print(firstname);
      push(
          context,
          SignUpPage(
            firstName: firstname,
            lastName: lastname,
          ));
    } else {
      showToastCommon("Please fill al the details");
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        //backgroundColor: Constants.kitGradients[30],
        body: SingleChildScrollView(
            child: Container(
      width: screenWidth(context, dividedBy: 1),
      height: screenHeight(context, dividedBy: 1),
      decoration: BoxDecoration(
        color: Constants.kitGradients[31],
        //color: Colors.grey[900],
      ),
      alignment: Alignment.center,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            width: screenWidth(context, dividedBy: 1),
            height: screenHeight(context, dividedBy: 1),
            decoration: BoxDecoration(
              //border: Border(top:BorderSide(color:Constants.kitGradients[28],width: 2.0),),
              color: Constants.kitGradients[0],
              borderRadius: BorderRadius.circular(8),
              // image: DecorationImage(
              //   image: AssetImage(
              //     "assets/images/foodapp_bg.jpg",
              //   ),
              //   fit: BoxFit.cover,
              // ),
            ),
            padding: EdgeInsets.symmetric(
                horizontal: screenWidth(context, dividedBy: 10)),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                SizedBox(
                  height: screenHeight(context, dividedBy: 8),
                ),
                Image.asset(
                  "assets/images/logo.png",
                  color: Constants.kitGradients[28],
                ),
                SizedBox(
                  height: screenHeight(context, dividedBy: 9),
                ),
                Align(
                  alignment: Alignment.centerLeft,
                  child: Text(
                    "Hey Welcome !",
                    textAlign: TextAlign.left,
                    style: TextStyle(
                      fontFamily: "Montserrat",
                      color: Constants.kitGradients[28],
                      fontWeight: FontWeight.bold,
                      fontSize: 24,
                    ),
                  ),
                ),
                SizedBox(
                  height: screenHeight(context, dividedBy: 30),
                ),
                FormFeildUserDetails(
                  labelText: "First Name",
                  textEditingController: firstNameTextEditingController,
                  //otp: false,
                ),
                SizedBox(
                  height: screenHeight(context, dividedBy: 20),
                ),
                FormFeildUserDetails(
                  labelText: "Last Name",
                  textEditingController: lastNameTextEditingController,
                  //otp: false,
                ),
                SizedBox(
                  height: screenHeight(context, dividedBy: 20),
                ),
                BuildButton(
                  title: "Next",
                  isLoading: loading,
                  onPressed: () {
                    print(lastNameTextEditingController.text);
                    detailsCheck(firstNameTextEditingController.text,
                        lastNameTextEditingController.text);
                  },
                ),
                SizedBox(
                  height: screenHeight(context, dividedBy: 30),
                ),
              ],
            ),
          )
        ],
      ),
    )));
  }
}
