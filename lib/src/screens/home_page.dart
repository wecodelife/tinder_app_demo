import 'dart:async';
import 'package:app_template/src/bloc/user_bloc.dart';
import 'package:app_template/src/models/get_food_response_Model.dart';
import 'package:app_template/src/models/home_page_hive_model.dart';
import 'package:app_template/src/models/imageModel.dart';
import 'package:app_template/src/models/update_like_request_model.dart';
import 'package:app_template/src/models/update_mustvisit_request_model.dart';
import 'package:app_template/src/screens/active_card.dart';
import 'package:app_template/src/screens/data_page.dart';
import 'package:app_template/src/screens/feed_page.dart';
import 'package:app_template/src/screens/profile_page.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/urls.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/widgets/back_button.dart';
import 'package:app_template/src/widgets/feature_discovery_swipe.dart';
import 'package:app_template/src/widgets/home_tab_bar.dart';
import 'package:draggable_widget/draggable_widget.dart';
import 'package:feature_discovery/feature_discovery.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart' show timeDilation;
import 'package:flutter/services.dart';
import 'package:hive/hive.dart';
import 'package:shimmer/shimmer.dart';
import '../utils/object_factory.dart';
import '../utils/utils.dart';
import 'liked_item_screen.dart';

enum DataFilter { ALL, COMPLETED, PROGRESS }

class HomeScreen extends StatefulWidget {
  final bool count;
  HomeScreen({
    this.count = false,
  });
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> with TickerProviderStateMixin {
  AnimationController _buttonController;
  Animation<double> rotate;
  Animation<double> right;
  Animation<double> bottom;
  Animation<double> width;
  Result result;
  bool visible = false;
  List<dynamic> resultDynamic;
  int flag = 0;
  int foodId;
  bool rewind = true;
  int imageNumber;
  int position = 0;
  List selectedData = [];
  UserBloc userBloc = new UserBloc();
  List<Map<String, dynamic>> removedData = [];
  ImageModel rewindData;
  Map<String, dynamic> foodData = new Map();
  Box<HomePageHiveModel> imageBox;
  // ValueNotifier<String> _homePageList = ValueNotifier("HomePagelist");
  ValueListenable foodList;
  bool hiveLoading = true;
  bool isLoading = false;
  bool isLiked = false;
  bool isFavourite = false;
  bool doubleTapTrue = false;
  int feature = 0;
  bool onRotation = true;
  bool rotationStatus = false;
  List<NetworkImage> cachedImages;
  List<List<Image>> images = [];
  List<Image> dummyImages = [];
  int index = 0;
  bool showError = false;
  String errorMessage = "";
  List<Image> foodImagelist = [];

  Future openBox() async {
    imageBox = await Hive.openBox<HomePageHiveModel>("imageBox");

    if (imageBox.isOpen == null) {
      print("Loading");
      hiveLoading = true;
    } else {
      hiveLoading = false;
    }
  }

  @override
  // void initState() {
  //   // TODO: implement initState
  //   WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
  //     FeatureDiscovery.discoverFeatures(context, <String>[
  //       'feature1',
  //       'feature2',
  //       // 'feature3',
  //       // 'feature4',
  //     ]);
  //   });
  //   super.initState();
  // }
  insertImage(List<String> imageUrl, int index) async {
    dummyImages.clear();
    // print("ImagesLength :" + imageUrl.length.toString());
    for (int i = 0; i < imageUrl.length; i++) {
      // print("insertImage" + index.toString());
      dummyImages.add((Image.network(imageUrl[i])));
    }
    images.add(dummyImages.toList());
    // print("ImageNetwork" + images.first.toString());
    // print("Image" + dummyImages.toString());
    // if (images.length > 1) print("ImageNetwork" + images[1].toString());
     cacheImage(context, index);
  }

  cacheImage(BuildContext context, int index) {
    // print("cacheImages");
    for (int i = 0; i < (images[index].length); i++) {
      // print("cahche" + i.toString());
      precacheImage(images[index][i].image, context);
      foodImagelist.add(images[index][i]);
    }
    print("Food Image List " + foodImagelist.toString());
  }

  toggleImageCache(
      BuildContext context, List<String> imageUrl, int index) async {
    await insertImage(imageUrl, index);
  }

  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      FeatureDiscovery.discoverFeatures(context, <String>[
        'feature1',
        // 'feature2',
        // 'feature3',
        // 'feature4',
        // 'feature5',
      ]);
    });
    // print("Email Id :" + ObjectFactory().appHive.getEmail());
    // print("data length:" + data.length.toString());
    if (data.length == 0) {
      setState(() {
        isLoading = true;
        // print("isLoading" + isLoading.toString());
        feature == 0
            ? setState(() {
                feature = 1;
              })
            : setState(() {
                feature = 0;
              });
      });
      print("FApi Calling");
      userBloc.getFoodDetails(null);
    } else {
      print("Data length " + data.length.toString());
      index = data.length - 1;
      print("Index value " + index.toString());
    }

    userBloc.getFoodDetailsResponse.listen((event) async {
      if (event.statusCode == 200) {
        ObjectFactory().appHive.putNextImageValue(event.next);

        for (int i = 0; i < event.results.length; i++) {
          // print("cahcing");

          await toggleImageCache(context, event.results[i].images, i);
        }
        for (int i = 0; i < event.results.length; i++) {
          // print("data adding");
          print("index" + i.toString());
          print("index" + event.results[i].images.toString());
          print("index" + event.results[i].name.toString());

          data.insert(
              i,
              ImageModel(
                id: event.results[i].id,
                name: event.results[i].name,
               // images: images[i],
                price: event.results[i].price,
                description: event.results[i].description,
                foodType: event.results[i].foodType,
                rating: event.results[i].rating,
                place: event.results[i].place,
                restaurant: event.results[i].restaurant,
              ));
        }
        print(data.length);
        index = data.length - 1;
        print("DataLength  " + index.toString());
        print("Images" + images.toString());

        // print("dataList 0 " + data[0].images[0].toString());
        // print("dataList 1 " + data[1].images[1].toString());
        // print("dataList 2 " + data[2].images[2].toString());
        // print("dataList 3 " + data[3].images[3].toString());

        // print("count value Api: " + count.toString());
      }
      setState(() {
        isLoading = false;
      });

      // print("imageBoxitem" + imageBox.get(key).results.toString());
    }).onError((event) {
      setState(() {
        isLoading = false;
        showError = true;
      });
      errorMessage = event;
    });

    userBloc.updateLikeResponse.listen((event) {
      if (event.status == 201 || event.status == 200) {
      } else {
        showToastCommon(event.message);
      }
    });
    // userBloc.updateFavouriteResponse.listen((event) {
    //   if (event.statusCode == 201 || event.statusCode == 200) {
    //     print("Favorite added successfully");
    //     print("Favorite Data " + event.data.toString());
    //     // showToastCommon("Food Added to Favourite List");
    //   } else {
    //     showToastCommon(event.message);
    //   }
    // });
    // super.initState();
    openBox();
    _buttonController = new AnimationController(
        duration: new Duration(seconds: 1), vsync: this);

    rotate = new Tween<double>(
      begin: -0.0,
      end: -40,
    ).animate(
      new CurvedAnimation(
        parent: _buttonController,
        curve: Curves.ease,
      ),
    );

    rotate.addListener(() async {
      setState(() {
        print("---------------------ListenIng-----------------");
      });

      if (rotate.isCompleted) {
        visible = false;
        rewind = false;
        rewindData = data.last;
        data.removeLast();
        index = data.length - 1;
        rotationStatus = false;

        _buttonController.reset();
        print(data.length);
        if (data.length == 0) {
          userBloc.getFoodDetails(ObjectFactory().appHive.getNextImageValue());
          isLoading = true;
          feature == 0 ? feature = 1 : feature = 0;
        }
      }

      //
    });

    right = new Tween<double>(
      begin: 0.0,
      end: 400.0,
    ).animate(
      new CurvedAnimation(
        parent: _buttonController,
        curve: Curves.ease,
      ),
    );
    bottom = new Tween<double>(
      begin: 15.0,
      end: 100.0,
    ).animate(
      new CurvedAnimation(
        parent: _buttonController,
        curve: Curves.ease,
      ),
    );
    width = new Tween<double>(
      begin: 20.0,
      end: 25.0,
    ).animate(
      new CurvedAnimation(
        parent: _buttonController,
        curve: Curves.bounceOut,
      ),
    );
  }

  @override
  void dispose() {
    print("Button Controller Dispose");
    _buttonController.dispose();
    super.dispose();
  }

  Future<Null> _swipeAnimation() async {
    try {
      await _buttonController.forward();
    } on TickerCanceled {}
  }

  dismissImg(String img) {
    setState(() {
      data.remove(img);
    });
  }

  addImg(String img) {
    setState(() {
      data.remove(img);
      selectedData.add(img);
    });
  }

  swipeRight(int foodId) async {
    if (flag == 0)
      setState(() {
        flag = 1;
      });
    onRotation = true;
    _swipeAnimation();

    await userBloc.updateLike(
        updateLikeRequestModel: UpdateLikeRequestModel(
      food: foodId,
      //user: int.parse(ObjectFactory().appHive.getUserId()),
      isLiked: false,
    )
    );
  }

  swipeLeft(int foodId) async {
    if (flag == 1)
      setState(() {
        flag = 0;
      });
    _swipeAnimation();
    // setState(() {});

    await userBloc.updateLike(
        updateLikeRequestModel: UpdateLikeRequestModel(
      food: foodId,
     // user: int.parse(ObjectFactory().appHive.getUserId()),
      isLiked: true,
    ));

    // _swipeAnimation();
  }

  var offset = Offset(0, 0);
  DragController dragController = new DragController();

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(
      SystemUiOverlayStyle(
        statusBarColor: Constants.kitGradients[28],
        statusBarBrightness: Brightness.light,
        statusBarIconBrightness: Brightness.light,
      ),
    );
    timeDilation = 0.9;
    // double initialBottom = screenHeight(context, dividedBy: 20);
    // // double backCardPosition = initialBottom +
    // //     (dataLength - 1) * screenHeight(context, dividedBy: 100) +
    // //     10;
    // double backCardPosition = initialBottom +
    //     (1) * screenHeight(context, dividedBy: 200);
    // double backCardWidth = screenWidth(context, dividedBy: 2);
    return SafeArea(
          child: Scaffold(
      backgroundColor: Constants.kitGradients[31],
      body: Container(
      width: screenWidth(context, dividedBy: 1),
      height: screenHeight(context, dividedBy: 1),
      decoration:BoxDecoration(
        color:Constants.kitGradients[31],
            // image:DecorationImage(
            //   fit: BoxFit.cover,
            //   image:AssetImage("assets/images/bg_image.jpg"),
            //   colorFilter: new ColorFilter.mode(
            //       Constants.kitGradients[28].withOpacity(0.6), BlendMode.lighten),
            // )
      ),
      child: showError == false
            ? isLoading == false
                ? Stack(
                    children: [
                      index >= 0
                          ? CardDemo(
                              rotationStatus: rotationStatus,
                              foodName: data[index].name,
                              price: data[index].price,
                              type: data[index].foodType.toString(),
                              //foodImageSection: data[index].images,
                              rating: 3.0,
                              hotel: data[index].restaurant,
                              bottom: bottom.value,
                              right: right.value,
                              keyValue: 1,
                              buttonController: _buttonController,
                              // onTap: () {
                              //   print("object" + item.toString());
                              //   print("object" + data.toString());
                              // },
                              left: 0.0,
                              cardWidth: screenWidth(context, dividedBy: 1),
                              rotation: rotate.value,
                              skew: rotate.value < -10 ? 0.1 : 0.0,
                              dismissImg: dismissImg,
                              flag: flag,
                              addImg: addImg,
                              foodId: data[index].id,
                              onDoubleTap: () async {
                                doubleTapTrue = true;
                                showToastLiked(context);
                                // await userBloc.updateLike(
                                //     updateLikeRequestModel:
                                //         UpdateLikeRequestModel(
                                //   food: data[index].id,
                                //   user: int.parse(
                                //       ObjectFactory().appHive.getUserId()),
                                //   isLiked: true,
                                // ));
                              },

                              swipeRight: () {
                                swipeRight(data[index].id);
                              },
                              swipeLeft: () {
                                swipeLeft(data[index].id);
                              },
                              onPressedFavourite: () {
                                print("Saved");
                                print("Saved By " + ObjectFactory().appHive.getUserId() ) ;
                                print("fOOD id " + data[index].id.toString() ) ;
                                // userBloc.updateFavourite(
                                //     updateFavouriteRequestModel:
                                //         UpdateFavouriteRequestModel(
                                //   food: data[index].id.toString(),
                                //   user:
                                //       ObjectFactory().appHive.getUserId(),
                                //   isFavourite: true,
                                // ));
                              },
                              onVerticalDrag: () {
                                visible = !visible;
                                print("visible" + visible.toString());
                                setState(() {});
                              },
                            )
                          : Container(
                              width: screenWidth(context, dividedBy: 1),
                              height: screenHeight(context, dividedBy: 1),
                        decoration:BoxDecoration(
                            image:DecorationImage(
                              fit: BoxFit.cover,
                              image:AssetImage("assets/images/bg_image.jpg"),
                              colorFilter: new ColorFilter.mode(
                                  Constants.kitGradients[28].withOpacity(0.6), BlendMode.lighten),
                            )
                        ),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Center(
                                    child:  Text(
                                      "You don't have any more Dishes to see!",
                                      textAlign: TextAlign.center,
                                      style:  TextStyle(
                                          color: Colors.white,
                                          fontWeight: FontWeight.bold,
                                          fontFamily: "OswaldRegular",
                                          fontSize: 26),
                                    ),

                                  ),
                                  Padding(
                                    padding: EdgeInsets.symmetric(horizontal:screenWidth(context, dividedBy:20),vertical:screenHeight(context, dividedBy:40)),
                                    child: Text(
                                      "Eat what you like, what's trending, we'll suggest you the right food for you. Paziq is not only a solution to the lack of cooking mood, it is the solution to exploring the best food for a food lover. Explore the best taste around you wherever you go with Paziq!",
                                      textAlign: TextAlign.center,
                                      style:  TextStyle(
                                          color: Colors.white,
                                          fontWeight: FontWeight.w600,
                                          fontFamily: "OswaldRegular",
                                          fontSize: 12),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                      DraggableBackButton(
                        backButtonPress: () {
                          if (rewind == false && isLoading == false) {
                            rewind = true;
                            data.add(rewindData);
                            index = data.length - 1;
                            print("dataLenght" + index.toString());
                            print("rewind is working");

                            setState(() {});
                          }
                          // pushAndReplacement(
                          //     context,
                          //     ProfilePage(
                          //         userPic:
                          //         "https://images.unsplash.com/photo-1467003909585-2f8a72700288?ixid=MnwxMjA3fDB8MHxzZWFyY2h8OXx8Zm9vZHxlbnwwfHwwfHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=600&q=60",
                          //         userName: "SanJE"));
                        },
                      ),

                      Visibility(
                        visible: visible ? true : false,
                        child: HomeTabBar(
                          onTapLiked: () {
                            push(context, LikedItemsScreen());
                          },
                          onTapHome: () {
                            push(context, HomeScreen(count: true));
                          },
                          // onTapSaved: () {
                          //   push(context, FeedPage());
                          // },
                          onTapProfile: () {
                            pushAndReplacement(
                                context,
                                ProfilePage(
                                    userPic:
                                        "https://images.unsplash.com/photo-1467003909585-2f8a72700288?ixid=MnwxMjA3fDB8MHxzZWFyY2h8OXx8Zm9vZHxlbnwwfHwwfHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=600&q=60",
                                    userName: "SanJE"));
                          },
                        ),
                      ),
                      Positioned(
                        left: screenWidth(context, dividedBy:9),
                        top: screenHeight(context, dividedBy: 13),
                        child:      DescribedFeatureOverlay(
                            featureId: 'feature1',
                            targetColor: Constants.kitGradients[29],
                            // textColor: Constants.kitGradients[19],
                            pulseDuration: Duration(seconds: 1),
                            enablePulsingAnimation: false,
                            barrierDismissible: false,
                            backgroundColor: Constants.kitGradients[29],
                            contentLocation: ContentLocation.below,
                            // onComplete: action,

                            title: Text(
                              'Swipe Down',
                              style: TextStyle(
                                fontSize: 20,
                                fontWeight: FontWeight.bold,
                                color: Constants.kitGradients[0],
                                fontFamily: "Prompt-Light",
                              ),
                            ),
                            overflowMode: OverflowMode.extendBackground,
                            openDuration: Duration(seconds: 1),
                            description: Text(
                              'Swipe down to get the Tab bar menu of other fields',
                              style: TextStyle(
                                fontSize: 17,
                                fontWeight: FontWeight.w500,
                                color: Constants.kitGradients[0].withOpacity(0.6),
                                fontFamily: "Prompt-Light",
                              ),
                            ),
                            tapTarget: Container(
                                child:Shimmer.fromColors(
                                  baseColor: Constants.kitGradients[31].withOpacity(0.6),
                                  highlightColor: Colors.white,
                                  enabled: true,
                                  child:Icon(Icons.keyboard_arrow_down_outlined)
                                //   Image(
                                //   image: AssetImage("assets/images/arrow_down.png"),
                                //   //fit: BoxFit.fitHeight,
                                //   height: screenWidth(context, dividedBy: 20),
                                //   width: screenWidth(context, dividedBy: 20),
                                // ),
                                ),
                            ),
                            child:Container()
                        ),
                      ),
                      Positioned(
                        left: 30,
                        top: screenHeight(context, dividedBy: 2),
                        child:      DescribedFeatureOverlay(
                            featureId: 'feature2',
                            targetColor: Constants.kitGradients[29],
                            // textColor: Constants.kitGradients[19],
                            pulseDuration: Duration(seconds: 1),
                            enablePulsingAnimation: false,
                            barrierDismissible: false,
                            backgroundColor: Constants.kitGradients[29],
                            contentLocation: ContentLocation.below,
                            // onComplete: action,

                            title: Text(
                              'Swipe Left',
                              style: TextStyle(
                                fontSize: 20,
                                fontWeight: FontWeight.bold,
                                color: Constants.kitGradients[0],
                                fontFamily: "Prompt-Light",
                              ),
                            ),
                            overflowMode: OverflowMode.extendBackground,
                            openDuration: Duration(seconds: 1),
                            description: Text(
                              'Swipe Left to swipe past the current food item',
                              style: TextStyle(
                                fontSize: 17,
                                fontWeight: FontWeight.w500,
                                color: Constants.kitGradients[0].withOpacity(0.6),
                                fontFamily: "Prompt-Light",
                              ),
                            ),
                            tapTarget: Container(
                                child:Shimmer.fromColors(
                                  baseColor: Constants.kitGradients[31].withOpacity(0.6),
                                  highlightColor: Colors.white,
                                  enabled: true,
                                  child:Icon(Icons.chevron_left)
                                //   Image(
                                //   image: AssetImage("assets/images/arrow_down.png"),
                                //   //fit: BoxFit.fitHeight,
                                //   height: screenWidth(context, dividedBy: 20),
                                //   width: screenWidth(context, dividedBy: 20),
                                // ),
                                ),
                            ),
                            child:Container()
                        ),
                      ),
                      Positioned(
                        right: 30,
                        top: screenHeight(context, dividedBy: 2),
                        child:      DescribedFeatureOverlay(
                            featureId: 'feature3',
                            targetColor: Constants.kitGradients[29],
                            // textColor: Constants.kitGradients[19],
                            pulseDuration: Duration(seconds: 1),
                            enablePulsingAnimation: false,
                            barrierDismissible: false,
                            backgroundColor: Constants.kitGradients[29],
                            contentLocation: ContentLocation.below,
                            // onComplete: action,

                            title: Text(
                              'Swipe Right',
                              style: TextStyle(
                                fontSize: 20,
                                fontWeight: FontWeight.bold,
                                color: Constants.kitGradients[0],
                                fontFamily: "Prompt-Light",
                              ),
                            ),
                            overflowMode: OverflowMode.extendBackground,
                            openDuration: Duration(seconds: 1),
                            description: Text(
                              'Swipe right to like the food item',
                              style: TextStyle(
                                fontSize: 17,
                                fontWeight: FontWeight.w500,
                                color: Constants.kitGradients[0].withOpacity(0.6),
                                fontFamily: "Prompt-Light",
                              ),
                            ),
                            tapTarget: Container(
                                child:Shimmer.fromColors(
                                  baseColor: Constants.kitGradients[31].withOpacity(0.6),
                                  highlightColor: Colors.white,
                                  enabled: true,
                                  child:Icon(Icons.chevron_right)
                                //   Image(
                                //   image: AssetImage("assets/images/arrow_down.png"),
                                //   //fit: BoxFit.fitHeight,
                                //   height: screenWidth(context, dividedBy: 20),
                                //   width: screenWidth(context, dividedBy: 20),
                                // ),
                                ),
                            ),
                            child:Container()
                        ),
                      ),
                      index >= 0
                          ? Positioned(
                              left: 0,
                              top: screenHeight(context, dividedBy: 3),
                              child: FeatureSwipe(
                                count: true,
                              ))
                          : Center(
                              child: CircularProgressIndicator(
                                valueColor: AlwaysStoppedAnimation(
                                    Constants.kitGradients[27]),
                                strokeWidth: 2,
                              ),
                            ),

                      // print("data" + data.length.toString());
                      // data.removeAt(0);
                      // print("data" + data.length.toString());
                      // data.insert(data.length - 1, rewindData);
                      // print("data" + data.length.toString());
                      // }
                    ],
                  )
                : Center(
                    child: CircularProgressIndicator(
                      valueColor: AlwaysStoppedAnimation(
                          Constants.kitGradients[28]),
                      strokeWidth: 2,
                    ),
                  )
            : Container(
                height: screenHeight(context, dividedBy: 1.2),
                child: Center(
                  child: Text(
                    errorMessage,
                    style: TextStyle(
                        color: Constants.kitGradients[28],
                        fontSize: 16,
                        fontFamily: "OswaldRegular"),
                  ),
                ),
              )),
    ),
        );
  }
}

// DescribedFeatureOverlay(
//   featureId: 'feature1',
//   targetColor: Constants.kitGradients[0],
//   // textColor: Constants.kitGradients[19],
//   pulseDuration: Duration(seconds: 1),
//   enablePulsingAnimation: true,
//   barrierDismissible: false,
//   backgroundColor:
//       Constants.kitGradients[28].withOpacity(0.5),
//   contentLocation: ContentLocation.above,
//   overflowMode: OverflowMode.extendBackground,
//   openDuration: Duration(seconds: 1),
//   //onComplete: action,
//
//   title: Text(
//     'Swipe right',
//     style: TextStyle(
//       fontSize: 20,
//       fontWeight: FontWeight.bold,
//       color: Constants.kitGradients[27],
//       fontFamily: "Prompt-Light",
//     ),
//   ),
//   description: Text(
//     'You can Swipe Right to add an item into liked list',
//     style: TextStyle(
//       fontSize: 17,
//       fontWeight: FontWeight.w500,
//       color: Constants.kitGradients[27],
//       fontFamily: "Prompt-Light",
//     ),
//   ),
//   tapTarget: Icon(
//     Icons.swipe,
//     color: Constants.kitGradients[28],
//   ),
//   child: Container(),
// ),
