import 'package:app_template/src/bloc/user_bloc.dart';
import 'package:app_template/src/screens/about_us_page.dart';
import 'package:app_template/src/screens/edit_profile_page.dart';
import 'package:app_template/src/screens/home_page_screen.dart';
import 'package:app_template/src/screens/liked_item_screen.dart';
import 'package:app_template/src/screens/login_page.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/object_factory.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/widgets/app_bar.dart';
import 'package:app_template/src/widgets/creator_name.dart';
import 'package:app_template/src/widgets/list_tile.dart';
import 'package:app_template/src/widgets/three_bounce..dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class ProfilePage extends StatefulWidget {
  //const ProfilePage({Key? key}) : super(key: key);
  final String userPic;
  final String userName;
  final Widget navigationWidget;
  ProfilePage({this.userPic, this.userName,this.navigationWidget});

  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  UserBloc userBloc = UserBloc();
  bool loading =  false;
  void logOut() {
    cancelAlertBox(
        context: context,
        button1Name: "Yes",
        button2Name: "No",
        msg: "Do you want to Logout?",
        titlePadding: 4,
        text2: "",
        text1: "",
        contentPadding: 20,
        insetPadding: 2.7,
        onPressedNo: () {
          pop(context);
         // setState(() {});
        },
        onPressedYes: () {
          loading = true;
          setState(() {
          });
          pop(context);

          userBloc.logout();
        });
  }
  @override
  void initState() {
    print("image"+ObjectFactory().appHive.getProfileImage().toString());
    print("name"+ObjectFactory().appHive.getPhoneNumber().toString());
    print("number"+ObjectFactory().appHive.getName().toString());
    userBloc.logoutResponse.listen((event) async{
setState(() {
  loading = false;
});
await hiveClear();
push(context, LoginPage());
    });
    super.initState();
  }
  Future<void> hiveClear() {
    ObjectFactory().appHive.clearToken();
    ObjectFactory().appHive.clearID();
    ObjectFactory().appHive.clearImageList();
    ObjectFactory().appHive.clearName();
    ObjectFactory().appHive.clearProfilePic();
    ObjectFactory().appHive.clearPhone();
    return null;
  }
  @override
  Widget build(BuildContext context) {

    return WillPopScope(
      onWillPop: () {
        Navigator.of(context).pushAndRemoveUntil(
            MaterialPageRoute(builder: (context) => HomePageScreen()),
                (Route<dynamic> route) => false);
      },
      child: Scaffold(
          backgroundColor: Constants.kitGradients[31],
          appBar: PreferredSize(
            preferredSize:Size.fromHeight(80.0),
            child: AppBar(
              automaticallyImplyLeading: false,
              backgroundColor: Constants.kitGradients[31],
              elevation: 0,
              leading: Container(),
              actions: [
                AppBarFoodApp(
                  title: "My Profile",
                  leftIcon: IconButton(
                    alignment: Alignment.center,
                    icon: Icon(
                      Icons.arrow_back_ios,
                      color: Constants.kitGradients[28],
                    ),
                  ),
                  onPressedLeftIcon: () {
                    pushAndRemoveUntil(context, HomePageScreen(), false);
                  },
                  rightIcon: IconButton(
                    icon: Icon(
                      Icons.favorite,
                      color: Constants.kitGradients[28],
                    ),
                  ),
                  onPressedRightIcon: (){
                    push(context, LikedItemsScreen());
                  },
                ),

              ],
            ),
          ),
          body: loading == false ?
          Stack(
            children: [
              Container(
                padding: EdgeInsets.symmetric(horizontal: 10),
                width: screenWidth(context, dividedBy: 1),
                height: screenHeight(context, dividedBy: 1),
                alignment: Alignment.center,
                child: Column(
                  children: [
                    Container(
                      // height: screenHeight(context, dividedBy: 3),
                      width: screenWidth(context, dividedBy: 1),
                      color: Constants.kitGradients[31],
                      child: SingleChildScrollView(
                        child: Column(
                          children: [
                            // Container(
                            //   width: screenWidth(context, dividedBy: 1),
                            //   height: screenHeight(context, dividedBy: 15),
                            //   color: Colors.transparent,
                            //   child: Row(
                            //     children: [
                            //       SizedBox(
                            //         width: screenWidth(context, dividedBy: 30),
                            //       ),
                            //       Text(
                            //         "PROFILE",
                            //         style: TextStyle(
                            //           fontSize: 17,
                            //           fontWeight: FontWeight.w600,
                            //           color: Constants.kitGradients[30],
                            //         ),
                            //       )
                            //     ],
                            //   ),
                            // ),
                            SizedBox(
                              height: screenHeight(context, dividedBy: 12.9),
                            ),
                            Row(
                              children: [
                                SizedBox(
                                  width: screenWidth(context, dividedBy: 6),
                                ),
                                Container(
                                  height: screenWidth(context, dividedBy: 3.5),
                                  width: screenWidth(context, dividedBy: 1.3),
                                  decoration: BoxDecoration(
                                      color: Constants.kitGradients[28],
                                      borderRadius: BorderRadius.circular(10)),
                                  padding: EdgeInsets.only(
                                    left: screenWidth(context, dividedBy: 6.4),
                                    top: screenWidth(context, dividedBy: 25),
                                    bottom: screenWidth(context, dividedBy: 20),
                                  ),
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    children: [
                                      SizedBox(
                                        height:
                                            screenHeight(context, dividedBy: 70),
                                      ),
                                      Text(
                                        ObjectFactory().appHive.getName() == null
                                            ? "Not Available"
                                            : ObjectFactory().appHive.getName(),
                                        style: TextStyle(
                                          fontSize: 20,
                                          fontWeight: FontWeight.w400,
                                          color: Constants.kitGradients[12],
                                        ),
                                      ),
                                      SizedBox(
                                        height:
                                        screenHeight(context, dividedBy: 70),
                                      ),
                                      Text(
                                        ObjectFactory().appHive.getPhoneNumber() == null
                                            ? "Not Available"
                                            : ObjectFactory()
                                            .appHive
                                            .getPhoneNumber()
                                            .toString(),
                                        style: TextStyle(
                                          fontSize: 13,
                                          fontWeight: FontWeight.w400,
                                          color: Constants.kitGradients[12],
                                        ),
                                      ),

                                      SizedBox(width:screenWidth(context, dividedBy:30)),
                                      // Text(
                                      //   "Swag",
                                      //   style: TextStyle(
                                      //     fontSize: 10,
                                      //     fontWeight: FontWeight.w400,
                                      //     color: Constants.kitGradients[31],
                                      //   ),
                                      // ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                            SizedBox(
                              height: screenHeight(context, dividedBy: 10),
                            ),
                            Container(
                              padding: EdgeInsets.symmetric(
                                  vertical: 20, horizontal: 30),
                              // height: screenHeight(context,dividedBy: 5.8),
                              decoration: BoxDecoration(
                                  color: Constants.kitGradients[28],
                                  borderRadius: BorderRadius.circular(15)),
                              child: Container(
                                child: Column(
                                  children: [
                                    // AppListTile(
                                    //   text: "Change Password",
                                    //   onTap: () {
                                    //     push(context, ChangePasswordPage());
                                    //   },
                                    // ),
                                    // SizedBox(
                                    //   height:
                                    //       screenHeight(context, dividedBy: 100),
                                    // ),
                                    // Divider(
                                    //   thickness: 0.2,
                                    //   color: Constants.kitGradients[31]
                                    //       .withOpacity(0.7),
                                    // ),
                                    // SizedBox(
                                    //   height:
                                    //       screenHeight(context, dividedBy: 100),
                                    // ),
                                    // AppListTile(
                                    //   text: "Add Foods",
                                    //   onTap: () {
                                    //     push(context, UpdateFoodDetails());
                                    //   },
                                    // ),
                                    // SizedBox(
                                    //   height:
                                    //       screenHeight(context, dividedBy: 100),
                                    // ),
                                    // Divider(
                                    //   thickness: 0.2,
                                    //   color: Constants.kitGradients[31]
                                    //       .withOpacity(0.7),
                                    // ),
                                    AppListTile(
                                      text: "Edit Profile",
                                      onTap: () {
                                        push(context, EditProfilePage(
                                          id: int.parse(ObjectFactory().appHive.getUserId()),
                                          profileImage: ObjectFactory().appHive.getProfileImage(),
                                          userName: ObjectFactory().appHive.getName(),
                                          phoneNumber:ObjectFactory().appHive.getPhoneNumber(),
                                        ));
                                      },
                                    ),
                                    SizedBox(
                                      height:
                                      screenHeight(context, dividedBy: 100),
                                    ),
                                    Divider(
                                      thickness: 0.2,
                                      color: Constants.kitGradients[31]
                                          .withOpacity(0.6),
                                    ),
                                    SizedBox(
                                      height:
                                          screenHeight(context, dividedBy: 100),
                                    ),
                                    AppListTile(
                                      text: "About Us",
                                      onTap: () {
                                        push(context, AboutUsPage());
                                      },
                                    ),
                                    SizedBox(
                                      height:
                                          screenHeight(context, dividedBy: 100),
                                    ),
                                    Divider(
                                      thickness: 0.2,
                                      color: Constants.kitGradients[31]
                                          .withOpacity(0.7),
                                    ),
                                    SizedBox(
                                      height:
                                          screenHeight(context, dividedBy: 100),
                                    ),
                                    AppListTile(
                                      text: "Logout",

                                      onTap: () {
                                        // setState(() {
                                           loading = true;
                                        // });
                                       // userBloc.logout();
                                        logOut();
                                      },
                                    ),
                                    SizedBox(
                                      height:
                                          screenHeight(context, dividedBy: 100),
                                    ),
                                    Divider(
                                      thickness: 0.2,
                                      color: Constants.kitGradients[31]
                                          .withOpacity(0.7),
                                    ),
                                    SizedBox(
                                      height:
                                      screenHeight(context, dividedBy: 100),
                                    ),

                                  ],
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                    Spacer(),
                    CreatorName(),
                    SizedBox(
                      height:
                      screenHeight(context, dividedBy: 100),
                    ),
                  ],
                ),
              ),
              Positioned(
                top: screenHeight(context, dividedBy: 52),
                left: screenHeight(context, dividedBy: 100),
                child: Container(
                  width: screenWidth(context, dividedBy: 2.8),
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    border: Border.all(color: Constants.kitGradients[28], width: 8),
                    color: Constants.kitGradients[28],
                  ),
                  child: CircleAvatar(
                    radius: screenWidth(context, dividedBy: 4),
                    backgroundColor: Constants.kitGradients[28],
                    child: CachedNetworkImage(
                      imageUrl: ObjectFactory().appHive.getProfileImage() ==
                              null
                          ?
                      "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRony1PUEAFW_rKWuriSeChlMZK05SNCoyhblOQpH5tBq1m5C_HHsKEJvveSdHRdSj_zJ4&usqp=CAU"
                          : ObjectFactory().appHive.getProfileImage(),
                      imageBuilder: (context, imageProvider) => Container(
                        // width: screenWidth(context, dividedBy: 3),
                        // height: screenWidth(context, dividedBy: 3),
                        decoration: BoxDecoration(
                          color: Constants.kitGradients[28],
                          shape: BoxShape.circle,
                          image: DecorationImage(
                              image: imageProvider, fit: BoxFit.cover),
                        ),
                      ),
                      placeholder: (context, url) => Center(
                        heightFactor: 1,
                        widthFactor: 1,
                        child: Center(
                          child: CircularProgressIndicator(
                            valueColor: AlwaysStoppedAnimation(
                                Constants.kitGradients[28]),
                            strokeWidth: 2,
                          ),
                        ),
                      ),
                      //fit: BoxFit.cover,
                    ),
                  ),
                ),
              ),
              // Positioned(
              //   bottom: 0,
              //   child: CustomBottomBar(
              //     currentIndex: 4,
              //     onTapHome: () {
              //       push(context, HomePage());
              //     },
              //     onTapProfile: () {},
              //     onTapSearch: () {
              //       push(context, SearchPage());
              //     },
              //     onMarketPlace: () {
              //       push(context, MarketPlacePage());
              //     },
              //     onTapUpcoming: () {
              //       push(context, UpcomingVideos());
              //     },
              //   ),
              // )
            ],
          ) : Container(
            height: screenHeight(context,dividedBy: 1.1),
            width: screenWidth(context,dividedBy: 1),
            color: Constants.kitGradients[31],
            child: Center(
              child: SpinKitThreeBounce(color: Constants.kitGradients[28],),
            ),
          )
          // SingleChildScrollView(
          //   child: Container(
          //       width: screenWidth(context, dividedBy: 1),
          //       // height: screenHeight(context, dividedBy: 1),
          //       decoration: BoxDecoration(
          //         color: Constants.kitGradients[31],
          //       ),
          //
          //       child: Column(
          //         //crossAxisAlignment: CrossAxisAlignment.start,
          //         children: [
          //           // SizedBox(
          //           //   height: screenHeight(context, dividedBy: 70),
          //           // ),
          //           Container(
          //             width: screenWidth(context, dividedBy: 1),
          //             height: screenHeight(context, dividedBy: 2.5),
          //               decoration: BoxDecoration(
          //
          //                 color: Colors.transparent,
          //               ),
          //             child:Stack(
          //               children:[
          //                 Container(
          //                   width: screenWidth(context, dividedBy: 1),
          //                   height: screenHeight(context, dividedBy: 3),
          //                   decoration:BoxDecoration(
          //                       image:DecorationImage(
          //                         fit: BoxFit.cover,
          //                         image:AssetImage("assets/images/profile_bg.jpg"),
          //                         colorFilter: new ColorFilter.mode(
          //                             Constants.kitGradients[28].withOpacity(0.6), BlendMode.lighten),
          //                       )
          //                   ),
          //
          //                 ),
          //                 Positioned(
          //                   bottom:screenHeight(context, dividedBy: 60),
          //                   left:screenWidth(context, dividedBy: 20),
          //                   child: Container(
          //                     width: screenWidth(context, dividedBy: 1),
          //                     child: Row(
          //                       children: [
          //                         widget.userPic == null
          //                             ? Container(
          //                             width: screenWidth(context, dividedBy: 4),
          //                             height: screenWidth(context, dividedBy: 4),
          //                             decoration: BoxDecoration(
          //                               color: Constants.kitGradients[31],
          //                               shape: BoxShape.circle,
          //                             ),
          //                             child: FaIcon(FontAwesomeIcons.solidUser,
          //                                 color: Constants.kitGradients[28], size: 24))
          //                             : CachedNetworkImage(
          //                             imageUrl: ObjectFactory().appHive.getProfileImage() == null ? "https://www.cornwallbusinessawards.co.uk/wp-content/uploads/2017/11/dummy450x450.jpg" : ObjectFactory().appHive.getProfileImage() ,
          //                             width: screenWidth(context, dividedBy: 4),
          //                             height: screenWidth(context, dividedBy: 4),
          //                             fit: BoxFit.cover,
          //                             imageBuilder: (context, imageProvider) {
          //                               return Container(
          //                                 width: screenWidth(context, dividedBy: 4),
          //                                 height: screenWidth(context, dividedBy: 4),
          //                                 decoration: BoxDecoration(
          //                                     color: Constants.kitGradients[31],
          //                                     shape: BoxShape.circle,
          //                                     border: Border.all(
          //                                         color: Constants.kitGradients[31],
          //                                         width: 4.0),
          //                                     image: DecorationImage(
          //                                       image: imageProvider,
          //                                       fit: BoxFit.cover,
          //                                     )),
          //                               );
          //                             }),
          //                         SizedBox(width:screenWidth(context, dividedBy:30)),
          //                         Column(
          //                           children: [
          //                             Container(
          //                               width: screenWidth(context, dividedBy: 1.5),
          //                               child: Row(
          //                                 children: [
          //                                   Text(
          //                                     ObjectFactory().appHive.getName() == null ? "Not Available" : ObjectFactory().appHive.getName(),
          //                                     textAlign: TextAlign.justify,
          //                                     style: TextStyle(
          //                                       fontSize: 24,
          //                                       fontWeight: FontWeight.bold,
          //                                       fontFamily: "Montserrat-ExtraBold",
          //                                       color: Constants.kitGradients[28].withOpacity(0.7),
          //                                     ),
          //                                   ),
          //                                   Spacer(),
          //                                   GestureDetector(
          //                                       child: Icon(
          //                                         Icons.star_outline_rounded,
          //                                         size: 22,
          //                                         color: Constants.kitGradients[28].withOpacity(0.6),
          //                                       ),
          //                                       onTap: () {
          //                                         push(context, SavedItemsList());
          //                                       }),
          //                                   SizedBox(width:screenWidth(context, dividedBy:15)),
          //                                 ],
          //                               ),
          //                             ),
          //                             SizedBox(
          //                               height: screenHeight(context, dividedBy: 60),
          //                             ),
          //                           ],
          //                         ),
          //                       ],
          //                     ),
          //                   ),
          //                 )
          //               ]
          //             )
          //           ),
          //           // SizedBox(
          //           //   height: screenHeight(context, dividedBy: 60),
          //           // ),
          //           // Text(
          //           //   ObjectFactory().appHive.getName() == null ? "Not Available" : ObjectFactory().appHive.getName(),
          //           //   textAlign: TextAlign.justify,
          //           //   style: TextStyle(
          //           //     fontSize: 18,
          //           //     fontWeight: FontWeight.bold,
          //           //     fontFamily: "Prompt-Light",
          //           //     color: Constants.kitGradients[19],
          //           //   ),
          //           // ),
          //           // Padding(
          //           //   padding: const EdgeInsets.all(12),
          //           //   child: Row(
          //           //     mainAxisAlignment: MainAxisAlignment.end,
          //           //     children: [
          //           //       GestureDetector(
          //           //           child: Icon(
          //           //             Icons.favorite,
          //           //             size: 30,
          //           //             color: Constants.kitGradients[28],
          //           //           ),
          //           //           onTap: () {
          //           //             push(context, SavedItemsList());
          //           //           })
          //           //     ],
          //           //   ),
          //           // ),
          //           SizedBox(
          //             height: screenHeight(context, dividedBy: 20),
          //           ),
          //           Padding(
          //               padding: EdgeInsets.symmetric(
          //                 horizontal: screenWidth(context, dividedBy: 20),
          //               ),
          //             child:Column(
          //               children:[
          //                 AppListTile(
          //                   text: "Add Foods",
          //                   onTap: () {
          //                     push(context, UpdateFoodDetails());
          //                   },
          //                 ),
          //                 // SizedBox(
          //                 //   height: screenHeight(context, dividedBy: 60),
          //                 // ),
          //                 AppListTile(
          //                   text: "Change Password",
          //                   onTap: () {
          //                     push(context, ChangePasswordPage());
          //                   },
          //                 ),
          //                 // SizedBox(
          //                 //   height: screenHeight(context, dividedBy: 60),
          //                 // ),
          //                 AppListTile(
          //                   text: "About Us",
          //                   onTap: () {
          //                     push(context, AboutUsPage());
          //                   },
          //                 ),
          //                 AppListTile(
          //                   text: "Logout",
          //                   onTap: () {
          //                     push(context, AboutUsPage());
          //                   },
          //                 ),
          //               ]
          //             )
          //           ),
          //           // Spacer(),
          //           SizedBox(
          //             height: screenHeight(context, dividedBy: 8),
          //           ),
          //           Center(child: CreatorName()),
          //           SizedBox(
          //             height: screenHeight(context, dividedBy: 60),
          //           ),
          //         ],
          //       )),
          // ),
          ),
    );
  }
}
