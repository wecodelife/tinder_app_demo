import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/widgets/app_bar_dog_app.dart';
import 'package:app_template/src/widgets/comment_box.dart';
import 'package:app_template/src/widgets/comment_tile.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

class CommentDetailsPage extends StatefulWidget {
  // const CommentDetailsPage({Key? key}) : super(key: key);
  final String comment;
  final String commentBy;
  final String feedImage;
  final String caption;
  CommentDetailsPage(
      {this.comment, this.commentBy, this.caption, this.feedImage});

  @override
  _CommentDetailsPageState createState() => _CommentDetailsPageState();
}

class _CommentDetailsPageState extends State<CommentDetailsPage> {
  int favcount = 1;
  TextEditingController commentTextEditingController =
      new TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Constants.kitGradients[17],
        appBar: AppBar(
            backgroundColor: Constants.kitGradients[17],
            elevation: 0,
            leading: Container(),
            actions: [
              AppBarDogApp(
                  leftIcon: Icon(
                    Icons.arrow_back,
                    color: Constants.kitGradients[27],
                  ),
                  onTapLeftIcon: () {
                    pop(context);
                  },
                  rightIcon: Container(),
                  onTapRightIcon: () {},
                  title: Row(
                    children: [
                      Text(
                        "Comments",
                        style: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.w700,
                          fontFamily: 'Montserrat',
                          color: Constants.kitGradients[27],
                        ),
                      ),
                    ],
                  )),
            ]),
        body: Container(
          height: screenHeight(context, dividedBy: 1),
          width: screenWidth(context, dividedBy: 1),
          child: Stack(
            children: [
              SingleChildScrollView(
                child: Container(
                  padding: EdgeInsets.symmetric(
                    horizontal: screenWidth(context, dividedBy: 30),
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      GestureDetector(
                        child: Container(
                          width: screenWidth(context, dividedBy: 1),
                          height: screenHeight(context, dividedBy: 3),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(8),
                          ),
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(8),
                            child: CachedNetworkImage(
                              fit: BoxFit.cover,
                              imageUrl: widget.feedImage,
                              imageBuilder: (context, imageProvider) =>
                                  Container(
                                width: screenWidth(context, dividedBy: 1),
                                // height: screenHeight(context, dividedBy: 3),
                                decoration: BoxDecoration(
                                  image: DecorationImage(
                                    image: imageProvider,
                                    fit: BoxFit.cover,
                                  ),
                                ),
                              ),
                              placeholder: (context, url) => Center(
                                heightFactor: 1,
                                widthFactor: 1,
                                child: SizedBox(
                                  height: 16,
                                  width: 16,
                                  child: CircularProgressIndicator(
                                    valueColor: AlwaysStoppedAnimation(
                                        Constants.kitGradients[8]),
                                    strokeWidth: 2,
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                      SizedBox(height: screenHeight(context, dividedBy: 40)),
                      Text(
                        "Comments:",
                        textAlign: TextAlign.left,
                        style: TextStyle(
                          fontSize: 12,
                          fontWeight: FontWeight.w400,
                          fontFamily: 'Montserrat',
                          color: Constants.kitGradients[27],
                        ),
                      ),
                      SizedBox(height: screenHeight(context, dividedBy: 40)),
                      Container(
                        child: ListView.builder(
                          itemCount: 5,
                          shrinkWrap: true,
                          physics: NeverScrollableScrollPhysics(),
                          itemBuilder: (BuildContext context, int index) {
                            return Container(
                              child: Column(
                                children: [
                                  CommentTile(
                                    comment: widget.comment,
                                    commentBy: widget.commentBy,
                                  ),
                                  SizedBox(
                                      height:
                                          screenHeight(context, dividedBy: 40))
                                ],
                              ),
                            );
                          },
                        ),
                      ),
                      SizedBox(height: screenHeight(context, dividedBy: 10))
                    ],
                  ),
                ),
              ),
              new Positioned(
                bottom: 0,
                child: Container(
                  color: Constants.kitGradients[17],
                  padding: EdgeInsets.symmetric(
                    horizontal: screenWidth(context, dividedBy: 25),
                    vertical: screenHeight(context, dividedBy: 50),
                  ),
                  child: CommentBox(
                    textEditingController: commentTextEditingController,
                    autofocus: false,
                  ),
                ),
              )
            ],
          ),
        ));
  }
}
