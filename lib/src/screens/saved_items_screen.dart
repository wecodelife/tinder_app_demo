import 'package:app_template/src/bloc/user_bloc.dart';
import 'package:app_template/src/models/liked_food_response.dart';
import 'package:app_template/src/screens/home_page.dart';
import 'package:app_template/src/screens/home_page_screen.dart';
import 'package:app_template/src/screens/liked_item_screen.dart';
import 'package:app_template/src/screens/profile_page.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/widgets/creator_name.dart';
import 'package:app_template/src/widgets/home_tab_bar.dart';
import 'package:app_template/src/widgets/liked_food_list_view.dart';
import 'package:flutter/material.dart';

class SavedItemsScreen extends StatefulWidget {
  @override
  _SavedItemsScreenState createState() => _SavedItemsScreenState();
}

class _SavedItemsScreenState extends State<SavedItemsScreen> {
  Future<bool> _willPopCallback() async {
    pushAndReplacement(
        context,
        HomePageScreen(
          count: false,
        ));
  }

  UserBloc userBloc = UserBloc();
  List<Result> favoriteFood = [];

  @override
  void initState() {
    // TODO: implement initState
    userBloc.getLikedFood();
    userBloc.likedFood.listen((event) {
      if(event.statusCode == 200 || event.statusCode == 201) {
        print("Favorite list response successfull");
        for (int i = 0; i < event.results.length; i++) {
          if (event.results[i].isFavourite == true) {
            setState(() {
              favoriteFood.add(event.results[i]);
            });
          }
        }
        print("Favourite Success");
        print(event.results);
        print(favoriteFood);
      }
    });
    // userBloc.likedFood.listen((event) {
    //   for (int i = 0; i < event.results.length; i++) {
    //     if (event.results[i].isFavourite == true) {
    //       setState(() {
    //         favoriteFood.add(event.results[i]);
    //       });
    //     }
    //   }
    //   print("Favourite Success");
    //   print(event.results);
    //   print(favoriteFood);
    // });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Constants.kitGradients[31],
      appBar: AppBar(
        leading: Container(),
        elevation: 0,
        actions: [
          HomeTabBar(
            currentIndex: 2,
            onTapLiked: () {
              push(context, LikedItemsScreen());
            },
            onTapHome: () {
              push(context, HomePageScreen(count: true));
            },
            onTapProfile: () {
              push(
                  context,
                  ProfilePage(
                      userPic:
                          "https://images.unsplash.com/photo-1467003909585-2f8a72700288?ixid=MnwxMjA3fDB8MHxzZWFyY2h8OXx8Zm9vZHxlbnwwfHwwfHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=600&q=60",
                      userName: "SanJE"));
            },
          ),
        ],
        backgroundColor: Constants.kitGradients[31],
      ),
      body: SingleChildScrollView(
        padding: EdgeInsets.zero,
        child: Container(
          color: Constants.kitGradients[31],
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                height: screenHeight(context, dividedBy: 50),
              ),
              Container(
                padding: EdgeInsets.symmetric(
                  horizontal: screenWidth(context, dividedBy: 30),
                ),
                child: favoriteFood.length != 0
                    ? ListView.builder(
                        itemCount: favoriteFood.length,
                        shrinkWrap: true,
                        physics: NeverScrollableScrollPhysics(),
                        itemBuilder: (BuildContext context, int index) {
                          return LikedFoodListView(
                            shopName: favoriteFood[index].food.restaurant,
                            foodName: favoriteFood[index].food.name,
                            index: index,
                            rating: 4,
                            price: favoriteFood[index].food.price,
                            foodType: 2,
                            likedFoodImageUrl: favoriteFood[index].food.image1,
                            navigationPage: HomePageScreen(),
                            isLikedList: false,
                            foodId: favoriteFood[index].food.id,
                            isMustVisit: true,
                          );
                        },
                      )
                    : Container(),
              ),
              favoriteFood.length < 2
                  ? SizedBox(
                      height: screenHeight(context, dividedBy: 2.15),
                    )
                  : SizedBox(
                      height: screenHeight(context, dividedBy: 30),
                    ),
              Center(child: CreatorName()),
              SizedBox(
                height: screenHeight(context, dividedBy: 100),
              ),
              // Row(
              //   crossAxisAlignment: CrossAxisAlignment.start,
              //   children: [
              //     SizedBox(
              //       width: 10,
              //     ),
              //     Expanded(
              //       child: favoriteFood.length != 0
              //           ? ListView.builder(
              //               padding: EdgeInsets.zero,
              //               itemCount: favoriteFood.length,
              //               shrinkWrap: true,
              //               physics: NeverScrollableScrollPhysics(),
              //               itemBuilder: (BuildContext context, int index) {
              //                 return index % 2 == 0
              //                     ? Column(
              //                         children: [
              //                           ClipRRect(
              //                             borderRadius:
              //                                 BorderRadius.circular(20),
              //                             child: GestureDetector(
              //                               onTap: () {
              //                                 push(
              //                                     context,
              //                                     FoodInformationPage(
              //                                       navigationWidget:
              //                                           LikedItemsList(),
              //                                     ));
              //                               },
              //                               child: CachedNetworkImage(
              //                                 height: screenHeight(context,
              //                                     dividedBy: 2.4),
              //                                 width: screenWidth(context,
              //                                     dividedBy: 2),
              //                                 fit: BoxFit.cover,
              //                                 imageUrl: favoriteFood[index]
              //                                             .food
              //                                             .image1 ==
              //                                         null
              //                                     ? "https://images.unsplash.com/photo-1476224203421-9ac39bcb3327?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MTJ8fGZvb2R8ZW58MHx8MHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=600&q=60"
              //                                     : favoriteFood[index]
              //                                         .food
              //                                         .image1,
              //                                 imageBuilder:
              //                                     (context, imageProvider) =>
              //                                         Container(
              //                                   height: screenHeight(context,
              //                                       dividedBy: 2.4),
              //                                   width: screenWidth(context,
              //                                       dividedBy: 2),
              //                                   decoration: BoxDecoration(
              //                                     image: DecorationImage(
              //                                       image: imageProvider,
              //                                       fit: BoxFit.cover,
              //                                     ),
              //                                     borderRadius:
              //                                         BorderRadius.circular(
              //                                             20),
              //                                   ),
              //                                   child: Stack(
              //                                     children: [
              //                                       InformationIcon(
              //                                         deleteSaved: true,
              //                                         icon: Icons.star,
              //                                         icon2:
              //                                             Icons.star_outlined,
              //                                         onPressed: () {},
              //                                         rightPadding: 20,
              //                                       ),
              //                                       Column(
              //                                         crossAxisAlignment:
              //                                             CrossAxisAlignment
              //                                                 .center,
              //                                         mainAxisAlignment:
              //                                             MainAxisAlignment
              //                                                 .end,
              //                                         children: [
              //                                           FavouritesBox(
              //                                             foodName:
              //                                                 favoriteFood[
              //                                                         index]
              //                                                     .food
              //                                                     .name,
              //                                             price: "19",
              //                                             foodType: favoriteFood[
              //                                                             index]
              //                                                         .food
              //                                                         .foodType ==
              //                                                     null
              //                                                 ? "Not Available"
              //                                                 : "Non-Veg",
              //                                             hotelName:
              //                                                 favoriteFood[
              //                                                         index]
              //                                                     .food
              //                                                     .restaurant,
              //                                             rating: 19,
              //                                           ),
              //                                           SizedBox(
              //                                             height:
              //                                                 screenHeight(
              //                                                     context,
              //                                                     dividedBy:
              //                                                         60),
              //                                           ),
              //                                         ],
              //                                       ),
              //                                     ],
              //                                   ),
              //                                 ),
              //                                 placeholder: (context, url) =>
              //                                     Center(
              //                                   heightFactor: 1,
              //                                   widthFactor: 1,
              //                                   child: SizedBox(
              //                                     height: 16,
              //                                     width: 16,
              //                                     child:
              //                                         CircularProgressIndicator(
              //                                       valueColor:
              //                                           AlwaysStoppedAnimation(
              //                                               Constants
              //                                                       .kitGradients[
              //                                                   28]),
              //                                       strokeWidth: 2,
              //                                     ),
              //                                   ),
              //                                 ),
              //                               ),
              //                             ),
              //                           ),
              //                           SizedBox(
              //                             height: screenHeight(context,
              //                                 dividedBy: 30),
              //                           )
              //                         ],
              //                       )
              //                     : Container();
              //               },
              //             )
              //           : Container(),
              //     ),
              //     SizedBox(
              //       width: 10,
              //     ),
              //     Expanded(
              //       child: favoriteFood.length != 0
              //           ? ListView.builder(
              //               padding: EdgeInsets.zero,
              //               itemCount: favoriteFood.length,
              //               shrinkWrap: true,
              //               physics: NeverScrollableScrollPhysics(),
              //               itemBuilder: (BuildContext context, int index) {
              //                 return index % 2 == 1
              //                     ? Column(
              //                         children: [
              //                           index == 1
              //                               ? Container(
              //                                   height: screenHeight(context,
              //                                       dividedBy: 16),
              //                                 )
              //                               : Container(),
              //                           ClipRRect(
              //                             borderRadius:
              //                                 BorderRadius.circular(20),
              //                             child: GestureDetector(
              //                               onTap: () {
              //                                 push(
              //                                   context,
              //                                   FoodInformationPage(
              //                                     navigationWidget:
              //                                         LikedItemsList(),
              //                                   ),
              //                                 );
              //                               },
              //                               child: CachedNetworkImage(
              //                                 height: screenHeight(context,
              //                                     dividedBy: 2.4),
              //                                 width: screenWidth(context,
              //                                     dividedBy: 2),
              //                                 fit: BoxFit.cover,
              //                                 imageUrl: favoriteFood[index]
              //                                             .food
              //                                             .image1 ==
              //                                         null
              //                                     ? "https://images.unsplash.com/photo-1476224203421-9ac39bcb3327?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MTJ8fGZvb2R8ZW58MHx8MHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=600&q=60"
              //                                     : favoriteFood[index]
              //                                         .food
              //                                         .image1,
              //                                 imageBuilder: (
              //                                   context,
              //                                   imageProvider,
              //                                 ) =>
              //                                     Container(
              //                                   height: screenHeight(context,
              //                                       dividedBy: 2.4),
              //                                   width: screenWidth(context,
              //                                       dividedBy: 2),
              //                                   decoration: BoxDecoration(
              //                                     image: DecorationImage(
              //                                       image: imageProvider,
              //                                       fit: BoxFit.cover,
              //                                     ),
              //                                     borderRadius:
              //                                         BorderRadius.circular(
              //                                             20),
              //                                   ),
              //                                   child: Stack(
              //                                     children: [
              //                                       InformationIcon(
              //                                         deleteSaved: true,
              //                                         icon: Icons.star,
              //                                         icon2:
              //                                             Icons.star_border,
              //                                         onPressed: () {},
              //                                         rightPadding: 20,
              //                                       ),
              //                                       Column(
              //                                         crossAxisAlignment:
              //                                             CrossAxisAlignment
              //                                                 .center,
              //                                         mainAxisAlignment:
              //                                             MainAxisAlignment
              //                                                 .end,
              //                                         children: [
              //                                           FavouritesBox(
              //                                             foodName:
              //                                                 favoriteFood[
              //                                                         index]
              //                                                     .food
              //                                                     .name,
              //                                             price: "19",
              //                                             foodType: favoriteFood[
              //                                                             index]
              //                                                         .food
              //                                                         .foodType ==
              //                                                     null
              //                                                 ? "Not Available"
              //                                                 : "Non-Veg",
              //                                             hotelName:
              //                                                 favoriteFood[
              //                                                         index]
              //                                                     .food
              //                                                     .restaurant,
              //                                             rating: 19,
              //                                           ),
              //                                           SizedBox(
              //                                             height:
              //                                                 screenHeight(
              //                                                     context,
              //                                                     dividedBy:
              //                                                         60),
              //                                           ),
              //                                         ],
              //                                       ),
              //                                     ],
              //                                   ),
              //                                 ),
              //                                 placeholder: (context, url) =>
              //                                     Center(
              //                                   heightFactor: 1,
              //                                   widthFactor: 1,
              //                                   child: SizedBox(
              //                                     height: 16,
              //                                     width: 16,
              //                                     child:
              //                                         CircularProgressIndicator(
              //                                       valueColor:
              //                                           AlwaysStoppedAnimation(
              //                                               Constants
              //                                                       .kitGradients[
              //                                                   28]),
              //                                       strokeWidth: 2,
              //                                     ),
              //                                   ),
              //                                 ),
              //                               ),
              //                             ),
              //                           ),
              //                           SizedBox(
              //                             height: screenHeight(context,
              //                                 dividedBy: 30),
              //                           )
              //                         ],
              //                       )
              //                     : Container();
              //               },
              //             )
              //           : Container(),
              //     ),
              //     SizedBox(
              //       width: 10,
              //     ),
              //   ],
              // ),
            ],
          ),
        ),
      ),
    );
  }
}
