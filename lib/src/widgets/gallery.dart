import 'dart:io';
import 'dart:typed_data';

import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/widgets/circle.dart';
import 'package:app_template/src/widgets/three_bounce..dart';
// import 'package:crop/crop.dart';
import 'package:flutter/material.dart';
// import 'package:image_crop/image_crop.dart';
import 'package:photo_manager/photo_manager.dart';

class InvertedCircleClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    return new Path()
      ..addOval(new Rect.fromCircle(
          center: new Offset(size.width / 2, size.height / 2),
          radius: size.height * 0.5))
      ..addRect(new Rect.fromLTWH(0.0, 0.0, size.width, size.height))
      ..fillType = PathFillType.evenOdd;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) => false;
}

class Gallery extends StatefulWidget {
  final Widget title;
  final Widget footer;
  final String pageValue;
  final ValueChanged<List<String>> imageSelected;

  Gallery({this.title, this.footer, this.imageSelected, this.pageValue});
  @override
  _GalleryState createState() => _GalleryState();
}

class _GalleryState extends State<Gallery> {
  // This will hold all the assets we fetched
  List<AssetEntity> assets = [];
  List<String> imagesSelected = [];
  List<File> imagesSelectedTemp = [];
  AssetEntity selectedAsset;
  List<bool> selected = [];
  File imageSelected;
  String selectedPath;
  int noOfImage;
  String message;
  bool loading = true;
  // final cropKey = GlobalKey<CropState>();
  File _file;
  File _sample;
  File _lastCropped;
  // final crop = GlobalKey<CropState>().currentState;
  // final controller = CropController(aspectRatio: 16 / 9);
// or
// final crop = Crop.of(context);
//   final scale = GlobalKey<CropState>().currentState.scale;
//   final area = GlobalKey<CropState>().currentState.area;

  void handleImageSelection(String value) {
    switch (value) {
      case 'Post':
        setState(() {
          noOfImage = 5;
          message = "Select only up to 4 images!";
        });
        // push(
        //     context,
        //     AddNewsFeedPage(
        //       eventId: widget.eventId,
        //       postId: widget.postId,
        //       isEdit: widget.isEdit,
        //       imageFileList: imageList,
        //       userName: widget.username,
        //       profileImage: widget.userPic,
        //     ));
        break;
      case 'Profile':
        setState(() {
          noOfImage = 1;
          message = "Select only one image!";
        });
        // push(
        //     context,
        //     UploadImage(
        //       userName: widget.username,
        //       imgUrl: widget.userPic,
        //       bio: widget.bio,
        //     ));
        break;
    }
  }

  @override
  void initState() {
    _fetchAssets();
    handleImageSelection(widget.pageValue);
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
    _file?.delete();
    _sample?.delete();
    _lastCropped?.delete();
  }

  @override
  Widget build(BuildContext context) {
    return loading == true
        ? Container(
            height: screenHeight(context, dividedBy: 1.2),
            width: screenWidth(context),
            child: Center(
              child: SpinKitThreeBounce(
                color: Constants.kitGradients[30],
                // size: 50.0,
                // controller: AnimationController(
                //     duration: const Duration(milliseconds: 1200))
              ),
            ),
          )
        : Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                height: screenHeight(context, dividedBy: 2.5),
                child: Stack(children: <Widget>[
                  // selectedAsset != null
                  //     ?
                  FutureBuilder<File>(
                    future: selectedAsset.originFile,
                    builder: (_, snapshot) {
                      final bytes = snapshot.data;
                      // If we have no data, display a spinner
                      if (bytes == null)
                        return Center(
                          child: SpinKitThreeBounce(
                            color: Constants.kitGradients[30],
                            // size: 50.0,
                            // controller: AnimationController(
                            //     duration: const Duration(milliseconds: 1200))
                          ),
                        );
                      // If there's data, display it as an image
                      return Stack(
                        //fit: StackFit.expand,
                        children: [
                          // Wrap the image in a Positioned.fill to fill the space
                          Positioned.fill(
                            child: Container(
                                child: FittedBox(
                                  fit: BoxFit.contain,
                                  child: Image.file(
                                    bytes,
                                    filterQuality: FilterQuality.none,
                                    // fit: BoxFit.fitHeight,
                                  ),
                                )),
                          ),
                          // imageSelected == null
                          //     ? Container()
                          //     : Container(
                          //         width: double.infinity,
                          //         height: double.infinity,
                          //         padding: EdgeInsets.all(20),
                          //         child:
                          //             Crop.file(imageSelected, key: cropKey)),

                          // Display a Play icon if the asset is a video
                        ],
                      );
                    },
                  ),
                  // : Container(),
                  if (widget.pageValue == "Profile")
                    ClipPath(
                      clipper: new InvertedCircleClipper(),
                      child: new Container(
                        color: new Color.fromRGBO(0, 0, 0, 0.3),
                      ),
                    ),
                ]),
              ),
              widget.title != null ? widget.title : Container(),
              Expanded(
                flex: 1,
                child: Container(
                  // constraints: BoxConstraints.expand(),
                  color: Constants.kitGradients[28],
                  width: MediaQuery.of(context).size.width,
                  // height: screenHeight(context, dividedBy: 2),
                  child: GridView.builder(
                    //padding: EdgeInsets.zero,
                    shrinkWrap: true,
                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      // A grid view with 3 items per row
                      crossAxisCount: 3,
                      crossAxisSpacing: 3,
                      mainAxisSpacing: 3,
                      //childAspectRatio: (9 / 16)
                    ),
                    itemCount: assets.length,
                    itemBuilder: (_, index) {
                      return assetThumbnail(context, assets[index], index);
                    },
                  ),
                ),
              ),
              // widget.footer != null ? widget.footer : Container(),
            ],
          );
  }

  Widget assetThumbnail(BuildContext context, AssetEntity asset, int index) {
    print(widget.imageSelected);

    // We're using a FutureBuilder since thumbData is a future
    return FutureBuilder<Uint8List>(
      future: asset.thumbData,
      builder: (_, snapshot) {
        final bytes = snapshot.data;

        print("width:" + asset.width.toString());
        print("height:" + asset.height.toString());
        // setState(() {
        //   selectedAsset = asset;
        // });
        // If we have no data, display a spinner
        if (bytes == null)
          return Center(
              child: SpinKitCircle(
            color: Constants.kitGradients[30],
            size: 20.0,
            // controller: AnimationController(
            //     duration: const Duration(milliseconds: 1200))
          ));
        // If there's data, display it as an image
        return Stack(
          children: [
            // Wrap the image in a Positioned.fill to fill the space
            Positioned.fill(
              child: GestureDetector(
                  child: selected[index] == false
                      ? Container(
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(6),
                              border: Border.all(
                                  color: Constants.kitGradients[30]
                                      .withOpacity(0.5))),
                          // height: screenWidth(context, dividedBy: 15),
                          // width: screenWidth(context, dividedBy: 15),
                          //color: Colors.amber,
                          child: Image.memory(
                            bytes,
                            fit: BoxFit.fill,
                            filterQuality: FilterQuality.none,
                          ),
                        )
                      : Container(
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(6)),
                          // width: screenWidth(context, dividedBy: 5),
                          // height: screenWidth(context, dividedBy: 5),
                          //color: Colors.amber,
                          child: Image.memory(
                            bytes,
                            fit: BoxFit.fill,
                            filterQuality: FilterQuality.none,
                            color: Constants.kitGradients[30].withOpacity(.5),
                            colorBlendMode: BlendMode.darken,
                            width: 100,
                            height: 100,
                          )),
                  onTap: () async {
                    if (index != -1) {
                      imageSelected = await asset.file;
                      // for (int i = 0; i < selected.length; i++) {
                      //   setState(() {
                      //     selected[i] = false;
                      //   });
                      // }

                      //crop image
                      // _cropImage();

                      // File file = File(imageSelected.path);
                      // File cropped = await ImageCropper.cropImage(
                      //     sourcePath: file.path,
                      //     // aspectRatio: CropAspectRatio(ratioX: 1, ratioY: 1),
                      //
                      //     compressQuality: 100,
                      //     maxWidth: 700,
                      //     maxHeight: 700,
                      //     compressFormat: ImageCompressFormat.jpg,
                      //     iosUiSettings: IOSUiSettings(
                      //       title: 'Crop Image',
                      //       showActivitySheetOnDone: true,
                      //     ),
                      //     androidUiSettings: AndroidUiSettings(
                      //       toolbarColor: Constants.kitGradients[30],
                      //       toolbarWidgetColor: Constants.kitGradients[28],
                      //       toolbarTitle: "",
                      //       statusBarColor: Constants.kitGradients[30],
                      //       backgroundColor: Constants.kitGradients[28],
                      //     ));
                      setState(() {
                        if (widget.pageValue == "Post") if (imagesSelected
                                    .length <
                                5 &&
                            imagesSelected.contains(imageSelected.path) !=
                                true) {
                          selectedAsset = asset;
                          if (selected[index] != true) {
                            imagesSelected.add(imageSelected.path);
                            selected[index] = true;
                          }
                        } else if (selected[index] == true) {
                          print("remove" + imageSelected.toString());

                          // imagesSelectedTemp.add(imageSelected);
                          // replytile.removeWhere((item) => item.id == '001')
                          print("image" +
                              imagesSelected
                                  .indexOf(imageSelected.path)
                                  .toString());
                          imagesSelected.removeAt(
                              imagesSelected.indexOf(imageSelected.path));
                          selected[index] = false;
                          print("removed" + imagesSelectedTemp.toString());
                          // imagesSelected.
                        } else
                          showToastInfo("Select only up to 5 images!", context);
                        if (widget.pageValue == "Profile") {
                          for (int i = 0; i < selected.length; i++) {
                            setState(() {
                              selected[i] = false;
                            });
                            setState(() {
                              selectedAsset = asset;
                              selected[index] = true;
                            });
                          }
                        }
                        print("added" + imagesSelected.toString());
                        print(
                            imagesSelected.contains(imageSelected).toString());
                        widget.imageSelected(widget.pageValue == "Post"
                            ? imagesSelected
                            : [imageSelected.path]);
                      });
                    }
                  }),
            ),
            if (selected[index] == true)
              Positioned(
                right: 5,
                top: 5,
                child: InkWell(
                  child: Icon(
                    Icons.remove_circle,
                    size: 20,
                    color: Colors.red,
                  ),
                  onTap: () async {
                    // imageSelected = await asset.file;
                    //
                    // print("removed" + imageSelected.toString());
                    //
                    // setState(() {
                    //   imagesSelectedTemp = imagesSelected;
                    //   imagesSelectedTemp.remove(imageSelected);
                    //   selected[index] = false;
                    // });
                    // widget.imageSelected(imagesSelected);
                    // print("removed" + imagesSelectedTemp.toString());
                  },
                ),
              ),
            // Display a Play icon if the asset is a video
            if (asset.type == AssetType.video)
              Center(
                child: Container(
                  color: Colors.blue,
                  child: Icon(
                    Icons.play_arrow,
                    color: Colors.white,
                  ),
                ),
              ),
          ],
        );
      },
    );
  }

  _fetchAssets() async {
    // Set onlyAll to true, to fetch only the 'Recent' album
    // which contains all the photos/videos in the storage

    final albums = await PhotoManager.getAssetPathList(
      type: RequestType.image,
      onlyAll: true,
    );
    final recentAlbum = albums.first;

    // Now that we got the album, fetch all the assets it contains
    final recentAssets = await recentAlbum.getAssetListRange(
      start: 0, // start at index 0
      end: 1000000, // end at a very big index (to get all the assets)
    );

    // Update the state and notify UI
    setState(() => assets = recentAssets);
    for (int i = 0; i < assets.length; i++) {
      setState(() {
        selected.add(false);
      });
    }
    print(assets[0].id.toString());
    setState(() {
      loading = false;
      selectedAsset = assets[0];
    });
  }
}

//AssetEntity selectedAsset;
