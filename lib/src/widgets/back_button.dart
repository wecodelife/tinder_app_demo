import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:draggable_widget/draggable_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class DraggableBackButton extends StatefulWidget {
  final Function backButtonPress;
  DraggableBackButton({this.backButtonPress});
  @override
  _DraggableBackButtonState createState() => _DraggableBackButtonState();
}

class _DraggableBackButtonState extends State<DraggableBackButton> {
  DragController dragController = new DragController();
  @override
  Widget build(BuildContext context) {
    return DraggableWidget(
        bottomMargin: screenHeight(context, dividedBy: 6),
        topMargin: screenHeight(context, dividedBy: 40),
        intialVisibility: true,
        horizontalSapce: 20,
        shadowBorderRadius: 50,
        initialPosition: AnchoringPosition.topRight,
        dragController: dragController,
        child: GestureDetector(
          onTap: () {
            widget.backButtonPress();
          },
          child: Container(
              width: screenWidth(context, dividedBy: 8),
              height: screenWidth(context, dividedBy: 8),
              decoration: BoxDecoration(
                color: Constants.kitGradients[28].withOpacity(0.7),
                shape: BoxShape.circle,
              ),
              child: Center(
                child: FaIcon(
                  FontAwesomeIcons.redo,
                  color: Constants.kitGradients[27],
                  size: 20,
                ),
              )),
        ));
  }
}
