import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'dart:ui';
import '../utils/utils.dart';

class HomeTabBar extends StatefulWidget {
  final Function onTapSaved;
  final Function onTapHome;
  final Function onTapLiked;
  final double currentIndex;
  final Function onTapProfile;
  HomeTabBar(
      {this.onTapHome,
      this.onTapSaved,
      this.onTapProfile,
      this.currentIndex,
      this.onTapLiked});
  @override
  _HomeTabBarState createState() => _HomeTabBarState();
}

class _HomeTabBarState extends State<HomeTabBar> {
  int count;
  int _currentIndex = 0;
  bool expanded = true;
  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: expanded == true
            ? ClipRRect(
          /*borderRadius: new BorderRadius.vertical(
              bottom: new Radius.elliptical(screenWidth(context, dividedBy: 1)
                  ,screenHeight(context, dividedBy: 9)
              )
          ),*/

          child: Container(
                  width: screenWidth(context, dividedBy: 1),
                  height: screenHeight(context, dividedBy: 12),
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                    border: Border(top: BorderSide(width: 2, color:Constants.kitGradients[28]),
                      bottom: BorderSide(width: 2, color:Constants.kitGradients[28],),
                  ),
                  ),
                  child: BackdropFilter(
                    filter: ImageFilter.blur(sigmaX: 10.0, sigmaY: 10.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        // SizedBox(
                        //   width: screenWidth(context, dividedBy: 10),
                        // ),
                        Spacer(),
                        GestureDetector(
                          onTap: () {
                            widget.onTapHome();
                          },
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              widget.currentIndex == 0
                                  ? Image(
                                      image: AssetImage(
                                          "assets/images/food_selected.png"),
                                      width: 30,
                                      color: Constants.kitGradients[28],
                                      height: 30)
                                  : Image(
                                      image:
                                          AssetImage("assets/images/food_icon.png"),
                                      color: Constants.kitGradients[28],
                                      width: 30,
                                      height: 30),
                            ],
                          ),
                        ),
                        Spacer(),
                        GestureDetector(
                          onTap: () {
                            widget.onTapLiked();
                          },
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              widget.currentIndex == 1
                                  ? FaIcon(FontAwesomeIcons.solidStar,
                                      color: Constants.kitGradients[28], size: 27)
                                  : FaIcon(FontAwesomeIcons.solidStar,
                                      color: Constants.kitGradients[28], size: 27),
                            ],
                          ),
                        ),
                        // Spacer(),
                        // GestureDetector(
                        //   onTap: () {
                        //     widget.onTapSaved();
                        //   },
                        //   child: Column(
                        //     mainAxisAlignment: MainAxisAlignment.center,
                        //     children: [
                        //       widget.currentIndex == 2
                        //           ? FaIcon(FontAwesomeIcons.image,
                        //               color: Constants.kitGradients[27], size: 25)
                        //           : FaIcon(FontAwesomeIcons.solidImage,
                        //               color: Constants.kitGradients[27], size: 25),
                        //     ],
                        //   ),
                        // ),
                        Spacer(),
                        GestureDetector(
                          onTap: () {
                            widget.onTapProfile();
                          },
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              widget.currentIndex == 3
                                  ? FaIcon(FontAwesomeIcons.solidUser,
                                      color: Constants.kitGradients[28], size: 27)
                                  : FaIcon(FontAwesomeIcons.solidUser,
                                      color: Constants.kitGradients[28], size: 27),
                            ],
                          ),
                        ),
                        Spacer(),

                        /* SizedBox(
                          width: screenWidth(context, dividedBy: 10),
                        ),*/
                      ],
                    ),
                  ),
                ),
            )
            : Container(
                width: screenWidth(context, dividedBy: 1),
              ));
  }
}
