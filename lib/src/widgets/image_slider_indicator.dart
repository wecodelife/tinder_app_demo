import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';

class ImageSliderIndicator extends StatefulWidget {
  // const ImageSliderIndicator({Key? key}) : super(key: key);
  final int currentIndex;
  final int imageCount;
  ImageSliderIndicator({this.currentIndex, this.imageCount});

  @override
  _ImageSliderIndicatorState createState() => _ImageSliderIndicatorState();
}

class _ImageSliderIndicatorState extends State<ImageSliderIndicator> {

  @override
  Widget build(BuildContext context) {
    return Container(
      width: screenWidth(context, dividedBy: 1),
      //height: screenHeight(context, dividedBy: 12),
      padding: EdgeInsets.symmetric(
        vertical: screenHeight(context, dividedBy: 50),
        horizontal: screenWidth(context, dividedBy: 50),
      ),
      alignment: Alignment.center,
      decoration: BoxDecoration(
          // color: Constants.kitGradients[31],
          ),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: new List.generate(
          widget.imageCount,
          (index) {
            return Container(
              width: screenWidth(context, dividedBy: 8),
              height: screenHeight(context, dividedBy: 200),
              margin: EdgeInsets.symmetric(
                //vertical: screenHeight(context, dividedBy:80),
                horizontal: screenWidth(context, dividedBy: 50),
              ),
              alignment: Alignment.center,
              decoration: BoxDecoration(
                  color: widget.currentIndex == index
                      ? Constants.kitGradients[28]
                      : Constants.kitGradients[19].withOpacity(0.5),
                  borderRadius: BorderRadius.circular(60)),
            );
          },
        ),
      ),
    );
  }
}
