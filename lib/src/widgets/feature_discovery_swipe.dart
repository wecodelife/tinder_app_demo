import 'package:flutter/material.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:feature_discovery/feature_discovery.dart';

class FeatureSwipe extends StatefulWidget {
  //const FeatureSwipe({Key? key}) : super(key: key);
  final bool count;
  FeatureSwipe({this.count});
  @override
  _FeatureSwipeState createState() => _FeatureSwipeState();
}

class _FeatureSwipeState extends State<FeatureSwipe> {
  @override
  void initState() {
    // TODO: implement initState
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      FeatureDiscovery.discoverFeatures(context, <String>[
        'feature1',
        'feature2',
        'feature3',
        // 'feature4',
      ]);
    });
    super.initState();
  }

  bool count = false;
  @override
  Widget build(BuildContext context) {
    return Container(
        width: screenWidth(context, dividedBy: 1),
        height: screenHeight(context, dividedBy: 11),
        padding: EdgeInsets.symmetric(
          horizontal: screenWidth(context, dividedBy: 9),
        ),
        child: Row(
          children: [
            widget.count == true
                ? Container()
                : DescribedFeatureOverlay(
                    featureId: 'feature1',
                    targetColor: Constants.kitGradients[0],
                    // textColor: Constants.kitGradients[19],
                    pulseDuration: Duration(seconds: 1),
                    enablePulsingAnimation: true,
                    barrierDismissible: false,
                    backgroundColor:
                        Constants.kitGradients[28].withOpacity(0.5),
                    contentLocation: ContentLocation.above,
                    overflowMode: OverflowMode.wrapBackground,
                    openDuration: Duration(seconds: 1),
                    //onComplete: action,

                    title: Text(
                      'Swipe Left',
                      style: TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                        color: Constants.kitGradients[27],
                        fontFamily: "Prompt-Light",
                      ),
                    ),
                    description: Text(
                      'You can Swipe left to add an item into favorite list',
                      style: TextStyle(
                        fontSize: 17,
                        fontWeight: FontWeight.w500,
                        color: Constants.kitGradients[27],
                        fontFamily: "Prompt-Light",
                      ),
                    ),
                    tapTarget: Icon(
                      Icons.swipe,
                      color: Constants.kitGradients[28],
                    ),
                    child: Container(),
                  ),
            Spacer(),
            widget.count == true
                ? Container()
                : DescribedFeatureOverlay(
                    featureId: 'feature2',
                    targetColor: Constants.kitGradients[0],
                    // textColor: Constants.kitGradients[19],
                    pulseDuration: Duration(seconds: 1),
                    enablePulsingAnimation: true,
                    barrierDismissible: false,
                    backgroundColor:
                        Constants.kitGradients[28].withOpacity(0.5),
                    contentLocation: ContentLocation.above,
                    overflowMode: OverflowMode.extendBackground,
                    openDuration: Duration(seconds: 1),
                    //onComplete: action,

                    title: Text(
                      'Swipe right',
                      style: TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                        color: Constants.kitGradients[27],
                        fontFamily: "Prompt-Light",
                      ),
                    ),
                    description: Text(
                      'You can Swipe Right to add an item into liked list',
                      style: TextStyle(
                        fontSize: 17,
                        fontWeight: FontWeight.w500,
                        color: Constants.kitGradients[27],
                        fontFamily: "Prompt-Light",
                      ),
                    ),
                    tapTarget: Icon(
                      Icons.swipe,
                      color: Constants.kitGradients[28],
                    ),
                    child: Container(),
                  ),
          ],
        ));
  }
}
