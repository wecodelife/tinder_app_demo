import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';

class CommentBox extends StatefulWidget {
  final TextEditingController textEditingController;
  final bool autofocus;
  CommentBox({this.textEditingController, this.autofocus});

  @override
  _CommentBoxState createState() => _CommentBoxState();
}

class _CommentBoxState extends State<CommentBox> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: screenHeight(context, dividedBy: 17),
      child: Row(
        children: [
          Container(
            width: screenWidth(context, dividedBy: 1.3),
            child: TextFormField(
              controller: widget.textEditingController,
              cursorColor: Constants.kitGradients[28],
              keyboardType: TextInputType.text,
              autofocus: widget.autofocus,
              style: TextStyle(
                color: Constants.kitGradients[28],
                fontFamily: 'Montserrat',
                fontSize: 16,
              ),
              autocorrect: false,
              decoration: InputDecoration(
                // labelText: widget.labelText,
                // labelStyle: TextStyle(
                //   color: Colors.grey,
                //   fontFamily: 'Montserrat',
                //   fontSize: 16,
                // ),
                contentPadding: EdgeInsets.symmetric(
                    horizontal: screenWidth(context, dividedBy: 30),
                    vertical: screenHeight(context, dividedBy: 90)),
                hintText: "Add a comment",
                hintStyle: TextStyle(
                  color: Colors.grey,
                  fontFamily: 'Montserrat',
                  fontSize: 13,
                ),
                border: InputBorder.none,
                filled: true,
                fillColor: Colors.transparent,
                focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(
                      color: Constants.kitGradients[28].withOpacity(0.1),
                      width: 0.0),
                  borderRadius: BorderRadius.circular(15.0),
                ),
                enabledBorder: OutlineInputBorder(
                  borderSide: BorderSide(
                      color: Constants.kitGradients[28].withOpacity(0.1),
                      width: 0.0),
                  borderRadius: BorderRadius.circular(15.0),
                ),
              ),
            ),
          ),
          SizedBox(
            width: screenWidth(context, dividedBy:25),
          ),
          GestureDetector(
              child: Text(
            "Post",
            style: TextStyle(
              color: Constants.kitGradients[28],
              fontFamily: 'Montserrat-bold',
              fontSize: 16,
            ),
          ))
        ],
      ),
    );
  }
}
