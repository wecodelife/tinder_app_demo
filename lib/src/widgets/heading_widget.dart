import 'package:app_template/src/utils/constants.dart';
import 'package:flutter/cupertino.dart';

class HeadingWidget extends StatefulWidget {
  String title;
  HeadingWidget({this.title});
  @override
  _HeadingWidgetState createState() => _HeadingWidgetState();
}

class _HeadingWidgetState extends State<HeadingWidget> {
  @override
  Widget build(BuildContext context) {
    return Text(
      widget.title + "",
      style: TextStyle(
          color: Constants.kitGradients[28],
          fontWeight: FontWeight.w400,
          fontSize: 17,
        fontFamily: "Prompt-Light",),
    );
  }
}
