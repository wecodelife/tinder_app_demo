import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';

class LikeButton extends StatefulWidget {
  final Function onPressed;
  final String title;
  LikeButton({this.onPressed, this.title});
  @override
  _LikeButtonState createState() => _LikeButtonState();
}

class _LikeButtonState extends State<LikeButton> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: screenWidth(context, dividedBy: 2.5),
      height: screenHeight(context, dividedBy: 20),
      child: ElevatedButton(
        style: ElevatedButton.styleFrom(
          primary: Constants.kitGradients[28],
          elevation: 3,
        ),
        onPressed: () {
          widget.onPressed();
        },
        child: Text(
          widget.title,
          style: TextStyle(
            fontSize: 16,
            fontWeight: FontWeight.bold,
            color: Constants.kitGradients[27],
          ),
        ),
      ),
    );
  }
}
