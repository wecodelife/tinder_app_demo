import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/widgets/animated_otp_fields.dart';
import 'package:flutter/material.dart';

class OtpFormFeild extends StatefulWidget {
  TextEditingController otpTextEditingController = new TextEditingController();
  Function otpTap;
  bool isLoading;
  OtpFormFeild({this.otpTextEditingController, this.otpTap, this.isLoading});

  @override
  _OtpFormFeildState createState() => _OtpFormFeildState();
}

class _OtpFormFeildState extends State<OtpFormFeild> {
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        // CircularProgressIndicator(),

        animated_otp_fields(
          widget.otpTextEditingController,
          fieldHeight: screenWidth(context, dividedBy: 7.5),
          fieldWidth: screenWidth(context, dividedBy: 9),
          OTP_digitsCount: 6,
          animation: TextAnimation.Fading,
          border: Border.all(width: 1.0, color: Constants.kitGradients[30]),
          borderRadius: BorderRadius.all(Radius.circular(4)),
          contentPadding:
              EdgeInsets.only(top: screenHeight(context, dividedBy: 100)),
          forwardCurve: Curves.linearToEaseOut,
          textStyle: TextStyle(
            color: Constants.kitGradients[31],
            fontSize: 30,
            fontWeight: FontWeight.bold,
          ),
          onFieldSubmitted: (text) {},
          spaceBetweenFields: 14,
          autoFocus: true,
        ),
        // SizedBox(
        //   height: screenHeight(context, dividedBy: 10),
        // ),

        // Padding(
        //   padding: EdgeInsets.symmetric(
        //       horizontal: screenWidth(context, dividedBy: 13)),
        //   child: Text(
        //     "(If you don’t see it in your Inbox, it may have been sorted into your Spam folder.)",
        //     style: TextStyle(
        //         color: Constants.kitGradients[30],
        //         fontFamily: 'Muli',
        //         fontSize: screenWidth(context, dividedBy: 26),
        //         fontStyle: FontStyle.italic),
        //   ),
        // ),
        // // CircularProgressIndicator(),
        // SizedBox(
        //   height: screenHeight(context, dividedBy: 10),
        // ),

        // BuildButton(
        //   label: "VERIFY MY ACCOUNT",
        //   // disabled: true,
        //   buttonWidth: 1.3,
        //   onPressed: () {
        //     widget.otpTap();
        //     // otpAlertBox(
        //     //     context: context,
        //     //     title: "Your account has been verified!",
        //     //     route: CreatePassword(),
        //     //     stayOnPage: false);
        //   },
        //   loading: widget.isLoading,
        // ),
        // SizedBox(
        //   height: screenHeight(context, dividedBy: 20),
        // ),
      ],
    );
  }
}
