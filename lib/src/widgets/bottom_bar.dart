import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';

class CustomBottomBar extends StatefulWidget {
  final Function onTapProfile;
  final Function onTapHome;
  final double currentIndex;
  final Function onTapSearch;
  CustomBottomBar(
      {this.onTapHome, this.onTapSearch, this.onTapProfile, this.currentIndex});
  @override
  _CustomBottomBarState createState() => _CustomBottomBarState();
}

class _CustomBottomBarState extends State<CustomBottomBar> {
  int count;
  int _currentIndex = 0;
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Container(
        width: screenWidth(context, dividedBy: 1),
        height: screenHeight(context, dividedBy: 12),
        padding: EdgeInsets.symmetric(
          horizontal: screenWidth(context, dividedBy: 15),
        ),
        alignment: Alignment.center,
        decoration: BoxDecoration(
          // color:Constants.kitGradients[28],
          color:Constants.kitGradients[28],
          border: Border(
            top: BorderSide(color: Constants.kitGradients[27].withOpacity(0.3)),
          ),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SizedBox(
              width: screenWidth(context, dividedBy: 10),
            ),
            GestureDetector(
              onTap: () {
                widget.onTapHome();
              },
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Icon(Icons.dynamic_feed,
                      color: widget.currentIndex == 0
                          ? Constants.kitGradients[27]
                          : Constants.kitGradients[27].withOpacity(0.2),
                      size: 25),
                  Text(
                    "Feed",
                    style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.w400,
                      color: widget.currentIndex == 0
                          ? Constants.kitGradients[27]
                          : Constants.kitGradients[27].withOpacity(0.2),
                    ),
                  ),
                ],
              ),
            ),
            Spacer(),
            GestureDetector(
              onTap: () {
                widget.onTapSearch();
              },
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Icon(Icons.home_outlined,
                      color: widget.currentIndex == 1
                          ? Constants.kitGradients[27]
                          : Constants.kitGradients[27].withOpacity(0.2),
                      size: 25),
                  Text(
                    "Home",
                    style: TextStyle(
                      fontSize: 14,
                      fontWeight: FontWeight.w400,
                      color: widget.currentIndex == 1
                          ? Constants.kitGradients[27]
                          : Constants.kitGradients[27].withOpacity(0.2),
                    ),
                  ),
                ],
              ),
            ),
            Spacer(),
            GestureDetector(
              onTap: () {
                widget.onTapProfile();
              },
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Icon(Icons.pets_rounded,
                      color: widget.currentIndex == 2
                          ? Constants.kitGradients[27]
                          : Constants.kitGradients[27].withOpacity(0.2),
                      size: 25),
                  Text(
                    "Profile",
                    style: TextStyle(
                      fontSize: 14,
                      fontWeight: FontWeight.w400,
                      color: widget.currentIndex == 2
                          ? Constants.kitGradients[27]
                          : Constants.kitGradients[27].withOpacity(0.2),
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(
              width: screenWidth(context, dividedBy: 10),
            ),
          ],
        ),
      ),
    );
  }
}
