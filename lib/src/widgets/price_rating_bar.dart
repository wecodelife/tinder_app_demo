import 'package:flutter/material.dart';

class PriceRatingBar extends StatefulWidget {
  final String price;
  final double size;
  PriceRatingBar({this.price, this.size});
  @override
  _PriceRatingBarState createState() => _PriceRatingBarState();
}

Widget _buildDollars(
  BuildContext context,
  String price,
  double size,
) {
  String dolls = "\u20B9";
  Color iColor = Colors.white;

  return Text(
    " \u20B9 " + price.toString(),
    style:
        TextStyle(color: iColor, fontWeight: FontWeight.bold, fontSize: size),
  );
}

class _PriceRatingBarState extends State<PriceRatingBar> {
  @override
  Widget build(BuildContext context) {
    return _buildDollars(context, widget.price, widget.size);
  }
}
