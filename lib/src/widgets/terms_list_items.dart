import 'package:flutter/material.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/utils/constants.dart';

class TermsListItems extends StatefulWidget {
  //const TermsListItems({Key? key}) : super(key: key);
  final String title;
  final String data;
  TermsListItems({this.title, this.data});

  @override
  _TermsListItemsState createState() => _TermsListItemsState();
}

class _TermsListItemsState extends State<TermsListItems> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: EdgeInsets.symmetric(
              vertical: screenHeight(context, dividedBy: 90),
            ),
            child: Container(
              width: 6,
              height: 6,
              decoration: BoxDecoration(
                color: Constants.kitGradients[7].withOpacity(0.7),
                shape: BoxShape.circle,
              ),
            ),
          ),
          SizedBox(
            width: screenWidth(context, dividedBy: 40),
          ),
          Expanded(
              child: Column(
            children: [
              Text(
                widget.title,
                textAlign: TextAlign.left,
                style: TextStyle(
                  fontSize: 15,
                  fontWeight: FontWeight.w600,
                  fontFamily: "Prompt-Light",
                  color: Constants.kitGradients[7].withOpacity(0.7),
                ),
              ),
              SizedBox(
                height: screenHeight(context, dividedBy: 80),
              ),
              Text(
                widget.data,
                textAlign: TextAlign.left,
                style: TextStyle(
                  fontSize: 15,
                  fontWeight: FontWeight.w400,
                  fontFamily: "Prompt-Light",
                  color: Constants.kitGradients[19],
                ),
              ),
            ],
          ))
        ],
      ),
    );
  }
}
