import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class InformationIcon extends StatefulWidget {
  final Function onPressed;
  final IconData icon;
  final double rightPadding;
  final bool deleteLiked;
  final IconData icon2;
  final String hero;
  final double buttonWidth;
  final double buttonHeight;
  InformationIcon({
    this.onPressed,
    this.icon,
    this.rightPadding,
    this.deleteLiked,
    this.icon2,
    this.hero,
    this.buttonWidth,
    this.buttonHeight
  });
  @override
  _InformationIconState createState() => _InformationIconState();
}

class _InformationIconState extends State<InformationIcon> {
  bool isFavourite = true;
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
      },
      child: Container(
           height: widget.buttonHeight,
          width: widget.buttonWidth,
    decoration: BoxDecoration(
    border: Border.all(color: Constants.kitGradients[28], width: 1),
    borderRadius: BorderRadius.circular(30),
    ),
          child: widget.deleteLiked != true
              ?  FloatingActionButton(
            onPressed: () {
              widget.onPressed();
            },
            child: Icon(
              widget.icon,
              size: 25,
              color:  Constants.kitGradients[28],
            ),
            backgroundColor: Colors.transparent ,
            heroTag: widget.hero,
          )
          /*IconButton(
                  alignment: Alignment.center,
                  icon: Icon(
                    widget.icon,
                    color: Constants.kitGradients[27],
                  ),
                  iconSize: screenWidth(context, dividedBy: 19),
                )*/
              : Center(
                  child: IconButton(
                    alignment: Alignment.center,
                    icon: Icon(
                      widget.icon,
                      color: Constants.kitGradients[27],
                    ),
                    iconSize: screenWidth(context, dividedBy: 20),
                  ),
                )),
    );
  }
}
