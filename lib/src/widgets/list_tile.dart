import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';

class AppListTile extends StatefulWidget {
  final String text;
  final Function onTap;
  //final String songCount;
  AppListTile({
    this.text,
    this.onTap,
    // this.songCount
  });
  @override
  _AppListTileState createState() => _AppListTileState();
}

class _AppListTileState extends State<AppListTile> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: widget.onTap,
      child: Column(children: [
        Container(
          child: Row(
            children: [
              Container(
                width: screenWidth(context,dividedBy: 3),
                child: Text(
                  widget.text,
                  style: TextStyle(
                    color: Constants.kitGradients[12],
                    fontWeight: FontWeight.w400,
                    fontSize: 15,
                  ),
                ),
              ),
              Spacer(),
              GestureDetector(
                onTap: widget.onTap,
                child: Container(
                    height: screenWidth(context,
                        dividedBy: 14),
                    width: screenWidth(context,
                        dividedBy: 14),
                    decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color: Constants.kitGradients[31]
                    ),
                    child:Icon(Icons.arrow_forward_ios,
                      color: Constants.kitGradients[28],
                      size: 15,)
                ),
              )
            ],
          ),
        ),
        // Container(
        //     width: screenWidth(context, dividedBy: 1),
        //     // height:
        //     // screenHeight(context, dividedBy: 15),
        //     padding: EdgeInsets.symmetric(
        //       horizontal: screenWidth(context, dividedBy: 50),
        //       vertical: screenHeight(context, dividedBy: 70),
        //     ),
        //     alignment: Alignment.centerLeft,
        //     child: Row(
        //       crossAxisAlignment: CrossAxisAlignment.center,
        //       mainAxisAlignment: MainAxisAlignment.center,
        //       children: [
        //         Text(
        //           widget.text,
        //           style: TextStyle(
        //             fontSize: 15,
        //             fontFamily: "Prompt-Light",
        //             fontWeight: FontWeight.w400,
        //             color: Constants.kitGradients[28],
        //           ),
        //         ),
        //         Spacer(),
        //         Icon(
        //           Icons.keyboard_arrow_right_rounded,
        //           size:20,
        //           color: Constants.kitGradients[28].withOpacity(0.4),
        //         ),
        //       ],
        //     )),
        // Divider(
        //   color: Constants.kitGradients[28].withOpacity(0.2),
        // ),
      ]),
    );
  }
}
