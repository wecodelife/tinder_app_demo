import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class DragabbleTabBar extends StatefulWidget {
  final Function onTapSaved;
  final Function onTapHome;
  final Function onTapLiked;
  final double currentIndex;
  final Function onTapProfile;
  final bool visible;
  DragabbleTabBar(
      {this.onTapHome,
      this.onTapSaved,
      this.onTapProfile,
      this.currentIndex,
      this.visible,
      this.onTapLiked});
  @override
  _DragabbleTabBarState createState() => _DragabbleTabBarState();
}

class _DragabbleTabBarState extends State<DragabbleTabBar>
    with SingleTickerProviderStateMixin {
  //Vertical drag details
  DragStartDetails startVerticalDragDetails;
  DragUpdateDetails updateVerticalDragDetails;
  int count;
  int _currentIndex = 0;
  bool expanded = false;

  double _scale;
  AnimationController _controller;
  Animation<double> animation;
  @override
  void initState() {
    _controller = AnimationController(
      vsync: this,
      duration: Duration(
        seconds: 2,
      ),
    );

    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
    print("disposing");

    _controller.dispose();
  }

  @override
  Widget build(BuildContext context) {
    _scale = 1 - _controller.value;

    return SafeArea(
        child: AnimatedBuilder(
      animation: _controller,
      builder: (context, child) => Transform.translate(
          //angle: -_controller.value *3.2,
          offset: Offset(_controller.value / 70, _controller.value / 70),
          child: Container(
            width: screenWidth(context, dividedBy: 1),
            height: screenHeight(context, dividedBy: 12),
            padding: EdgeInsets.symmetric(
              horizontal: screenWidth(context, dividedBy: 15),
            ),
            alignment: Alignment.center,
            decoration: BoxDecoration(
              color: Constants.kitGradients[28],
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                // SizedBox(
                //   width: screenWidth(context, dividedBy: 10),
                // ),

                GestureDetector(
                  onTap: () {
                    widget.onTapHome();
                  },
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      widget.currentIndex == 0
                          ? Image(
                              image:
                                  AssetImage("assets/images/food_selected.png"),
                              color: Constants.kitGradients[0],
                              width: 28,
                              height: 28)
                          : Image(
                              image: AssetImage("assets/images/food_icon.png"),
                              color: Constants.kitGradients[0],
                              width: 28,
                              height: 28),
                    ],
                  ),
                ),
                Spacer(),
                GestureDetector(
                  onTap: () {
                    widget.onTapLiked();
                  },
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      widget.currentIndex == 1
                          ? FaIcon(FontAwesomeIcons.solidHeart,
                              color: Constants.kitGradients[0], size: 25)
                          : FaIcon(FontAwesomeIcons.heart,
                              color: Constants.kitGradients[0], size: 25),
                    ],
                  ),
                ),
                Spacer(),
                GestureDetector(
                  onTap: () {
                    widget.onTapSaved();
                  },
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      widget.currentIndex == 2
                          ? FaIcon(FontAwesomeIcons.solidImage,
                              color: Constants.kitGradients[0], size: 25)
                          : FaIcon(FontAwesomeIcons.image,
                              color: Constants.kitGradients[0], size: 25),
                    ],
                  ),
                ),
                Spacer(),
                GestureDetector(
                  onTap: () {
                    widget.onTapProfile();
                  },
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      widget.currentIndex == 3
                          ? FaIcon(FontAwesomeIcons.solidUser,
                              color: Constants.kitGradients[0], size: 24)
                          : FaIcon(FontAwesomeIcons.user,
                              color: Constants.kitGradients[0], size: 24),
                    ],
                  ),
                ),
              ],
            ),
          )),
    ));
  }
}
