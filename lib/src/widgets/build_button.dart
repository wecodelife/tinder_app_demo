import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

class BuildButton extends StatefulWidget {
  final Function onPressed;
  final String title;
  final bool isLoading;
  final double buttonWidth;
  final isImage;
  final bool isDisabled;

  BuildButton({this.onPressed, this.title, this.isLoading,this.buttonWidth,this.isImage,this.isDisabled});
  @override
  _BuildButtonState createState() => _BuildButtonState();
}

class _BuildButtonState extends State<BuildButton> {
  bool image=false;
  @override
  void initState() {
    // TODO: implement initState
    if(widget.isImage==true)
      image=widget.isImage;

    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        widget.onPressed();
      },
      child: Container(
        width: widget.buttonWidth == null ?
        screenWidth(context, dividedBy: 2.5) : screenWidth(context, dividedBy: widget.buttonWidth),
        height: screenHeight(context, dividedBy: 17),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(13),
          color: widget.isLoading == false?
          Constants.kitGradients[28]:Constants.kitGradients[31],
          // boxShadow: [
          //   BoxShadow(
          //     blurRadius: 6,
          //     spreadRadius: 3,
          //     offset: Offset(-4, -2),
          //     // color: Color.fromRGBO(255, 255, 255, 0.9),
          //     color: Color.fromRGBO(0, 0, 0, 0.12),
          //   ),
          //   BoxShadow(
          //     blurRadius: 6,
          //     spreadRadius: 3,
          //     offset: Offset(4, 2),
          //     color: Color.fromRGBO(255, 255, 255, 0.9),
          //     // color: Color.fromRGBO(0, 0, 0, 0.15),
          //   ),
          // ],
        ),
        child: Center(
          child:
          widget.isLoading != true ?
          image?Row(
            mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Image(image: AssetImage('assets/images/Glogo.png'),
                    height: screenWidth(context,dividedBy: 13),
                    width: screenWidth(context,dividedBy: 13),
                    color: Constants.kitGradients[0],),
                  // SizedBox(
                  //   width: screenWidth(context, dividedBy: 35),
                  // ),
                  Text(
                    " "+widget.title,
                    style: TextStyle(
                      color: Constants.kitGradients[0],
                      fontWeight: FontWeight.bold,
                      fontFamily: "Prompt-Light",
                      fontSize: 16,
                    ),
                  )
                ],
              ):Text(
                  widget.title,
                  style: TextStyle(
                    color: Constants.kitGradients[0],
                    fontWeight: FontWeight.bold,
                    fontFamily: "Prompt-Light",
                    fontSize: 16,
                  ),
                ):Container(
            // height: screenWidth(context, dividedBy: 40),
            // width:screenWidth(context, dividedBy: 40),
    decoration: BoxDecoration(
      color: Constants.kitGradients[28],
      borderRadius: BorderRadius.circular(13),),
            child: Center(
              child: SizedBox(
                height: 16,
                width:16,
                child: CircularProgressIndicator(
                  strokeWidth: 2,
                  valueColor: new AlwaysStoppedAnimation<Color>(
                      Constants.kitGradients[0]),
                ),
              ),
            ),
          ),
        //       :Shimmer.fromColors(
        // baseColor: Constants.kitGradients[31],
        //   highlightColor: Colors.grey[100],
        //   enabled: true,
        //   child: Container(
        //     height: screenHeight(context, dividedBy: 18),
        //     width: screenWidth(context, dividedBy: 2.5),
        //     padding: EdgeInsets.symmetric(
        //         horizontal: screenWidth(context, dividedBy: 30),
        //         vertical: screenHeight(context, dividedBy: 80)),
        //     decoration: BoxDecoration(
        //       color: widget.isDisabled == true
        //           ? Colors.transparent
        //           : Constants.kitGradients[31],
        //       border: Border.all(color: Constants.kitGradients[31]),
        //       borderRadius: BorderRadius.all(Radius.circular(15)),
        //     ),
        //     alignment: Alignment.center,
        //   ),
        // )
        ),
      ),
    );
  }
}
