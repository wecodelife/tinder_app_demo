import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class OnBoardingTextField extends StatefulWidget {
  final String labelText;
  final Widget suffixIcon;
  final bool hasSuffix;
  Function onTapIcon;
  Function onTap;
  bool textArea;
  bool readOnly;
  final TextEditingController textEditingController;
  OnBoardingTextField({
    this.labelText,
    this.textEditingController,
    this.hasSuffix,
    this.onTapIcon,
    this.suffixIcon,
    this.readOnly,
    this.onTap,
    this.textArea,
  });

  @override
  _OnBoardingTextFieldState createState() => _OnBoardingTextFieldState();
}

class _OnBoardingTextFieldState extends State<OnBoardingTextField> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: screenHeight(context, dividedBy: 16),
      width: screenWidth(context, dividedBy: 1),
      decoration: BoxDecoration(
        border: widget.textArea == false
            ? Border.all(
                color: Constants.kitGradients[27].withOpacity(0.7), width: 0.0)
            : Border.all(color: Colors.transparent),
        borderRadius: BorderRadius.circular(20),
      ),
      child: TextFormField(
        controller: widget.textEditingController,
        cursorColor: Constants.kitGradients[19],
        style: TextStyle(
          color: Constants.kitGradients[28],
          fontFamily: 'Montserrat',
          fontSize: 16,
        ),
        autofocus: true,
        readOnly: widget.readOnly,
        onTap: widget.onTap,
        keyboardType: widget.textArea == true
            ? TextInputType.multiline
            : TextInputType.text,
        maxLines: widget.textArea == true ? 5 : 1,
        decoration: InputDecoration(
          floatingLabelBehavior: FloatingLabelBehavior.never,

          suffixIcon: widget.hasSuffix
              ? GestureDetector(
                  onTap: widget.onTapIcon, child: widget.suffixIcon)
              : Container(
                  width: screenWidth(context, dividedBy: 120),
                  height: screenHeight(context, dividedBy: 120),
                ),
          contentPadding: EdgeInsets.symmetric(
            horizontal: screenWidth(context, dividedBy: 30),
            vertical: screenHeight(context, dividedBy: 120),
          ),
          hintText: widget.labelText,
          hintStyle: TextStyle(
            color: Constants.kitGradients[19],
            fontFamily: 'Montserrat',
            fontSize: 16,
          ),
          // labelText: widget.labelText,
          // labelStyle: TextStyle(
          //   color: Constants.kitGradients[19],
          //   fontFamily: 'OpenSansRegular',
          //   fontSize: 16,
          // ),
          filled: false,
          fillColor: Constants.kitGradients[27],
          focusedBorder: OutlineInputBorder(
            borderSide: widget.textArea == false
                ? BorderSide(color:  Constants.kitGradients[28].withOpacity(0.12), width: 0.0)
                : BorderSide(color: Colors.transparent, width: 0.0),
            borderRadius: widget.textArea == false
                ? BorderRadius.circular(10.0)
                : BorderRadius.circular(0),
          ),
          enabledBorder: OutlineInputBorder(
            borderSide: widget.textArea == false
                ? BorderSide(color:  Constants.kitGradients[28].withOpacity(0.12), width: 0.0)
                :BorderSide(color: Colors.transparent, width: 0.0),
            borderRadius: widget.textArea == false
                ? BorderRadius.circular(10.0)
                : BorderRadius.circular(0),
          ),
        ),
      ),
    );
  }
}
