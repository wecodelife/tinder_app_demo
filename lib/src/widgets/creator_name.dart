import 'package:flutter/material.dart';
import 'package:app_template/src/utils/constants.dart';

class CreatorName extends StatefulWidget {
  @override
  _CreatorNameState createState() => _CreatorNameState();
}

class _CreatorNameState extends State<CreatorName> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Text(
        "WeCodeLife",
        style: TextStyle(
          fontSize: 12,
          fontWeight: FontWeight.w500,
          color: Constants.kitGradients[28],
          fontFamily: "Prompt-Light",
        ),
      ),
    );
  }
}
