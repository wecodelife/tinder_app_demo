import 'package:app_template/src/bloc/user_bloc.dart';
import 'package:app_template/src/models/get_food_response_Model.dart';
import 'package:app_template/src/models/update_like_request_model.dart';
import 'package:app_template/src/models/update_mustvisit_request_model.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:app_template/src/screens/food_information_page.dart';
import 'package:app_template/src/screens/home_page_screen.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:maps_launcher/maps_launcher.dart';
import 'package:shimmer/shimmer.dart';
import 'package:carousel_pro/carousel_pro.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/widgets/favourites_box.dart';
import 'package:app_template/src/widgets/image_slider_indicator.dart';
import 'package:app_template/src/widgets/information_icon.dart';
import 'package:app_template/src/widgets/like_button.dart';
class ImageCard extends StatefulWidget {
 // final String img;
  final String foodName;
  final String hotel;
  final String price;
  final String type;
  final double rating;
  final int foodId;
  final int index;
  final Function onTap;
  final Function onDoubleTap;
  final Function onPressedFavourite;
  final Function onPressedLocation;
  final Function onPressedStar;
  final Function onPressedDetails;
  final List<String> pageViewImage;
  final double latitude;
  final double longitude;
  bool likeBool=true;
  bool mustBool;
  // final int count;
  ImageCard({
    this.price,
    this.rating,
    this.foodId,
    this.mustBool,
    this.likeBool,
    this.foodName,
    this.hotel,
    this.type,
    this.onPressedFavourite,
    this.onPressedLocation,
    this.onPressedDetails,
    this.onPressedStar,
    this.longitude,
    this.latitude,
    this.onTap,
    this.onDoubleTap,
    this.pageViewImage,
   // this.img,
    this.index,
  //  this.count
   });
  @override
  _ImageCardState createState() => _ImageCardState();
}

class _ImageCardState extends State<ImageCard> {
  int index = 0;
  bool reverse = false;
  bool swipe = false;
  bool visible = false;
  bool isLoading = false;
  bool checkIndex = false;
  bool bgColor = false;
  bool imageChange = false;
  bool likedBool = true;
  //bool mustBool = true;
  List<bool> boolValues=[
  ];
  final foodImageSection=[
    'https://thumbs.dreamstime.com/b/masala-dosa-chutney-sambaar-traditionally-south-indian-dish-masala-dosa-chutney-sambaar-traditionally-south-96973772.jpg'
  'https://im.whatshot.in/img/2020/Apr/vertical-1585861220.jpg'
    "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTRTFE_cQpB5qhHpgxE7VL1POCGcsPY-arPXW6VtefMeVRBpkVU&s"
  ];

   addOrRemove()async{
     if (widget.mustBool == true) {
       setState(() {
         widget.mustBool = false;
       });
       userBloc.updateMustVisit(
           updateMustVisitRequestModel: UpdateMustVisitRequestModel(
             food: widget.foodId,
             //     user: int.parse(ObjectFactory().appHive.getUserId()),
             // isFavourite: false,
             mustVisit: false,

           ));
     } else {
       setState(() {
         widget.mustBool = true;
       });
       userBloc.updateMustVisit(
           updateMustVisitRequestModel: UpdateMustVisitRequestModel(
             food: widget.foodId,
             //     user: int.parse(ObjectFactory().appHive.getUserId()),
             // isFavourite: false,
             mustVisit: true,

           ));
       // pushAndRemoveUntil(context, LikedItemsScreen(), false);
     }
   }

  addOrRemoveLiked()async{
    if (widget.likeBool == true) {
      setState(() {
        widget.likeBool = false;
      });
      userBloc.updateLike(
          updateLikeRequestModel: UpdateLikeRequestModel(
            food: widget.foodId,
            //     user: int.parse(ObjectFactory().appHive.getUserId()),
            // isFavourite: false,
            isLiked: false,

          ));
    } else {
      setState(() {
        widget.likeBool = true;
      });
      userBloc.updateLike(
          updateLikeRequestModel: UpdateLikeRequestModel(
            food: widget.foodId,
            //     user: int.parse(ObjectFactory().appHive.getUserId()),
            // isFavourite: false,
            isLiked: true,

          ));
      // pushAndRemoveUntil(context, LikedItemsScreen(), false);
    }
  }
  UserBloc userBloc = new UserBloc();

   @override
  void initState() {
     userBloc.likedFood.listen((event) {
       setState(() {

       });
       // for(int i=0;i<event.results.length;i++){
       //   boolValues.add(event.results[i].mustVisit);
       // }
     });
     // userBloc.updateMustVisitResponse.listen((event) {
     //   print("must visited item deleted////");
     //   setState(() {
     //    // mustBool=widget.mustBool;
     //   });
     //   if (event.status == 200) {
     //     userBloc.getLikedFood(id:"?must_visit=true");
     //     //showToastCommon("Favourite Food Response Updated ");
     //   } else {
     //     showToastCommon(event.message);
     //   }
     // });
    super.initState();
  }
  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: (){
        widget.onTap();
      },
      onDoubleTap: (){
        widget.onDoubleTap();
      },

      child: Container(
        child: Stack(
          children:[
            Carousel(
              images:widget.pageViewImage
                  .map(
                      (item) =>
                      isLoading == false ?
                CachedNetworkImage(
              fit: BoxFit.fill,
              imageUrl:item,
                  placeholder: (context, url) => Center(
                    heightFactor: 1,
                    widthFactor: 1,
                    child: SpinKitThreeBounce(color: Constants.kitGradients[28],)
                  ),
              imageBuilder: (context, imageProvider) => Container(
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: imageProvider,
                    fit: BoxFit.fill,
                  ),
                ),
              ),
          ):Container(),
              ).toList(),
              autoplay: true,
              //animationDuration: Duration(seconds: 5),
                autoplayDuration:Duration(seconds: 9) ,
              dotSize: 5.0,
                dotIncreaseSize: 2,
              dotSpacing: 15.0,
              dotColor: Constants.kitGradients[28],
             // dotColor: Colors.grey[850],
              dotBgColor: Colors.transparent,
              dotIncreasedColor:Constants.kitGradients[28].withOpacity(.5)
             // borderRadius: true,
            ),
            Column(
              children: [
                Spacer(),
                Padding(
                  padding: EdgeInsets.symmetric(
                      horizontal: screenWidth(context,
                          dividedBy: 13)),
                  child: GestureDetector(
                    onTap: (){
                      widget.onPressedDetails();
                    },
                    child: Container(
                      width:
                      screenWidth(context, dividedBy: 1.2),
                      child: FavouritesBox(
                        foodName: widget.foodName,
                        price: widget.price,
                        foodType: widget.type,
                        hotelName: widget.hotel,
                        rating: widget.rating,
                        id: widget.foodId,
                        // onPressed: (){
                        //   widget.onPressedDetails();
                        //
                        // },
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  height:
                  screenHeight(context, dividedBy: 40),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(
                      horizontal: screenWidth(context,
                          dividedBy: 20)),
                  child: Container(
                    width: screenWidth(context, dividedBy: 1),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        InformationIcon(
                          icon: Icons.location_on,
                          rightPadding: 4.2,
                          buttonWidth: screenWidth(context, dividedBy: 9),
                          buttonHeight: screenHeight(context, dividedBy: 20),
                          onPressed: () {
                           // widget.onPressedLocation();
                            MapsLauncher.launchCoordinates(widget.latitude, widget.longitude);
                          },
                        ),
                        InformationIcon(
                          icon: widget.likeBool== false ?
                          Icons.favorite_outline:
                          Icons.favorite,
                          rightPadding: 4.2,
                          buttonWidth: screenWidth(context, dividedBy: 8),
                          buttonHeight: screenHeight(context, dividedBy: 18),
                          onPressed: () {
                            //if(widget.mustBool==true){
                              addOrRemoveLiked();
                           // }
                            widget.onPressedFavourite();
                          },
                        ),
                        InformationIcon(
                          icon: widget.mustBool== false ?
                          Icons.star_border:
                          Icons.star,
                          rightPadding: 4.2,
                          buttonWidth: screenWidth(context, dividedBy:9),
                          buttonHeight: screenHeight(context, dividedBy: 20),
                          onPressed: () {
                            addOrRemove();
                            widget.onPressedStar();
                          },
                        ),

                      ],
                    ),
                  ),
                ),
                SizedBox(
                  height:
                  screenHeight(context, dividedBy: 15),
                ),
              ],
            )
        ]
        ),
      ),
    );
  }
}
