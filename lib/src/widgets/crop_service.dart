import 'dart:typed_data';
import 'dart:ui' as UI;

import 'package:extended_image/extended_image.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:image/image.dart' as ui;
// import 'package:image/image.dart';

class CropService {
  // bool loading = false;
  Future<Uint8List> cropImageDataWithDartLibrary(
      {ExtendedImageEditorState state, ValueChanged editLoading}) async {
    print('dart library start cropping');
    editLoading(true);

    ///crop rect base on raw image
    final Rect cropRect = state.getCropRect();

    print('getCropRect : $cropRect');
    Uint8List data;
    // in web, we can't get rawImageData due to .
    // using following code to get imageCodec without download it.
    // final Uri resolved = Uri.base.resolve(key.url);
    // // This API only exists in the web engine implementation and is not
    // // contained in the analyzer summary for Flutter.
    // return ui.webOnlyInstantiateImageCodecFromUrl(
    //     resolved); //
    // Future<void> getData() {
    data = (await state.image.toByteData(format: UI.ImageByteFormat.png))
        .buffer
        .asUint8List();
    print("data" + data.toString());
    // return null;
    // }

    // await getData();
    // final EditActionDetails editAction = state.editAction;

    // final DateTime time1 = DateTime.now();

    //Decode source to Animation. It can holds multi frame.
    ui.Animation src;
    //LoadBalancer lb;
    // if (kIsWeb) {
    //   src = decodeAnimation(data);
    // } else {
    src = await compute(ui.decodeAnimation, data);
    print("src" + src.toString());
    // }
    if (src != null) {
      //handle every frame.
      src.frames = src.frames.map((ui.Image image) {
        // final DateTime time2 = DateTime.now();
        //clear orientation
        image = ui.bakeOrientation(image);

        // if (editAction.needCrop) {
        image = ui.copyCrop(image, cropRect.left.toInt(), cropRect.top.toInt(),
            cropRect.width.toInt(), cropRect.height.toInt());
        // }

        // if (editAction.needFlip) {
        //   late Flip mode;
        //   if (editAction.flipY && editAction.flipX) {
        //     mode = Flip.both;
        //   } else if (editAction.flipY) {
        //     mode = Flip.horizontal;
        //   } else if (editAction.flipX) {
        //     mode = Flip.vertical;
        //   }
        //   image = flip(image, mode);
        // }
        //
        // if (editAction.hasRotateAngle) {
        //   image = copyRotate(image, editAction.rotateAngle);
        // }
        // final DateTime time3 = DateTime.now();
        // print('${time3.difference(time2)} : crop/flip/rotate');
        return image;
      }).toList();
    }

    /// you can encode your image
    ///
    /// it costs much time and blocks ui.
    //var fileData = encodeJpg(src);

    /// it will not block ui with using isolate.
    //var fileData = await compute(encodeJpg, src);
    //var fileData = await isolateEncodeImage(src);
    List<int> fileData;
    print('start encode');
    final DateTime time4 = DateTime.now();
    if (src != null) {
      final bool onlyOneFrame = src.numFrames == 1;
      //If there's only one frame, encode it to jpg.
      // if (kIsWeb) {
      //   fileData = onlyOneFrame ? ui.encodeJpg(src.first) : ui.encodeGifAnimation(src);
      // } else {
      //fileData = await lb.run<List<int>, Image>(encodeJpg, src);
      fileData = onlyOneFrame
          ? await compute(ui.encodeJpg, src.first)
          : await compute(ui.encodeGifAnimation, src);
    }
    final DateTime time5 = DateTime.now();
    print('${time5.difference(time4)} : encode');
    // print('${time5.difference(time1)} : total time');
    return Uint8List.fromList(fileData);
  }
}
