import 'package:app_template/src/utils/constants.dart';
import 'package:flutter/material.dart';

typedef void RatingChangeCallback(double rating);
class StarRatingBar extends StatelessWidget {
  final int starCount;
  final double rating;
  final RatingChangeCallback onRatingChanged;
  final Color color;

  StarRatingBar({this.starCount = 5, this.rating = .0, this.onRatingChanged, this.color});

  Widget buildStar(BuildContext context, int index) {
    Icon icon;
    if (index >= rating) {
      icon = new Icon(
        Icons.star_border,
        size:40,
        color: Constants.kitGradients[28],
      );
    }
    else if (index > rating - 1 && index < rating) {
      icon = new Icon(
        Icons.star_half,
        size:40,
        color: color ?? Constants.kitGradients[28],
      );
    } else {
      icon = new Icon(
        Icons.star,
        size:40,
        color: color ?? Constants.kitGradients[28],
      );
    }
    return new GestureDetector(
      onTap: onRatingChanged == null ? null : () => onRatingChanged(index + 1.0),
      onDoubleTap: onRatingChanged == null ? null : () => onRatingChanged(index + 0.5),
      child: icon,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child:Row(
        mainAxisAlignment: MainAxisAlignment.center,
          children: new List.generate(starCount, (index) => buildStar(context, index)))
    );
  }
}
