import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class LikeMustVisitIcon extends StatefulWidget {
  final Function onPressed;
  final IconData icon;
  final double rightPadding;
  final bool deleteLiked;
  final IconData icon2;
  final String hero;
  final double buttonWidth;
  final double buttonHeight;
  LikeMustVisitIcon({
    this.onPressed,
    this.icon,
    this.rightPadding,
    this.deleteLiked,
    this.icon2,
    this.hero,
    this.buttonWidth,
    this.buttonHeight
  });
  @override
  _LikeMustVisitIconState createState() => _LikeMustVisitIconState();
}

class _LikeMustVisitIconState extends State<LikeMustVisitIcon> {
  bool isFavourite = true;
  @override
  Widget build(BuildContext context) {
    return  Positioned(
      bottom: widget.deleteLiked != true ? 0 : null,
      top: widget.deleteLiked == true ? 0 : null,
      right: screenWidth(context, dividedBy: widget.rightPadding),
      child: GestureDetector(
        onTap: () {
          widget.onPressed();
        },
        child: Container(
          // height: screenHeight(context, dividedBy: 21),
            width: screenWidth(context, dividedBy: 10),
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              color: Constants.kitGradients[28].withOpacity(0.5),
            ),
            child: widget.deleteLiked != true
                ? IconButton(
              alignment: Alignment.center,
              icon: Icon(
                widget.icon,
                color: Constants.kitGradients[27],
              ),
              iconSize: screenWidth(context, dividedBy: 18),
            )
                : Center(
              child: IconButton(
                alignment: Alignment.center,
                icon: Icon(
                  widget.icon,
                  color: Constants.kitGradients[27],
                ),
                iconSize: screenWidth(context, dividedBy: 18),
              ),
            )),
      ),
    );
  }
}
