import 'dart:convert';

import 'package:app_template/src/widgets/filtered_image_widget.dart';
import 'package:flutter/material.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:image_picker/image_picker.dart';
import 'package:image/image.dart' as img;
import 'package:photofilters/filters/filters.dart';
import 'dart:io';

import 'package:photofilters/filters/preset_filters.dart';

class FilteredImageList extends StatelessWidget {
  final List<Filter> filters;
  final img.Image image;
  final ValueChanged<Filter> onValueChangedFilter;
  const FilteredImageList(
      {Key key, this.filters, this.image, this.onValueChangedFilter})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        height: screenWidth(context, dividedBy: 2.5),
        child: ListView.builder(
          itemCount: filters.length,
          shrinkWrap: true,
          scrollDirection: Axis.horizontal,
          itemBuilder: (BuildContext context, int index) {
            final filter = filters[index];
            return InkWell(
              onTap: () => onValueChangedFilter(filter),
              splashColor: Colors.transparent,
              child: Container(
                padding: EdgeInsets.all(
                  screenWidth(context, dividedBy: 40),
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    FilteredImageWidget(
                      image: image,
                      filter: filter,
                      successBuilder: (imageBytes) {
                        return CircleAvatar(
                          radius: screenWidth(context, dividedBy: 8),
                          backgroundColor:
                              Constants.kitGradients[27].withOpacity(0.1),
                          backgroundImage: MemoryImage(
                            imageBytes,
                          ),
                        );
                      },
                      errorBuilder: () => CircleAvatar(
                          radius: screenWidth(context, dividedBy: 8),
                          backgroundColor:
                              Constants.kitGradients[27].withOpacity(0.1),
                          child: Icon(Icons.report,
                              color:
                                  Constants.kitGradients[17].withOpacity(0.3))),
                      loadingBuilder: () => CircleAvatar(
                        radius: screenWidth(context, dividedBy: 8),
                        child: Center(
                          heightFactor: 1,
                          widthFactor: 1,
                          child: SizedBox(
                            height: 16,
                            width: 16,
                            child: CircularProgressIndicator(
                              valueColor: AlwaysStoppedAnimation(
                                  Constants.kitGradients[19]),
                              strokeWidth: 2,
                            ),
                          ),
                        ),
                        backgroundColor:
                            Constants.kitGradients[27].withOpacity(0.1),
                      ),
                    ),
                    SizedBox(
                      height: screenWidth(context, dividedBy: 50),
                    ),
                    Text(
                      filter.name,
                      style: TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.bold,
                        fontFamily: "Montserrat",
                        color: Constants.kitGradients[27],
                      ),
                    ),
                  ],
                ),
              ),
            );
          },
        ),
      ),
    );
  }
}
