import 'package:app_template/src/screens/user_feed_image.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

class ProfileTabBar extends StatefulWidget {
  final List<String> imgCollection;
  final List<String> taggedImages;
  final List<bool> hasCaption;
  final List<String> captions;
  final String userName;
  final String userProfile;
  ProfileTabBar(
      {this.imgCollection,
      this.taggedImages,
      this.hasCaption,
      this.captions,
      this.userProfile,
      this.userName});
  @override
  _ProfileTabBarState createState() => _ProfileTabBarState();
}

class _ProfileTabBarState extends State<ProfileTabBar> {
  int count = 1;
  //int tIndex = 1;
  double currentIndex = 0;
  bool isEvent = false;

  @override
  Widget build(BuildContext context) {
    return Container(
      // height: screenHeight(context, dividedBy: 1),
      width: screenWidth(context, dividedBy: 1),
      child: Column(
        children: [
          Container(
            padding: EdgeInsets.symmetric(
                //horizontal: screenWidth(context, dividedBy: 30),
                //vertical: screenHeight(context, dividedBy: 50),
                ),
            width: screenWidth(context, dividedBy: 1),
            color: Constants.kitGradients[28].withOpacity(0.12),
            child: Row(
              children: [
                GestureDetector(
                  onTap: () {
                    setState(() {
                      isEvent = !isEvent;
                    });
                  },
                  child: Container(
                    width: screenWidth(context, dividedBy: 2.3),
                    padding: EdgeInsets.symmetric(
                      vertical: screenHeight(context, dividedBy: 150),
                    ),
                    decoration: BoxDecoration(
                      //color: Colors.blue,
                      // color: count == 1 ? Constants.kitGradients[30]:Constants.kitGradients[29],
                      border: Border(
                          bottom: isEvent == false
                              ? BorderSide(
                            color: Constants.kitGradients[28],
                            width:2.0
                          )
                              : BorderSide(
                            color: Constants.kitGradients[28].withOpacity(0.12),
                          )),
                    ),
                    child: Icon(
                      Icons.grid_on_sharp,
                      color: isEvent == false
                          ? Constants.kitGradients[28]
                          : Constants.kitGradients[28].withOpacity(0.12),
                    ),
                  ),
                ),
                Spacer(
                  flex: 5,
                ),
                GestureDetector(
                  onTap: () {
                    // count == 1
                    //     ?
                    setState(() {
                      isEvent = !isEvent;
                    });
                    // : setState(() {});
                  },
                  child: Container(
                    width: screenWidth(context, dividedBy: 2.3),
                    padding: EdgeInsets.symmetric(
                      vertical: screenHeight(context, dividedBy: 150),
                    ),
                    decoration: BoxDecoration(
                      //color: count == 2 ? Constants.kitGradients[30]:Constants.kitGradients[29],
                      border: Border(
                          bottom: isEvent == true
                              ? BorderSide(
                              color: Constants.kitGradients[28],
                              width:2.0
                          )
                              : BorderSide(
                            color: Constants.kitGradients[28].withOpacity(0.12),
                          )),
                    ),
                    child: Icon(
                      Icons.animation,
                      color: isEvent == true
                          ? Constants.kitGradients[28]
                          : Constants.kitGradients[28].withOpacity(0.12),
                    ),
                  ),
                ),
              ],
            ),
          ),
          SizedBox(
            height: screenHeight(context, dividedBy: 40),
          ),
          isEvent == true
              ? Container(
                  padding: EdgeInsets.symmetric(
                    horizontal: screenWidth(context, dividedBy: 40),
                    //vertical: screenHeight(context, dividedBy: 50),
                  ),
                  child: GridView.builder(
                    shrinkWrap: true,
                    physics: NeverScrollableScrollPhysics(),
                    itemCount: widget.imgCollection.length,
                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 3,
                      crossAxisSpacing: 5,
                      mainAxisSpacing: 5,
                    ),
                    itemBuilder: (BuildContext context, int index) {
                      return GestureDetector(
                          onTap: () {
                            setState(() {
                              currentIndex = index.toDouble();
                            });
                            push(
                              context,
                              SingleImagePage(
                                userImage: widget.imgCollection,
                                hasCaption: widget.hasCaption,
                                userCaption: widget.captions,
                                userName: widget.userName,
                                userProfile: widget.userProfile,
                                currentIndex: currentIndex,
                              ),
                            );
                          },
                          child: Container(
                            height: screenWidth(context, dividedBy: 15),
                            width: screenWidth(context, dividedBy: 15),
                            decoration: BoxDecoration(
                              border:
                                  Border.all(color: Constants.kitGradients[28]),
                            ),
                            child: CachedNetworkImage(
                              height: screenWidth(context, dividedBy: 15),
                              width: screenWidth(context, dividedBy: 15),
                              imageUrl: widget.imgCollection[index],
                              fit: BoxFit.cover,
                              placeholder: (context, url) => Center(
                                heightFactor: 1,
                                widthFactor: 1,
                                child: SizedBox(
                                  height: 16,
                                  width: 16,
                                  child: CircularProgressIndicator(
                                    valueColor: AlwaysStoppedAnimation(
                                        Constants.kitGradients[28].withOpacity(0.12)),
                                    strokeWidth: 2,
                                  ),
                                ),
                              ),
                            ),
                          ));
                    },
                  ),
                )
              : Container(
                  padding: EdgeInsets.symmetric(
                    horizontal: screenWidth(context, dividedBy: 40),
                    //vertical: screenHeight(context, dividedBy: 50),
                  ),
                  child: GridView.builder(
                    shrinkWrap: true,
                    physics: NeverScrollableScrollPhysics(),
                    itemCount: widget.taggedImages.length,
                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 3,
                      crossAxisSpacing: 5,
                      mainAxisSpacing: 5,
                    ),
                    itemBuilder: (BuildContext context, int index) {
                      return GestureDetector(
                        onTap: () {
                          setState(() {
                            currentIndex = index.toDouble();
                          });
                          push(
                            context,
                            SingleImagePage(
                              userImage: widget.imgCollection,
                              hasCaption: widget.hasCaption,
                              userCaption: widget.captions,
                              userName: widget.userName,
                              userProfile: widget.userProfile,
                              currentIndex: currentIndex,
                            ),
                          );
                        },
                        child: Container(
                          height: screenWidth(context, dividedBy: 15),
                          width: screenWidth(context, dividedBy: 15),
                          decoration: BoxDecoration(
                            border:
                                Border.all(color: Constants.kitGradients[28]),
                          ),
                          child: CachedNetworkImage(
                            height: screenWidth(context, dividedBy: 15),
                            width: screenWidth(context, dividedBy: 15),
                            imageUrl: widget.taggedImages[index],
                            fit: BoxFit.cover,
                            placeholder: (context, url) => Center(
                              heightFactor: 1,
                              widthFactor: 1,
                              child: SizedBox(
                                height: 16,
                                width: 16,
                                child: CircularProgressIndicator(
                                  valueColor: AlwaysStoppedAnimation(
                                      Constants.kitGradients[28].withOpacity(0.12)),
                                  strokeWidth: 2,
                                ),
                              ),
                            ),
                          ),
                        ),
                      );
                    },
                  ),
                ),
          SizedBox(
            height: screenHeight(context, dividedBy: 10),
          ),
        ],
      ),
    );
  }
}
