import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class AppBarDogApp extends StatefulWidget {
   Widget leftIcon;
  Function onTapLeftIcon;
   Widget rightIcon;
   Function onTapRightIcon;
  Widget title;
  AppBarDogApp({this.leftIcon, this.onTapLeftIcon, this.title, this.onTapRightIcon,this.rightIcon});
  @override
  _AppBarDogAppState createState() => _AppBarDogAppState();
}

class _AppBarDogAppState extends State<AppBarDogApp> {
  @override
  Widget build(BuildContext context) {
    return Container(
        //height: screenHeight(context, dividedBy: 1),
        width: screenWidth(context, dividedBy: 1),
        padding: EdgeInsets.symmetric(
          horizontal: screenWidth(context, dividedBy: 30),
          //vertical: screenHeight(context, dividedBy: 6),
        ),
      child:Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children:[
          GestureDetector(
            child:widget.leftIcon,
            onTap: widget.onTapLeftIcon,
          ),
          SizedBox(
            width: screenWidth(context, dividedBy: 30),
          ),
          Container(
            child: widget.title,
          ),
          // SizedBox(
          //   width: screenWidth(context, dividedBy: 30),
          // ),
          Spacer(),
          GestureDetector(
            child:widget.rightIcon,
            onTap: widget.onTapRightIcon,
          )
        ]
      )
    );
  }
}
