import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/widgets/build_button.dart';
import 'package:app_template/src/widgets/bulleted_list_items.dart';
import 'package:app_template/src/widgets/food_description_textfield.dart';
import 'package:flutter/material.dart';

class RecipeTextField extends StatefulWidget {
  final ValueChanged onRecipeAdded;
  RecipeTextField({this.onRecipeAdded});
  // final String labelText;
  // final TextEditingController textEditingController;
  // final String hintText;
  // RecipeTextField({this.textEditingController, this.labelText, this.hintText});

  @override
  _RecipeTextFieldState createState() => _RecipeTextFieldState();
}

class _RecipeTextFieldState extends State<RecipeTextField> {
  TextEditingController recipeTextEditingController =
      new TextEditingController();
  List<String> recipes = [];

  addRecipe(String recipe) {
    setState(() {
      recipes.add(recipe);
      print("========================");
      print(recipes);
      recipeTextEditingController.clear();
    });
  }

  removeRecipe(String recipe) {
    setState(() {
      recipes.remove(recipe);
      print(recipe + "==removed==");
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      //height: screenHeight(context, dividedBy: 12),
      width: screenWidth(context, dividedBy: 1),
      child: Padding(
        padding:
            EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom),
        child: Column(
          //crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              alignment: Alignment.center,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                    width: screenWidth(context, dividedBy: 1.5),
                    child: FoodDetailsTextField(
                        labelText: "Recipe",
                        textEditingController: recipeTextEditingController,
                        isContent: false,
                        onPressed: () {},
                        hintText: "preparation steps"),
                  ),
                  SizedBox(
                    width: screenWidth(context, dividedBy: 50),
                  ),
                  Padding(
                    padding: EdgeInsets.only(
                      top: screenHeight(context, dividedBy: 25),
                    ),
                    child: BuildButton(
                      buttonWidth: 4.7,
                      title: "Add",
                      isLoading: false,
                      onPressed: () {
                        print(recipes.toString());
                        if (recipeTextEditingController.text.isEmpty) {
                          showToastCommon("Enter details");
                        } else {
                          addRecipe(recipeTextEditingController.text);
                          widget.onRecipeAdded(recipes);
                        }

                        // push(context, OTPInputPage());
                      },
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: screenHeight(context, dividedBy: 40),
            ),
            recipes.length == 0
                ? Container()
                : ListView.builder(
                    shrinkWrap: true,
                    //reverse: true,
                    physics: NeverScrollableScrollPhysics(),
                    itemCount: recipes.length,
                    // gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    //   crossAxisCount: 3,
                    //   mainAxisSpacing: 5,
                    //   crossAxisSpacing: 5,
                    //   childAspectRatio: screenWidth(context, dividedBy: 3.5) /
                    //       (MediaQuery.of(context).size.height / 14),
                    // ),
                    itemBuilder: (BuildContext context, int index) {
                      return Container(
                          child: BulletedListItem(data: recipes[index]
                              // "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi",
                              ));
                      //   Container(
                      //   // height: screenHeight(context, dividedBy: 15),
                      //   // width: screenWidth(context, dividedBy: 2.5),
                      //   alignment: Alignment.center,
                      //   padding: EdgeInsets.symmetric(
                      //       horizontal: screenWidth(context, dividedBy: 30)),
                      //   decoration: BoxDecoration(
                      //     color: Constants.kitGradients[28].withOpacity(0.12),
                      //     borderRadius: BorderRadius.circular(30),
                      //   ),
                      //   child: Row(
                      //     mainAxisAlignment: MainAxisAlignment.center,
                      //     crossAxisAlignment: CrossAxisAlignment.center,
                      //     children: [
                      //       Expanded(
                      //         child: Text(
                      //           recipes[index],
                      //           overflow: TextOverflow.clip,
                      //           style: TextStyle(
                      //             color: Constants.kitGradients[28],
                      //             fontFamily: "Prompt-Light",
                      //             fontSize: 16,
                      //           ),
                      //         ),
                      //       ),
                      //       GestureDetector(
                      //         onTap: () {
                      //           removeRecipe(recipes[index]);
                      //         },
                      //         child: Icon(Icons.close,
                      //             color: Constants.kitGradients[31], size: 16),
                      //       )
                      //     ],
                      //   ),
                      // );
                    },
                  ),
          ],
        ),
      ),
    );
  }
}
