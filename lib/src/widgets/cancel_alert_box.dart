import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/widgets/alert_box_button.dart';
import 'package:flutter/material.dart';

class CancelAlertBox extends StatefulWidget {
  String title;
  String text1;
  String text2;
  final Function onPressedYes;
  final Function onPressedNo;
  double insetPadding;
  double titlePadding;
  double contentPadding;
  final String button1name;
  final String button2name;
  final bool singleButton;
  final bool isLoading;
  CancelAlertBox(
      {this.title,
      this.singleButton,
      this.isLoading,
      this.text1,
      this.onPressedNo,
      this.onPressedYes,
      this.text2,
      this.contentPadding,
      this.button1name,
      this.button2name,
      this.insetPadding,
      this.titlePadding});
  @override
  _CancelAlertBoxState createState() => _CancelAlertBoxState();
}

class _CancelAlertBoxState extends State<CancelAlertBox> {
  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      insetPadding: EdgeInsets.symmetric(
        vertical: screenHeight(context, dividedBy: widget.insetPadding),
      ),
      titlePadding: EdgeInsets.symmetric(
        horizontal: screenWidth(context, dividedBy: widget.titlePadding),
      ),
      contentPadding: EdgeInsets.symmetric(
          horizontal: screenWidth(context, dividedBy: widget.contentPadding)),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.all(
          Radius.circular(30),
        ),
      ),
      backgroundColor: Colors.transparent,
      // title: Container(
      //       height: screenHeight(context,dividedBy: 20),
      //     decoration: BoxDecoration(
      //       color: Colors.white,
      //       borderRadius: BorderRadius.vertical(top: Radius.circular(10))
      //     ),
      //     child: Column(
      //       mainAxisAlignment: MainAxisAlignment.spaceBetween,
      //       children: [
      //
      //         SizedBox(
      //           height: screenHeight(context,dividedBy: 100),
      //         ),
      //
      //         Row(
      //           crossAxisAlignment: CrossAxisAlignment.center,
      //
      //           children: [
      //
      //
      //            SizedBox(
      //              width: screenWidth(context,dividedBy: 4),
      //            ),
      //
      //             Text(widget.title),
      //           ],
      //         ),
      //       ],
      //     )),
      content: Container(
        // height: screenHeight(context,dividedBy: 10),
        width: screenWidth(context, dividedBy: 1.2),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.all(Radius.circular(10)),
        ),
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              SizedBox(
                height: screenHeight(context, dividedBy: 25),
              ),
              Container(
                width: screenWidth(context, dividedBy: 1.5),
                child: Center(
                  child: Text(
                    widget.title,
                    style: TextStyle(
                        fontFamily: "sfProSemiBold",
                        fontSize: 18,
                        fontWeight: FontWeight.w600),
                  ),
                ),
              ),
              SizedBox(
                height: screenHeight(context, dividedBy: 150),
              ),
              // Row(
              //   children: [
              //     SizedBox(
              //       width: screenWidth(context, dividedBy: 20),
              //       child: Container(
              //         color: Colors.white,
              //       ),
              //     ),
              //     // Container(
              //     //   width: screenWidth(context, dividedBy: 1.3),
              //     //   child: Text(
              //     //     // "If you have already made the deposit, you will receive it within 3 working days.  ",
              //     //     widget.text1,
              //     //     style: TextStyle(
              //     //       color: Colors.black,
              //     //       fontFamily: 'SFProText-Regular',
              //     //       fontSize: 16,
              //     //     ),
              //     //     textAlign: TextAlign.center,
              //     //   ),
              //     // ),
              //     SizedBox(
              //       width: screenWidth(context, dividedBy: 20),
              //     ),
              //   ],
              // ),
              widget.singleButton != true
                  ? Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        AlertBoxButton(
                            title: widget.button1name,
                            onPressed: widget.onPressedYes,
                            isLoading: false),
                        SizedBox(
                          width: screenWidth(context, dividedBy: 20),
                        ),
                        AlertBoxButton(
                          title: "No",
                          onPressed: widget.onPressedNo,
                          isLoading: widget.isLoading,
                        ),
                      ],
                    )
                  : Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        AlertBoxButton(
                          title: widget.button2name,
                          onPressed: widget.onPressedYes,
                          isLoading: widget.isLoading,
                        ),
                      ],
                    )
            ],
          ),
        ),
      ),
    );
  }
}
