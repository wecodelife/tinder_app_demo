import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';

class AppBarFoodApp extends StatefulWidget {
  final String title;
  final Widget rightIcon;
  final Widget leftIcon;
  final Function onPressedLeftIcon;
  final Function onPressedRightIcon;
  AppBarFoodApp({
    this.title,
    this.leftIcon,
    this.onPressedLeftIcon,
    this.onPressedRightIcon,
    this.rightIcon,
  });
  @override
  _AppBarFoodAppState createState() => _AppBarFoodAppState();
}

class _AppBarFoodAppState extends State<AppBarFoodApp> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: screenWidth(context, dividedBy: 1),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          SizedBox(
            width: screenWidth(context, dividedBy: 17),
          ),
          widget.leftIcon ==  null
              ? SizedBox(
                  width: screenWidth(context, dividedBy: 20),
                )
              : GestureDetector(
                  onTap: () {
                    widget.onPressedLeftIcon();
                  },
                  child: widget.leftIcon,
                ),

          // Spacer(
          //   flex: 2,
          //  ),
          SizedBox(
            width: screenWidth(context, dividedBy: 35),
          ),
          Container(
            //width: screenWidth(context, dividedBy: 2.3),
            child: Center(
              child: Text(
                widget.title,
                style: TextStyle(
                  color:widget.rightIcon == null ? Constants.kitGradients[28] :Constants.kitGradients[28],
                  fontWeight: FontWeight.bold,
                  fontFamily: "Prompt-Light",
                  fontSize: 21,
                ),
              ),
            ),
          ),
          Spacer(
            flex: 2,
          ),
          widget.rightIcon == null
              ? SizedBox(
                  width: screenWidth(context, dividedBy: 20),
                )
              : GestureDetector(
                  onTap: () {
                    widget.onPressedRightIcon();
                  },
                  child: Container(
                      width: screenWidth(context, dividedBy: 9),
                     height: 42,
                     // height: screenHeight(context,dividedBy: 2),
    decoration: BoxDecoration(
        border: Border.all(
            color: Constants.kitGradients[28].withOpacity(0.2)
        ),
        borderRadius: BorderRadius.circular(12),
        //shape: BoxShape.circle,
        color: Constants.kitGradients[31],
      ),
                      child: widget.rightIcon)
          ),

          SizedBox(
            width: screenWidth(context, dividedBy: 15),
          )
        ],
      ),
    );
  }
}
