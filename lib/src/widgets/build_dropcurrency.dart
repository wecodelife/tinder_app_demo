import 'package:app_template/src/models/get_food_type_model.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class DropDownFood extends StatefulWidget {
  final ValueChanged onDropDownSelected;
  final GetFoodType getFoodType;
  DropDownFood({this.onDropDownSelected,this.getFoodType});

  @override
  _DropDownFoodState createState() => _DropDownFoodState();
}

int dropdownvalue;
String foodName;

class _DropDownFoodState extends State<DropDownFood> {
  @override
  void initState() {
    super.initState();
    for(int i=0;i<widget.getFoodType.results.length;i++){
    print("abc"+widget.getFoodType.results[i].name.toString());
    print("abc"+widget.getFoodType.results[i].id.toString());}
  }
  Result food;
  @override
  Widget build(BuildContext context) {
    return DropdownButtonHideUnderline(
      child: Container(
        width: screenWidth(context, dividedBy: 2),
        height: screenHeight(context,dividedBy: 15),

        decoration: BoxDecoration(
          color: Constants.kitGradients[28].withOpacity(0.14),
          borderRadius: BorderRadius.circular(20),
        ),
        margin: EdgeInsets.only(top: 0),
        child: DropdownButton<Result>(
          elevation: 50,
          iconSize: 0.0,
          isExpanded: false,
          itemHeight: screenHeight(context, dividedBy: 3),
          value: food,
          style: TextStyle(fontSize: 9.0, color: Constants.kitGradients[28]),
          items: widget.getFoodType.results
              .map<DropdownMenuItem<Result>>(
                  (Result value) => DropdownMenuItem<Result>(
                      value:value,
                      child: Padding(
                        padding: EdgeInsets.only(
                            left: screenWidth(context, dividedBy: 20)),
                        child: Text(
                          value.name,
                          style: TextStyle(
                              fontSize: 16,
                              color: Constants.kitGradients[28],
                              fontWeight: FontWeight.bold,
                              fontFamily: "Montserrat"),
                        ),
                      )))
              .toList(),
          onChanged: (value) {
            setState(() {
              dropdownvalue = value.id;
              print(value.name+"abc");
              print(value.id.toString()+"abc");
              food = value;
            });
            widget.onDropDownSelected(dropdownvalue);
          },
          hint: Padding(
            padding: EdgeInsets.only(left: screenWidth(context, dividedBy: 30)),
            child: Text(
              "Select Item Type",
              style: TextStyle(
                color: Constants.kitGradients[28],
                fontSize: 16,
              ),
              textAlign: TextAlign.end,
            ),
          ),
        ),
      ),
    );
  }
}
