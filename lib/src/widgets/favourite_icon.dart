import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class FavouriteIcon extends StatefulWidget {
  final Function onPressed;
  final IconData icon;
  final double rightPadding;
  final bool deleteSaved;
  final IconData icon2;
  FavouriteIcon({
    this.onPressed,
    this.icon,
    this.rightPadding,
    this.deleteSaved,
    this.icon2,
  });
  @override
  _FavouriteIconState createState() => _FavouriteIconState();
}

class _FavouriteIconState extends State<FavouriteIcon> {
  bool isFavourite = true;
  @override
  Widget build(BuildContext context) {
    return Positioned(
      bottom: widget.deleteSaved != true ? 0 : null,
      top: widget.deleteSaved == true ? 0 : null,
      right: screenWidth(context, dividedBy: widget.rightPadding),
      child: GestureDetector(
        onTap: () {
          widget.onPressed();
          isFavourite = !isFavourite;
        },
        child: Container(
            // height: screenHeight(context, dividedBy: 21),
            width: screenWidth(context, dividedBy: 10),
            alignment: Alignment.center,
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              color: Constants.kitGradients[1].withOpacity(0.5),
            ),
            child: widget.deleteSaved != true
                ? IconButton(
                    alignment: Alignment.center,
                    icon: Icon(
                      widget.icon,
                      color: Constants.kitGradients[27],
                    ),
                    iconSize: screenWidth(context, dividedBy: 18),
                  )
                : IconButton(
                    alignment: Alignment.center,
                    icon: Icon(
                      isFavourite == true ? widget.icon : widget.icon2,
                      color: Constants.kitGradients[27],
                    ),
                    iconSize: screenWidth(context, dividedBy: 18),
                  )),
      ),
    );
  }
}
