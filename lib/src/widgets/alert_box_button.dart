import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class AlertBoxButton extends StatefulWidget {
  final Function onPressed;
  final String title;
  final bool isLoading;
  AlertBoxButton({this.title, this.onPressed, this.isLoading});
  @override
  _AlertBoxButtonState createState() => _AlertBoxButtonState();
}

class _AlertBoxButtonState extends State<AlertBoxButton> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(10.0),
      child: Center(
        child: RaisedButton(
          onPressed: () {
            widget.onPressed();
          },
          elevation: 0,
          padding: EdgeInsets.zero,
          color: Colors.transparent,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10),
          ),
          child: Container(
            width: screenWidth(context, dividedBy: 4),
            height: screenHeight(context, dividedBy: 18),
            decoration: BoxDecoration(
              color: Constants.kitGradients[28],
              borderRadius: BorderRadius.circular(10),
            ),
            child: widget.isLoading != true
                ? Center(
                    child: Text(
                    widget.title,
                    style: TextStyle(
                        fontSize: 18,
                        color: Colors.white,
                        fontWeight: FontWeight.w800,
                        fontFamily: "SofiaProRegular"),
                    textAlign: TextAlign.center,
                  ))
                : Container(
                    height: screenHeight(context, dividedBy: 1.3),
                    width: screenWidth(context, dividedBy: 1),
                    child: Center(
                      child: CircularProgressIndicator(
                        valueColor: new AlwaysStoppedAnimation<Color>(
                            Constants.kitGradients[2]),
                      ),
                    ),
                  ),
          ),
        ),
      ),
    );
  }
}
