import 'package:app_template/src/utils/constants.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:photofilters/filters/filters.dart';
import 'package:image/image.dart' as img;

bool isLoading = false;

class FilterUtils {
  static final Map<String, List<int>> _cacheFilter = {};

  static void clearCache() => _cacheFilter.clear();

  static void saveCachedFilter(Filter filter, List<int> imageBytes) {
    if (filter == null) return;
    print("one");
    _cacheFilter[filter.name] = imageBytes;
  }

  static List<int> getCachedFilter(Filter filter) {
    isLoading = true;
    if (filter == null) return null;
    print("two");
    return _cacheFilter[filter.name];
  }

  static Future<List<int>> applyFilter(img.Image image, Filter filter) {
    isLoading = true;
    if (filter == null) throw Exception('Filter not set');
    print("three");
    return compute(
      _applyFilterInternal,
      <String, dynamic>{
        'filter': filter,
        'image': image,
        'width': image.width,
        'height': image.height,
      },
    );
  }

  static List<int> _applyFilterInternal(Map<String, dynamic> params) {
    print("four");
    isLoading = false;
    Filter filter = params["filter"];
    img.Image image = params["image"];
    int width = params["width"];
    int height = params["height"];
    final bytes = image.getBytes();
    filter.apply(bytes, width, height);
    final newImage = img.Image.fromBytes(width, height, bytes);
    return img.encodeJpg(newImage);
  }
}
