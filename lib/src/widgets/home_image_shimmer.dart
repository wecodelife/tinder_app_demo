import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:shimmer/shimmer.dart';

class HomeImageShimmer extends StatefulWidget {
  @override
  _HomeImageShimmerState createState() => _HomeImageShimmerState();
}

class _HomeImageShimmerState extends State<HomeImageShimmer> {
  @override
  Widget build(BuildContext context) {
    return Shimmer.fromColors(
      baseColor: Constants.kitGradients[0],
      highlightColor: Constants.kitGradients[27].withOpacity(0.3),
      enabled: true,
      child: Center(
        child: Container(
          height: screenHeight(context, dividedBy: 1.2),
          width: screenWidth(context, dividedBy: 1.1),
          margin: EdgeInsets.symmetric(
              horizontal: screenWidth(context, dividedBy: 30),
              vertical: screenHeight(context, dividedBy: 80)),
          decoration: BoxDecoration(
            //border: Border.all(color: Constants.kitGradients[28]),
            borderRadius: BorderRadius.circular(7),
            color: Constants.kitGradients[23],
          ),
          alignment: Alignment.center,
        ),
      ),
    );
  }
}
