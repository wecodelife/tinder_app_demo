import 'dart:ui';

import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';

class FormFieldUserEmail extends StatefulWidget {
  final String labelText;
  final ValueChanged onValueChanged;
  final TextEditingController textEditingController;

  FormFieldUserEmail({
    this.labelText,
    this.textEditingController,
    this.onValueChanged,
  });
  @override
  _FormFieldUserEmailState createState() => _FormFieldUserEmailState();
}

class _FormFieldUserEmailState extends State<FormFieldUserEmail> {
  Pattern pattern =
      r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
  RegExp regex = new RegExp(
      r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$');
  validateEmail(String value) {
    if (value.isEmpty) {
      return 'Please enter an email id';
    }
    if (!(regex.hasMatch(value))) {
      return "Invalid Email";
    }
    return null;
  }

  int flag = 0;

  @override
  Widget build(BuildContext context) {
    return Container(
          height: screenHeight(context, dividedBy: 16),
         // width: screenWidth(context, dividedBy: 1),
          //color: Constants.kitGradients[0].withOpacity(0.1),
          child: TextField(
            controller: widget.textEditingController,
            cursorColor: Constants.kitGradients[28],
            obscureText: false,
            style: TextStyle(
              color: Constants.kitGradients[28],
              fontFamily: "Prompt-Light",
              fontSize: 15,
            ),
            decoration: InputDecoration(
              //errorText: widget.errorText,
              labelText: widget.labelText,
              labelStyle: TextStyle(
                color: Constants.kitGradients[28],
                fontFamily: "Prompt-Light",
                fontSize: 16,
              ),
              // filled: true,
              // fillColor:Constants.kitGradients[28].withOpacity(0.1),
              focusedBorder: OutlineInputBorder(
                borderSide:
                BorderSide(color: Constants.kitGradients[28], width: 0.0),
                borderRadius: BorderRadius.circular(8.0),
              ),
              enabledBorder: OutlineInputBorder(
                borderSide:
                BorderSide(color: Colors.grey[400], width: 0.0),
                borderRadius: BorderRadius.circular(8.0),
              ),
            ),
          ),
      //   ),
      // )
    );
  }
}

//
// ClipRRect(
// borderRadius: BorderRadius.circular(30.0),
// child:BackdropFilter(
// filter: ImageFilter.blur(sigmaX: 10, sigmaY: 10),
// child: