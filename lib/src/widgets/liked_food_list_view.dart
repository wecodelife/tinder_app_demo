import 'package:app_template/src/bloc/user_bloc.dart';
import 'package:app_template/src/models/update_like_request_model.dart';
import 'package:app_template/src/models/update_mustvisit_request_model.dart';
import 'package:app_template/src/screens/food_information_page.dart';
import 'package:app_template/src/screens/liked_item_screen.dart';
import 'package:app_template/src/screens/must_vist_screen.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/object_factory.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/widgets/favourites_box.dart';
import 'package:app_template/src/widgets/information_icon.dart';
import 'package:app_template/src/widgets/like_mustvisit_icon.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

class LikedFoodListView extends StatefulWidget {
  final String likedFoodImageUrl;
  final int index;
  final navigationPage;
  final String foodName;
  final String shopName;
  final int foodType;
  final double rating;
  final String price;
  final Function onTapFavourite;
  final Function onTap;
  final int foodId;
  final bool isLikedList;
  final bool isMustVisit;

  LikedFoodListView(
      {this.index,
      this.likedFoodImageUrl,
      this.navigationPage,
      this.foodType,
      this.price,
      this.onTap,
      this.foodName,
      this.rating,
      this.shopName,
      this.foodId,
      this.isLikedList,
      this.onTapFavourite,
      this.isMustVisit});
  @override
  _LikedFoodListViewState createState() => _LikedFoodListViewState();
}

class _LikedFoodListViewState extends State<LikedFoodListView> {
  bool likedBool = true;
  bool mustVisitBool = true;
  List<bool> likeBool = [];
  List<bool> mustBool = [];

  likeOrDislike() async {
    if (likedBool == true) {
      setState(() {
        likedBool = false;
      });
      userBloc.updateLike(
          updateLikeRequestModel: UpdateLikeRequestModel(
        food: widget.foodId,
        //user: int.parse(ObjectFactory().appHive.getUserId()),
        isLiked: false,
      ));
    } else {
      setState(() {
        likedBool = true;
      });
      await userBloc.updateLike(
          updateLikeRequestModel: UpdateLikeRequestModel(
       // food: widget.foodId,
      //  user: int.parse(ObjectFactory().appHive.getUserId()),
        isLiked: true,
      ));
    }
  }

  mustVisitOrRemoveMustVisit() async {

    if (mustVisitBool == true) {
      setState(() {
        mustVisitBool = false;
      });
      userBloc.updateMustVisit(
          updateMustVisitRequestModel: UpdateMustVisitRequestModel(
       food: widget.foodId,
        //     user: int.parse(ObjectFactory().appHive.getUserId()),
        // isFavourite: false,
            mustVisit: false,

      ));
    } else {
      setState(() {
        mustVisitBool = true;
      });
      userBloc.updateMustVisit(
          updateMustVisitRequestModel: UpdateMustVisitRequestModel(
            food: widget.foodId,
            //     user: int.parse(ObjectFactory().appHive.getUserId()),
            // isFavourite: false,
            mustVisit: true,

      ));
     // pushAndRemoveUntil(context, LikedItemsScreen(), false);
    }
  }

  UserBloc userBloc = new UserBloc();
  @override
  void initState() {
    userBloc.likedFood.listen((event) {
      // for(int i=0;i<event.results.length;i++){
      //   likeBool.add(event.results[i].isLiked);
      // }
      // for(int i=0;i<event.results.length;i++){
      //   mustBool.add(event.results[i].mustVisit);
      // }
    });
    userBloc.updateLikeResponse.listen((event) {
      setState(() {

      });
      if (event.status == 200) {
        userBloc.getLikedFood(id: "?is_liked=true",
            uid: ObjectFactory().appHive.getUserId()
        );
        showToastCommon("Liked Item Updated ");
      } else {
        showToastCommon(event.message);
      }

    });

    userBloc.updateMustVisitResponse.listen((event) {
      print("must visited item deleted////");
     setState(() {
        });
      if (event.status == 200) {
        userBloc.getLikedFood(id:"?must_visit=true",
            uid: ObjectFactory().appHive.getUserId()
        );
        showToastCommon("Must Visit Screen Updated ");
      } else {
        showToastCommon(event.message);
      }
    });
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        ClipRRect(
          borderRadius: BorderRadius.circular(8),
          child: GestureDetector(
            onTap: () {
              Navigator.pushReplacement(
                context,
                MaterialPageRoute(
                    builder: (context) =>
                        FoodInformationPage(
                          foodId: widget.foodId,
                          count: true,
                          pageViewImage: [widget.likedFoodImageUrl],
                          navigationWidget:
                         widget.navigationPage,
                          mustLike:  widget.isMustVisit==true ?
                          true:false,
                        )),
              );
            },
            child: CachedNetworkImage(
              width: screenWidth(context, dividedBy: 1),
              height: screenHeight(context, dividedBy: 3),
              imageUrl: widget.likedFoodImageUrl == null
                  ? "https://images.unsplash.com/photo-1476224203421-9ac39bcb3327?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MTJ8fGZvb2R8ZW58MHx8MHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=600&q=60"
                  : widget.likedFoodImageUrl,
              placeholder: (context, url) => Center(
                heightFactor: 1,
                widthFactor: 1,
                child: Shimmer.fromColors(
                  baseColor: Constants.kitGradients[31],
                  highlightColor: Colors.grey[100],
                  enabled: true,
                  child: Container(
                    height: screenHeight(context, dividedBy: 3),
                    width: screenWidth(context, dividedBy:1),
                    padding: EdgeInsets.symmetric(
                        horizontal: screenWidth(context, dividedBy: 30),
                        vertical: screenHeight(context, dividedBy: 80)),
                    decoration: BoxDecoration(
                      color: Constants.kitGradients[31],
                      border: Border.all(color: Constants.kitGradients[31]),
                      borderRadius: BorderRadius.all(Radius.circular(15)),
                    ),
                    alignment: Alignment.center,
                  ),
                )
              ),
              imageBuilder: (context, imageProvider) {
                return Container(
                  width: screenWidth(context, dividedBy: 1),
                  height: screenHeight(context, dividedBy: 3),
                  padding: EdgeInsets.symmetric(
                      horizontal: screenWidth(context, dividedBy: 60),
                      vertical: screenHeight(context, dividedBy: 60)),
                  decoration: BoxDecoration(
                    color: Constants.kitGradients[29],
                    borderRadius: BorderRadius.circular(8),
                    border: Border.all(color: Constants.kitGradients[27]),
                    image: DecorationImage(
                        image: imageProvider, fit: BoxFit.cover),
                    boxShadow: [
                      BoxShadow(
                          blurRadius: 0.5,
                          spreadRadius: 0.5,
                          offset: Offset(1.0, 1.0),
                          color: Constants.kitGradients[19])
                    ],
                  ),
                  child: Stack(
                    children: [
                      LikeMustVisitIcon(
                        deleteLiked: true,
                        buttonHeight: screenHeight(context, dividedBy: 20),
                        buttonWidth: screenWidth(context, dividedBy: 9),
                        hero: "aa",
                        icon: widget.isMustVisit == true ?
                        likedBool == true
                            ? Icons.favorite
                            : Icons.favorite_outline:
                        mustVisitBool == true
                            ? Icons.star
                            : Icons.star_border,

                        onPressed: () async {
                          if (widget.isLikedList == true) {
                            likeOrDislike();
                            pushAndRemoveUntil(context,LikedItemsScreen(),false);
                          } else {
                            print("Saved");
                            mustVisitOrRemoveMustVisit();
                            pushAndRemoveUntil(context,MustVisitItemScreen(),false);

                          }
                        },

                        rightPadding: 50,
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          FavouritesBox(
                            foodName: widget.foodName ,
                            price: widget.price,
                            foodType: widget.foodType == 1
                                ? "Veg"
                                : "Non-Veg",
                            hotelName: widget.shopName,
                            rating: widget.rating,
                          ),
                          // SizedBox(
                          //   height: screenHeight(context,
                          //       dividedBy: 60),
                          // ),
                        ],
                      ),
                    ],
                  ),
                );
              },
            ),
          ),
        ),
        SizedBox(
          height: screenHeight(context, dividedBy: 60),
        ),
      ],
    );

  }
}
