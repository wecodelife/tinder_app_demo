import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class RatingNumberWidget extends StatefulWidget {
  final String rating;
  RatingNumberWidget({this.rating});
  @override
  _RatingNumberWidgetState createState() => _RatingNumberWidgetState();
}

class _RatingNumberWidgetState extends State<RatingNumberWidget> {
  final ButtonStyle style = ElevatedButton.styleFrom(
    textStyle: const TextStyle(fontSize: 20),
    onPrimary: Colors.grey,
    primary: Constants.kitGradients[4],
    padding: EdgeInsets.zero,
  );
  @override
  Widget build(BuildContext context) {
    return Container(
        width: screenWidth(context, dividedBy: 12),
        height: screenWidth(context, dividedBy: 12),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(8.0),
          color: Constants.kitGradients[28].withOpacity(0.1),
        ),
        alignment: Alignment.center,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              widget.rating,
              style: TextStyle(
                fontSize: 12,
                fontFamily: "Prompt-Light",
                color: Constants.kitGradients[28],
              ),
            ),
            // SizedBox(
            //   width: screenWidth(context, dividedBy: 200),
            // ),
            Icon(
              Icons.star,
              size: 14,
              color: Constants.kitGradients[28],
            )
          ],
        ));
  }
}

// Container(
// width: screenWidth(context, dividedBy: 6),
// child: ElevatedButton(
// style: style,
// onPressed: () {},
// child: Row(
// mainAxisAlignment: MainAxisAlignment.center,
// children: [
// Text(
// widget.rating,
// style: TextStyle(
// fontSize: 15,
// fontFamily: "Prompt-Light",
// color: Constants.kitGradients[0],
// ),
// ),
// SizedBox(
// width:screenWidth(context, dividedBy: 90),
// ),
// Icon(
// Icons.star,
// size: 20,
// color: Constants.kitGradients[28],
// )
// ],
// )),
// );
