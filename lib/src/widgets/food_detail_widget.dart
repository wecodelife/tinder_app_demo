import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:maps_launcher/maps_launcher.dart';

class FoodLocationWidget extends StatefulWidget {
  final double latitude;
  final double longitude;
  FoodLocationWidget({this.longitude, this.latitude});
  @override
  _FoodLocationWidgetState createState() => _FoodLocationWidgetState();
}

class _FoodLocationWidgetState extends State<FoodLocationWidget> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        MapsLauncher.launchCoordinates(widget.latitude, widget.longitude);
      },
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Icon(
            Icons.location_on_outlined,
            color: Constants.kitGradients[28],
            size: 25,
          ),
          SizedBox(
            width: screenWidth(context, dividedBy: 70),
          ),
          Container(
            width: screenWidth(context, dividedBy: 8),
            child: FittedBox(
              child: Text(
                "Location",
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 16,
                  fontFamily: 'OpenSansLight',
                  color: Constants.kitGradients[28],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
