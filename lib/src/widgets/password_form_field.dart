import 'dart:ui';

import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';

class FormFieldUserPassword extends StatefulWidget {
  final String labelText;
  final ValueChanged onValueChanged;
  final TextEditingController textEditingController;

  FormFieldUserPassword({
    this.labelText,
    this.textEditingController,
    this.onValueChanged,
  });
  @override
  _FormFieldUserPasswordState createState() => _FormFieldUserPasswordState();
}

class _FormFieldUserPasswordState extends State<FormFieldUserPassword> {
  validatePassword(String value) {
    if (value.isEmpty) {
      return "Please Enter a password";
    } else if (value.length < 5) {
      return "Please Enter a password with Minimum 5 letters";
    } else if (value.length > 10) {
      return "Please Enter a password with Maximun 10 letters";
    } else {
      return null;
    }
  }

  int flag = 0;
  @override
  Widget build(BuildContext context) {
    return Container(
    height: screenHeight(context, dividedBy: 16),
    // width: screenWidth(context, dividedBy: 1),
    //color: Constants.kitGradients[0].withOpacity(0.1),
    child: TextField(
        controller: widget.textEditingController,
        cursorColor: Constants.kitGradients[28],
        obscureText: true,
        obscuringCharacter: '*',
        style: TextStyle(
          color: Constants.kitGradients[28],
          fontFamily: "Prompt-Light",
          fontSize: 15,
        ),
        decoration: InputDecoration(
          //errorText: widget.errorText,
          labelText: widget.labelText,
          labelStyle: TextStyle(
            color: Constants.kitGradients[28],
            fontFamily: "Prompt-Light",
            fontSize: 15,
          ),
          // filled: true,
          // fillColor: Constants.kitGradients[0].withOpacity(0.1),
          focusedBorder: OutlineInputBorder(
            borderSide:
            BorderSide(color: Constants.kitGradients[28], width: 0.0),
            borderRadius: BorderRadius.circular(8.0),
          ),
          enabledBorder: OutlineInputBorder(
            borderSide:
            BorderSide(color: Colors.grey[400], width: 0.0),
            borderRadius: BorderRadius.circular(8.0),
          ),
        ),
      ),
    );
  }
}
