import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';

class FoodDetailsTextField extends StatefulWidget {
  //const FoodDescriptionTextField({Key? key}) : super(key: key);
  final String labelText;
  final bool isContent;
  final TextEditingController textEditingController;
  final String hintText;
  final Widget suffixIcon;
  final Function onTapIcon;
  final Function onPressed;
  final Function onChanged;
  final bool readOnly;
  FoodDetailsTextField(
      {this.labelText,
        this.textEditingController,
        this.isContent,
        this.hintText,
        this.suffixIcon,
        this.readOnly,
        this.onChanged,
        this.onTapIcon,
        this.onPressed});

  @override
  _FoodDetailsTextFieldState createState() => _FoodDetailsTextFieldState();
}

class _FoodDetailsTextFieldState extends State<FoodDetailsTextField> {
  bool empty = false;
  validateUserData(String value) {
    if (value.isEmpty) {
      return "Please Enter Data";
    } else {
      return null;
    }
  }
  @override
  Widget build(BuildContext context) {
    return Container(
      //height: screenHeight(context, dividedBy: 12),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              SizedBox(width: screenWidth(context, dividedBy: 90)),
              Text(
                widget.labelText + " :",
                style: TextStyle(
                  fontSize: 15,
                  fontWeight: FontWeight.w600,
                  color: Constants.kitGradients[28],
                  fontFamily: "Prompt-Light",
                ),
              ),
            ],
          ),
          SizedBox(
            height: screenHeight(context, dividedBy: 90),
          ),
          Container(
            // height: screenHeight(context, dividedBy: 15),
            child: TextField(
              onTap: () {
                widget.onPressed();
              },
              readOnly: widget.readOnly != null ? widget.readOnly : false,
              controller: widget.textEditingController,
              cursorColor: Constants.kitGradients[19],
              keyboardType: widget.isContent
                  ? TextInputType.multiline
                  : TextInputType.text,
              maxLines: widget.isContent ? 6 : null,
              obscureText: false,
              style: TextStyle(
                fontSize: 15,
                fontWeight: FontWeight.w400,
                color: Constants.kitGradients[28],
                fontFamily: "Prompt-Light",
              ),
              //readOnly: widget.readOnly,
              onChanged: widget.onChanged,
              decoration: InputDecoration(
                //errorText: widget.errorText,
                suffixIcon: widget.suffixIcon == null
                    ? Container(width: screenWidth(context, dividedBy: 90)):GestureDetector(
                    onTap: widget.onTapIcon, child: widget.suffixIcon),

                contentPadding: EdgeInsets.symmetric(
                    horizontal: screenWidth(context, dividedBy: 50),
                    vertical: screenHeight(context, dividedBy: 120)),
                hintText: widget.hintText,
                hintStyle: TextStyle(
                  fontSize: 15,
                  fontWeight: FontWeight.w400,
                  color: Constants.kitGradients[19],
                  fontFamily: "Prompt-Light",
                ),
                fillColor: Colors.white,
                focusedBorder: OutlineInputBorder(
                  borderSide:
                  BorderSide(color: Constants.kitGradients[28], width: 0.0),
                  borderRadius: BorderRadius.circular(8.0),
                ),
                enabledBorder: OutlineInputBorder(
                  borderSide:
                  BorderSide(color: Constants.kitGradients[28], width: 0.0),
                  borderRadius: BorderRadius.circular(8.0),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
