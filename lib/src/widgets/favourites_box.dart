import 'dart:ui';

import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/widgets/price_rating_bar.dart';
import 'package:app_template/src/widgets/rating_bar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class FavouritesBox extends StatefulWidget {
  final String foodName;
  final String hotelName;
  final String foodType;
  final double rating;
  final String price;
  final int id;
  final Function onPressedFavourite;
  final Function onPressed;
  final double foodNameSize;
  final double shopNameSize;
  final double ratingSize;
  final double priceSize;
  FavouritesBox(
      {this.rating,
      this.price,
      this.onPressedFavourite,
      this.onPressed,
      this.foodType,
      this.hotelName,
      this.foodName,
      this.foodNameSize,
      this.priceSize,
      this.ratingSize,
      this.shopNameSize,
      this.id});

  @override
  _FavouritesBoxState createState() => _FavouritesBoxState();
}

class _FavouritesBoxState extends State<FavouritesBox> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: ClipRRect(
        borderRadius: BorderRadius.circular(8.0),
        child: Container(
          width: screenWidth(context, dividedBy: 1),
          child: BackdropFilter(
            filter: ImageFilter.blur(sigmaX: 10.0, sigmaY: 10.0),
            child: Padding(
              padding: EdgeInsets.symmetric(
                horizontal: screenWidth(context, dividedBy: 60),
                vertical: screenHeight(context, dividedBy: 60),
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Row(
                    children: [
                      Text(
                        widget.foodName ==  null ? "Not Available" : widget.foodName,
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 18.0,
                          fontWeight: FontWeight.bold,
                        ),
                        softWrap: true,
                      ),
                      SizedBox(width: 5.0),
                      Icon(
                        Icons.info,
                        size: 17,
                        color: Colors.white60,
                      ),
                    ],
                  ),
                  Container(
                    width: screenWidth(context, dividedBy: 1),
                    height: screenHeight(context, dividedBy: 30),
                    child: Row(
                      children: <Widget>[
                        Text(
                          widget.hotelName == null ? "Not Available":widget.hotelName,
                          style: TextStyle(
                            color: Colors.white60,
                          ),
                          softWrap: true,
                          overflow: TextOverflow.ellipsis,
                        ),
                        SizedBox(width: 5.0),
                        _filledCircle,
                        SizedBox(width: 5.0),
                        Flexible(
                          child: Container(
                            height: screenHeight(context, dividedBy: 60),
                            child: Text(
                              widget.foodType,
                              style: TextStyle(
                                fontSize: 12.0,
                                color: Colors.white60,
                              ),
                              overflow: TextOverflow.ellipsis,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Row(
                    mainAxisSize: MainAxisSize.max,
                    children: <Widget>[
                      RatingBar(
                        rating: widget.rating,
                        color: Colors.white,
                        size: 16,
                      ),
                       SizedBox(width: 5.0),
                      _filledCircle,
                      SizedBox(width: 5.0),
                      PriceRatingBar(
                        price: widget.price,
                        size: 14,
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  final _filledCircle = Container(
    height: 4.0,
    width: 4.0,
    decoration: BoxDecoration(
      shape: BoxShape.circle,
      color: Colors.white60,
    ),
  );
}
