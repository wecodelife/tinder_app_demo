import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';

class FormFeildUserDetails extends StatefulWidget {
  final String labelText;
  final ValueChanged onValueChanged;
  final TextEditingController textEditingController;
  final bool isNumber;
  // final String errorText;

  FormFeildUserDetails({
    this.labelText,
    this.textEditingController,
    this.onValueChanged,
    this.isNumber = false,
    //this.errorText,
  });
  @override
  _FormFeildUserDetailsState createState() => _FormFeildUserDetailsState();
}

class _FormFeildUserDetailsState extends State<FormFeildUserDetails> {
  bool empty = false;
  validateUserData(String value) {
    if (value.isEmpty) {
      return "Please Enter Data";
    } else {
      return null;
    }
  }

  int flag = 0;
//return "Please Enter Data";
  @override
  Widget build(BuildContext context) {
    return Container(
     height: screenHeight(context, dividedBy: 16),
      child: 
          TextField(
            controller: widget.textEditingController,
            cursorColor: Constants.kitGradients[28],
            keyboardType: widget.isNumber == true ? TextInputType.number: TextInputType.text,
            obscureText: false,
            style: TextStyle(
              color: Constants.kitGradients[28],
              fontFamily: "Prompt-Light",
              fontSize: 16,
            ),
            decoration: InputDecoration(
              //errorText: widget.errorText,
              labelText: widget.labelText,
              labelStyle: TextStyle(
                color: Constants.kitGradients[28],
                fontFamily: "Prompt-Light",
                fontSize: 16,
              ),
              fillColor: Colors.white,
              focusedBorder: OutlineInputBorder(
                borderSide:
                BorderSide(color: Constants.kitGradients[28], width: 0.0),
                borderRadius: BorderRadius.circular(8.0),
              ),
              enabledBorder: OutlineInputBorder(
                borderSide:
                BorderSide(color: Colors.grey[400], width: 0.0),
                borderRadius: BorderRadius.circular(8.0),
              ),
            ),
          ),

    );
  }
}
