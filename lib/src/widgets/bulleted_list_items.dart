import 'package:flutter/material.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';

class BulletedListItem extends StatefulWidget {
  // const BulletedListItem({Key? key}) : super(key: key);
  final String data;
  BulletedListItem({this.data});

  @override
  _BulletedListItemState createState() => _BulletedListItemState();
}

class _BulletedListItemState extends State<BulletedListItem> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: screenWidth(context, dividedBy: 1),
      padding: EdgeInsets.symmetric(
        //horizontal: screenWidth(context, dividedBy: 20),
        vertical: screenHeight(context, dividedBy: 60),
      ),
      child: Column(
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 8.0),
                child: Container(
                    width: 5,
                    height: 5,
                    decoration: BoxDecoration(
                        color: Constants.kitGradients[28],
                        shape: BoxShape.circle)),
              ),
              SizedBox(width: screenWidth(context, dividedBy: 50)),
              Expanded(
                child: Text(
                  widget.data,
                  textAlign: TextAlign.justify,
                  style: TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.w400,
                    color: Constants.kitGradients[30],
                    fontFamily: "Prompt-Light",
                  ),
                ),
              ),
              SizedBox(width: screenWidth(context, dividedBy: 60)),
            ],
          ),
        ],
      ),
    );
  }
}
