import 'package:app_template/src/widgets/filter_utilities.dart';
import 'package:flutter/material.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:image_picker/image_picker.dart';
import 'package:image/image.dart' as img;
import 'package:photofilters/filters/filters.dart';
import 'dart:io';

class FilteredImageWidget extends StatelessWidget {
  //const FilteredImageWidget({Key? key}) : super(key: key);
  final Filter filter;
  final img.Image image;
  final Widget Function(List<int> imageBytes) successBuilder;
  final Widget Function() errorBuilder;
  final Widget Function() loadingBuilder;
  FilteredImageWidget(
      {this.image,
      this.filter,
      this.errorBuilder,
      this.successBuilder,
      this.loadingBuilder});

  Widget buildFilter(List<int> imageBytes) => successBuilder(imageBytes);

  Widget buildError() => errorBuilder();

  @override
  Widget build(BuildContext context) {
    final cachedImageBytes = FilterUtils.getCachedFilter(filter);
    if (cachedImageBytes == null) {
      return FutureBuilder<List<int>>(
        future: FilterUtils.applyFilter(image, filter),
        builder: (context, snapshot) {
          switch (snapshot.connectionState) {
            case ConnectionState.waiting:
              return loadingBuilder();
            case ConnectionState.active:
              return Container(
                  width: screenWidth(context, dividedBy: 2),
                  height: screenHeight(context, dividedBy: 2),
                  color: Constants.kitGradients[27],
                  child: Center(child: CircularProgressIndicator()));
            default:
              if (snapshot.hasError) {
                return buildError();
              } else {
                FilterUtils.saveCachedFilter(filter, snapshot.data);
                return buildFilter(snapshot.data);
              }
          }
        },
      );
    } else {
      return buildFilter(cachedImageBytes);
    }
  }
}
