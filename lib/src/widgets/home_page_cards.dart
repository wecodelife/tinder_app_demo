import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/widgets/comment_box.dart';
import 'package:app_template/src/widgets/comment_tile.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

class HomePageCard extends StatefulWidget {
  final String profilePic;
  final String feedImage;
  final String name;
  final String location;
  final String caption;
  final String likeCount;
  final String postDate;
  final String comment;
  final String commentBy;
  HomePageCard(
      {this.name,
      this.profilePic,
      this.feedImage,
      this.location,
      this.likeCount,
      this.postDate,
      this.comment,
      this.commentBy,
      this.caption});
  @override
  _HomePageCardState createState() => _HomePageCardState();
}

class _HomePageCardState extends State<HomePageCard> {
  int tagcount = 1;
  int favcount = 1;
  int count = 1;
  int comment = 1;
  TextEditingController commentTextEditingController =
      new TextEditingController();
  @override
  Widget build(BuildContext context) {
    print(widget.caption.length.toString());
    return Container(
      width: screenWidth(context, dividedBy: 1),
      // height: count == 2 && comment == 2
      //     ? screenHeight(context, dividedBy: 1.05)
      //     : count == 2
      //         ? screenHeight(context, dividedBy: 1.05)
      //         : comment == 2
      //             ? screenHeight(context, dividedBy: 1.05)
      //             : screenHeight(context, dividedBy: 1.27),
      padding: EdgeInsets.symmetric(
        horizontal: screenWidth(context, dividedBy: 60),
      ),
      // margin:
      //     EdgeInsets.symmetric(vertical: screenHeight(context, dividedBy: 50)),
      decoration: BoxDecoration(
        // color:Colors.yellow,
        color: Colors.transparent,
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            width: screenWidth(context, dividedBy: 1),
            //color: Colors.red,
            child: Row(
              //mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  width: screenWidth(context, dividedBy: 8),
                  height: screenWidth(context, dividedBy: 8),
                  //color: Colors.red,
                  child: CircleAvatar(
                    backgroundColor: Constants.kitGradients[28].withOpacity(0.2),
                    radius: screenWidth(context, dividedBy: 1),
                    child: CachedNetworkImage(
                      imageUrl: widget.profilePic,
                      imageBuilder: (context, imageProvider) => Container(
                        width: screenWidth(context, dividedBy: 8),
                        height: screenWidth(context, dividedBy: 8),
                        decoration: BoxDecoration(
                          //color: Colors.amber,
                          shape: BoxShape.circle,
                          image: DecorationImage(
                              image: imageProvider, fit: BoxFit.cover),
                        ),
                      ),
                      placeholder: (context, url) => Center(
                        heightFactor: 1,
                        widthFactor: 1,
                        child: SizedBox(
                          height: 16,
                          width: 16,
                          child: CircularProgressIndicator(
                            valueColor: AlwaysStoppedAnimation(
                                Constants.kitGradients[19]),
                            strokeWidth: 2,
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  width: screenWidth(context, dividedBy: 30),
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      widget.name,
                      style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.w600,
                        fontFamily: 'Montserrat',
                        color: Constants.kitGradients[28],
                      ),
                    ),
                    SizedBox(height: screenHeight(context, dividedBy: 150)),
                    Text(
                      widget.location,
                      style: TextStyle(
                        fontSize: 12,
                        fontWeight: FontWeight.w600,
                        fontFamily: 'Montserrat',
                        color: Constants.kitGradients[28],
                      ),
                    )
                  ],
                ),
                Spacer(),
                GestureDetector(
                    child: Icon(
                  Icons.more_vert,
                  color: Constants.kitGradients[28],
                )),
              ],
            ),
          ),

          //SizedBox(height: screenHeight(context, dividedBy: 50)),
          // count == 2
          //     ? Spacer()
          //     : SizedBox(),
          SizedBox(height: screenHeight(context, dividedBy: 100)),
          Row(mainAxisAlignment: MainAxisAlignment.end, children: [
            Text(
              "Posted on : " + widget.postDate,
              style: TextStyle(
                fontSize: 12,
                fontWeight: FontWeight.w400,
                fontFamily: 'Montserrat',
                color: Constants.kitGradients[28],
              ),
            ),
          ]),
          // count == 2
          //     ? Spacer()
          //     : SizedBox(height:3),
          SizedBox(height: screenHeight(context, dividedBy: 70)),

          GestureDetector(
            onDoubleTap: () {
              favcount == 1
                  ? setState(() {
                      favcount = 2;
                    })
                  : setState(() {
                      favcount = 1;
                    });
            },
            child: Container(
              width: screenWidth(context, dividedBy: 1),
              height: widget.caption == null
                  ? screenHeight(context, dividedBy: 2.7)
                  : screenHeight(context, dividedBy: 3),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(8),
              ),
              child: ClipRRect(
                borderRadius: BorderRadius.circular(8),
                child: CachedNetworkImage(
                  fit: BoxFit.cover,
                  imageUrl: widget.feedImage,
                  imageBuilder: (context, imageProvider) => Container(
                    width: screenWidth(context, dividedBy: 1),
                    // height: screenHeight(context, dividedBy: 3),
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        image: imageProvider,
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                  placeholder: (context, url) => Center(
                    heightFactor: 1,
                    widthFactor: 1,
                    child: SizedBox(
                      height: 16,
                      width: 16,
                      child: CircularProgressIndicator(
                        valueColor:
                            AlwaysStoppedAnimation(Constants.kitGradients[28]),
                        strokeWidth: 2,
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ),
          // count == 2
          //     ? Spacer()
          //     : SizedBox(height:7),
          SizedBox(height: screenHeight(context, dividedBy: 70)),
          Container(
            width: screenWidth(context, dividedBy: 1),
            child: Row(
              children: [
                GestureDetector(
                  onTap: () {
                    favcount == 1
                        ? setState(() {
                            favcount = 2;
                          })
                        : setState(() {
                            favcount = 1;
                          });
                  },
                  child: favcount == 1
                      ? Icon(
                          Icons.favorite_border_outlined,
                          color: Constants.kitGradients[28],
                        )
                      : Icon(
                          Icons.favorite,
                          color: Constants.kitGradients[28],
                        ),
                ),
                SizedBox(
                  width: screenWidth(context, dividedBy: 40),
                ),
                GestureDetector(
                  onTap: () {
                    comment == 1
                        ? setState(() {
                            comment = 2;
                          })
                        : setState(() {
                            comment = 1;
                          });
                  },
                  child: Icon(
                    Icons.comment,
                    color: Constants.kitGradients[28],
                  ),
                ),
                Spacer(),
                GestureDetector(
                  onTap: () {
                    tagcount == 1
                        ? setState(() {
                            tagcount = 2;
                          })
                        : setState(() {
                            tagcount = 1;
                          });
                  },
                  child: tagcount == 1
                      ? Icon(
                          Icons.bookmark_outline_sharp,
                          color: Constants.kitGradients[28],
                        )
                      : Icon(
                          Icons.bookmark_sharp,
                          color: Constants.kitGradients[28],
                        ),
                ),
              ],
            ),
          ),
          // Container(
          //   width: screenWidth(context, dividedBy: 1),
          //   child: Row(
          //     children: [
          //       // SizedBox(
          //       //   width: screenWidth(context, dividedBy: 40),
          //       // ),
          //
          //       GestureDetector(
          //           onTap: () {
          //             favcount == 1
          //                 ? setState(() {
          //                     favcount = 2;
          //                   })
          //                 : setState(() {
          //                     favcount = 1;
          //                   });
          //           },
          //           child: favcount == 1
          //               ? Icon(
          //                   Icons.favorite_border_outlined,
          //                   color: Constants.kitGradients[27],
          //                 )
          //               : Icon(
          //                   Icons.favorite,
          //                   color: Constants.kitGradients[27],
          //                 )),
          //       SizedBox(
          //         width: screenWidth(context, dividedBy: 20),
          //       ),
          //       GestureDetector(
          //         onTap: () {
          //           comment == 1
          //               ? setState(() {
          //                   comment = 2;
          //                 })
          //               : setState(() {
          //                   comment = 1;
          //                 });
          //         },
          //         child: Icon(
          //           Icons.comment,
          //           color: Constants.kitGradients[27],
          //         ),
          //       ),
          //       SizedBox(
          //         width: screenWidth(context, dividedBy: 20),
          //       ),
          //       GestureDetector(
          //           child: Image(
          //               image: AssetImage("assets/images/share_icon.png"),
          //               width: 25,
          //               height: 25)),
          //       Spacer(),
          //       GestureDetector(
          //           onTap: () {
          //             tagcount == 1
          //                 ? setState(() {
          //                     tagcount = 2;
          //                   })
          //                 : setState(() {
          //                     tagcount = 1;
          //                   });
          //           },
          //           child: tagcount == 1
          //               ? Icon(
          //                   Icons.bookmark_outline_sharp,
          //                   color: Constants.kitGradients[27],
          //                 )
          //               : Icon(
          //                   Icons.bookmark_sharp,
          //                   color: Constants.kitGradients[27],
          //                 )),
          //     ],
          //   ),
          // ),
          //SizedBox(height: screenHeight(context, dividedBy: 50)),
          //Spacer(),
          // count == 2
          //     ? Spacer()
          //     : SizedBox(height: screenHeight(context, dividedBy: 70)),
          SizedBox(height: screenHeight(context, dividedBy: 70)),
          Row(children: [
            Text(
              widget.likeCount + " Likes ",
              style: TextStyle(
                fontSize: 16,
                fontWeight: FontWeight.w600,
                fontFamily: 'Montserrat',
                color: Constants.kitGradients[28],
              ),
            ),
          ]),
          // count == 2
          //     ? Spacer()
          //     : SizedBox(height: screenHeight(context, dividedBy: 70)),
          SizedBox(height: screenHeight(context, dividedBy: 70)),
          GestureDetector(
            onTap: () {
              count == 1
                  ? setState(() {
                      count = 2;
                    })
                  : setState(() {
                      count = 1;
                    });
            },
            child: RichText(
              overflow:
                  count == 2 ? TextOverflow.visible : TextOverflow.ellipsis,
              maxLines: count == 2 ? 6 : 2,
              text: TextSpan(
                  text: widget.name,
                  style: TextStyle(
                    fontSize: 14,
                    fontWeight: FontWeight.w600,
                    fontFamily: 'Montserrat',
                    color: Constants.kitGradients[28],
                  ),
                  children: <TextSpan>[
                    widget.caption == null
                        ? null
                        : TextSpan(
                            text: "  " + widget.caption,
                            style: TextStyle(
                              fontSize: 13,
                              fontWeight: FontWeight.w400,
                              color: Constants.kitGradients[28],
                            ),
                          )
                  ]),
            ),
          ),
          SizedBox(height: screenHeight(context, dividedBy: 80)),
          // GestureDetector(
          //   onTap: () {
          //     //showToast("View all comments");
          //     push(
          //         context,
          //         CommentDetailsPage(
          //           commentBy: widget.commentBy,
          //           comment: widget.comment,
          //           feedImage: widget.feedImage,
          //           caption: widget.caption,
          //         ));
          //   },
          //   child: Text(
          //     "View all comments",
          //     style: TextStyle(
          //       fontSize: 13,
          //       fontWeight: FontWeight.w700,
          //       fontFamily: 'Montserrat',
          //       color: Constants.kitGradients[19],
          //     ),
          //   ),
          // ),
          SizedBox(height: screenHeight(context, dividedBy: 150)),
          widget.comment == null
              ? Container()
              : GestureDetector(
                  onTap: () {
                    comment == 1
                        ? setState(() {
                            comment = 2;
                          })
                        : setState(() {
                            comment = 1;
                          });
                  },
                  child: CommentTile(
                    commentBy: widget.commentBy,
                    comment: widget.comment,
                  ),
                ),
          SizedBox(height: screenHeight(context, dividedBy: 80)),
          comment == 2
              ? CommentBox(
                  textEditingController: commentTextEditingController,
                  autofocus: true,
                )
              : Container(),
          //SizedBox(height: screenHeight(context, dividedBy: 50)),
          //SizedBox(height: screenHeight(context, dividedBy: 80)),

          Divider(color: Constants.kitGradients[28].withOpacity(0.1)),
          SizedBox(height: screenHeight(context, dividedBy: 70)),
        ],
      ),
    );
  }
}
